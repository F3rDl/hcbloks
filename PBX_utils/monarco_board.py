#!/usr/bin/python3.7
# -*- coding: utf-8 -*-

import multiprocessing
import time
import crcmod
import spidev
import atexit


class MonarcoBoard(multiprocessing.Process):

    DELAY = 0.025

    dict_default_output = {'status_led_mask': False, 'status_led_on': False, '1w_power_shutdown': False,
                           'counter_1_reset': False, 'counter_2_reset': False,

                           'user_led_1_mask': False, 'user_led_2_mask': False, 'user_led_3_mask': False,
                           'user_led_4_mask': False, 'user_led_5_mask': False, 'user_led_6_mask': False,
                           'user_led_7_mask': False, 'user_led_8_mask': False,

                           'user_led_1_value': False, 'user_led_2_value': False, 'user_led_3_value': False,
                           'user_led_4_value': False, 'user_led_5_value': False, 'user_led_6_value': False,
                           'user_led_7_value': False, 'user_led_8_value': False,

                           'digital_output_1': False, 'digital_output_2': False, 'digital_output_3': False,
                           'digital_output_4': False,

                           'pwm_1_freq': 0.0, 'pwm_1_a_val': 0.0, 'pwm_1_b_val': 0.0, 'pwm_1_c_val': 0.0,
                           'pwm_2_freq': 0.0, 'pwm_2_a_val': 0.0,

                           'analog_output_1': 0.0, 'analog_output_2': 0.0}

    dict_default_input = {'digital_inputs': 0,
                          'counter_1_val': 0,
                          'counter_2_val': 0,
                          'counter_2_cap': 0,
                          'analog_in_1': 0.0,
                          'analog_in_2': 0.0}

    def __init__(self, logging=None, dict_transfer_in: dict = None, dict_transfer_out: dict = None,
                 dict_transfer_par: dict = None):

        """
        Args:
            logging:
            dict_transfer_in (dict):
            dict_transfer_out (dict):
            dict_transfer_par (dict):
        """
        multiprocessing.Process.__init__(self, name="IO_board")

        # Sichere die Logging-Referenz
        self.logging = logging

        # Weise das Dictionary des Base-Manger zu
        self.dict_transfer_in = dict_transfer_in
        self.dict_transfer_out = dict_transfer_out
        self.dict_transfer = dict_transfer_par

        # Initalisiere und oeffne die SPI-Schnittstelle
        self.spi_0_2 = self.instantiate_spi(port=0, cs=2, hz=4000000)

        # Erstelle ein CRC16-Objekt mit folgender Einstellung "Modbus RTU CRC-16 / CRC-16-IBM"
        self.crc16 = crcmod.Crc(0x18005, initCrc=0xFFFF, xorOut=0x0)  # TODO: In der Doku steht "0x8005"

        self.Current_input_1_mode = False
        self.Current_input_2_mode = False

        self.Counter_1_is_enabled = False
        self.Counter_2_is_enabled = False

        # Sende-Buffer
        self.sc_data_2_send = 0  # [0 for x in range(3)]

        # In diesen Dictionary werden alle Sensor-Zustaende hinterlegt
        self.dict_data_input = self.dict_default_input.copy()

        # In diesen Dictionary sind alle Initial-Werte der Aktor-Zustaende hinterlegt
        self.dict_data_output = self.dict_default_output.copy()

        # self.dict_transfer_in['data_in_io'] = self.dict_data_input.copy()
        # self.dict_transfer_out['data_out_io'] = self.dict_data_output.copy()

        # Bereinigungs-Routine
        atexit.register(self.watchdog_1_disable)

    @staticmethod
    def instantiate_spi(port: int = 0, cs: int = 2, hz: int = 4000000) -> spidev.SpiDev:
        """Diese Funktion installiert die SPI-Schnittstelle und gibt ein Object
        zurueck um diese ansprechen zu koennen

        Args:
            port (int): Die geforderte SPI-Schnittstelle (SPI0, SPI1, ...)
            cs (int): Der geforderte Chip-Select
            hz (int): Die geforderte maximale Frequenz der Clock-Leitung

        Returns:
            object: Ein Object um auf die SPI-Schnittstelle zuzugreifen
        """

        # Erstelle ein SPI-Objekt
        spi = spidev.SpiDev()

        # Oeffne den SPI-Port 0 mit Chip-Select 3
        spi.open(port, cs)

        # Lege die Geschwindigkeit auf 4 Mhz fest
        spi.max_speed_hz = hz

        return spi

    @staticmethod
    def conv_pwm_freq(freq_hz: float = 100):

        """Diese Funktion konvertiert den Parameter "freq_hz" ins Monarco-format
        und retourniert diesen

        Args:
            freq_hz (float):
        """

        if freq_hz < 1:
            return 0

        elif freq_hz < 10:
            return 3 + (round(32000000 / 512 / freq_hz) & 0xFFFC)

        elif freq_hz < 100:
            return 2 + (round(32000000 / 64 / freq_hz) & 0xFFFC)

        elif freq_hz < 1000:
            return 1 + (round(32000000 / 8 / freq_hz) & 0xFFFC)

        elif freq_hz < 100000:
            return 0 + (round(32000000 / 1 / freq_hz) & 0xFFFC)

        else:
            return 0

    @staticmethod
    def conv_pwm_dc(dc):

        """Convert the pwm-duty-cycle to Monarco-format

        Args:
            dc:
        """

        # Wenn weniger als 0% verlangt werden, dann ...
        if dc <= 0.0:

            # Begrenze auf 0%
            return 0

        # Wenn mehr als 100% verlangt werden, dann ...
        elif dc > 100.0:

            # Begrenze auf 100%
            return 65535

        # Sonst berechne den den PWM-Datenwert
        else:
            return round(65535 * (dc/100))

    @staticmethod
    def conv_aout_volt(volts):

        """Convert the voltage to Monarco-format

        Args:
            volts:
        """

        if volts < 0.0:
            return 0
        if volts > 10.0:
            return 4095
        else:
            return round(volts / 10.0 * 4095)

    @staticmethod
    def conv_ain_10v(ain):

        """Convert the Monarco-format to voltage

        Args:
            ain:
        """

        return ain * 10.0 / 4095

    @staticmethod
    def conv_ain_20ma(ain):

        """Convert the Monarco-format to current

        Args:
            ain:
        """

        return ain * 52.475 / 4095

    def watchdog_1_enable(self, timeout: int = 1, communication_delay: float = 0.0005):

        # Wenn im Sende-buffer etwas eingetragen ist ...
        """
        Args:
            timeout (int):
            communication_delay (float):
        """
        if not self.sc_data_2_send == 0:

            # Dann fuehre einen Datentransfer durch
            self.data_exchange()

            # Verarbeitungszeit des Monarco-Boards
            time.sleep(communication_delay)

        # Schiebe den Wert des timeouts in Variable der zu senden Daten
        self.sc_data_2_send = self.sc_data_2_send | timeout

        # Schiebe die Adresse (0x30) auf das zweite byte
        self.sc_data_2_send = self.sc_data_2_send | (0x30 << 16)

        # Schiebe ein Bit fuer das write-command auf das zweite nibbel des dritten byte
        self.sc_data_2_send = self.sc_data_2_send | (16 << 24)

        # Fuehre einen Datentransfer durch
        self.data_exchange()

        # Verarbeitungszeit des Monarco-Boards
        time.sleep(communication_delay)

    def watchdog_1_disable(self, communication_delay: float = 0.0005):

        # Wenn im Sende-buffer etwas eingetragen ist ...
        """
        Args:
            communication_delay (float):
        """
        if not self.sc_data_2_send == 0:

            # Dann fuehre einen Datentransfer durch
            self.data_exchange()

            # Verarbeitungszeit des Monarco-Boards
            time.sleep(communication_delay)

        # Schiebe den Wert 0 in Variable der zu senden Daten
        self.sc_data_2_send = self.sc_data_2_send | (0x00 << 0)

        # Schiebe die Adresse (0x30) auf das zweite byte
        self.sc_data_2_send = self.sc_data_2_send | (0x30 << 16)

        # Schiebe ein Bit fuer das write-command auf das zweite nibbel des dritten byte
        self.sc_data_2_send = self.sc_data_2_send | (16 << 24)

        # Fuehre einen Datentransfer durch
        self.data_exchange()

        # Verarbeitungszeit des Monarco-Boards
        time.sleep(communication_delay)

    def watchdog_2_enable(self, timeout: int = 60, communication_delay: float = 0.0005):

        # Wenn im Sende-buffer etwas eingetragen ist ...
        """
        Args:
            timeout (int):
            communication_delay (float):
        """
        if not self.sc_data_2_send == 0:

            # Dann fuehre einen Datentransfer durch
            self.data_exchange()

            # Verarbeitungszeit des Monarco-Boards
            time.sleep(communication_delay)

        self.sc_data_2_send = self.sc_data_2_send | timeout

        # Schiebe die Adresse (0x31) auf das zweite byte
        self.sc_data_2_send = self.sc_data_2_send | (0x31 << 16)

        # Schiebe ein Bit fuer das write-command auf das zweite nibbel des dritten byte
        self.sc_data_2_send = self.sc_data_2_send | (16 << 24)

        # Fuehre einen Datentransfer durch
        self.data_exchange()

        # Verarbeitungszeit des Monarco-Boards
        time.sleep(communication_delay)

    def watchdog_2_disable(self, communication_delay: float = 0.0005):

        # Wenn im Sende-buffer etwas eingetragen ist ...
        """
        Args:
            communication_delay (float):
        """
        if not self.sc_data_2_send == 0:
            # Dann fuehre einen Datentransfer durch
            self.data_exchange()

            # Verarbeitungszeit des Monarco-Boards
            time.sleep(communication_delay)

        # Schiebe den Wert 0 in Variable der zu senden Daten
        self.sc_data_2_send = self.sc_data_2_send | (0x00 << 0)

        # Schiebe die Adresse (0x31) auf das zweite byte
        self.sc_data_2_send = self.sc_data_2_send | (0x31 << 16)

        # Schiebe ein Bit fuer das write-command auf das zweite nibbel des dritten byte
        self.sc_data_2_send = self.sc_data_2_send | (16 << 24)

        # Fuehre einen Datentransfer durch
        self.data_exchange()

        # Verarbeitungszeit des Monarco-Boards
        time.sleep(communication_delay)

    def analog_in_curret_mode(self, analog_input=0, status=False):

        """Set status the conversion-mode from analog-inputs

        Args:
            analog_input:
            status:
        """

        # Geht es um den Analog-Eingang Nummer 1
        if analog_input == 1:

            # Wird der Strom-Schleifen-Modus gefordert
            if status:

                # Wenn im Sende-buffer nicht eingetragen ist ...
                if self.sc_data_2_send == 0:  # all(i == 0 for i in self.sc_data_2_send) is True:

                    # Schiebe ein Bit auf die zweite Stelle um den Shunt fuer AI zu aktivieren
                    self.sc_data_2_send = self.sc_data_2_send | (0x02 << 0)

                    # Schiebe die Adresse (0xA) auf das zweite byte
                    self.sc_data_2_send = self.sc_data_2_send | (0x0A << 16)

                    # Schiebe ein Bit fuer das write-command auf das zweite nibbel des dritten byte
                    self.sc_data_2_send = self.sc_data_2_send | (16 << 24)

                    # Makiere das der Stromschleifenmodus fuer Analog-Eingang 1 aktiviert ist
                    self.Current_input_1_mode = True

            else:

                # Wenn im Sende-buffer nicht eingetragen ist ...
                if self.sc_data_2_send == 0:  # all(i == 0 for i in self.sc_data_2_send) is True:

                    # Schiebe kein Bit auf die zweite Stelle um den Shunt fuer AI zu deaktivieren
                    self.sc_data_2_send = self.sc_data_2_send | (0x00 << 0)

                    # Schiebe die Adresse (0xA) auf das zweite byte
                    self.sc_data_2_send = self.sc_data_2_send | (0x0A << 16)

                    # Schiebe ein Bit fuer das write-command auf das zweite nibbel des dritten byte
                    self.sc_data_2_send = self.sc_data_2_send | (16 << 24)

                    # Makiere das der Stromschleifenmodus fuer Analog-Eingang 1 deaktiviert ist
                    self.Current_input_1_mode = False

        elif analog_input == 2:
            if status:

                # Wenn im Sende-buffer nicht eingetragen ist ...
                if self.sc_data_2_send == 0:  # all(i == 0 for i in self.sc_data_2_send) is True:

                    # Schiebe ein Bit auf die dritte Stelle um den Shunt fuer AI zu aktivieren
                    self.sc_data_2_send = self.sc_data_2_send | (0x06 << 0)

                    # Schiebe die Adresse (0xA) auf das zweite byte
                    self.sc_data_2_send = self.sc_data_2_send | (0x0A << 16)

                    # Schiebe ein Bit fuer das write-command auf das zweite nibbel des dritten byte
                    self.sc_data_2_send = self.sc_data_2_send | (16 << 24)

                    # Makiere das der Stromschleifenmodus fuer Analog-Eingang 1 aktiviert ist
                    self.Current_input_2_mode = True

            else:
                # Wenn im Sende-buffer nicht eingetragen ist ...
                if self.sc_data_2_send == 0:  # all(i == 0 for i in self.sc_data_2_send) is True:

                    # Schiebe kein Bit auf die zweite Stelle um den Shunt fuer AI zu deaktivieren
                    self.sc_data_2_send = self.sc_data_2_send | (0x00 << 0)

                    # Schiebe die Adresse (0xA) auf das zweite byte
                    self.sc_data_2_send = self.sc_data_2_send | (0x0A << 16)

                    # Schiebe ein Bit fuer das write-command auf das zweite nibbel des dritten byte
                    self.sc_data_2_send = self.sc_data_2_send | (16 << 24)

                    # Makiere das der Stromschleifenmodus fuer Analog-Eingang 1 deaktiviert ist
                    self.Current_input_2_mode = False

    def counter_mode(self, channel: int = 1, modus: int = 1, direction: int = 0, active_edge: int = 0):
        """0x024: COUNTER1 Configuration [W] bit 00..02 - mode: 0: off 1: pulse
        counting 2: quadrature encoder 3: counting pulses generated by PWM1 4:
        counting pulses generated by PWM2

        bit 03..05 - direction (only for pulse counting mode): 0: up 1:
        [FUTURE] external control, low/high = up/down

        bit 06..07 - active edge (only for pulse counting mode): 0: rising 1:
        falling 2: both bit 08..15 - RESERVED (write zeros)

        Args:
            channel (int):
            modus (int):
            direction (int):
            active_edge (int):
        """

        # Wenn im Sende-buffer nicht eingetragen ist ...
        if self.sc_data_2_send == 0:  # all(i == 0 for i in self.sc_data_2_send) is True:

            # Schiebe ein Bit auf die zweite Stelle um die Impulszaehlung zu aktivieren
            self.sc_data_2_send = self.sc_data_2_send | (modus << 0)

            # Schiebe schiebe die "direction" auf die vierte Stelle um die Zaehlrichtung (++/--) festzulegen
            self.sc_data_2_send = self.sc_data_2_send | (direction << 3)

            # Schiebe schiebe die "direction" auf die vierte Stelle um die Zaehlrichtung (++/--) festzulegen
            self.sc_data_2_send = self.sc_data_2_send | (active_edge << 6)

            # Schiebe die Adresse (0x24/0x25) auf das zweite byte
            self.sc_data_2_send = self.sc_data_2_send | ((0x23 + channel) << 16)

            # Schiebe ein Bit fuer das write-command auf das zweite nibbel des dritten byte
            self.sc_data_2_send = self.sc_data_2_send | (16 << 24)

    def create_msg(self, data_send):
        """Mit dem "control_byte" werden unten angefuehrte Einstellungen
        festgelegt, welche dem Monarco gesendet werden

        offset Description 0.0 Status LED mask 0.1 Status LED on 0.2 1-Wire
        power shutdown 0.3 RESERVED 0.4 Counter 1 reset (edge sensitive) 0.5
        Counter 2 reset (edge sensitive) 0.6 Sign of Life 0 0.7 Sign of Life 1

        Args:
            data_send:
        """

        control_byte: int = 0  # 1

        user_led_mask = int(data_send['user_led_1_mask'])

        user_led_val = (int(data_send['user_led_1_value']) << 0) + \
                       (int(data_send['user_led_2_value']) << 1) + \
                       (int(data_send['user_led_3_value']) << 2) + \
                       (int(data_send['user_led_4_value']) << 3) + \
                       (int(data_send['user_led_5_value']) << 4) + \
                       (int(data_send['user_led_6_value']) << 5) + \
                       (int(data_send['user_led_7_value']) << 6) + \
                       (int(data_send['user_led_8_value']) << 7)

        dig_out = (int(data_send['digital_output_1']) << 0) + \
                  (int(data_send['digital_output_2']) << 1) + \
                  (int(data_send['digital_output_3']) << 2) + \
                  (int(data_send['digital_output_4']) << 3)

        pwm1_freq_h = (self.conv_pwm_freq(data_send['pwm_1_freq']) >> 0) & 0xFF
        pwm1_freq_l = (self.conv_pwm_freq(data_send['pwm_1_freq']) >> 8) & 0xFF

        pwm1_chan_a_h = (self.conv_pwm_dc(data_send['pwm_1_a_val']) >> 0) & 0xFF
        pwm1_chan_a_l = (self.conv_pwm_dc(data_send['pwm_1_a_val']) >> 8) & 0xFF

        pwm1_chan_b_h = (self.conv_pwm_dc(data_send['pwm_1_b_val']) >> 0) & 0xFF
        pwm1_chan_b_l = (self.conv_pwm_dc(data_send['pwm_1_b_val']) >> 8) & 0xFF

        pwm1_chan_c_h = (self.conv_pwm_dc(data_send['pwm_1_c_val']) >> 0) & 0xFF
        pwm1_chan_c_l = (self.conv_pwm_dc(data_send['pwm_1_c_val']) >> 8) & 0xFF

        pwm2_frequency_h = (self.conv_pwm_freq(data_send['pwm_2_freq']) >> 0) & 0xFF
        pwm2_frequency_l = (self.conv_pwm_freq(data_send['pwm_2_freq']) >> 8) & 0xFF

        pwm2_channel_a_h = (self.conv_pwm_dc(data_send['pwm_2_a_val']) >> 0) & 0xFF
        pwm2_channel_a_l = (self.conv_pwm_dc(data_send['pwm_2_a_val']) >> 8) & 0xFF

        analog_output_1_h = (self.conv_aout_volt(data_send['analog_output_1']) >> 0) & 0xFF
        analog_output_1_l = (self.conv_aout_volt(data_send['analog_output_1']) >> 8) & 0xFF

        analog_output_2_h = (self.conv_aout_volt(data_send['analog_output_2']) >> 0) & 0xFF
        analog_output_2_l = (self.conv_aout_volt(data_send['analog_output_2']) >> 8) & 0xFF

        data = [(self.sc_data_2_send >> 0) & 0xFF,  # 0x06, 0x00, 0x0A, 0x10
                (self.sc_data_2_send >> 8) & 0xFF,
                (self.sc_data_2_send >> 16) & 0xFF,
                (self.sc_data_2_send >> 24) & 0xFF,
                control_byte,
                user_led_mask,
                user_led_val,
                dig_out,
                pwm1_freq_h, pwm1_freq_l,
                pwm1_chan_a_h, pwm1_chan_a_l,
                pwm1_chan_b_h, pwm1_chan_b_l,
                pwm1_chan_c_h, pwm1_chan_c_l,
                pwm2_frequency_h, pwm2_frequency_l,
                pwm2_channel_a_h, pwm2_channel_a_l,
                analog_output_1_h, analog_output_1_l,
                analog_output_2_h, analog_output_2_l]

        # Setze den Sendebuffer zurueck
        self.sc_data_2_send = 0

        # Gebe die fertige Message zurueck
        return data

    def msg_2_input_var(self, response_msg):

        """In dieser Funktion wirde die Nachricht vom IO-Board auf die Variablen
        aufgeteilt

        Mit dem Status-Byte kommunziert das Monarco folgende
        Status-Zustaende: offset Description 0.0 RESERVED 0.1 RESERVED 0.2
        RESERVED 0.3 RESERVED 0.4 Counter 1 reset done 0.5 Counter 2 reset done
        0.6 Sign of Life 0 0.7 Sign of Life 1

        Args:
            response_msg:
        """

        # In dieser Variable wird die Antwort von Service-Channel-Anfragen abgelegt
        service_channel_response = response_msg[0:2]

        status_byte = response_msg[3]

        # Der Zustand der digitalen Eingaenge werden in dieser Variable zusamengefasst
        # Bits 0..3 = DIN1..DIN4, bits 4..7 = RESERVED
        digital_inputs = response_msg[7]

        # Wenn der Stromschleifenmodus fuer den Analogen Eingang 1 aktiviert wurde ...
        if self.Current_input_1_mode:
            # Dann muss der empfangene Wert in die Funktion fuer die mA-berechnung verwendet werden
            analog_input_1 = self.conv_ain_20ma((response_msg[21] << 8) + response_msg[20])

        else:
            # Ansonsten wird der Wert der Spannungs-berechnungs-Funktion uebergeben
            analog_input_1 = self.conv_ain_10v((response_msg[21] << 8) + response_msg[20])

        # Wenn der Stromschleifenmodus fuer den Analogen Eingang 2 aktiviert wurde ...
        if self.Current_input_2_mode:
            # Dann muss der empfangene Wert in die Funktion fuer die mA-berechnung verwendet werden
            analog_input_2 = self.conv_ain_20ma((response_msg[23] << 8) + response_msg[22])
        else:
            # Ansonsten wird der Wert der Spannungs-berechnungs-Funktion uebergeben
            analog_input_2 = self.conv_ain_10v((response_msg[23] << 8) + response_msg[22])

        counter_1 = (response_msg[11] << 8) + (response_msg[10] << 8) + (response_msg[9] << 8) + response_msg[8]
        counter_2 = (response_msg[15] << 8) + (response_msg[14] << 8) + (response_msg[13] << 8) + response_msg[12]
        counter_2_cap = (response_msg[19] << 8) + (response_msg[18] << 8) + (response_msg[17] << 8) + response_msg[16]

        write_time = time.time()

        self.dict_data_input = {

            'digital_inputs': {'value': digital_inputs,
                               'time': write_time},

            'analog_in_1': {'value': analog_input_1,
                            'time': write_time},
            'analog_in_2': {'value': analog_input_2,
                            'time': write_time},

            'counter_1_val': {'value': counter_1,
                              'time': write_time},
            'counter_2_val': {'value': counter_2,
                              'time': write_time},
            'counter_2_cap': {'value': counter_2_cap,
                              'time': write_time}}
                                                                    # TODO: Schaun ob .copy() besser ist als zuweisen
        self.dict_transfer_in['data_in_io'] = self.dict_data_input.copy()

    def data_exchange(self):

        self.dict_data_output.update(self.dict_transfer_out['data_out_io'].copy())

        # Speichere zu sendenden Daten in die lokale Variable "send_msg"
        send_msg = self.create_msg(self.dict_data_output)

        # Setzte Initial-Wert der Pruefsummenvariable
        self.crc16.crcValue = 0xFFFF

        # Update die zu senden Daten byte-weise in die pruefsummenvariable
        for element in send_msg:
            self.crc16.update(bytes([element]))

        # Haenge das Low-byte an die zu sendenden Daten
        send_msg.append((self.crc16.crcValue >> 0) & 0xFF)

        # Haenge das High-byte an die zu sendenden Daten
        send_msg.append((self.crc16.crcValue >> 8) & 0xFF)

        # Sende die Daten auf der SPI-Schnittstelle
        response_msg = self.spi_0_2.xfer2(send_msg)

        # Setzte initalvalue der pruefsummenvariable
        self.crc16.crcValue = 0xFFFF

        # Update die empfangenen Daten byte-weise in die pruefsummenvariable
        for element in response_msg[:-2]:
            self.crc16.update(bytes([element]))

        # Wenn die berechnete Checksumme zu der empfangenen zusammpen passt ...
        if self.crc16.crcValue == (response_msg[25] << 8) + response_msg[24]:

            # Extrahiere die Informationen aus der Nachricht und weise sie den Variablen zu
            self.msg_2_input_var(response_msg)
            # print(response_msg)
        else:
            # Gib eine Fehlermeldung aus
            print("Checksum-error")

    def run(self):

        # In dieser lokalen Liste wird die Sende-Nachricht vom aktuellen Durchlauf gehalten
        send_msg: list

        # self.watchdog_1_enable(timeout=3)
        # self.data_exchange()
        # self.watchdog_2_enable()
        self.data_exchange()
        self.analog_in_curret_mode(analog_input=1, status=True)
        self.data_exchange()
        self.analog_in_curret_mode(analog_input=2, status=True)
        self.data_exchange()
        self.counter_mode(channel=1, modus=1)
        self.data_exchange()
        self.counter_mode(channel=2, modus=1)
        self.data_exchange()

        while True:
            self.data_exchange()
            time.sleep(self.DELAY)


class ProcessDataHandler:

    def __init__(self, dict_transfer_par: dict, dict_transfer_in: dict, dict_transfer_out: dict):

        """
        Args:
            dict_transfer_par (dict):
            dict_transfer_in (dict):
            dict_transfer_out (dict):
        """
        self.dict_transfer_par = dict_transfer_par
        self.dict_transfer_in = dict_transfer_in
        self.dict_transfer_out = dict_transfer_out

        local_dict_in = self.dict_transfer_in.copy()
        local_dict_out = self.dict_transfer_out.copy()

        # Durch-wandere alle Key's der ersten Ebene von dem Parameter-Dictionary
        for interface in dict_transfer_par.keys():

            # Wenn der Parameter die Teil-Strings 'io' und 'in' beinhaltet, dann ...
            if 'io' in interface and 'in' in interface:

                # Durch-wandere alle Elemente im Sub-Parameter-Dictionary
                for sub_sys in dict_transfer_par[interface].keys():

                    # Erzeuge einen Eintrag im "dict_transfer_in" mit Init-Werten
                    local_dict_in[str(interface)].update({str(sub_sys): {'time': 0.0, 'value': 0.0}})

                    # Durch-wandere alle Prozess-Daten-Namen
                    for data_name in dict_transfer_par[interface][sub_sys].keys():

                        # Lade den Prozess-Daten-Namen welcher aus 'sub_sys' extrahiert
                        # werden konnten in das Prozess-Eingangs-Daten-Dictionary
                        local_dict_in['data_in_process_values'].update({data_name: 0.0})

            # Wenn-sonst der Parameter die Teil-Strings 'io' und 'out' beinhaltet, dann ...
            elif 'io' in interface and 'out' in interface:

                # Durch-wandere alle Schnittstellen im Sub-Parameter-Dictionary
                for sub_sys in dict_transfer_par[interface].keys():

                    # Erzeuge einen Eintrag im "dict_transfer_out" mit Init-Werten
                    local_dict_out[str(interface)].update({str(sub_sys): MonarcoBoard.dict_default_output[sub_sys]})

                    # Durch-wandere alle Prozess-Daten-Namen
                    for data_name, value in dict_transfer_par[interface][sub_sys].items():

                        # Lade den Prozess-Daten-Namen welcher aus 'sub_sys' extrahiert
                        # werden konnten in das Prozess-Eingangs-Daten-Dictionary
                        local_dict_out['data_out_process_values'].update(
                            {data_name: MonarcoBoard.dict_default_output[sub_sys]})

        # Update den Inhalt des aktualisierten lokalen Dictionary dem Transfer-Dictionary
        dict_transfer_in.update(local_dict_in.copy())
        dict_transfer_out.update(local_dict_out.copy())

    @staticmethod
    def update_process_data_in(local_dict_in: dict, local_dict_par: dict):

        # Durch-wandere alle Key's der ersten Ebene von dem Parameter-Dictionary
        """
        Args:
            local_dict_in (dict):
            local_dict_par (dict):
        """
        for interface in local_dict_in.keys():

            # Wenn das Interface die Teil-Strings 'can' und 'in' beinhaltet, dann ...
            if 'io' in interface and 'in' in interface:

                # Durch-wandere alle IO-Typen
                for sub_sys, data in local_dict_in[interface].items():

                    # Lade die Daten vom Sub-System in die lokale Variable "datagram"
                    datagram = data['value']

                    # Wenn es fuer das Subsystem in Parameter-Dictionary einen Eintrag gibt, dann ...
                    if sub_sys in local_dict_par[interface]:

                        # Durch-wandere das Parameter-Dictionary und hole den Prozess-Daten-Namen
                        # und seine zugewiesene Umrechnung
                        for par_name, conversion in local_dict_par[interface][sub_sys].items():

                            try:
                                # Versuche die Umrechnung zu machen und weise das Ergebnis
                                # den Eingags-Daten-Dictionary im Prozess-Daten-Teil zu
                                local_dict_in['data_in_process_values'][par_name] = eval(conversion)

                            except TypeError:
                                pass

        # Update den Inhalt des aktualisierten lokalen Dictionary dem Transfer-Dictionary zu
        return local_dict_in.copy()

    @staticmethod
    def update_process_data_out(local_dict_out: dict, local_dict_par: dict):

        # Durch-wandere alle Key's der ersten Ebene von dem Ausgangs-Daten-Dictionary
        """
        Args:
            local_dict_out (dict):
            local_dict_par (dict):
        """
        for interface in local_dict_out.keys():

            # Wenn das Interface die Teil-Strings 'io' und 'out' beinhaltet, dann ...
            if 'io' in interface and 'out' in interface:

                # Durch-wandere alle Sende-Ports
                for port in local_dict_out[interface].keys():

                    # Erstelle eine lokale Liste
                    datagram = [0]

                    # Durchwande auf dem Port alle Eintraege
                    for data_name, conversion in local_dict_par[interface][port].items():

                        # Erstelle eine lokale Variable mit den Namen der Prozess-Ausgangs-Variable und
                        # dem aktuellen Wert aus diesem
                        exec(data_name + ' = ' + str(local_dict_out['data_out_process_values'][data_name]))

                        # Fuehre die Konvertierung durch und weise das Ergebnis "datagram" zu
                        exec(conversion)

                    # Weise "datagram" dem Ausgangs-Daten-Dictionary zu
                    local_dict_out[interface][port] = datagram[0]

        # Update den Inhalt des aktualisierten lokalen Dictionary dem Transfer-Dictionary zu
        return local_dict_out.copy()
