#!/usr/local/bin/python3.7

import time


def rate_limiter(current_value, target_value, dt_pos, dt_neg, time_step):

    # Verletzt der geforderte Sprung die die maximal erlaubte negative Sprungweite, dann ...
    """
    Args:
        current_value:
        target_value:
        dt_pos:
        dt_neg:
        time_step:
    """
    if target_value < current_value + (dt_neg * time_step):

        # Mit maximal erlaubter negativen Sprungweite in Richtung "target_value"
        return current_value + (dt_neg * time_step)

    # Verletzt der geforderte Sprung die die maximal erlaubte positive Sprungweite, dann ...
    elif target_value > current_value + (dt_pos * time_step):

        # Mit maximal erlaubter positiven Sprungweite in Richtung "target_value"
        return current_value + (dt_pos * time_step)

    # Geforderter Sprung ist kleiner als maximal positiv/negativer Sprungweite deshalb, ...
    else:
        # fuehre direkete zuweisung durch
        return target_value


def saturation(current_value, limit_max, limit_min):

    # Ist der aktuelle Wert groesser als das oberen Limit, dann ...
    """
    Args:
        current_value:
        limit_max:
        limit_min:
    """
    if current_value > limit_max:

        # Gib das obere Limit zurueck
        return limit_max

    # Ist der aktuelle Wert kleiner als das untere Limit, dann ...
    elif current_value < limit_min:

        # Gib das untere Limit zurueck
        return limit_min

    # Keine Grenze verletzt
    else:

        # Gib den Wert unveraendert zurueck
        return current_value


class Derivation:

    def __init__(self, length=250, init_last_val=0.0):

        """
        Args:
            length:
            init_last_val:
        """
        self.time_array = []
        self.length = length
        self.last_value = init_last_val
        self.derivation_value = 0.0

    def update_val(self, value):

        """
        Args:
            value: Wert welcher abgeleitet werden soll
        """
        # Haenge das Delta zwischen den letzten und aktuellen Wert an die Liste
        self.time_array.append(value - self.last_value)

        # Schreibe den aktuellen Wert fuer den naechsten Durchlauf in "last_value"
        self.last_value = value

        # Wenn die Liste die vorgegebene Laenge erreicht hat, dann ...
        if len(self.time_array) > self.length:

            # Entferne das Element an der vordersten Stelle
            self.time_array.pop(0)

        # Berechne die Summe der Differenzen
        self.derivation_value = float(sum(self.time_array))

        return self.derivation_value

    def get_time_array(self):
        return self.time_array

    def get_derivation_value(self):
        return self.derivation_value

class MeanValue:

    def __init__(self, length=250):

        """
        Args:
            length:
        """
        self.time_array = []
        self.length = length

    def update(self, value):

        """
        Args:
            value:
        """
        self.time_array.append(value)

        if len(self.time_array) > self.length:
            self.time_array.pop(0)

        return sum(self.time_array) / len(self.time_array)


class PIController:

    def __init__(self, kp: float = 2.0, ki: float = 0.5, kd=0.0,
                 init_value: float = 0.0,
                 sample_time: float = 0.1, max_sample_time: float = 10.0,
                 output_limit_max: float = 50.0, output_limit_min: float = -50.0,
                 limit_output=False, anti_windup=False):

        # P-Anteil von Parameter-argument entgegennehmen
        """
        Args:
            kp (float):
            ki (float):
            kd:
            init_value (float):
            sample_time (float):
            max_sample_time (float):
            output_limit_max (float):
            output_limit_min (float):
            limit_output:
            anti_windup:
        """
        self.kp = float(kp)

        # I-Anteil von Parameter-argument entgegennehmen
        self.ki = float(ki)

        # D-Anteil von Parameter-argument entgegennehmen
        self.kd = float(kd)

        # Inital-Wert des Integrators von Parameter-argument entgegennehmen
        self.integrator_init_val = float(init_value)

        # Integrator mit Inital-Wert initalisieren
        self.integrator = self.integrator_init_val / self.ki

        # Ausgangsbeschraenkung oberes Limit von Parameter-argument entgegennehmen
        self.output_limit_max = float(output_limit_max)

        # Ausgangsbeschraenkung unteres Limit von Parameter-argument entgegennehmen
        self.output_limit_min = float(output_limit_min)

        # Steuervariable ob der die Ausgangsbeschraenkung aktiviert werden soll von Parameter-argument entgegennehmen
        self.limit_output = bool(limit_output)

        # Steuervariable ob der die Anti-Windupmethode aktiviert werden soll von Parameter-argument entgegennehmen
        self.anti_windup = bool(anti_windup)

        # Zeit welche zwischen den Aufrufen vergehen sollte, von Parameter-argument entgegennehmen
        self.sample_time = float(sample_time)

        # Zeit welche maximal zwischen zwei Aufrufen vergehen darf, sonst droht limitierung
        self.max_sample_time = float(max_sample_time)

        # In dieser Variabel wird die Regeldifferenz zwischen den Aufrufen gesichert
        self.error = 0.0

        # In dieser Variable wird
        self.current_time = time.time()
        self.previous_time = 0

        self.prev_err = 0

        # term result variables
        self.proportional_term = 0
        self.integral_term = 0
        self.derivative_term = 0

    def update_controller(self, error: float, feedback: float):

        # Lade die aktuelle Zeit
        """
        Args:
            error (float):
            feedback (float):
        """
        current_time = time.time()

        # Bestimme die verstrichene Zeit seit dem letzten aufruf
        delta_time = current_time - self.previous_time

        # Sicher den aktuellen Zeit-Stempel fuer den naechsten Durchlauf
        self.previous_time = current_time

        # Wenn mehr als die 10-fache Zeit verstrichen ist limitiere auf "self.sample_time"
        # um einen zu grossen I-Anteil-Ausschlag zu vermeiden --> Start des Regler, etc
        if delta_time > self.max_sample_time * self.sample_time:
            delta_time = self.sample_time

        # Bestimme das delta zwischen den aktuellen Regel-Fehler und jenen des letzten Zeit-Schrittes
        delta_error = error - self.prev_err

        # Bestimmen den Proportional-Anteil
        self.proportional_term = self.kp * error

        # Sichere den Integral-Anteil aus dem letzten Durchlauf
        old_integrator = self.integrator

        # Berechne den neuen Integrator-Wert
        self.integrator += (error * delta_time)

        # Bestimme den I-Anteil
        self.integral_term = self.ki * self.integrator

        # Um die Division durch Null und negative Zahlen zu vermeiden
        # Wenn die vergange Zeit nicht negativ oder Null ist, dann ...
        if delta_time > 0:
            derivator = delta_error/delta_time
        else:
            derivator = 0

        self.derivative_term = self.kd * derivator

        # Sicher den aktuellen Regelfehler fuer den naechsten Durchlauf
        self.prev_err = error

        # Berechne den "output"
        output = self.proportional_term + self.integral_term + self.derivative_term

        # Wenn mehr Drehzahl gefordert wird als Feedback schaft, dann ...
        if abs(output - feedback) > 150 and feedback > self.output_limit_min:

            # Begrenze den Integrator
            self.integrator = old_integrator

        # Wenn die clamping-Funktion gefordert wird, dann ...
        if self.limit_output:

            # Wenn der Output des PI-Reglers groesser ist als das obere Limit
            if output > self.output_limit_max:

                # Begrenze den Output
                output = self.output_limit_max

                # Wenn die Anti-Windup-Methode gefordert ist, dann ...
                if self.anti_windup:

                    # Begrenze den Integrator
                    self.integrator = old_integrator

            # Wenn der Output des PI-Reglers kleiner ist als das untere Limit
            elif output < self.output_limit_min:

                # Begrenze den Output
                output = self.output_limit_min

                # Wenn die Anti-Windup-Methode gefordert ist, dann ...
                if self.anti_windup:

                    # Begrenze den Integrator
                    self.integrator = old_integrator

        # Gib folgende Werte als Tupel zurueck
        return output, self.proportional_term, self.integral_term, self.derivative_term

    def reset_integrator(self):
        """Diese Funktion restiert den Integrator auf seinen Initalwert"""

        self.integrator = self.integrator_init_val / self.ki

    def set_integrator(self, set_value: float):
        """
        Args:
            set_value (float): Der Wert auf den der Integrator gesetzt werden
                soll
        """
        self.integrator = set_value

    def set_kp(self, kp: float):
        """
        Args:
            kp (float): Verstaerkungs-Faktor kp dem Regler uebergeben
        """
        self.kp = kp

    def set_ki(self, ki: float):
        """
        Args:
            ki (float): Verstaerkungsfaktor ki dem Regler uebergeben
        """
        self.ki = ki

    def set_kd(self, kd: float):
        """
        Args:
            kd (float): Verstaerkungsfaktor kd dem Regler uebergeben
        """
        self.kd = kd

    def get_kp(self):
        """Diese Funktion gibt den Verstaerkungsfaktor kp zurueck

        Returns:
            float: Gibt den Verstaerkungsfaktor kp zurueck
        """
        return self.kp

    def get_ki(self):
        """Diese Funktion gibt den Verstaerkungsfaktor ki zurueck

        Returns:
            float: Gibt den Verstaerkungsfaktor ki zurueck
        """
        return self.ki

    def get_kd(self):
        """Diese Funktion gibt den Verstaerkungsfaktor kd zurueck

        Returns:
            float: Gibt den Verstaerkungsfaktor kd zurueck
        """
        return self.kd

    def get_integrator(self):
        """Diese Funktion gibt den Integrator-Wert des Reglers zurueck

        Returns:
            float: Gibt den Integrator des Reglers zurueck
        """
        return self.integrator

    def get_time_last_call(self):
        """Diese Funktion gibt die Zeit (time.time()) des letzten
        Regler-aufrufes zurueck

        Returns:
            float: Zeitpunkt des letzten Regler-aufrufes
        """
        return self.previous_time
