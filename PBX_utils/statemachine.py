#!/usr/local/bin/python3.7

import time
from typing import Dict, Any

from PBX_utils import control_functions as cont_func
# from transitions.extensions import HierarchicalMachine as Machine
from transitions.extensions import HierarchicalGraphMachine as Machine, HierarchicalGraphMachine
from transitions.extensions.nesting import NestedState
import PBX_utils
import statistics
from PBX_utils import pbx_misc

from multiprocessing import Process
import warnings

from PBX_utils.control_functions import Derivation

NestedState.separator = '↦'

time_step = 0.1


class HCBloksData4SM:
    ctl_data_in = {

        'communication_status': {
            'can_0': True,
            'can_1': True,
            'SPI': True,
            '1-W': True,
            # 'RTC': True
            'Sanden_com_ok': True
        },

        'Settings': {
            'Mode': 0.0,
            'Temp_Set': 0.0,
            'Control_IO_cool': 'in',
            'Control_IO_heat': 'in'
        },

        'Manual_Input': {
            'man_main_relay': False,
            'man_heater_relay': False,
            'man_fan_cond_rpm': 0.0,
            'man_fan_chill_rpm': 0.0,
            'man_pump_pwm_set': 0.0,
            'man_comp_relay': False,
            'man_comp_start_request': False,
            'man_comp_rpm_set': 0.0,
            'man_comp_pwr_set': 0.0
        },

        'Sensor_Data': {

            # Luft-Temperatur am Chiller-Eingang
            'Temp_Air_Chil_in': 0.0,
            'temp_air_chil_in_rate': 0.0,

            # Luft-Temperatur am Chiller-Ausgang
            'Temp_Air_Chil_out': 0.0,
            'temp_air_chil_out_rate': 0.0,

            # Luft-Temperatur am Verfluessiger-Eingang
            'Temp_Air_Cond_in': 0.0,
            'Temp_Air_Cond_in_rate': 0.0,

            # Niederdruck in Temperatur
            'Temp_Ref_Evap': 0.0,
            'Temp_Ref_Evap_rate': 0.0,

            # Hochdruck in Temperatur
            'Temp_Ref_Cond': 0.0,
            'Temp_Ref_Cond_rate': 0.0,

            # Kaeltemittel-Temperatur am Verfluessiger-Eingang
            'Temp_Ref_Cond_in': 0.0,
            'Temp_Ref_Cond_in_rate': 0.0,

            # Kaeltemittel-Temperatur am Verfluessiger-Ausgang
            'Temp_Ref_Cond_out': 0.0,
            'Temp_Ref_Cond_out_rate': 0.0,

            # Kaeltemittel-Temperatur am Plattenwaermetauscher-Eingang
            'Temp_Ref_Phex_in': 0.0,
            'Temp_Ref_Phex_in_rate': 0.0,

            # Kaeltemittel-Temperatur am Plattenwaermetauscher-Ausgang
            'Temp_Ref_Phex_out': 0.0,
            'Temp_Ref_Phex_out_rate': 0.0,

            # Glycol-Temperatur am Plattenwaermetauscher-Eingang
            'Temp_Gly_Phex_in': 0.0,
            'temp_gly_phex_in_rate': 0.0,

            # Glycol-Temperatur am Plattenwaermetauscher-Ausgang
            'Temp_Gly_Phex_out': 0.0,
            'temp_gly_phex_out_rate': 0.0,

            # Glycol-Temperatur am Chiller-Eingang
            'Temp_Gly_Chil_in': 0.0,
            'temp_gly_chil_in_rate': 0.0,

            # Glycol-Temperatur am Chiller-Ausgang
            'Temp_Gly_Chil_out': 0.0,
            'temp_gly_chil_out_rate': 0.0,

            'press_ref_low': 0.0,
            'press_ref_low_rate': 0.0,

            'press_ref_high': 0.0,
            'press_ref_high_rate': 0.0,

            'FB_Comp_RPM': 0.0,
            'FB_Comp_Volt': 0.0,
            'FB_Comp_Current': 0.0,

            'FB_Fan_Chil_RPM': 0.0,
            'FB_Fan_Cond_RPM': 0.0,
            'FB_Pump_Gly_RPM': 0.0,
        }
    }

    cont_par = {

        'comp': {
            'pid_kp': -700.0,
            'pid_ki': -10.0,
            'pid_kd': 0.0,
            'pid_init': 1000.0,
            'constant_rate_factor': 2.0,  # Ã‚Â°C/30sec, da Rate auf 30sec geht (5.0 --> sehr konservativ)
                                          #                                  (3.0 --> aggressiver)
            'rpm_val_min': 700.0,
            'rpm_val_max': 5000.0,

            'volt_val_min': 18.0,
            'volt_val_max': 33.0,

            'rpm_dt_sec_min': -5000.0,
            'rpm_dt_sec_max': 100.0,

            'Pwr_val_min': 0.0,
            'pwr_val_max': 0.95,

            'Cont_Temp_Cond_max': 40.0,
            'Cont_Temp_Evap_min': -35.0
        },

        'Heater': {
            'Pi_Kp': 2.0,
            'Pi_Ki': 0.1,
            'Pi_Init': 50.0,

            'Duration_Period': 30.0,
            'Temp_dt_sec_max': 1.0,
            'Temp_Gly_max': 80.0
        },

        'fan_cond': {
            'PWM_default': 60.0,
            'PWM_max': 60.0,
            'PWM_min': 25.0,
            'PWM_dt_sec_min': -10.0,
            'PWM_dt_sec_max': 20.0,

            'Cont_Temp_Cond_max': 35.0,
            'Cont_Temp_Cond_min': 25.0,
            'Cont_Temp_Evap_max': 10.0,
            'Cont_Temp_Evap_min': -30.0,
        },

        'fan_chill': {
            'PWM_default': 60.0,
            'PWM_max': 60.0,
            'PWM_min': 15.0,
            'PWM_dt_sec_min': -10.0,
            'PWM_dt_sec_max': 20.0,

            'Cont_Temp_Cond_max': 35.0,
            'Cont_Temp_Cond_min': 25.0,
            'Cont_Temp_Evap_max': 10.0,
            'Cont_Temp_Evap_min': -30.0,
        },

        'pump': {
            'Pi_Kp': 2.0,
            'Pi_Ki': 0.1,
            'Pi_Init': 50.0,

            'PWM_default': 100.0,
            'PWM_max': 100.0,
            'PWM_min': 15.0,
            'PWM_dt_sec_min': -10.0,
            'PWM_dt_sec_max': 20.0,

            # Fuer Sensor-training
            'toggle_periode': 20.0,  # sec
            'toggle_duty_cycle': 0.5  # on_time
        },

        'Temp_Control_Parameters': {
            'Hyst_cooling': 0.5,
            'Hyst_heating': -0.5
        },

        'sensor_in_range_pvariance': 0.01,
        'heat_time_4_sensor_train_1': 90.0,
        'heat_time_4_sensor_train_2': 120.0,
        'least_time_in_faded': 30.0,
        'run_ref_cycle_before_sensor_eval': 60.0,

        'sensor_dt_trigger_Temp_Gly_Chil_out': 0.5,
        'sensor_dt_trigger_Temp_Gly_Phex_in': 0.5,
        'sensor_dt_trigger_Temp_Gly_Phex_out': 0.5,
        'sensor_dt_trigger_Temp_Gly_Chil_in': 0.5,

        'sensor_dt_trigger_Temp_Air_Chil_out': 0.35,
        'sensor_dt_trigger_Temp_Air_Chil_in': 0.35,

        'sensor_dt_trigger_Temp_Ref_Cond_in': 2.0,
        'sensor_dt_trigger_Temp_Ref_Cond_out': 2.0,

        'sensor_dt_trigger_Temp_Ref_Phex_in': -1.0,
        'sensor_dt_trigger_Temp_Ref_Phex_out': -1.0,
        'sensor_dt_trigger_Temp_Air_Cond_in': 0.25,

        # In dieser Liste befinden sich alle erkannten 1w-Sensor-ID's
        'list_sensor_id': [],

        'sum_detected_sensors': 0.0,

        'list_glyc_pos_order': ['Temp_Gly_Chil_out',
                                'Temp_Gly_Phex_in',
                                'Temp_Gly_Phex_out',
                                'Temp_Gly_Chil_in'],

        'list_ref_air_pos_order': ['Temp_Air_Chil_out',
                                   'Temp_Air_Chil_in',
                                   'Temp_Ref_Cond_in',
                                   'Temp_Ref_Cond_out',
                                   'Temp_Ref_Phex_in',
                                   'Temp_Ref_Phex_out',
                                   'Temp_Air_Cond_in'],

        'hottest_sensor': ['Temp_Ref_Cond_in'],
        'coldest_sensor': ['Temp_Ref_Phex_in',
                           'Temp_Ref_Phex_out'],

        'dict_know_sensor_ids': {}

    }

    ctl_data_out = {
        'Main_Relay': {
            'Status': False
        },
        'comp': {
            'Relay': False,
            'Status': False,
            'RPM': 1000.0,
            'PWR': 0.0
        },

        'Heat': {
            'Status': False
        },

        'pump': {
            'PWM': 0.0
        },

        'fan_cond': {
            'PWM': 0.0
        },

        'fan_chill': {
            'PWM': 0.0
        }
    }


class StateMachineComp:
    parent: object

    def __init__(self, data):

        """
        Args:
            data:
        """
        self.data = data

        # Im folgenden String wird der Name des Parallel-States definiert
        self.name = 'comp'

        # Erstelle einen Praefix der aus den State-Machine-Namen und den "sub-State"-Symbol besteht
        self.pref = self.name + '↦'

        # Erstelle einen Praefix der aus den State-Machine-Namen und einen "kleiner"-Symbol besteht
        # fuer die Event-Signale
        self.e_pref = 'event_' + self.name + '_'

        # Im folgenden String wird der Init-Zustand definiert:
        self.init_state = 'init_comp'

        # In der folgenden Liste sind alle States eingetragen
        self.states = [
            'init_comp',
            'off',
            {'name': 'on', 'children': [
                'check',
                'error',
                {'name': 'standby', 'children': [
                    'standstill',
                    'running_out']},
                'automatic_1',
                'automatic_2',
                'automatic_3',
                'idle',
                'half',
                'full',
                'manual']
             }
        ]

        # ---------------- Erstelle die "Transitions" von der Zustands-Machine --------------------------------------- #
        # trigger, source, dest, conditions=None, unless=None, before=None, after=None, prepare=None
        self.transitions = [

            # trigger (str): Event-Signal of the transitions
            # source (str): Source state of the transition.
            # dest (str): Destination state of the transition.
            # prepare (list): Callbacks executed before conditions checks.
            # conditions (list): Callbacks evaluated to determine if
            #     the transition should be executed.
            # before (list): Callbacks executed before the transition is executed
            #     but only if condition checks have been successful.
            # after (list): Callbacks executed after the transition is executed
            #     but only if condition checks have been successful.

            # -------------------- External Events ------------------------------------------------------------------- #
            #
            # Diese Signale werden verwendet um die Zustands-Aenderungen zu bewirken. Sie stellen eine Art Interaktion-
            # Schnittstelle dar. Folgende Signal stehen zu Verfuegung:
            #   TODO: Die Paefixe in einer Schleife im nachhienein hinzufuegen --> exl. loop
            #   -) wake_up      ↦ In den Betriebs-bereiten Zustand versetzen
            #   -) fall_asleep  ↦ In den Energie-spar-Modus versetzen
            #   -) manual       ↦ In den Betriebs-bereiten Zustand versetzen und Manuelle vorgaben weiterreichen
            #   -) stop         ↦ Von einen aktiven Betriebs-Zustand in den Betriebs-bereiten Zustand wechseln
            #   -) idle         ↦ In den aktiven Betriebs-Zustand "geringste Leistung" versetzen
            #   -) full         ↦ In den aktiven Betriebs-Zustand "maximale Leistung" versetzen
            #   -) auto_1       ↦ In den aktiven Betriebs-Zustand "automatic"  versetzen --> dt-Regler auf Cargo-Air
            #   -) auto_2       ↦ In den aktiven Betriebs-Zustand "automatic"  versetzen --> PI-Regler auf Cargo-Air
            #   -) auto_3       ↦ In den aktiven Betriebs-Zustand "automatic"  versetzen --> PI-Regler auf Glycol
            #
            # (1)
            # Wenn das Event "wake_up" geschickt wurde, wird in den Zustand "on" gewechselt
            {'trigger': self.e_pref + 'wake_up', 'source': self.pref + 'off', 'dest': self.pref + 'on',
             'after': 'pic_update'},
            #
            # (2)
            # Wenn das Event "fall_asleep" geschickt wurde, wird in den Zustand "off" gewechselt
            {'trigger': self.e_pref + 'fall_asleep', 'source': self.pref + 'on', 'dest': self.pref + 'off',
             'after': 'pic_update'},
            #
            # (3)
            # Wenn das Event "manual" geschickt wurde, wird in den Zustand "manual" gewechselt
            {'trigger': self.e_pref + 'manual', 'source': self.pref + 'on↦standby', 'dest': self.pref + 'on↦manual',
             'after': 'pic_update'},
            #
            # (4)
            #  Wenn das Event "stop" geschickt wurde, wird in den Zustand "standby" gewechselt
            {'trigger': self.e_pref + 'stop', 'source': [self.pref + 'on↦automatic_1', self.pref + 'on↦automatic_2',
                                                         self.pref + 'on↦automatic_3',
                                                         self.pref + 'on↦idle', self.pref + 'on↦full',
                                                         self.pref + 'on↦manual', self.pref + 'on↦half'],
             'dest': self.pref + 'on↦standby', 'after': 'pic_update'},
            #
            # (5)
            # Wenn Event "idle" geschickt wurde, wird in den Zustand "idle" gewechselt
            {'trigger': self.e_pref + 'idle',
             'source': [self.pref + 'on↦standby', self.pref + 'on↦automatic_1', self.pref + 'on↦automatic_2',
                        self.pref + 'on↦automatic_3', self.pref + 'on↦half', self.pref + 'on↦full'],
             'dest': self.pref + 'on↦idle',
             'after': 'pic_update'},
            #
            # (6)
            # Wenn das Event "full" geschickt wurde, wird in den Zustand "full" gewechselt
            {'trigger': self.e_pref + 'full', 'source': self.pref + 'on↦standby', 'dest': self.pref + 'on↦full',
             'after': 'pic_update'},
            #
            # (7)
            #  Wenn das Event "auto_1" geschickt wurde, wird in den Zustand "automatic_1" gewechselt
            {'trigger': self.e_pref + 'auto_1',
             'source': [self.pref + 'on↦standby', self.pref + 'on↦automatic_2', self.pref + 'on↦automatic_3',
                        self.pref + 'on↦idle', self.pref + 'on↦half', self.pref + 'on↦full'],
             'dest': self.pref + 'on↦automatic_1',
             'after': 'pic_update'},
            #
            # (8)
            #  Wenn das Event "auto_2" geschickt wurde, wird in den Zustand "automatic_2" gewechselt
            {'trigger': self.e_pref + 'auto_2', 'source': self.pref + 'on↦standby',
             'dest': self.pref + 'on↦automatic_2',
             'after': 'pic_update'},
            #
            # (9)
            #  Wenn das Event "auto_3" geschickt wurde, wird in den Zustand "automatic_3" gewechselt
            {'trigger': self.e_pref + 'auto_3',
             'source': [self.pref + 'on↦standby', self.pref + 'on↦automatic_1', self.pref + 'on↦automatic_2',
                        self.pref + 'on↦half', self.pref + 'on↦full'],
             'dest': self.pref + 'on↦automatic_3',
             'after': 'pic_update'},
            #
            # (10)
            #  Wenn das Event "half" geschickt wurde, wird in den Zustand "automatic_2" gewechselt
            {'trigger': self.e_pref + 'half', 'source': [self.pref + 'on↦standby', self.pref + 'on↦full',
                                                         self.pref + 'on↦idle'],
             'dest': self.pref + 'on↦half',
             'after': 'pic_update'},


            # -------------------- Transfer transitions -------------------------------------------------------------- #
            #
            # Diese Transitions bewirken interne Wechsel zwischen den States. Die Bedingungen in Ihnen werden in jeden
            # zyklischen Aufruf ueberprrueft und ein Wechsel bei erfuellung durchgefuehrt
            #
            # (101)
            # Wenn das Relais eine positives Feedback gibt und die Kommunikation mit dem Kompressor passt dann
            # wird die vom Zustand "check" in den State "standby" gesprungen
            {'trigger': 'loop', 'source': self.pref + 'on↦check', 'dest': self.pref + 'on↦standby',
             'conditions': ['if_ok_supply_voltage', 'if_ok_com_to_comp'], 'after': 'pic_update'},
            #
            # (102)
            # Wenn laenger als 3 Sekunden keine positive Freigabe kommt dann wird der Error-Zustand aktiv
            {'trigger': 'loop', 'source': self.pref + 'on↦check', 'dest': self.pref + 'on↦error',
             'prepare': 'timer_comp_check_count', 'conditions': ['timer_comp_check_timeout'], 'after': 'pic_update'},
            #
            # (104)
            # Wenn die Komponente sich ausgelaufen hat (langsam heruntergefahren) und somit still steht soll in den
            # State "standstill" gewechselt werden
            {'trigger': 'loop', 'source': self.pref + 'on↦standby↦running_out',
             'dest': self.pref + 'on↦standby↦standstill', 'unless': ['if_comp_turns'], 'after': 'pic_update'},

            # -------------------- Internal transitions -------------------------------------------------------------- #
            #
            # Dieser Kategorie werden Transitions zugewiesen welche fuer Initalisierungen oder zyklische Aufrufe
            # verwendet werden
            #
            # (201)
            # Nach der Initalisierung soll die Statemachine sich im Off-Zustand befinden
            {'trigger': 'loop', 'source': self.pref + 'init_comp', 'dest': self.pref + 'off', 'after': 'pic_update'},
            #
            # (202)
            # Das Event "init_on" wird von "enter_on" gesendet und ermoeglicht den sprung direkt in den sub-state
            # "check". Andere Methoden wuerden einen weiteren Verarbeitung-Schritt benoetigen
            {'trigger': self.e_pref + 'init_on', 'source': self.pref + 'on', 'dest': self.pref + 'on↦check',
             'after': 'pic_update'},
            #
            # # (203)
            # # Zyklischer call fuer den "idle"-State
            # {'trigger': 'loop', 'source': self.pref + 'on↦idle', 'dest': None},
            # #
            # # (204)
            # # Zyklischer call fuer den "full"-State
            # {'trigger': 'loop', 'source': self.pref + 'on↦full', 'dest': None},
            #
            # (205)
            # Zyklischer call fuer den "manual"-State
            {'trigger': 'loop', 'source': self.pref + 'on↦manual', 'dest': None,
             'after': 'during_' + self.name + '_manual'},
            #
            # (206)
            # Zyklischer call fuer den "automatic_1"-State
            {'trigger': 'loop', 'source': self.pref + 'on↦automatic_1', 'dest': None,
             'after': 'cyclic_call_automatic_1'},
            #
            # (207)
            # Zyklischer call fuer den "automatic_2"-State
            {'trigger': 'loop', 'source': self.pref + 'on↦automatic_2', 'dest': None,
             'after': 'cyclic_call_automatic_2'},
            #
            # (208)
            # Zyklischer call fuer den "automatic_2"-State
            {'trigger': 'loop', 'source': self.pref + 'on↦automatic_3', 'dest': None,
             'after': 'cyclic_call_automatic_3'},
            #
            # (209)
            # Wenn die Komponente beim betreten des "standby"-States bereits heruntergefahren ist,
            # soll direkt der State "standstill" aktiv werden
            {'trigger': self.e_pref + 'init_standby', 'source': self.pref + 'on↦standby',
             'dest': self.pref + 'on↦standby↦standstill', 'unless': ['if_comp_turns'], 'after': 'pic_update'},
            #
            # (210)
            # Wenn die Komponente beim betreten des "standby"-States bereits heruntergefahren ist,
            # soll direkt der State "standstill" aktiv werden
            {'trigger': self.e_pref + 'init_standby', 'source': self.pref + 'on↦standby',
             'dest': self.pref + 'on↦standby↦running_out', 'conditions': ['if_comp_turns'], 'after': 'pic_update'},
            #
            # (211)
            # Zyklischer call fuer den "Running_Out"-State
            {'trigger': 'loop', 'source': self.pref + 'on↦standby↦running_out', 'dest': None,
             'conditions': ['if_comp_turns'], 'after': 'comp_ramp_down'}
        ]

        # Timer-Variabel vom State "Check"
        self.timer_comp_check_value = 0.0

        self.p_anteil = 0
        self.i_anteil = 0
        self.d_anteil = 0
        self.ctrl_error = 0
        self.ctrl_error_rate = 0

        # Der PI-Regler wird initalisiert
        self.Temp_PI = cont_func.PIController(kp=self.data.cont_par['comp']['pid_kp'],
                                              ki=self.data.cont_par['comp']['pid_ki'],
                                              kd=self.data.cont_par['comp']['pid_kd'],
                                              init_value=self.data.cont_par['comp']['pid_init'],
                                              limit_output=True,
                                              anti_windup=True,
                                              output_limit_min=self.data.cont_par['comp']['rpm_val_min'],
                                              output_limit_max=self.data.cont_par['comp']['rpm_val_max']
                                              )

        self.comp_rpm_smoother = cont_func.MeanValue(length=100)

    def set_parent(self, parent):
        """
        Args:
            parent:
        """
        self.parent = parent

    # ------------------------ Cyclic-functions ---------------------------------------------------------------------- #

    def during_comp_manual(self):
        """Diese Funktion wird zyklisch aufgerufen um die Manuellen Vorgaben auf
        die Ausgabe-Variablen zu schreiben
        """
        self.data.ctl_data_out[self.name]['RPM'] = self.data.ctl_data_in['Manual_Input'][
            'man_' + self.name + '_rpm_set']
        self.data.ctl_data_out[self.name]['PWR'] = self.data.ctl_data_in['Manual_Input'][
            'man_' + self.name + '_pwr_set']
        self.data.ctl_data_out[self.name]['Status'] = self.data.ctl_data_in['Manual_Input'][
            'man_' + self.name + '_start_request']
        self.data.ctl_data_out[self.name]['Relay'] = self.data.ctl_data_in['Manual_Input'][
            'man_' + self.name + '_relay']

    def comp_ramp_down(self):

        self.data.ctl_data_out['comp']['RPM'] = cont_func.rate_limiter(
            self.data.ctl_data_out['comp']['RPM'],
            self.data.cont_par['comp']['rpm_val_min'],
            self.data.cont_par['comp']['rpm_dt_sec_max'],
            self.data.cont_par['comp']['rpm_dt_sec_min'],
            time_step)

    def cyclic_call_automatic_1(self):

        self.data.ctl_data_out['comp']['Status'] = True
        self.data.ctl_data_out['comp']['PWR'] = self.data.cont_par['comp']['pwr_val_max']

        # Soll auf die angesaugte Temperatur geregelt werden, dann ...
        if self.data.ctl_data_in['Settings']['Control_IO_cool'] is 'in':

            self.ctrl_error = \
                self.data.ctl_data_in['Settings']['Temp_Set'] - self.data.ctl_data_in['Sensor_Data'][
                    'Temp_Air_Chil_in']

            self.ctrl_error_rate = self.ctrl_error - (self.data.ctl_data_in['Sensor_Data']['temp_air_chil_in_rate'] *
                                                      self.data.cont_par['comp']['constant_rate_factor'])

        # # Soll auf die ausgeworfene Temperatur geregelt werden, dann ...
        elif self.data.ctl_data_in['Settings']['Control_IO_cool'] is 'out':

            self.ctrl_error = \
                self.data.ctl_data_in['Settings']['Temp_Set'] - self.data.ctl_data_in['Sensor_Data'][
                    'Temp_Air_Chil_out']

            self.ctrl_error_rate = self.ctrl_error - (self.data.ctl_data_in['Sensor_Data']['temp_air_chil_out_rate'] *
                                                      self.data.cont_par['comp']['constant_rate_factor'])

        # Sonst ...
        else:
            self.ctrl_error = \
                self.data.ctl_data_in['Settings']['Temp_Set'] - self.data.ctl_data_in['Sensor_Data'][
                    'Temp_Air_Chil_in']

            self.ctrl_error_rate = self.ctrl_error - (self.data.ctl_data_in['Sensor_Data']['temp_air_chil_in_rate'] *
                                                      self.data.cont_par['comp']['constant_rate_factor'])

        # Update den PI-Regler
        self.data.ctl_data_out['comp']['RPM'], self.p_anteil, self.i_anteil, self.d_anteil = \
            self.Temp_PI.update_controller(self.ctrl_error_rate, self.data.ctl_data_in['Sensor_Data']['FB_Comp_RPM'])

        # Mittelwert-Filter
        self.data.ctl_data_out['comp']['RPM'] = \
            round(self.comp_rpm_smoother.update(self.data.ctl_data_out['comp']['RPM']))

    def cyclic_call_automatic_2(self):

        self.data.ctl_data_out['comp']['Status'] = True
        self.data.ctl_data_out['comp']['PWR'] = self.data.cont_par['comp']['pwr_val_max']

        # Soll auf die angesaugte Temperatur geregelt werden, dann ...
        if self.data.ctl_data_in['Settings']['Control_IO_cool'] is 'in':

            self.ctrl_error = \
                self.data.ctl_data_in['Settings']['Temp_Set'] - self.data.ctl_data_in['Sensor_Data'][
                    'Temp_Air_Chil_in']

        # # Soll auf die ausgeworfene Temperatur geregelt werden, dann ...
        elif self.data.ctl_data_in['Settings']['Control_IO_cool'] is 'out':

            self.ctrl_error = \
                self.data.ctl_data_in['Settings']['Temp_Set'] - self.data.ctl_data_in['Sensor_Data'][
                    'Temp_Air_Chil_out']

        # Sonst ...
        else:
            self.ctrl_error = \
                self.data.ctl_data_in['Settings']['Temp_Set'] - self.data.ctl_data_in['Sensor_Data'][
                    'Temp_Air_Chil_in']

        # Update den PI-Regler
        self.data.ctl_data_out['comp']['RPM'], self.p_anteil, self.i_anteil, self.d_anteil = \
            self.Temp_PI.update_controller(self.ctrl_error, self.data.ctl_data_in['Sensor_Data']['FB_Comp_RPM'])

        # Mittelwert-Filter
        self.data.ctl_data_out['comp']['RPM'] = \
            round(self.comp_rpm_smoother.update(self.data.ctl_data_out['comp']['RPM']))

    def cyclic_call_automatic_3(self):

        self.data.ctl_data_out['comp']['Status'] = True
        self.data.ctl_data_out['comp']['PWR'] = self.data.cont_par['comp']['pwr_val_max']

        self.ctrl_error = \
            (self.data.ctl_data_in['Settings']['Temp_Set'] - 6.0) - \
            self.data.ctl_data_in['Sensor_Data']['Temp_Gly_Phex_out']

        # Update den PI-Regler
        self.data.ctl_data_out['comp']['RPM'], self.p_anteil, self.i_anteil, self.d_anteil = \
            self.Temp_PI.update_controller(self.ctrl_error, self.data.ctl_data_in['Sensor_Data']['FB_Comp_RPM'])

        if self.data.ctl_data_out['comp']['RPM'] > 1500.0:
            self.data.ctl_data_out['comp']['RPM'] = 1500.0

        if self.data.ctl_data_in['Sensor_Data']['Temp_Gly_Phex_out'] < -28.0:
            self.data.ctl_data_out['comp']['Status'] = False

        if (self.data.ctl_data_out['comp']['Status'] is False) and \
                self.data.ctl_data_in['Sensor_Data']['Temp_Gly_Phex_out'] > -20.0:
            self.data.ctl_data_out['comp']['Status'] = True

        # Mittelwert-Filter
        self.data.ctl_data_out['comp']['RPM'] = \
            round(self.comp_rpm_smoother.update(self.data.ctl_data_out['comp']['RPM']))

    # ------------------------ On_enter_Events ----------------------------------------------------------------------- #

    def on_enter_on_full(self):
        self.data.ctl_data_out['comp']['Status'] = True
        self.data.ctl_data_out['comp']['RPM'] = self.data.cont_par['comp']['rpm_val_max']
        self.data.ctl_data_out['comp']['PWR'] = self.data.cont_par['comp']['pwr_val_max']

    def on_enter_off(self):
        self.data.ctl_data_out['comp']['RPM'] = 0.0
        self.data.ctl_data_out['comp']['PWR'] = 0.0
        self.data.ctl_data_out['comp']['Relay'] = False

    def on_enter_on(self):
        self.data.ctl_data_out['comp']['Relay'] = True
        self.parent.event_comp_init_on()

    def on_enter_on_standby(self):
        self.parent.event_comp_init_standby()
        # self.data.ctl_data_out['comp']['RPM'] = 0

    def on_enter_on_automatic_1(self):

        # Setzte den Integrator auf seinen Initial-Wert
        self.Temp_PI.reset_integrator()

    def on_enter_on_idle(self):
        self.data.ctl_data_out['comp']['Status'] = True
        self.data.ctl_data_out['comp']['RPM'] = self.data.cont_par['comp']['rpm_val_min']
        self.data.ctl_data_out['comp']['PWR'] = self.data.cont_par['comp']['pwr_val_max']

    def on_enter_on_half(self):
        self.data.ctl_data_out['comp']['Status'] = True
        self.data.ctl_data_out['comp']['RPM'] = 1500.0  # self.data.cont_par['comp']['rpm_val_min']
        self.data.ctl_data_out['comp']['PWR'] = self.data.cont_par['comp']['pwr_val_max']

    def on_enter_on_standby_standstill(self):
        self.data.ctl_data_out['comp']['Status'] = False
        self.data.ctl_data_out['comp']['RPM'] = 0.0

    # ------------------------ On_exit_Events ------------------------------------------------------------------------ #

    def on_exit_on_standby_running_out(self):
        self.data.ctl_data_out['comp']['Status'] = False
        self.data.ctl_data_out['comp']['RPM'] = 0.0

    # ------------------------ Conditions ---------------------------------------------------------------------------- #

    def timer_comp_check_timeout(self):

        if self.timer_comp_check_value > 3.0:
            self.timer_comp_check_value = 0.0
            return True

    def timer_comp_check_count(self):
        self.timer_comp_check_value = self.timer_comp_check_value + time_step

    def if_ok_supply_voltage(self):
        """Diese Funktion bewertet ob die Versorgungsspannung in einen
        vorgegebenen Range liegt

        Returns:
            bool: Aussage ob die Versorgung-Spannung in Ordnung ist
        """

        states: list = self.parent.state

        if 'modus↦manual' in '\t'.join(states):
            return True

        # Wenn die Versorgung-Spannung innerhalb vorgegebener Grenzen liegt, dann ...
        if self.data.cont_par['comp']['volt_val_min'] <= \
                self.data.ctl_data_in['Sensor_Data']['FB_Comp_Volt'] < \
                self.data.cont_par['comp']['volt_val_max']:

            # Gib ein True zurueck da die Spannung ok ist
            return True

        # Die Spannung liegt nicht in vorgegebenen Grenzen, also ...
        else:

            # Gib ein False zurueck da die Spannung nicht ok ist
            return False

    def if_ok_com_to_comp(self):

        return self.data.ctl_data_in['communication_status']['can_0']

    def if_comp_turns(self):
        """
        Returns:
            bool: Ob der comp ausgeschalten ist
        """
        if self.data.ctl_data_in['Sensor_Data']['FB_Comp_RPM'] <= self.data.cont_par['comp']['rpm_val_min']:
            return False
        else:
            return True


class StateMachineModus:
    dict_sensor_slope_glycol: Dict[Any, Derivation]
    dict_sensor_slope_ref_air: Dict[Any, Derivation]
    parent: object

    def __init__(self, data, dict_transfer_par):

        """
        Args:
            data:
            dict_transfer_par:
        """
        self.dict_transfer_par = dict_transfer_par

        self.timer_modus_check_value = 0.0

        # Timer-Variable vom State "start_up_procedure"
        self.timer_modus_startup_value = 0.0

        # Timer, wie lange gewartet wird ob cooling oder heating
        # erforderlich ist bevor in ventilation gewechselt wird
        self.timer_modus_to_ventilation_value = 0.0

        # Timer, wie lange in "pre_ventilation" gewartet wird bis in "cooling"
        # gewechselt wird, wenn die Temperatur-bedingung erfuellt ist
        self.timer_modus_to_cooling_value = 0.0

        # Timer, wie lange in "pre_ventilation" gewartet wird bis in "heating"
        # gewechselt wird, wenn die Temperatur-bedingung erfuellt ist
        self.timer_modus_to_heating_value = 0.0

        # Timer, wie lange in "preapering_door_opening" verweilt wird bevor
        # ins regulaere "cooling" gewechselt wird
        self.timer_modus_prep_door_value = 0.0

        # Defrost-Timer, so lange der Timer laeuft ist der Defrost aktiv
        self.timer_7 = 0.0

        # Solange dieser Timer laeuft wird nur mit den Lueftern geheizt bevor die Heizung dazu geschalten wird
        self.timer_8 = 0.0

        # Solange dieser Timer laeuft kann der State "...↦learn_sensor_pos↦faded_temp" nicht verlassen werden
        self.timer_modus_least_in_faded_value = 0.0

        # Solange der Timer laeuft wird das Glycol im Bereich des Heizstabes aufgeheizt bevor die Pumpe
        # die entstandene Waerme-walze weiter-schiebt
        self.timer_heat_up_glycol_1_value = 0.0

        # Solange der Timer laeuft wird das Glycol bei eingeschalteter Pumpe aufgeheizt
        self.timer_heat_up_glycol_2_value = 0.0

        # Wenn der Timer abgelaufen ist wird die detektierung der Sensoren gestartet
        self.timer_run_ref_cycle_before_sensor_eval_value = 0.0

        # Dieser Timer wird beim detektieren einer "Tuer-Oeffnung"gestartet und nach abluaf des Timers
        # wird in den State "...↦cooling↦run_comp" gewechselt
        self.timer_max_time_door_open_value = 0.0

        # Dieser Timer wird beim detektieren einer "Tuer-Oeffnung"gestartet und nach abluaf des Timers
        # wird in den State "...↦cooling↦run_comp" gewechselt
        self.timer_max_time_door_open_fan_off_value = 0.0

        # Dieser Timer soll die State-Machine solange verzoegern bis das Daten-Erfassungssystem FiLo
        # Mess-bereit ist
        self.timer_filo_delay_value = 0.0

        self.data = data

        # Im folgenden String wird der Name des Parallel-States definiert
        self.name = 'modus'

        # Erstelle einen Praefix der aus den State-Machine-Namen und den "sub-State"-Symbol besteht
        self.pref = self.name + '↦'

        # Erstelle einen Praefix der aus den State-Machine-Namen und einen "underline"-Symbol besteht
        # fuer die Event-Signale
        self.e_pref = 'event_' + self.name + '_'

        # Im folgenden String wird der Init-Zustand definiert:
        # self.init_state = 'init_check'
        self.init_state = 'filo_delay'

        # In der folgenden Liste sind alle States eingetragen
        self.states = [
            'filo_delay',
            'init_check',
            'error',
            'man_defrost',
            {'name': 'standby', 'children': [
                'running_out',
                'standstill']},
            {'name': 'temp_control', 'children': [
                'wake_up_components',
                'pre_ventilation',
                'ventilation',
                {'name': 'heating', 'children': [
                    'forced_ventilation',
                    'heat']},
                {'name': 'cooling', 'children': [
                    'start_up_procedure',
                    {'name': 'high_ambient_start', 'initial': 'stop_heat_transfer', 'children': [
                        'stop_heat_transfer',
                        'start_heat_transfer']},
                    'run_comp',
                    'prepare_door_opening',
                    {'name': 'door_open', 'initial': 'fan_off', 'children': [
                        'fan_off',
                        'fan_on']}]},
                'defrost']},
            {'name': 'manual', 'children': [
                'wake_up_components',
                'active']},
            {'name': 'learn_sensor_pos', 'children': [
                'wake_up_components',
                'faded_temp',
                'heat_up_glycol_1',
                'observe_1',
                'found_sensor_1',   # Temp_Gly_Chil_out
                'found_sensor_2',   # Temp_Gly_Phex_in
                'found_sensor_3',   # Temp_Gly_Phex_out
                'found_sensor_4',   # Temp_Gly_Chil_in
                'heat_up_glycol_2',
                'observe_2',
                'found_sensor_5',   # Temp_Air_Chil_out
                'found_sensor_6',   # Temp_Air_Chil_in
                'start_ref_cycle',  # without Condfan
                'found_sensor_7',   # Temp_Ref_Cond_in
                'found_sensor_8',   # Temp_Ref_Cond_out
                'start_cond_fan',
                'found_sensor_9',   # Temp_Ref_Phex_in
                'found_sensor_10',  # Temp_Ref_Phex_out
                'found_sensor_11'   # Temp_Air_Cond_in

            ]},
        ]

        # ---------------- Erstelle die "Transitions" von der Zustands-Machine --------------------------------------- #
        # trigger, source, dest, conditions=None, unless=None, before=None, after=None, prepare=None
        self.transitions = [

            # trigger (str): Event-Signal of the transitions
            # source (str): Source state of the transition.
            # dest (str): Destination state of the transition.
            # prepare (list): Callbacks executed before conditions checks.
            # unless (list): Callbacks evaluated to determine if the transition should be not executed.
            # conditions (list): Callbacks evaluated to determine if the transition should be executed.
            # before (list): Callbacks executed before the transition is executed
            #     but only if condition checks have been successful.
            # after (list): Callbacks executed after the transition is executed
            #     but only if condition checks have been successful.

            # -------------------- External Events ------------------------------------------------------------------- #
            #
            # Diese Signale werden verwendet um die Zustands-Aenderungen zu bewirken. Sie stellen eine Art Interaktion-
            # Schnittstelle dar. Folgende Signal stehen zu Verfuegung:
            #
            #   -) reset_error          ↦ Von Fehler-Zustand in den Betriebs-bereiten Zustand versetzen
            #   -) prepare_door_opening ↦ Es wird mit der Tuer-oeffnungs-Methode begonnen
            #   -) defrost              ↦ Es wird die Defrost-Prozedur gestartet
            #
            # (1)
            # Wenn das Event "reset_error" empfangen wurde und kein Fehler-Zustand/Bedingung aktiv ist
            # wird in den Zustand "standby" gewechselt
            {'trigger': self.e_pref + 'reset_error', 'source': self.pref + 'error', 'dest': self.pref + 'standby',
             'unless': ['if_error_detected'], 'after': 'pic_update'},
            #
            # (2)
            # Von der Erkennung wird eine Turoeffnung vorhergesagt
            {'trigger': self.e_pref + 'prepare_door_opening',
             'source': self.pref + 'temp_control↦cooling↦run_comp',
             'dest': self.pref + 'temp_control↦cooling↦prepare_door_opening', 'after': 'pic_update'},
            #
            # (3)
            # Von der Erkennung wird ein Defrost gefordert
            {'trigger': self.e_pref + 'defrost', 'source': self.pref + 'temp_control↦cooling',
             'dest': self.pref + 'temp_control↦defrost', 'after': 'pic_update'},

            # -------------------- Transfer transitions -------------------------------------------------------------- #
            #
            # (undoc)
            # Verzoegere den State-Machine-Start bis Filo soweit ist
            {'trigger': 'loop', 'source': self.pref + 'filo_delay', 'dest': self.pref + 'init_check',
             'prepare': 'timer_filo_delay_count', 'conditions': ['timer_filo_delay_timeout'], 'after': 'pic_update'},
            #
            # (101a)
            # Ueberpruefe ob die Kommunikation zu den notwendigen Bauteilen in Ordung ist
            {'trigger': 'loop', 'source': self.pref + 'init_check', 'dest': self.pref + 'learn_sensor_pos',
             'unless': ['if_all_sensors_are_trained'], 'after': 'pic_update'},
            #
            # (101b)
            # Ueberpruefe ob die Kommunikation zu den notwendigen Bauteilen in Ordung ist
            {'trigger': 'loop', 'source': self.pref + 'init_check', 'dest': self.pref + 'standby',
             'conditions': ['if_communication_good'], 'after': 'pic_update'},
            #
            # (102)
            # Wenn laenger als 3 Sekunden keine positive Freigabe kommt dann wird der Error-Zustand aktiv
            {'trigger': 'loop', 'source': self.pref + 'init_check', 'dest': self.pref + 'error',
             'prepare': 'timer_modus_check_count', 'conditions': ['timer_modus_check_timeout'], 'after': 'pic_update'},
            #
            # (103)
            # Wenn ein schwere Fehler entdeckt wurde, wird in den Zustand Error gewechselt
            {'trigger': 'loop', 'source': self.pref + 'standby', 'dest': self.pref + 'error',
             'conditions': ['if_error_detected'], 'after': 'pic_update'},
            #
            # (104)
            # Wenn ueber die Variabel Modus die Temperaturregelung gefordert wird
            {'trigger': 'loop', 'source': self.pref + 'standby', 'dest': self.pref + 'temp_control',
             'conditions': ['if_modus_temp_control'], 'after': 'pic_update'},
            #
            # (105)
            # Wenn ueber die Variabel Modus die Temperaturregelung nicht mehr gefordert wird
            {'trigger': 'loop', 'source': self.pref + 'temp_control', 'dest': self.pref + 'standby',
             'unless': ['if_modus_temp_control'], 'after': 'pic_update'},
            #
            # (107)
            # Wenn ueber die Variabel Modus der Manual-Modus nicht mehr gefordert wird
            {'trigger': 'loop', 'source': self.pref + 'manual', 'dest': self.pref + 'standby',
             'unless': ['if_modus_manual'], 'after': 'pic_update'},
            #
            # (106)
            # Wenn ueber die Variabel Modus der Manual-Modus gefordert wird
            {'trigger': 'loop', 'source': self.pref + 'standby', 'dest': self.pref + 'manual',
             'conditions': ['if_modus_manual'], 'after': 'pic_update'},

            #
            # # (107a)
            # # Wenn ueber die Variabel Modus der Manual-Modus nicht mehr gefordert wird
            # {'trigger': 'loop', 'source': self.pref + 'manual↦wake_up_components', 'dest': self.pref + 'standby',
            #  'unless': ['if_modus_manual'], 'after': 'pic_update'},
            # #
            # # (107b)
            # # Wenn ueber die Variabel Modus der Manual-Modus nicht mehr gefordert wird
            # {'trigger': 'loop', 'source': self.pref + 'manual↦active', 'dest': self.pref + 'standby',
            #  'unless': ['if_modus_manual'], 'after': 'pic_update'},
            #
            # (108)
            # Wenn ueber die Variabel Modus der Manual-Defrost gefordert wird
            {'trigger': 'loop', 'source': self.pref + 'standby', 'dest': self.pref + 'man_defrost',
             'conditions': ['if_modus_man_defrost'], 'after': 'pic_update'},
            #
            # (109)
            # Wenn ueber die Variabel Modus der Manual-Defrost nicht mehr gefordert wird
            {'trigger': 'loop', 'source': self.pref + 'man_defrost', 'dest': self.pref + 'standby',
             'unless': ['if_modus_man_defrost'], 'after': 'pic_update'},
            #
            # (110)
            # Wenn die Hysteresen fuer "cooling" und "heating" nicht verletzt sind, dann wechsle in "ventilation"
            {'trigger': 'loop', 'source': self.pref + 'temp_control↦pre_ventilation',
             'dest': self.pref + 'temp_control↦ventilation', 'prepare': 'timer_modus_to_ventilation_count',
             'conditions': ['timer_modus_to_ventilation_timeout'], 'after': 'pic_update'},
            #
            # (111)
            # Wenn die Hysteresen fuer "ventilation" und "heating" nicht verletzt sind, dann wechsle in "cooling"
            {'trigger': 'loop', 'source': self.pref + 'temp_control↦pre_ventilation',
             'dest': self.pref + 'temp_control↦cooling', 'prepare': 'timer_modus_to_cooling_count',
             'conditions': ['if_cooling_required', 'timer_modus_to_cooling_timeout'], 'after': 'pic_update'},
            #
            # (112)
            # Wenn die Hysteresen fuer "cooling" und "ventilation" nicht verletzt sind, dann wechsle in "heating"
            {'trigger': 'loop', 'source': self.pref + 'temp_control↦pre_ventilation',
             'dest': self.pref + 'temp_control↦heating', 'prepare': 'timer_modus_to_heating_count',
             'conditions': ['if_heating_required', 'timer_modus_to_heating_timeout'], 'after': 'pic_update'},
            #
            # (113)
            # Wenn die Hysteresen fuer "heating" und "ventilation" nicht verletzt sind, dann wechsle in "cooling"
            {'trigger': 'loop', 'source': self.pref + 'temp_control↦ventilation',
             'dest': self.pref + 'temp_control↦cooling', 'conditions': ['if_cooling_required'], 'after': 'pic_update'},
            #
            # (114)
            # Wenn die Hysteresen fuer "cooling" und "ventilation" nicht verletzt sind, dann wechsle in "heating"
            {'trigger': 'loop', 'source': self.pref + 'temp_control↦ventilation',
             'dest': self.pref + 'temp_control↦heating', 'conditions': ['if_heating_required'], 'after': 'pic_update'},
            #
            # (115)
            # Wenn die Hysteresen fuer "cooling" und "heating" nicht verletzt sind, dann wechsle in "ventilation"
            {'trigger': 'loop', 'source': self.pref + 'temp_control↦cooling',
             'dest': self.pref + 'temp_control↦ventilation', 'conditions': ['if_cooling_not_required'],
             'after': 'pic_update'},
            #
            # (116)
            # Wenn die Hysteresen fuer "cooling" und "heating" nicht verletzt sind, dann wechsle in "ventilation"
            {'trigger': 'loop', 'source': self.pref + 'temp_control↦heating',
             'dest': self.pref + 'temp_control↦ventilation', 'conditions': ['if_heating_not_required'],
             'after': 'pic_update'},
            #
            # (117)
            # Es werden die Peripherie gestartet und 5 Sekunden gewartet das die Komponenten voll hochgefahren sind
            {'trigger': 'loop', 'source': self.pref + 'temp_control↦cooling↦start_up_procedure',
             'dest': self.pref + 'temp_control↦cooling↦run_comp', 'prepare': 'timer_modus_startup_count',
             'conditions': ['timer_modus_startup_timeout'], 'after': 'pic_update'},
            #
            # (118)
            # Wenn die Vorbereitung zur Tueroeffnung zu lange aktiv ist sorgt der Timer dafuer das
            # zurueck in "...↦run_comp" gewechselt wird
            {'trigger': 'loop', 'source': self.pref + 'temp_control↦cooling↦prepare_door_opening',
             'dest': self.pref + 'temp_control↦cooling↦run_comp', 'prepare': 'timer_modus_prep_door_count',
             'conditions': ['timer_modus_prep_door_timeout'], 'after': 'pic_update'},
            #
            # (119)
            # Wenn die Cargo-Tuere geoeffnet wird
            {'trigger': 'loop', 'source': self.pref + 'temp_control↦cooling↦prepare_door_opening',
             'dest': self.pref + 'temp_control↦cooling↦door_open', 'conditions': ['if_detect_door_opening'],
             'after': 'pic_update'},
            #
            # (120)
            # Wenn die Cargo-Tuere geschlossen wurde oder wenn der State "...↦cooling↦door_open" laenger als
            # "self.timer_max_time_door_open_value" aktiv ist dann wechsle in den State "...↦cooling↦run_comp"
            {'trigger': 'loop', 'source': self.pref + 'temp_control↦cooling↦door_open',
             'dest': self.pref + 'temp_control↦cooling↦run_comp', 'prepare': 'timer_max_time_door_open_count',
             'conditions': ['timer_max_time_door_open_timeout'], 'after': 'pic_update'},
            #
            {'trigger': 'loop', 'source': self.pref + 'temp_control↦cooling↦door_open',
             'dest': self.pref + 'temp_control↦cooling↦run_comp',
             'conditions': ['if_detect_door_closeing'], 'after': 'pic_update'},
            #
            # (121)
            # Wenn die Cargo-Tuere geoeffnet wird
            # {'trigger': 'loop', 'source': self.pref + 'temp_control↦cooling↦run_comp',
            #  'dest': self.pref + 'temp_control↦cooling↦door_open', 'conditions': ['if_detect_door_opening'],
            #  'after': 'pic_update'},
            #
            # (122)
            # Wenn der Defrost-Zyklus beendet ist
            {'trigger': 'loop', 'source': self.pref + 'temp_control↦defrost',
             'dest': self.pref + 'temp_control↦ventilation', 'prepare': 'timer_7_count',
             'conditions': ['timer_7_after_900_sec'], 'after': 'pic_update'},
            #
            # (123)
            # Wenn die Luft genug umgewaelzt worden ist bevor geheizt wird
            {'trigger': 'loop', 'source': self.pref + 'temp_control↦heating↦forced_ventilation',
             'dest': self.pref + 'temp_control↦heating↦heat', 'prepare': 'timer_8_count',
             'conditions': ['timer_8_after_15_sec'], 'after': 'pic_update'},
            #
            # (124)
            # Wenn folgende Bedinungen erfÃƒÂ¼llt sind:
            # - Ein Timer mit eier Mienderst-Laufzeit muss abgelaufen sein
            # - die Temperaturen der Glycol-Sensoren liegen nahe beieinander
            # - die Temperaturaenderung der Kaeltemittel/Luft-Sensoren muss klein sein
            # dann wird in "...↦heat_up_glycol_1" gewechselt
            {'trigger': 'loop', 'source': self.pref + 'learn_sensor_pos↦faded_temp',
             'dest': self.pref + 'learn_sensor_pos↦heat_up_glycol_1',
             'prepare': ['update_glycol_dt_dict', 'update_ref_air_dt_dict', 'timer_modus_least_in_faded_count'],
             'conditions': ['if_temp_sensors_glycol_in_range', 'timer_modus_least_in_faded_timeout',
                            'if_temp_sensors_ref_air_is_still'],
             'after': 'pic_update'},
            #
            # (125)
            # Das Glycol soll im Bereich der Heizung lokal solange erwaermt werden bis der Timer ablaeuft um
            # eine ordentliche Waermewalze durch den Kreis zu schieben damit die dt-Schwelle gut ueberschritten wird
            {'trigger': 'loop', 'source': self.pref + 'learn_sensor_pos↦heat_up_glycol_1',
             'dest': self.pref + 'learn_sensor_pos↦observe_1',
             'prepare': ['timer_heat_up_glycol_1_count', 'update_glycol_dt_dict'],
             'conditions': ['timer_heat_up_glycol_1_timeout'], 'after': 'pic_update'},
            #
            # (126)
            # Wenn der erste Sensor die dt-Schwelle uebersteigt dann wechsele in "found_sensor_1"
            {'trigger': 'loop', 'source': self.pref + 'learn_sensor_pos↦observe_1',
             'dest': self.pref + 'learn_sensor_pos↦found_sensor_1',
             'conditions': ['if_detect_glyc_dt_trigger'], 'after': 'pic_update'},
            #
            # (127)
            # Wenn der zweite Sensor die dt-Schwelle uebersteigt dann wechsele in "found_sensor_2"
            {'trigger': 'loop', 'source': self.pref + 'learn_sensor_pos↦found_sensor_1',
             'dest': self.pref + 'learn_sensor_pos↦found_sensor_2',
             'conditions': ['if_detect_glyc_dt_trigger'], 'after': 'pic_update'},
            #
            # (128)
            # Wenn der dritte Sensor die dt-Schwelle uebersteigt dann wechsele in "found_sensor_3"
            {'trigger': 'loop', 'source': self.pref + 'learn_sensor_pos↦found_sensor_2',
             'dest': self.pref + 'learn_sensor_pos↦found_sensor_3',
             'conditions': ['if_detect_glyc_dt_trigger'], 'after': 'pic_update'},
            #
            # (129)
            # Wenn der vierte Sensor die dt-Schwelle uebersteigt dann wechsele in "found_sensor_4"
            {'trigger': 'loop', 'source': self.pref + 'learn_sensor_pos↦found_sensor_3',
             'dest': self.pref + 'learn_sensor_pos↦found_sensor_4',
             'conditions': ['if_detect_glyc_dt_trigger'], 'after': 'pic_update'},
            #
            # (130)
            # Wenn der vierte Sensor die dt-Schwelle uebersteigt dann wechsele in "found_sensor_4"
            {'trigger': 'loop', 'source': self.pref + 'learn_sensor_pos↦found_sensor_4',
             'dest': self.pref + 'learn_sensor_pos↦heat_up_glycol_2', 'after': 'pic_update'},
            #
            # (131)
            # Das Glycol wird bei eingeschalteter Pumpe so lange aufgeheizt bis der timer
            # "timer_heat_up_glycol_2" abgelaufen ist
            {'trigger': 'loop', 'source': self.pref + 'learn_sensor_pos↦heat_up_glycol_2',
             'dest': self.pref + 'learn_sensor_pos↦observe_2',
             'prepare': ['timer_heat_up_glycol_2_count', 'update_ref_air_dt_dict'],
             'conditions': ['timer_heat_up_glycol_2_timeout'],
             # 'conditions': ['timer_heat_up_glycol_2_timeout', 'if_temp_sensors_ref_air_is_still',
             #                'if_temp_sensors_ref_air_in_range'],
             'after': 'pic_update'},
            #
            # (132)
            # Wenn einer der Sensoren die dt-Schwelle uebersteigt dann wechsele in "found_sensor_5"
            {'trigger': 'loop', 'source': self.pref + 'learn_sensor_pos↦observe_2',
             'dest': self.pref + 'learn_sensor_pos↦found_sensor_5',
             'conditions': ['if_detect_ref_air_dt_trigger'], 'after': 'pic_update'},
            #
            # (133)
            # Wenn einer der Sensoren die dt-Schwelle uebersteigt dann wechsele in "found_sensor_6"
            {'trigger': 'loop', 'source': self.pref + 'learn_sensor_pos↦found_sensor_5',
             'dest': self.pref + 'learn_sensor_pos↦found_sensor_6',
             'conditions': ['if_detect_ref_air_dt_trigger'], 'after': 'pic_update'},
            #
            # (134)
            # Nach dem der 6te Sensor gefunden wurde, wechsle im naechsten Zeitschritt in
            # den State "...↦learn_sensor_pos↦start_ref_cycle"
            {'trigger': 'loop', 'source': self.pref + 'learn_sensor_pos↦found_sensor_6',
             'prepare': 'update_ref_air_dt_dict',
             'dest': self.pref + 'learn_sensor_pos↦start_ref_cycle', 'after': 'pic_update'},
            #
            # (135)
            # Nach dem start des Kaeltekreises wird nach den heissesten Sensor gesucht,
            # sollte dieser gefunden sein wird in den State "...↦learn_sensor_pos↦found_sensor_7" gewechselt
            {'trigger': 'loop', 'source': self.pref + 'learn_sensor_pos↦start_ref_cycle',
             'prepare': 'update_ref_air_dt_dict',
             'conditions': 'if_detect_ref_air_dt_trigger',
             'dest': self.pref + 'learn_sensor_pos↦found_sensor_7', 'after': 'pic_update'},
            #
            # (136)
            # Es wurde der "Temp_Ref_Cond_in" gefunden
            {'trigger': 'loop', 'source': self.pref + 'learn_sensor_pos↦found_sensor_7',
             'conditions': 'if_detect_ref_air_dt_trigger',
             'dest': self.pref + 'learn_sensor_pos↦found_sensor_8', 'after': 'pic_update'},
            #
            # (137)
            # Es wurde der "Temp_Ref_Cond_out" gefunden
            {'trigger': 'loop', 'source': self.pref + 'learn_sensor_pos↦found_sensor_8',
             'conditions': 'if_detect_ref_air_dt_trigger',
             'dest': self.pref + 'learn_sensor_pos↦start_cond_fan', 'after': 'pic_update'},
            #
            # (138)
            # Starte den Cond-Fan
            {'trigger': 'loop', 'source': self.pref + 'learn_sensor_pos↦start_cond_fan',
             'prepare': 'update_ref_air_dt_dict',
             'dest': self.pref + 'learn_sensor_pos↦found_sensor_9', 'after': 'pic_update'},
            #
            # (139)
            # Es wurde der "Temp_Ref_Phex_in" gefunden
            {'trigger': 'loop', 'source': self.pref + 'learn_sensor_pos↦found_sensor_9',
             'conditions': 'if_detect_ref_air_dt_trigger',
             'dest': self.pref + 'learn_sensor_pos↦found_sensor_10', 'after': 'pic_update'},
            #
            # (140)
            # Es wurde der "Temp_Ref_Phex_in" gefunden
            {'trigger': 'loop', 'source': self.pref + 'learn_sensor_pos↦found_sensor_10',
             'conditions': 'if_detect_ref_air_dt_trigger',
             'dest': self.pref + 'learn_sensor_pos↦found_sensor_11', 'after': 'pic_update'},
            #
            # (141)
            # Es wurde der Sensor-Anlern-Vorgang abgeschlossen und es wird mit der
            # Normalen Start-Prozedur fortgesetzt
            {'trigger': 'loop', 'source': self.pref + 'learn_sensor_pos↦found_sensor_11',
             'dest': self.pref + 'init_check', 'after': 'pic_update'},
            #
            # (142)
            # Der Luefter soll, waehrend einer Tuer-Oeffnung, fuer eine definierte Zeit ausgeschalten sein und wenn
            # der Timer "timer_max_time_door_open_fan_off" abgelaufen ist, auf min-RPM laufen
            {'trigger': 'loop', 'source': self.pref + 'temp_control↦cooling↦door_open↦fan_off',
             'dest': self.pref + 'temp_control↦cooling↦door_open↦fan_on',
             'prepare': 'timer_max_time_door_open_fan_off_count',
             'conditions': ['timer_max_time_door_open_fan_off_timeout']},
            #
            # (143)
            # Wenn die Nieder-Druck-Seite einen zu hohen Wert aufweist, dann liegt liegt eine
            # High-Ambient-Situation vor und es soll in den entsprechenden State gewechselt werden
            {'trigger': 'loop', 'source': self.pref + 'temp_control↦cooling↦start_up_procedure',
             'dest': self.pref + 'temp_control↦cooling↦high_ambient_start',
             'conditions': ['if_high_ambient_is_necessary']},
            #
            # (144)
            # Wenn die Nieder-Druck-Seite wieder akzeptable Werte angenommen hat, dann wird
            # High-Ambient-Routine beendet und in den regulaeren Kuehlbetrieb gewechselt
            {'trigger': 'loop', 'source': self.pref + 'temp_control↦cooling↦high_ambient_start',
             'dest': self.pref + 'temp_control↦cooling↦run_comp',
             'conditions': ['if_high_ambient_is_over']},
            #
            # (145)

            {'trigger': 'loop', 'source': self.pref + 'temp_control↦cooling↦high_ambient_start↦start_heat_transfer',
             'dest': self.pref + 'temp_control↦cooling↦high_ambient_start↦stop_heat_transfer',
             'conditions': ['if_low_pressure_is_over']},
            #
            # (146)

            {'trigger': 'loop', 'source': self.pref + 'temp_control↦cooling↦high_ambient_start↦stop_heat_transfer',
             'dest': self.pref + 'temp_control↦cooling↦high_ambient_start↦start_heat_transfer',
             'conditions': ['if_low_pressure_is_under']},

            # -------------------- Internal transitions -------------------------------------------------------------- #
            #
            # Dieser Kategorie werden Transitions zugewiesen welche fuer Initalisierungen oder zyklische Aufrufe
            # verwendet werden
            #

            # # (202)-Visio
            # # Zyklischer call fuer den "error"-State
            # {'trigger': 'loop', 'source': self.pref + 'error', 'dest': None},

            # (204)-Visio
            # Zyklischer call fuer den "manual"-State
            {'trigger': 'loop', 'source': self.pref + 'manual↦active', 'conditions': ['if_modus_manual'],
             'dest': None, 'after': 'during_modus_manual'},


            # (206)-Visio
            # Initalisiere die Temperaturregelung mit dem State "pre_ventilation"
            {'trigger': self.e_pref + 'init_temp_control', 'source': self.pref + 'temp_control',
             'dest': self.pref + 'temp_control↦wake_up_components', 'after': 'pic_update'},

            # (206a)-Visio
            # Initalisiere die Temperaturregelung mit dem State "pre_ventilation"
            {'trigger': 'loop', 'source': self.pref + 'temp_control↦wake_up_components',
             'dest': self.pref + 'temp_control↦pre_ventilation', 'after': 'pic_update'},

            # (207) TODO: hier gehoert das "self.e_pref" dazu --> auswirkungen beachten
            # Initalisiere die Temperaturregelung mit dem State "pre_ventilation"
            {'trigger': 'init_control_cooling', 'source': self.pref + 'temp_control↦cooling',
             'dest': self.pref + 'temp_control↦cooling↦start_up_procedure', 'after': 'pic_update'},
            #
            # (208)
            # Initalisiere die Temperaturregelung mit dem State "forced_ventilation"
            {'trigger': self.e_pref + 'init_control_heating', 'source': self.pref + 'temp_control↦heating',
             'dest': self.pref + 'temp_control↦heating↦forced_ventilation', 'after': 'pic_update'},
            #
            # (209)
            # Initalisiere den Manuellen Betrieb mit dem State "wake_up_components"
            {'trigger': self.e_pref + 'init_manual', 'source': self.pref + 'manual',
             'dest': self.pref + 'manual↦wake_up_components', 'after': 'pic_update'},
            #
            # (210)
            # Initalisiere den Manuellen Betrieb mit dem State "active"
            {'trigger': 'loop', 'source': self.pref + 'manual↦wake_up_components',
             'dest': self.pref + 'manual↦active', 'after': 'pic_update'},
            #
            # (211)
            # Initalisiere den Sensor-anlern-Algorithmus mit dem State "wake_up_components"
            {'trigger': self.e_pref + 'init_learn_sensor_pos', 'source': self.pref + 'learn_sensor_pos',
             'dest': self.pref + 'learn_sensor_pos↦wake_up_components', 'after': 'pic_update'},
            #
            # (212)
            # Springe mit dem loop-Event in dem State "faded_temp"
            {'trigger': 'loop', 'source': self.pref + 'learn_sensor_pos↦wake_up_components',
             'dest': self.pref + 'learn_sensor_pos↦faded_temp', 'after': 'pic_update'},

            # ----------------------- Nicht Dokumentierte Transitionen ------------------------------------------------#

            # (20x)-Visio

            {'trigger': self.e_pref + 'init_standby', 'source': self.pref + 'standby',
             'dest': self.pref + 'standby↦standstill', 'unless': ['if_components_running'], 'after': 'pic_update'},

            # (20x)-Visio

            {'trigger': self.e_pref + 'init_standby', 'source': self.pref + 'standby',
             'dest': self.pref + 'standby↦running_out', 'conditions': ['if_components_running'],
             'after': ['pic_update']},

            # (20x)-Visio

            {'trigger': 'loop', 'source': self.pref + 'standby↦running_out', 'dest': self.pref + 'standby↦standstill',
             'unless': ['if_components_running'], 'after': 'pic_update'}
        ]

    def during_modus_manual(self):
        self.data.ctl_data_out['Main_Relay']['Status'] = self.data.ctl_data_in['Manual_Input']['man_main_relay']
        self.data.ctl_data_out['Heat']['Status'] = self.data.ctl_data_in['Manual_Input']['man_heater_relay']

    def if_all_sensors_are_trained(self):
        """Diese Funktion beurteilt ob die Sensoren bereits bekannt sind und
        somit kein Sensor-Training notwendig ist

        Returns:
            bool: Ob alle Sensoren der SW bekannt sind
        """
        # Wenn weniger als 11 Sensor-Positionen bekannt sind, dann ...
        if self.data.cont_par['sum_detected_sensors'] < 11.0:

            # gib "False" zurueck um ein Sensor-training auszuloesen
            return False
        else:

            # gib "True" zurueck --> kein Sensor-training notwendig
            return True

    def if_communication_good(self):

        # Durchwandere alle Eintraege von 'communication_status'
        for element in self.data.ctl_data_in['communication_status'].values():

            # Wenn nur ein element False ist dann, ...
            if element is False:
                # Brich die for-Schleiche ab und gib False zurueck
                return False

        # Alle Elemente sind True und somit passt alles und es kann auch True zurueckgegeben werden
        return True

    def if_components_running(self):

        states: list = self.parent.state

        if ('comp↦off' in '\t'.join(states) or 'comp↦standby↦standstill' in '\t'.join(states)) and \
                ('fan_cond↦off' in '\t'.join(states) or 'fan_cond↦standby↦standstill' in '\t'.join(states)) and \
                ('fan_chill↦off' in '\t'.join(states) or 'fan_chill↦standby↦standstill' in '\t'.join(states)) and \
                ('pump↦off' in '\t'.join(states) or 'pump↦standby↦standstill' in '\t'.join(states)):
            return False
        else:
            return True

    def if_cooling_not_required(self):

        """Diese Methode wird zyklisch im State "cooling" durchlaufen und
        beantwortet die Frage ob der State "cooling" verlassen werden soll.

        Hierfuer wird ermittelt ob auf angesaugte oder ausgeblasene Luft
        geregelt wird und in Abhaengikeit davon mittels der Set-Temperatur,
        Aktuellen-Temperatur und dem Hysterese- Wert die Aussage getroffen.

        https://hcbloks.atlassian.net/wiki/spaces/HPD/pages/1773436937/Switch-points+HVC

        Returns:
            bool: Die Aussage ob der State "cool" verlassen werden soll/muss
        """

        # Wird die Temperaturregelung auf den Eintrittstemperaturfuehler gefordert dann, ...
        if self.data.ctl_data_in['Settings']['Control_IO_heat'] is 'in':

            # Weise der aktuellen Temperatur den Wert von "Temp_Air_Chil_in" zu
            actual_temp = self.data.ctl_data_in['Sensor_Data']['Temp_Air_Chil_in']

        # Sonst wenn die Temperaturregelung auf den Austrittstemperaturfuehler gefordert ist dann, ...
        elif self.data.ctl_data_in['Settings']['Control_IO_heat'] is 'out':

            # Weise der aktuellen Temperatur den Wert von "Temp_Air_Chil_out" zu
            actual_temp = self.data.ctl_data_in['Sensor_Data']['Temp_Air_Chil_out']

        # Sonst ...
        else:
            # Weise der aktuellen Temperatur den Wert von "Temp_Air_Chil_in" zu
            actual_temp = self.data.ctl_data_in['Sensor_Data']['Temp_Air_Chil_in']

        hysteresis_cool = self.data.cont_par['Temp_Control_Parameters']['Hyst_cooling']
        settemp = self.data.ctl_data_in['Settings']['Temp_Set']

        # Wenn die aktuelle Temperatur die Set-Temperatur um den Hysterese-Wert unterschreitet und der
        # Verdichter eine Drehzahl kleiner 800 haben sollte dann, ...
        # Als Zusatz gibt es noch einen harten Kicker, wenn der doppelt Hysterese-wert verletzt wird, dann ...
        if ((actual_temp < (settemp - hysteresis_cool)) and (self.data.ctl_data_out['comp']['RPM'] < 800)) or \
                (actual_temp < (settemp - hysteresis_cool * 2)):

            # Dann gib True zurueck --> cooling ist nicht erforderlich
            return True

        # Sonst ...
        else:

            # Dann gib False zurueck --> cooling ist erforderlich
            return False

    def if_cooling_required(self):

        """Diese Methode wird zyklisch im State "ventilation" und
        "pre-ventilation" durchlaufen und beantwortet die Frage ob der State
        "cooling" betreten werden soll.

        Hierfuer wird ermittelt ob auf angesaugte oder ausgeblasene Luft
        geregelt wird und in Abhaengikeit davon mittels der Set-Temperatur,
        Aktuellen-Temperatur und dem Hysterese- Wert die Aussage getroffen.

        https://hcbloks.atlassian.net/wiki/spaces/HPD/pages/1773436937/Switch-points+HVC

        Returns:
            bool: Die Aussage ob der State "cool" betreten werden soll/muss
        """

        # Wird die Temperaturregelung auf den Eintrittstemperaturfuehler gefordert dann, ...
        if self.data.ctl_data_in['Settings']['Control_IO_heat'] is 'in':

            # Weise der aktuellen Temperatur den Wert von "Temp_Air_Chil_in" zu
            actual_temp = self.data.ctl_data_in['Sensor_Data']['Temp_Air_Chil_in']

        # Sonst wenn die Temperaturregelung auf den Austrittstemperaturfuehler gefordert ist dann, ...
        elif self.data.ctl_data_in['Settings']['Control_IO_heat'] is 'out':

            # Weise der aktuellen Temperatur den Wert von "Temp_Air_Chil_out" zu
            actual_temp = self.data.ctl_data_in['Sensor_Data']['Temp_Air_Chil_out']

        # Sonst ...
        else:
            # Weise der aktuellen Temperatur den Wert von "Temp_Air_Chil_in" zu
            actual_temp = self.data.ctl_data_in['Sensor_Data']['Temp_Air_Chil_in']

        hysteresis_cool = self.data.cont_par['Temp_Control_Parameters']['Hyst_cooling']
        settemp = self.data.ctl_data_in['Settings']['Temp_Set']

        # Liegt die Abweichung unter den Hysterenwert dann, ...
        # if (settemp - actual_temp) < hysteresis_cool:
        if actual_temp > (settemp + hysteresis_cool):

            # Dann gib True zurueck --> cooling ist erforderlich
            return True

        # Sonst ...
        else:

            # Dann gib False zurueck --> cooling ist nicht erforderlich
            return False

    def if_detect_ref_air_dt_trigger(self):
        """Diese Funktion aktualisiert in einer Schleife, das Dictionary
        "self.dict_sensor_slope_ref_air" mit den aktuellen Temperatur-Werten.
        Gleichzeitig wird jeder Eintrag ueberprueft ob einer der
        Kaeltemittel/Luft-Temperatur- sensoren einen Temperatur-Anstieg oder
        Gefaelle welcher groesser oder kleiner ist als im Trigger-Parameter
        "self.data.cont_par['sensor_dt_trigger_xxx']" hinterlegt ist, aufweist.

        Wenn der Trigger-Parameter groesser als Null ist, dann wird auf den
        Anstieg abgefragt und somit muss der Anstieg Groesser-Gleich " >= " dem
        Trigger-Parameter sein um ein "True" zu return'en.

        Wenn der Trigger-Parameter kleiner als Null ist, dann wird auf
        Gefaelle abgefragt und somit muss der Anstieg Kleiner-Gleich " <= " dem
        Trigger-Parameter sein um ein "True" zu return'en.

        Ansonsten wird "False" zurueck gegeben.

        Returns:
            bool: Aussage ob einer der Werte von
            "self.data.cont_par['list_ref_air_pos_order'][0]" eine Aenderung
            aufweist welcher einen Schwell-Wert unter/ueberschreitet der in
            "self.data.cont_par['sensor_dt_trigger_xxx']" hinterlegt ist,
            aufweist.
        """

        # Durch-wandere das dt-Dictionary
        for element_key, element_value in self.dict_sensor_slope_ref_air.items():

            # Aktualisiere das dt-Dictionary mit dem aktuellen Temperatur-wert
            element_value.update_val(self.data.cont_par['1w_ref_air'][element_key])

            # Wenn die Ableitung der Temperatur nach der Zeit den trigger "sensor_dt_trigger_xxx"
            # ueber-/unter-steigt, dann ...
            if (0.0 < self.data.cont_par['sensor_dt_trigger' + '_' + self.data.cont_par['list_ref_air_pos_order'][0]] <=
                element_value.get_derivation_value()) or \
                    (0.0 > self.data.cont_par['sensor_dt_trigger' + '_' + self.data.cont_par['list_ref_air_pos_order'][0]]
                     >= element_value.get_derivation_value()):

                # Sichere die Sensor-Position in ein Dictionary mit der ID als "value"
                self.data.cont_par['dict_know_sensor_ids'].update(
                    {self.data.cont_par['list_ref_air_pos_order'].pop(0): element_key})

                # Entferne den Sensor aus den Beobachtungs-Dictionary
                self.dict_sensor_slope_ref_air.pop(element_key, None)

                # Der Trigger-Level wurde ueberschritten also gib True zum State-wechsel
                return True

        # Der Trigger-Level wurde nicht erreicht also gib False zum State-wechsel
        return False

    def if_detect_door_closeing(self):
        """Diese Funktion Ueberwacht den Temperatur-Sensor der angesaugten Luft
        im Lade-Raum. Wenn ein Temperatur-Gefaelle in diesen Mess-kanal
        festegestellt werden kann, wird davon ausgegangen das eine
        Tuer-schiessung statt findet und es wird der aufrufenden Funktion "True"
        zurueckgegeben.

        Returns:
            bool: Status --> Ob eine Tuer-Schliessung erkannt werden kann
        """
        # Wenn eine negative Temperatur-Aenderung von weniger als -1K/30sec detektiert wird, dann ...
        if self.data.ctl_data_in['Sensor_Data']['temp_air_chil_in_rate'] < 0.0:

            # Gibt "True" zurueck um zu signalisieren das eine Tuer-Schliessung erkannt wird
            return True
        else:
            # Gibt "False" zurueck um zu signalisieren das keine Tuer-Schliessung erkannt wird
            return False

    def if_detect_door_opening(self):
        """Diese Funktion Ueberwacht den Temperatur-Sensor der angesaugten Luft
        im Lade-Raum. Wenn ein Temperatur-Anstieg in diesen Mess-kanal
        festegestellt werden kann, wird davon ausgegangen das eine Tuer-oeffnung
        statt findet und es wird der aufrufenden Funktion "True" zurueckgegeben.

        Returns:
            bool: Status --> Ob eine Tuer-Oeffnung erkannt werden kann
        """
        # Wenn eine positive Temperatur-Aenderung von mehr als 2K/30sec detektiert wird, dann ...
        if self.data.ctl_data_in['Sensor_Data']['temp_air_chil_in_rate'] > 1.0:

            # Gibt "True" zurueck um zu signalisieren das eine Tuer-Oeffnung erkannt wird
            return True
        else:
            # Gibt "False" zurueck um zu signalisieren das keine Tuer-Oeffnung erkannt wird
            return False

    def if_detect_glyc_dt_trigger(self):
        """Diese Funktion aktualisiert in einer Schleife, das Dictionary
        "self.dict_sensor_slope_ref_air" mit den aktuellen Temperatur-Werten.
        Gleichzeitig wird jeder Eintrag ueberprueft ob einer der
        Kaeltemittel/Luft-Temperatur- sensoren einen Temperatur-Anstieg oder
        Gefaelle welcher groesser oder kleiner ist als im Trigger-Parameter
        "self.data.cont_par['sensor_dt_trigger_xxx']" hinterlegt ist, aufweist.

        Wenn der Trigger-Parameter groesser als Null ist, dann wird auf den
        Anstieg abgefragt und somit muss der Anstieg Groesser-Gleich " >= " dem
        Trigger-Parameter sein um ein "True" zu return'en.

        Wenn der Trigger-Parameter kleiner als Null ist, dann wird auf
        Gefaelle abgefragt und somit muss der Anstieg Kleiner-Gleich " <= " dem
        Trigger-Parameter sein um ein "True" zu return'en.

        Ansonsten wird "False" zurueck gegeben.

        Returns:
            bool: Aussage ob einer der Werte von
            "self.data.cont_par['list_glyc_pos_order'][0]" eine Aenderung
            aufweist welcher einen Schwell-Wert unter/ueberschreitet der in
            "self.data.cont_par['sensor_dt_trigger_xxx']" hinterlegt ist,
            aufweist.
        """

        # Durch-wandere die das dt-Dictionary
        for element_key, element_value in self.dict_sensor_slope_glycol.items():

            # Aktualisiere das dt-Dictionary mit dem aktuellen Temperatur-wert
            element_value.update_val(self.data.cont_par['1w_glycol'][element_key])

            # Wenn die Ableitung der Temperatur nach der Zeit den trigger "sensor_dt_trigger_xxx"
            # ueber-/unter-steigt, dann ...
            if (0.0 < self.data.cont_par['sensor_dt_trigger' + '_' + self.data.cont_par['list_glyc_pos_order'][0]] <=
                element_value.get_derivation_value()) or \
                    (0.0 > self.data.cont_par['sensor_dt_trigger' + '_' + self.data.cont_par['list_glyc_pos_order'][0]]
                     >= element_value.get_derivation_value()):

                # Sichere die Sensor-Position in ein Dictionary mit der ID als "value"
                self.data.cont_par['dict_know_sensor_ids'].update(
                    {self.data.cont_par['list_glyc_pos_order'].pop(0): element_key})

                # Entferne den Sensor aus den Beobachtungs-Dictionary
                self.dict_sensor_slope_glycol.pop(element_key, None)

                # Der Trigger-Level wurde ueberschritten also gib True zum State-wechsel
                return True

        # Der Trigger-Level wurde nicht erreicht also gib False zum State-wechsel
        return False

    def if_error_detected(self):
        return False

    def if_found_coldest_sensor(self):

        local_temp_dic = {}

        for sensor_id in self.data.cont_par['1w_ref_air'].keys():

            if sensor_id not in self.data.cont_par['dict_know_sensor_ids'].values():
                local_temp_dic.update({sensor_id: self.data.cont_par['1w_ref_air'][sensor_id]})

        id_of_coldest_sensor = min(local_temp_dic, key=local_temp_dic.get)

        if local_temp_dic[id_of_coldest_sensor] < 0.0:

            # Sichere die Sensor-Position in ein Dictionary mit der ID als "value"
            self.data.cont_par['dict_know_sensor_ids'].update(
                {self.data.cont_par['list_ref_air_pos_order'].pop(0): id_of_coldest_sensor})

            return True
        else:
            return False

    def if_found_hottest_sensor(self):
        """
        Returns:
            bool: Ob in den naechsten State gewechselt werden darf
        """

        # Erstell ein lokale Dictionary in welchen die nicht-angelerneten 1w-ID's hinterlegt werden
        local_temp_dic = {}

        # Durch-wandere das Dictionary "1w_ref_air" an seinen "key's"
        for sensor_id in self.data.cont_par['1w_ref_air'].keys():

            # Wenn die 1w-ID nicht im Dictionary der bekannten Sensoren gefunden werden kann, dann ...
            if sensor_id not in self.data.cont_par['dict_know_sensor_ids'].values():

                # Fuege in "local_temp_dic" am "key" die 1w-ID und am "value" den aktuellen Temperatur-wert ein
                local_temp_dic.update({sensor_id: self.data.cont_par['1w_ref_air'][sensor_id]})

        # Selektiere aus "local_temp_dic" die 1w-ID mit heisseste Temperatur heraus in "id_of_hottest_sensor"
        id_of_hottest_sensor = max(local_temp_dic, key=local_temp_dic.get)

        # Wenn der Temperatur-Wert des heissesten Sensor 35.0Ã‚Â°C uebersteigt, dann ...
        if local_temp_dic[id_of_hottest_sensor] > 35.0:

            # Sichere die Sensor-Position in ein Dictionary mit der ID als "value"
            self.data.cont_par['dict_know_sensor_ids'].update(
                {self.data.cont_par['list_ref_air_pos_order'].pop(0): id_of_hottest_sensor})

            return True
        else:
            return False

    def if_heating_not_required(self):

        """Diese Methode wird zyklisch im State "heating" durchlaufen und
        beantwortet die Frage ob der State "heating" verlassen werden soll.

        Hierfuer wird ermittelt ob auf angesaugte oder ausgeblasene Luft
        geregelt wird und in Abhaengikeit davon mittels der Set-Temperatur,
        Aktuellen-Temperatur und dem Hysterese- Wert die Aussage getroffen.

        https://hcbloks.atlassian.net/wiki/spaces/HPD/pages/1773436937/Switch-points+HVC

        Returns:
            bool: Die Aussage ob der State "heating" verlassen werden soll/muss
        """

        # Wird die Temperaturregelung auf den Eintrittstemperaturfuehler gefordert dann, ...
        if self.data.ctl_data_in['Settings']['Control_IO_heat'] is 'in':

            # Weise der aktuellen Temperatur den Wert von "Temp_Air_Chil_in" zu
            actual_temp = self.data.ctl_data_in['Sensor_Data']['Temp_Air_Chil_in']

        # Sonstwenn die Temperaturregelung auf den Austrittstemperaturfuehler gefordert ist dann, ...
        elif self.data.ctl_data_in['Settings']['Control_IO_heat'] is 'out':

            # Weise der aktuellen Temperatur den Wert von "Temp_Air_Chil_out" zu
            actual_temp = self.data.ctl_data_in['Sensor_Data']['Temp_Air_Chil_out']

        # Sonst ...
        else:
            # Weise der aktuellen Temperatur den Wert von "Temp_Air_Chil_in" zu
            actual_temp = self.data.ctl_data_in['Sensor_Data']['Temp_Air_Chil_in']

        hysteresis_heat = self.data.cont_par['Temp_Control_Parameters']['Hyst_heating']
        settemp = self.data.ctl_data_in['Settings']['Temp_Set']

        # Liegt die Abweichung zwischen den Hysteren dann, ...
        # if actual_temp > (settemp + hysteresis_heat):
        if actual_temp > settemp:

            # Dann gib True zurueck --> heating ist erforderlich
            return True

        # Sonst ...
        else:

            # Dann gib False zurueck --> heating ist nicht erforderlich
            return False

    def if_heating_required(self):

        """Diese Methode wird zyklisch im State "ventilation" und
        "pre-ventilation" durchlaufen und beantwortet die Frage ob der State
        "heating" betreten werden soll.

        Hierfuer wird ermittelt ob auf angesaugte oder ausgeblasene Luft
        geregelt wird und in Abhaengikeit davon mittels der Set-Temperatur,
        Aktuellen-Temperatur und dem Hysterese- Wert die Aussage getroffen.

        https://hcbloks.atlassian.net/wiki/spaces/HPD/pages/1773436937/Switch-points+HVC

        Returns:
            bool: Die Aussage ob der State "heating" betreten werden soll/muss
        """

        # Wird die Temperaturregelung auf den Eintrittstemperaturfuehler gefordert dann, ...
        if self.data.ctl_data_in['Settings']['Control_IO_heat'] is 'in':

            # Weise der aktuellen Temperatur den Wert von "Temp_Air_Chil_in" zu
            actual_temp = self.data.ctl_data_in['Sensor_Data']['Temp_Air_Chil_in']

        # Sonstwenn die Temperaturregelung auf den Austrittstemperaturfuehler gefordert ist dann, ...
        elif self.data.ctl_data_in['Settings']['Control_IO_heat'] is 'out':

            # Weise der aktuellen Temperatur den Wert von "Temp_Air_Chil_out" zu
            actual_temp = self.data.ctl_data_in['Sensor_Data']['Temp_Air_Chil_out']

        # Sonst ...
        else:
            # Weise der aktuellen Temperatur den Wert von "Temp_Air_Chil_in" zu
            actual_temp = self.data.ctl_data_in['Sensor_Data']['Temp_Air_Chil_in']

        # hysteresis_heat = self.data.cont_par['Temp_Control_Parameters']['Hyst_heating']
        settemp = self.data.ctl_data_in['Settings']['Temp_Set']

        # Liegt die Abweichung zwischen den Hysteren dann, ...
        # if actual_temp < (settemp + hysteresis_heat):
        if actual_temp < settemp:

            # Dann gib True zurueck --> heating ist erforderlich
            return True

        # Sonst ...
        else:

            # Dann gib False zurueck --> heating ist nicht erforderlich
            return False

    def if_high_ambient_is_necessary(self):
        if self.data.ctl_data_in['Sensor_Data']['Temp_Air_Cond_in'] > 40.0:
            return True

    def if_high_ambient_is_over(self):
        if self.data.ctl_data_in['Sensor_Data']['Temp_Air_Chil_in'] < 6.0:
            return True

    def if_low_pressure_is_over(self):
        if self.data.ctl_data_in['Sensor_Data']['press_ref_low'] > 5.5:
            return True

    def if_low_pressure_is_under(self):
        if self.data.ctl_data_in['Sensor_Data']['press_ref_low'] < 4.5:
            return True

    def if_modus_man_defrost(self):

        if int(self.data.ctl_data_in['Settings']['Mode']) is 3:
            return True
        else:
            return False

    def if_modus_manual(self):

        if int(self.data.ctl_data_in['Settings']['Mode']) is 2:
            return True
        else:
            return False

    def if_modus_temp_control(self):

        if int(self.data.ctl_data_in['Settings']['Mode']) is 1:
            return True
        else:
            return False


    def if_temp_sensors_glycol_in_range(self):
        """Diese Funktion bewertet ob die Glycol-Temperaturen so nahe
        beieinander liegen sodass mit der anlern-prozedur begonnen werden kann

        Returns:
            bool: Die Funktion gibt True zurueck wenn alle Temperaturwerte nahe
            beieinander liegen
        """
        # Wenn die Populations-Varianz der Glycol-Temperaturwerte kleiner ist als der Parameter, dann ...
        if statistics.pvariance(list(self.data.cont_par['1w_glycol'].values())) \
                <= self.data.cont_par['sensor_in_range_pvariance']:
            return True
        else:
            return False

    def if_temp_sensors_ref_air_in_range(self):
        """Diese Funktion bewertet ob die Kaeltemittel/Luft-Temperaturen so nahe
        beieinander liegen sodass mit der Anlern-prozedur begonnen werden kann

        Returns:
            bool: Die Funktion gibt True zurueck wenn alle Temperaturwerte nahe
            beieinander liegen
        """
        # Wenn die Populations-Varianz der Glycol-Temperaturwerte kleiner ist als der Parameter, dann ...
        if statistics.pvariance(list(self.data.cont_par['1w_ref_air'].values())) \
                <= 0.5:
            return True
        else:
            return False

    def if_temp_sensors_ref_air_is_still(self):
        """Diese Funktion bewertet ob die Kaeltemittel/Luft-Temperaturen so
        ruhig sind, so dass mit der anlern-prozedur begonnen werden kann.
        Hierfuer muss das dt der Kaeltemittel/Luft-Temperaturen-Sensoren in
        einen definierten Toleranz-Band liegen

        Returns:
            bool: Die Funktion gibt True zurueck wenn mit der Anlern-Prozedur
            fortgefahren werden kann
        """
        # Durch-wandere die das dt-Dictionary
        for element_key, element_value in self.dict_sensor_slope_ref_air.items():

            if element_value.get_derivation_value() > 0.25 or element_value.get_derivation_value() < -0.25:
                return False
        return True

    def if_ventilation_required(self):

        # Wird die Temperaturregelung auf den Eintrittstemperaturfuehler gefordert dann, ...
        if self.data.ctl_data_in['Settings']['Control_IO_heat'] is 'in':

            # Weise der aktuellen Temperatur den Wert von "Temp_Air_Chil_in" zu
            actual_temp = self.data.ctl_data_in['Sensor_Data']['Temp_Air_Chil_in']

        # Sonstwenn die Temperaturregelung auf den Austrittstemperaturfuehler gefordert ist dann, ...
        elif self.data.ctl_data_in['Settings']['Control_IO_heat'] is 'out':

            # Weise der aktuellen Temperatur den Wert von "Temp_Air_Chil_out" zu
            actual_temp = self.data.ctl_data_in['Sensor_Data']['Temp_Air_Chil_out']

        # Sonst ...
        else:
            # Weise der aktuellen Temperatur den Wert von "Temp_Air_Chil_in" zu
            actual_temp = self.data.ctl_data_in['Sensor_Data']['Temp_Air_Chil_in']

        hysteresis_heat = self.data.cont_par['Temp_Control_Parameters']['Hyst_heating']
        hysteresis_cool = self.data.cont_par['Temp_Control_Parameters']['Hyst_cooling']
        settemp = self.data.ctl_data_in['Settings']['Temp_Set']

        # Liegt die Abweichung zwischen den Hysterese-Werten dann, ...
        if hysteresis_heat < (settemp - actual_temp) < hysteresis_cool:

            # Dann gib True zurueck --> Ventilation ist erforderlich
            return True

        # Sonst ...
        else:

            # Dann gib False zurueck --> Ventilation ist nicht erforderlich
            return False

    def on_enter_temp_control_cooling_door_open_fan_off(self):
        """Diese Funktion wird einmalig beim betreten des States
        "...↦door_open" aufgerufen und es werden folgende Aktionen
        gesetzt: -) Der Chiller-Luefter wird ausgeschalten -) Der Verdichter
        regelt (mit max 1500RPM) auf die Glycol-Temperatur
        """
        self.parent.event_fan_chill_stop()
        self.parent.event_comp_auto_3()

    def on_enter_temp_control_cooling_door_open_fan_on(self):
        """Diese Funktion wird einmalig beim betreten des States
        "...↦door_open" aufgerufen und es werden folgende Aktionen
        gesetzt: -) Der Chiller-Luefter wird auf Leerlauf-Leistung gestellt
        """
        self.parent.event_fan_chill_idle()

    def on_enter_init_check(self):
        pass

    def on_enter_learn_sensor_pos(self):
        """Diese Funktion wird einmalig beim betreten des States
        "...↦learn_sensor_pos" aufgerufen und loest das
        Initialisierung-Event aus
        """
        self.parent.event_modus_init_learn_sensor_pos()

    def on_enter_learn_sensor_pos_faded_temp(self):
        """Diese Funktion wird einmalig beim betreten des States
        "...↦learn_sensor_pos↦faded_temp" aufgerufen. Es werden der
        Chiller-Fan, Cond-Fan und die Glycol-Pumpe auf volle Leistung geschalten
        """
        # Schalte den Chiller Fan auf volle Leistung
        self.parent.event_fan_chill_full()

        # Schalte die Glycol-Pumpe auf volle Leistung
        self.parent.event_pump_full()

        # Schalte den Condenser Fan auf volle Leistung
        self.parent.event_fan_cond_full()

    def on_enter_learn_sensor_pos_found_sensor_4(self):
        """Wenn der 4te Temperatur-sensor gefunden wurde dann, schalte die
        Glycol-Pumpe auf volle Leistung.

        Bei diesen Sensor handelt es sich um den letzten Glycol-Sensor im
        Kreislauf --> "Temp_Gly_Chil_in"
        """
        self.parent.event_pump_full()

    def on_enter_learn_sensor_pos_found_sensor_5(self):
        """Wenn der 5te Temperatur-Sensor gefunden wurde dann, schalte die
        Chiller-Fan auf volle Leistung

        Bei diesen Sensor handelt es sich um den Luft-Austritts-
        Temperatur---> "Temp_Air_Chil_out"
        """
        # Schalte den Chiller Fan auf volle Leistung
        self.parent.event_fan_chill_full()

    def on_enter_learn_sensor_pos_found_sensor_6(self):
        """Wenn der 6te Temperatur-Sensor gefunden wurde dann, schalte die
        Heizung Aus

        Bei diesen Sensor handelt es sich um den Luft-Eintritts- Temperatur
        am Chiller --> "Temp_Air_Chil_in"
        """
        self.data.ctl_data_out['Heat']['Status'] = False

    def on_enter_learn_sensor_pos_found_sensor_7(self):
        """Wenn der 7te Glycol-Temperatursensor gefunden wurde dann, schalte die
        Glycol-Pumpe auf halbe Leistung.

        Bei diesen Sensor handelt es sich um den Kaeltemittel-Eintritts-
        Temperatur am Verfluessiger--> "Temp_Ref_Cond_in"
        """
        self.parent.event_pump_half()

    def on_enter_learn_sensor_pos_found_sensor_8(self):
        """Wenn der 8te Glycol-Temperatursensor gefunden wurde dann, schalte die
        Glycol-Pumpe AUS.

        Bei diesen Sensor handelt es sich um den Kaeltemittel-Eintritts-
        Temperatur am Verfluessiger--> "Temp_Ref_Cond_out"
        """
        self.parent.event_pump_stop()

    def on_enter_learn_sensor_pos_found_sensor_11(self):
        """Wenn der 11te Glycol-Temperatursensor gefunden wurde dann, schalte
        die Glycol-Pumpe AUS.

        Bei diesen Sensor handelt es sich um den Luft-Eintritts- Temperatur
        am Verfluessiger--> "Temp_Air_Cond_in" Da dies der letzte Sensor ist
        wird der Verdichter und der Cond-Fan ausgeschalten.
        """
        self.parent.event_comp_stop()
        self.parent.event_fan_cond_stop()
        self.parent.event_fan_chill_stop()
        self.parent.event_pump_stop()

        dict_tmp = {w1_id: {sensor_pos: 'datagram'}
                    for sensor_pos, w1_id in self.data.cont_par['dict_know_sensor_ids'].items()}

        # pbx_misc.deep_update(self.dict_transfer_par, {'data_in_1w': dict_tmp})
        self.dict_transfer_par['data_in_1w'] = dict_tmp.copy()
        self.parent.configfile_write()

    def on_enter_learn_sensor_pos_heat_up_glycol_1(self):
        """Diese Funktion wird einmalig beim betreten des States
        "...↦learn_sensor_pos↦heat_up_glycol_1" aufgerufen. Es wird
        die Heizung eingeschalten und die Glycol-Pumpe sowie der Chiller-Fan
        ausgeschalten.
        """
        self.data.ctl_data_out['Heat']['Status'] = True
        self.parent.event_fan_chill_stop()
        self.parent.event_fan_cond_stop()
        self.parent.event_pump_stop()

    def on_enter_learn_sensor_pos_heat_up_glycol_2(self):
        """Diese Funktion wird einmalig beim betreten des States
        "...↦learn_sensor_pos↦heat_up_glycol_1" aufgerufen. Es
        werden die Komponenten-Zustaende des vorherigen State's "found_sensor_4"
        beibehalten bis auf die Glycol-Pumpe. Dies wird auf die halbe Leistung
        reduziert.

        Ziel dieses States ist es die Glycol-Temperatur im
        FlÃƒÆ’Ã‚Â¼ssigkeits-kreislauf solange zu steigern bis der timer
        "timer_heat_up_glycol_2" abgelaufen ist.
        """
        self.parent.event_pump_half()

    def on_enter_learn_sensor_pos_observe_1(self):
        """Diese Funktion wird einmalig beim betreten des States
        "...↦learn_sensor_pos↦observe_1" aufgerufen. Es wird die
        Glycol-Pumpe auf halbe Leistung geschalten um die Waerme-walze in
        bewegung zu versetzen.
        """
        self.parent.event_pump_half()

    def on_enter_learn_sensor_pos_observe_2(self):
        """Diese Funktion wird einmalig beim betreten des States
        "...↦learn_sensor_pos↦observe_1" aufgerufen. Es wird Luefter
        Chiller auf volle Leistung eingeschalten und die Glycol-Pumpe von halber
        auf volle Leistung geschalten.

        Die aufgebaute Waerme im Fluessigkeitskreislauf soll auf den
        "Temp_Air_Chil_out" geblasen werden, um an ihm den "air_chil_dt_trigger2
        sicher auszuloesen
        """
        self.parent.event_fan_chill_full()
        self.parent.event_pump_full()

    def on_enter_learn_sensor_pos_start_cond_fan(self):
        """Diese Funktion wird einmalig beim betreten des States
        "...↦learn_sensor_pos↦start_cond_fan" aufgerufen. Es wird
        der Condenser-Fan auf volle Leistung geschalten.
        """
        self.parent.event_fan_cond_full()

    def on_enter_learn_sensor_pos_start_ref_cycle(self):
        """Diese Funktion wird einmalig beim betreten des States
        "...↦learn_sensor_pos↦start_ref_cycle" aufgerufen. Es wird
        der Verdichter mit der hoechst moeglichen Drehzahl gestartet
        """
        self.parent.event_comp_full()

    def on_enter_learn_sensor_pos_wake_up_components(self):
        """Diese Funktion wird einmalig beim betreten des States
        "...↦learn_sensor_pos↦wake_up_components" aufgerufen.

        Es wird die Haupt-Strom-Versorgung eingeschalten und die parallel-states der Komponenten:
            - Chiller-Fan
            - Pumpe Glycol
            - Verdichter

        aufgeweckt

        Desweiteren werdem zwei (hilfs)-Listen erstellt in welcher sich die
        1w-Sensor-Typen getrennt befinden. Diese werden benoatigt um das
        Dictionary mit den dt-Object der Sensoren zu erstellen.

        Es werden der Chiller-Fan und die Glycol-Pumpe auf volle Leistung
        geschalten.

        Des weiteren werden die Derivation-Objecte getrennt fÃƒÆ’Ã‚Â¼r die
        Glycol und Kaeltemittel/Luft-Sensoren erstellt. Hierfuer werden zwei
        lokale Listen erstellt ("list_gly_sensors" / "list_ref_air_sensors").

        Mit diesen werden die werden die Dictionary's
        ("dict_sensor_slope_glycol" / "dict_sensor_slope_ref_air") befuellt. Als
        "key" wird die 1w-ID des Sensors genommen und am "value" wird das
        Derivation-Object eingetragen.
        """

        # Schalte das Haupt-Relais ein um die Komponenten mit Strom zu versorgen
        self.data.ctl_data_out['Main_Relay']['Status'] = True

        # Wecke die beteiligten Komponenten auf
        self.parent.event_fan_chill_wake_up()
        self.parent.event_fan_cond_wake_up()
        self.parent.event_pump_wake_up()
        self.parent.event_comp_wake_up()

        # Erstelle eine Liste in welcher sich die ID's der Glycol-Temperatur-Sensoren befinden
        list_gly_sensors = [sensor_id for sensor_id in self.data.cont_par['list_sensor_id'] if '10-' in sensor_id]

        # Erstelle eine Liste in welcher sich die ID's der Kaeltemittel-Luft-Temperatur-Sensoren befinden
        list_ref_air_sensors = [sensor_id for sensor_id in self.data.cont_par['list_sensor_id'] if '28-' in sensor_id]

        # Erstelle ein Dictionary welches an den 'keys' die 1w-ID's der Glycol-Sensoren hat und die
        # dt-Derivation-Objects an den 'values' enthaelt.
        # Es wird mit den aktuellen Temperatur-Wert initialisiert
        self.dict_sensor_slope_glycol = {element: cont_func.Derivation(
            init_last_val=self.data.cont_par['1w_glycol'][element]) for element in list_gly_sensors}

        # Und das gleich wie in der Funktion darueber, nur mit den Kaeltemittel/Luft-Sensoren
        self.dict_sensor_slope_ref_air = {element: cont_func.Derivation(
            init_last_val=self.data.cont_par['1w_ref_air'][element]) for element in list_ref_air_sensors}

    def on_enter_manual(self):
        self.parent.event_modus_init_manual()

    def on_enter_manual_active(self):
        self.parent.event_fan_cond_manual()
        self.parent.event_fan_chill_manual()
        self.parent.event_pump_manual()
        self.parent.event_comp_manual()

    def on_enter_manual_wake_up_components(self):
        self.parent.event_fan_cond_wake_up()
        self.parent.event_fan_chill_wake_up()
        self.parent.event_pump_wake_up()
        self.parent.event_comp_wake_up()

    def on_enter_standby(self):
        self.parent.event_modus_init_standby()
        self.parent.event_comp_stop()
        self.parent.event_fan_cond_stop()
        self.parent.event_fan_chill_stop()
        self.parent.event_pump_stop()

    def on_enter_standby_running_out(self):
        self.parent.event_comp_stop()
        self.parent.event_fan_cond_stop()
        self.parent.event_fan_chill_stop()
        self.parent.event_pump_stop()

    def on_enter_temp_control(self):
        # Loese das Event zum starten des PI-Reglers aus
        self.parent.event_modus_init_temp_control()
        self.data.ctl_data_out['Main_Relay']['Status'] = True

    def on_enter_temp_control_cooling(self):
        self.parent.init_control_cooling()

    def on_enter_temp_control_cooling_run_comp(self):
        self.parent.event_comp_auto_1()

    def on_enter_temp_control_cooling_start_up_procedure(self):
        self.parent.event_fan_cond_full()
        self.parent.event_fan_chill_full()
        self.parent.event_pump_full()

    def on_enter_temp_control_heating(self):
        self.parent.event_modus_init_control_heating()
        self.parent.event_pump_full()
        self.parent.event_fan_chill_full()

    def on_enter_temp_control_heating_heat(self):
        self.data.ctl_data_out['Heat']['Status'] = True

    def on_enter_temp_control_cooling_high_ambient_start(self):
        self.parent.event_comp_full()

    def on_enter_temp_control_cooling_high_ambient_start_start_heat_transfer(self):
        self.parent.event_fan_chill_full()
        self.parent.event_pump_full()

    def on_enter_temp_control_cooling_high_ambient_start_stop_heat_transfer(self):
        self.parent.event_fan_chill_stop()
        self.parent.event_pump_stop()

    def on_enter_temp_control_pre_ventilation(self):
        self.parent.event_fan_chill_full()

    def on_enter_temp_control_wake_up_components(self):
        # Versetze den Kompressor vom OFF-Zustand in den Standby-Zustand
        self.parent.event_comp_wake_up()

        # Versetze den Chiller-Luefter vom OFF-Zustand in den Standby-Zustand
        self.parent.event_fan_chill_wake_up()

        # Versetze den Kondensator-Luefter vom OFF-Zustand in den Standby-Zustand
        self.parent.event_fan_cond_wake_up()

        # Versetze den Kondensator-Luefter vom OFF-Zustand in den Standby-Zustand
        self.parent.event_pump_wake_up()

    def on_exit_temp_control_cooling_door_open(self):
        self.parent.event_fan_chill_full()

    def on_exit_init_check(self):
        self.timer_modus_check_value = 0.0

    def on_exit_manual(self):
        # self.parent.event_modus_init_standby()
        self.parent.event_comp_stop()
        self.parent.event_fan_cond_stop()
        self.parent.event_fan_chill_stop()
        self.parent.event_pump_stop()
        self.data.ctl_data_out['Main_Relay']['Status'] = False
        self.data.ctl_data_out['Heat']['Status'] = False

    def on_exit_standby_running_out(self):
        self.parent.event_comp_stop()
        self.parent.event_fan_cond_stop()
        self.parent.event_fan_chill_stop()
        self.parent.event_pump_stop()
        self.data.ctl_data_out['Main_Relay']['Status'] = False

    def on_exit_temp_control_cooling(self):
        self.parent.event_comp_stop()
        self.parent.event_fan_cond_stop()

    def on_exit_temp_control_cooling_start_up_procedure(self):
        self.timer_modus_startup_value = 0.0

    def on_exit_temp_control_heating(self):
        pass

    def on_exit_temp_control_heating_heat(self):
        self.data.ctl_data_out['Heat']['Status'] = False

    def on_exit_temp_control_cooling_high_ambient_start(self):
        self.parent.event_fan_chill_full()
        self.parent.event_pump_full()
        self.parent.event_comp_auto_1()

    def on_exit_temp_control_pre_ventilation(self):

        self.timer_modus_to_ventilation_value = 0.0
        self.timer_modus_to_cooling_value = 0.0
        self.timer_modus_to_heating_value = 0.0

    def on_exit_learn_sensor_pos(self):
        """Nachdem der Sensor-Anlern-Vorgang abgeschlossen ist versetze die
        jeweiligen Komponenten wieder in den Tief-Schlaf
        """
        self.parent.event_comp_fall_asleep()
        self.parent.event_pump_fall_asleep()
        self.parent.event_fan_chill_fall_asleep()
        self.parent.event_fan_cond_fall_asleep()

    def set_parent(self, parent):
        """
        Args:
            parent:
        """
        self.parent: HCBloksStateMachine = parent

    def timer_7_after_900_sec(self):
        if self.timer_7 > 15.0:
            self.timer_7 = 0.0
            return True

    def timer_7_count(self):
        self.timer_7 = self.timer_7 + time_step

    def timer_8_after_15_sec(self):
        if self.timer_8 > 15.0:
            self.timer_8 = 0.0
            return True

    def timer_8_count(self):
        self.timer_8 = self.timer_8 + time_step

    def timer_filo_delay_count(self):
        self.timer_filo_delay_value += time_step

    def timer_filo_delay_timeout(self):
        if self.timer_filo_delay_value > 150.0:
            self.timer_filo_delay_value = 0.0
            return True

    def timer_heat_up_glycol_1_count(self):
        self.timer_heat_up_glycol_1_value += time_step

    def timer_heat_up_glycol_1_timeout(self):
        if self.timer_heat_up_glycol_1_value > self.data.cont_par['heat_time_4_sensor_train_1']:
            self.timer_heat_up_glycol_1_value = 0.0
            return True

    def timer_heat_up_glycol_2_count(self):
        self.timer_heat_up_glycol_2_value += time_step

    def timer_heat_up_glycol_2_timeout(self):
        if self.timer_heat_up_glycol_2_value > self.data.cont_par['heat_time_4_sensor_train_2']:
            # self.timer_heat_up_glycol_2_value = 0.0
            return True

    def timer_modus_check_count(self):
        self.timer_modus_check_value = self.timer_modus_check_value + time_step

    def timer_modus_check_timeout(self):
        if self.timer_modus_check_value > 3.0:
            self.timer_modus_check_value = 0.0
            return True

    def timer_modus_least_in_faded_count(self):
        self.timer_modus_least_in_faded_value += time_step

    def timer_modus_least_in_faded_timeout(self):
        if self.timer_modus_least_in_faded_value > self.data.cont_par['least_time_in_faded']:
            # self.timer_modus_least_in_faded_value = 0.0
            return True

    def timer_modus_prep_door_count(self):
        self.timer_modus_prep_door_value += time_step

    def timer_modus_prep_door_timeout(self):
        if self.timer_modus_prep_door_value > 15.0:
            self.timer_modus_prep_door_value = 0.0
            return True

    def timer_modus_run_ref_cycle_before_sensor_eval_count(self):
        self.timer_run_ref_cycle_before_sensor_eval_value += time_step

    def timer_modus_run_ref_cycle_before_sensor_eval_timeout(self):
        if self.timer_run_ref_cycle_before_sensor_eval_value > \
                self.data.cont_par['run_ref_cycle_before_sensor_eval']:
            self.timer_run_ref_cycle_before_sensor_eval_value = 0.0
            return True

    def timer_modus_startup_count(self):
        self.timer_modus_startup_value += time_step

    def timer_modus_startup_timeout(self):
        if self.timer_modus_startup_value > 10.0:
            self.timer_modus_startup_value = 0.0
            return True

    def timer_modus_to_cooling_count(self):
        self.timer_modus_to_cooling_value += time_step

    def timer_modus_to_cooling_timeout(self):
        if self.timer_modus_to_cooling_value > 15.0:
            self.timer_modus_to_cooling_value = 0.0
            return True

    def timer_modus_to_heating_count(self):
        self.timer_modus_to_heating_value += time_step

    def timer_modus_to_heating_timeout(self):
        if self.timer_modus_to_heating_value > 15.0:
            self.timer_modus_to_heating_value = 0.0
            return True

    def timer_modus_to_ventilation_count(self):
        self.timer_modus_to_ventilation_value += time_step

    def timer_modus_to_ventilation_timeout(self):
        if self.timer_modus_to_ventilation_value > 30.0:
            self.timer_modus_to_ventilation_value = 0.0
            return True

    def timer_max_time_door_open_count(self):
        self.timer_max_time_door_open_value += time_step

    def timer_max_time_door_open_timeout(self):
        if self.timer_max_time_door_open_value > 120.0:
            self.timer_max_time_door_open_value = 0.0
            return True

    def timer_max_time_door_open_fan_off_count(self):
        self.timer_max_time_door_open_fan_off_value += time_step

    def timer_max_time_door_open_fan_off_timeout(self):
        if self.timer_max_time_door_open_fan_off_value > 20.0:
            self.timer_max_time_door_open_fan_off_value = 0.0
            return True

    def update_glycol_dt_dict(self):
        """In dieser Funktion werden alle dt-Objects der eingetragenen
        1w-Glycol-Sensoren aktualisiert
        """
        # Durch-wandere die das dt-Dictionary
        for element_key, element_value in self.dict_sensor_slope_glycol.items():
            # Aktualisiere das dt-Dictionary mit dem aktuellen Temperatur-wert
            element_value.update_val(self.data.cont_par['1w_glycol'][element_key])

    def update_ref_air_dt_dict(self):
        """In dieser Funktion werden alle dt-Objects der eingetragenen
        1w-Glycol-Sensoren aktualisiert
        """

        # Durch-wandere die das dt-Dictionary
        for element_key, element_value in self.dict_sensor_slope_ref_air.items():
            # Aktualisiere das dt-Dictionary mit dem aktuellen Temperatur-wert
            element_value.update_val(self.data.cont_par['1w_ref_air'][element_key])


class StateMachinePumpGlycol:
    parent: object

    def __init__(self, data):

        # Timer-Variabel vom State "check"
        """
        Args:
            data:
        """
        self.timer_pump_check_value = 0.0

        # Timer fuer toggle_pump_on
        self.timer_pump_toggle_on_value = 0.0

        # Timer fuer toggle_pump_off
        self.timer_pump_toggle_off_value = 0.0

        # In dieser Variable sind alle Eingangs/Ausgangs/Parameter-Daten als Dictionary hinterlegt
        self.data = data

        # Im folgenden String wird der Name des Parallel-States definiert
        self.name = 'pump'

        # Erstelle einen Praefix der aus den State-Machine-Namen und den "sub-State"-Symbol besteht
        self.pref = self.name + '↦'

        # Erstelle einen Praefix der aus den State-Machine-Namen und einen "underline"-Symbol besteht
        # fuer die Event-Signale
        self.e_pref = 'event_' + self.name + '_'

        # Im folgenden String wird der Init-Zustand definiert:
        self.init_state = 'init'

        # In der folgenden Liste sind alle States eingetragen
        self.states = [
            'init',
            'off',
            {'name': 'on', 'children': [
                'check',
                'error',
                'automatic',
                'idle',
                'half',
                'full',
                'manual',
                {'name': 'toggle', 'children': [
                    'on',
                    'off']},
                {'name': 'standby', 'children': [
                    'standstill',
                    'running_out']}]
             }
        ]

        # ---------------- Erstelle die "Transitions" von der Zustands-Machine --------------------------------------- #
        # trigger, source, dest, conditions=None, unless=None, before=None, after=None, prepare=None
        self.transitions = [

            # trigger (str): Event-Signal of the transitions
            # source (str): Source state of the transition.
            # dest (str): Destination state of the transition.
            # prepare (list): Callbacks executed before conditions checks.
            # unless (list): Callbacks evaluated to determine if the transition should be not executed.
            # conditions (list): Callbacks evaluated to determine if the transition should be executed.
            # before (list): Callbacks executed before the transition is executed
            #     but only if condition checks have been successful.
            # after (list): Callbacks executed after the transition is executed
            #     but only if condition checks have been successful.

            # -------------------- External Events ------------------------------------------------------------------- #
            #
            # Diese Signale werden verwendet um die Zustands-Aenderungen zu bewirken. Sie stellen eine Art Interaktion-
            # Schnittstelle dar. Folgende Signal stehen zu Verfuegung:
            #   TODO: Die Paefixe in einer Schleife im nachhienein hinzufuegen --> exl. loop
            #   -) wake_up      ↦ In den Betriebs-bereiten Zustand versetzen
            #   -) fall_asleep  ↦ In den Energie-spar-Modus versetzen
            #   -) manual       ↦ In den Betriebs-bereiten Zustand versetzen und Manuelle vorgaben weiterreichen
            #   -) stop         ↦ Von einen aktiven Betriebs-Zustand in den Betriebs-bereiten Zustand wechseln
            #   -) idle         ↦ In den aktiven Betriebs-Zustand "geringste Leistung" versetzen
            #   -) full         ↦ In den aktiven Betriebs-Zustand "maximale Leistung" versetzen
            #   -) toggle       ↦ In diesen Betriebszustand wird Zeit-abhaengig
            #
            # (1)
            # Wenn das Event "wake_up" geschickt wurde, wird in den Zustand "on" gewechselt
            {'trigger': self.e_pref + 'wake_up', 'source': self.pref + 'off', 'dest': self.pref + 'on',
             'after': 'pic_update'},
            #
            # (2)
            # Wenn das Event "fall_asleep" geschickt wurde, wird in den Zustand "off" gewechselt
            {'trigger': self.e_pref + 'fall_asleep', 'source': self.pref + 'on', 'dest': self.pref + 'off',
             'after': 'pic_update'},
            #
            # (3)
            # Wenn das Event "manual" geschickt wurde, wird in den Zustand "...↦on↦manual" gewechselt
            {'trigger': self.e_pref + 'manual', 'source': self.pref + 'on↦standby', 'dest': self.pref + 'on↦manual',
             'after': 'pic_update'},
            #
            # (4)
            #  Wenn das Event "stop" geschickt wurde, wird in den Zustand "...↦on↦standby" gewechselt
            {'trigger': self.e_pref + 'stop', 'source': [self.pref + 'on↦idle', self.pref + 'on↦full',
                                                         self.pref + 'on↦manual', self.pref + 'on↦automatic',
                                                         self.pref + 'on↦toggle', self.pref + 'on↦half'],
             'dest': self.pref + 'on↦standby', 'after': 'pic_update'},
            #
            # (5)
            # Wenn Event "idle" geschickt wurde, wird in den Zustand "...↦on↦idle" gewechselt
            {'trigger': self.e_pref + 'idle', 'source': [self.pref + 'on↦standby', self.pref + 'on↦full',
                                                         self.pref + 'on↦toggle', self.pref + 'on↦half'],
             'dest': self.pref + 'on↦idle', 'after': 'pic_update'},
            #
            # (6)
            # Wenn das Event "full" geschickt wurde, wird in den Zustand "...↦on↦full" gewechselt
            {'trigger': self.e_pref + 'full', 'source': [self.pref + 'on↦standby', self.pref + 'on↦idle',
                                                         self.pref + 'on↦toggle', self.pref + 'on↦half'],
             'dest': self.pref + 'on↦full', 'after': 'pic_update'},
            #
            # (7)
            # Wenn das Event "automatic" geschickt wurde, wird in den Zustand "...↦on↦full" gewechselt
            {'trigger': self.e_pref + 'automatic', 'source': [self.pref + 'on↦standby', self.pref + 'on↦toggle',
                                                              self.pref + 'on↦idle', self.pref + 'on↦half'],
             'dest': self.pref + 'on↦automatic', 'after': 'pic_update'},
            #
            # (8)
            # Wenn das Event "toggle" geschickt wurde, wird in den Zustand "...↦on↦toggle" gewechselt
            {'trigger': self.e_pref + 'toggle', 'source': [self.pref + 'on↦standby', self.pref + 'on↦full',
                                                           self.pref + 'on↦idle', self.pref + 'on↦half'],
             'dest': self.pref + 'on↦toggle', 'after': 'pic_update'},
            #
            # (8)
            # Wenn das Event "half" geschickt wurde, wird in den Zustand "...↦on↦half" gewechselt
            {'trigger': self.e_pref + 'half', 'source': [self.pref + 'on↦standby', self.pref + 'on↦full',
                                                         self.pref + 'on↦idle', self.pref + 'on↦toggle'],
             'dest': self.pref + 'on↦half', 'after': 'pic_update'},
            #
            # -------------------- Transfer transitions -------------------------------------------------------------- #
            #
            # Diese Transitions bewirken interne Wechsel zwischen den States. Die Bedingungen in Ihnen werden in jeden
            # zyklischen Aufruf ueberprrueft und ein Wechsel bei erfuellung durchgefuehrt
            #
            # (101)
            # Ueberpruefe ob die Versorgung-Spannung in Ordnung ist
            {'trigger': 'loop', 'source': self.pref + 'on↦check', 'dest': self.pref + 'on↦standby',
             'conditions': ['if_voltage_supply_ok'], 'after': 'pic_update'},
            #
            # (102)
            # Wenn laenger als 3 Sekunden keine positive Freigabe kommt dann wird der Error-Zustand aktiv
            {'trigger': 'loop', 'source': self.pref + 'on↦check', 'dest': self.pref + 'on↦error',
             'prepare': 'timer_pump_check_count', 'conditions': ['timer_pump_check_timeout'], 'after': 'pic_update'},
            #
            # (103)
            # Wenn ein schwere Fehler entdeckt wurde, wird in den Zustand Error gewechselt
            {'trigger': 'loop', 'source': [self.pref + 'on↦standby', self.pref + 'on↦automatic', self.pref + 'on↦full',
                                           self.pref + 'on↦idle', self.pref + 'on↦standby'],
             'dest': self.pref + 'on↦error', 'conditions': ['if_error_detected'], 'after': 'pic_update'},
            #
            # (104)
            # Wenn die Komponente sich ausgelaufen hat (langsam heruntergefahren) und somit still steht soll in den
            # State "standstill" gewechselt werden
            {'trigger': 'loop', 'source': self.pref + 'on↦standby↦running_out',
             'dest': self.pref + 'on↦standby↦standstill', 'conditions': ['if_pump_turns'], 'after': 'pic_update'},
            #
            # (105)
            # Wenn der Zustand "...↦toggle↦on" seit x Sekunden aktiv ist dann wechsle in "...↦toggle↦off"
            {'trigger': 'loop', 'source': self.pref + 'on↦toggle↦on', 'dest': self.pref + 'on↦toggle↦off',
             'prepare': 'timer_pump_toggle_on_count', 'conditions': ['timer_pump_toggle_on_timeout'],
             'after': 'pic_update'},
            #
            # (106)
            # Wenn der Zustand "...↦toggle↦off" seit x Sekunden aktiv ist dann wechsle in "...↦toggle↦on"
            {'trigger': 'loop', 'source': self.pref + 'on↦toggle↦off', 'dest': self.pref + 'on↦toggle↦on',
             'prepare': 'timer_pump_toggle_off_count', 'conditions': ['timer_pump_toggle_off_timeout'],
             'after': 'pic_update'},
            #
            #
            # -------------------- Internal transitions -------------------------------------------------------------- #
            #
            # Dieser Kategorie werden Transitions zugewiesen welche fuer Initalisierungen oder zyklische Aufrufe
            # verwendet werden
            #
            # (201)
            # Initialisierung-Transition damit ein "on_enter_off_xxx" moeglich ist
            {'trigger': 'loop', 'source': self.pref + 'init', 'dest': self.pref + 'off', 'after': 'pic_update'},
            #
            # (202)
            # Das Event "init_on" wird von "enter_on" gesendet und ermoeglicht den sprung in den sub-state "on↦check"
            #
            {'trigger': self.e_pref + 'init_on', 'source': self.pref + 'on', 'dest': self.pref + 'on↦check',
             'after': 'pic_update'},
            #
            # (203)
            # Wenn die Komponente beim betreten des "standby"-States bereits heruntergefahren ist,
            # soll direkt der State "standstill" aktiv werden
            {'trigger': self.e_pref + 'init_standby', 'source': self.pref + 'on↦standby',
             'dest': self.pref + 'on↦standby↦standstill', 'unless': ['if_pump_turns'], 'after': 'pic_update'},

            # (204)
            # Wenn die Komponete eine groessere RPM als minRPM aufweist dann gehe davon aus das sie noch eingeschalten
            # ist, Ramp-down notwendig
            {'trigger': self.e_pref + 'init_standby', 'source': self.pref + 'on↦standby',
             'dest': self.pref + 'on↦standby↦running_out', 'conditions': ['if_pump_turns'], 'after': 'pic_update'},

            # (205)
            # Zyklischer call fuer den "standstill"-State
            {'trigger': 'loop', 'source': self.pref + 'on↦standby↦standstill', 'dest': None},

            # (206)
            # Zyklischer call fuer den "Running_Out"-State
            {'trigger': 'loop', 'source': self.pref + 'on↦standby↦running_out', 'dest': None,
             'conditions': ['is_not_zero_pump_rpm'], 'after': 'pump_ramp_down'},

            # (207)-Visio
            # Zyklischer call fuer den "automatic"-State
            {'trigger': 'loop', 'source': self.pref + 'on↦automatic', 'dest': None, 'after': 'pic_update'},

            # (208)
            # Zyklischer call fuer den "idle"-State
            {'trigger': 'loop', 'source': self.pref + 'on↦idle', 'dest': None},

            # (209)
            # Zyklischer call fuer den "full"-State
            {'trigger': 'loop', 'source': self.pref + 'on↦full', 'dest': None},

            # (210)
            # Zyklischer call fuer den "manual"-State
            {'trigger': 'loop', 'source': self.pref + 'on↦manual', 'dest': None,
             'after': 'during_' + self.name + '_manual'},
            #
            # (211)
            # Das Event "init_toggle" wird von "enter_on" gesendet und ermoeglicht den sprung in den
            # sub-state "toggle↦on"
            {'trigger': self.e_pref + 'init_toggle', 'source': self.pref + 'on↦toggle',
             'dest': self.pref + 'on↦toggle↦on', 'after': 'pic_update'}
        ]

    def set_parent(self, parent):
        """
        Args:
            parent:
        """
        self.parent = parent

    def if_voltage_supply_ok(self):
        return True

    def during_pump_manual(self):

        self.data.ctl_data_out[self.name]['PWM'] = self.data.ctl_data_in['Manual_Input'][
            'man_' + self.name + '_pwm_set']

    # ------------------------ Cyclic-functions ---------------------------------------------------------------------- #

    def pump_ramp_down(self):

        self.data.ctl_data_out['pump']['PWM'] = cont_func.rate_limiter(
            self.data.ctl_data_out['pump']['PWM'],
            self.data.cont_par['pump']['PWM_min'],
            self.data.cont_par['pump']['PWM_dt_sec_max'],
            self.data.cont_par['pump']['PWM_dt_sec_min'],
            time_step)

    # ------------------------ On_enter_Events ----------------------------------------------------------------------- #

    def on_enter_on_full(self):
        self.data.ctl_data_out['pump']['PWM'] = self.data.cont_par['pump']['PWM_max']

    def on_enter_off(self):
        self.data.ctl_data_out['pump']['PWM'] = 0.0

    def on_enter_on(self):
        self.parent.event_pump_init_on()

    def on_enter_on_standby(self):
        self.parent.event_pump_init_standby()

    def on_enter_on_automatic(self):
        pass

    def on_enter_on_idle(self):
        self.data.ctl_data_out['pump']['PWM'] = self.data.cont_par['pump']['PWM_min']

    def on_enter_on_standby_standstill(self):
        self.data.ctl_data_out['pump']['PWM'] = 0.0

    def on_enter_on_toggle(self):
        self.parent.event_pump_init_toggle()

    def on_enter_on_toggle_on(self):
        self.data.ctl_data_out['pump']['PWM'] = self.data.cont_par['pump']['PWM_min']

    def on_enter_on_toggle_off(self):
        self.data.ctl_data_out['pump']['PWM'] = 0.0

    def on_enter_on_half(self):
        self.data.ctl_data_out['pump']['PWM'] = self.data.cont_par['pump']['PWM_max'] / 2.0

    # ------------------------ On_exit_Events ------------------------------------------------------------------------ #

    def on_exit_on_standby_running_out(self):
        self.data.ctl_data_out['pump']['PWM'] = 0.0

    # ------------------------ Conditions ---------------------------------------------------------------------------- #

    def timer_pump_check_timeout(self):

        if self.timer_pump_check_value > 3.0:
            self.timer_pump_check_value = 0.0
            return True

    def timer_pump_check_count(self):
        self.timer_pump_check_value = self.timer_pump_check_value + time_step

    def timer_pump_toggle_on_timeout(self):

        active_time = self.data.cont_par['pump']['toggle_periode'] * self.data.cont_par['pump']['toggle_duty_cycle']

        if self.timer_pump_toggle_on_value > active_time:
            self.timer_pump_toggle_on_value = 0.0
            return True

    def timer_pump_toggle_on_count(self):
        self.timer_pump_toggle_on_value = self.timer_pump_toggle_on_value + time_step

    def timer_pump_toggle_off_timeout(self):

        inactive_time = self.data.cont_par['pump']['toggle_periode'] * (
                1 - self.data.cont_par['pump']['toggle_duty_cycle'])

        if self.timer_pump_toggle_off_value > inactive_time:
            self.timer_pump_toggle_off_value = 0.0
            return True

    def timer_pump_toggle_off_count(self):
        self.timer_pump_toggle_off_value = self.timer_pump_toggle_off_value + time_step

    def is_ok_fb_pump(self):
        return True

    def if_pump_turns(self):
        if self.data.ctl_data_out['pump']['PWM'] <= self.data.cont_par['pump']['PWM_min']:
            return False
        else:
            return True


class StateMachineFanChill:
    parent: object

    def __init__(self, data):

        # Timer-Variabel vom State "check"
        """
        Args:
            data:
        """
        self.timer_fan_chill_check_value = 0.0

        self.data = data

        # Im folgenden String wird der Name des Parallel-States definiert
        self.name = 'fan_chill'

        # Erstelle einen Praefix der aus den State-Machine-Namen und den "sub-State"-Symbol besteht
        self.pref = self.name + '↦'

        # Erstelle einen Praefix der aus den State-Machine-Namen und einen "underline"-Symbol besteht
        # fuer die Event-Signale
        self.e_pref = 'event_' + self.name + '_'

        # Im folgenden String wird der Init-Zustand definiert:
        self.init_state = 'init'

        # In der folgenden Liste sind alle States eingetragen
        self.states = [
            'init',
            'off',
            {'name': 'on', 'initial': 'check', 'children': [
                'check',
                'error',
                {'name': 'standby', 'initial': 'running_out', 'children': [
                    'standstill',
                    'running_out']},
                'automatic',
                'idle',
                'half',
                'full',
                'manual']
             }
        ]

        # ---------------- Erstelle die "Transitions" von der Zustands-Machine --------------------------------------- #
        # trigger, source, dest, conditions=None, unless=None, before=None, after=None, prepare=None
        self.transitions = [

            # trigger (str): Event-Signal of the transitions
            # source (str): Source state of the transition.
            # dest (str): Destination state of the transition.
            # prepare (list): Callbacks executed before conditions checks.
            # unless (list): Callbacks evaluated to determine if the transition should be not executed.
            # conditions (list): Callbacks evaluated to determine if the transition should be executed.
            # before (list): Callbacks executed before the transition is executed
            #     but only if condition checks have been successful.
            # after (list): Callbacks executed after the transition is executed
            #     but only if condition checks have been successful.

            # -------------------- External Events ------------------------------------------------------------------- #
            #
            # Diese Signale werden verwendet um die Zustands-Aenderungen zu bewirken. Sie stellen eine Art Interaktion-
            # Schnittstelle dar. Folgende Signal stehen zu Verfuegung:
            #   TODO: Die Paefixe in einer Schleife im nachhienein hinzufuegen --> exl. loop
            #   -) wake_up      ↦ In den Betriebs-bereiten Zustand versetzen
            #   -) fall_asleep  ↦ In den Energie-spar-Modus versetzen
            #   -) manual       ↦ In den Betriebs-bereiten Zustand versetzen und Manuelle vorgaben weiterreichen
            #   -) stop         ↦ Von einen aktiven Betriebs-Zustand in den Betriebs-bereiten Zustand wechseln
            #   -) idle         ↦ In den aktiven Betriebs-Zustand "geringste Leistung" versetzen
            #   -) half         ↦ In den aktiven Betriebs-Zustand "halber Leistung" versetzen
            #   -) full         ↦ In den aktiven Betriebs-Zustand "maximale Leistung" versetzen
            #
            # (1)
            # Wenn das Event "wake_up" geschickt wurde, wird in den Zustand "on" gewechselt
            {'trigger': self.e_pref + 'wake_up', 'source': self.pref + 'off', 'dest': self.pref + 'on'},
            #
            # (2)
            # Wenn das Event "fall_asleep" geschickt wurde, wird in den Zustand "off" gewechselt
            {'trigger': self.e_pref + 'fall_asleep', 'source': self.pref + 'on', 'dest': self.pref + 'off'},
            #
            # (3)
            # Wenn das Event "manual" geschickt wurde, wird in den Zustand "manual" gewechselt
            {'trigger': self.e_pref + 'manual', 'source': self.pref + 'on↦standby', 'dest': self.pref + 'on↦manual'},
            #
            # (4)
            #  Wenn das Event "stop" geschickt wurde, wird in den Zustand "standby" gewechselt
            {'trigger': self.e_pref + 'stop',
             'source': [self.pref + 'on↦idle', self.pref + 'on↦half', self.pref + 'on↦full', self.pref + 'on↦manual',
                        self.pref + 'on↦automatic'],
             'dest': self.pref + 'on↦standby'},
            #
            # (5)
            # Wenn Event "idle" geschickt wurde, wird in den Zustand "idle" gewechselt
            {'trigger': self.e_pref + 'idle',
             'source': [self.pref + 'on↦standby', self.pref + 'on↦half', self.pref + 'on↦full',
                        self.pref + 'on↦manual', self.pref + 'on↦automatic'],
             'dest': self.pref + 'on↦idle'},
            #
            # (6)
            # Wenn das Event "full" geschickt wurde, wird in den Zustand "full" gewechselt
            {'trigger': self.e_pref + 'full',
             'source': [self.pref + 'on↦standby', self.pref + 'on↦idle', self.pref + 'on↦half',
                        self.pref + 'on↦manual', self.pref + 'on↦automatic'],
             'dest': self.pref + 'on↦full'},
            #
            # (7)
            # Wenn das Event "automatic" geschickt wurde, wird in den Zustand "automatic" gewechselt
            {'trigger': self.e_pref + 'automatic',
             'source': [self.pref + 'on↦standby', self.pref + 'on↦idle', self.pref + 'on↦half',
                        self.pref + 'on↦full', self.pref + 'on↦manual'],
             'dest': self.pref + 'on↦automatic'},
            #
            # (8)
            # Wenn das Event "half" geschickt wurde, wird in den Zustand "...↦on↦half" gewechselt
            {'trigger': self.e_pref + 'half',
             'source': [self.pref + 'on↦standby', self.pref + 'on↦idle', self.pref + 'on↦full',
                        self.pref + 'on↦manual', self.pref + 'on↦automatic'],
             'dest': self.pref + 'on↦half'},
            #
            # -------------------- Transfer transitions -------------------------------------------------------------- #
            #
            # Diese Transitions bewirken interne Wechsel zwischen den States. Die Bedingungen in Ihnen werden in jeden
            # zyklischen Aufruf ueberprrueft und ein Wechsel bei erfuellung durchgefuehrt
            #
            # (101)
            # Ueberpruefe ob die Versorgung-Spannung in Ordnung ist
            {'trigger': 'loop', 'source': self.pref + 'on↦check', 'dest': self.pref + 'on↦standby',
             'conditions': ['if_voltage_supply_ok']},
            #
            # (102)
            # Wenn laenger als 3 Sekunden keine positive Freigabe kommt dann wird der Error-Zustand aktiv
            {'trigger': 'loop', 'source': self.pref + 'on↦check', 'dest': self.pref + 'on↦error',
             'prepare': 'timer_fan_chill_check_count', 'conditions': ['timer_fan_chill_check_timeout']},
            #
            # (103)
            # Wenn ein schwere Fehler entdeckt wurde, wird in den Zustand Error gewechselt
            {'trigger': 'loop', 'source': [self.pref + 'on↦standby', self.pref + 'on↦idle', self.pref + 'on↦half',
                                           self.pref + 'on↦full', self.pref + 'on↦manual', self.pref + 'on↦automatic'],
             'dest': self.pref + 'on↦error', 'conditions': ['if_error_detected']},
            #
            # (104)
            # Wenn die Komponente sich ausgelaufen hat (langsam heruntergefahren) und somit still steht soll in den
            # State "standstill" gewechselt werden
            {'trigger': 'loop', 'source': self.pref + 'on↦standby↦running_out', 'unless': ['if_fan_chill_turns'],
             'prepare': 'during_' + self.name + '_ramp_down', 'dest': self.pref + 'on↦standby↦standstill'},
            #
            # -------------------- Internal transitions -------------------------------------------------------------- #
            #
            # Dieser Kategorie werden Transitions zugewiesen welche fuer Initalisierungen oder zyklische Aufrufe
            # verwendet werden
            #
            # (201)
            # Initialisierung-Transition damit ein "on_enter_off_xxx" moeglich ist
            {'trigger': 'loop', 'source': self.pref + 'init', 'dest': self.pref + 'off'},
            #
            # (210)
            # Zyklischer call fuer den "manual"-State
            {'trigger': 'loop', 'source': self.pref + 'on↦manual', 'dest': None,
             'after': 'during_' + self.name + '_manual'}

        ]

    def set_parent(self, parent):
        """
        Args:
            parent:
        """
        self.parent = parent

    def during_fan_chill_manual(self):

        self.data.ctl_data_out[self.name]['PWM'] = self.data.ctl_data_in['Manual_Input']['man_' + self.name + '_rpm']

    def during_fan_chill_ramp_down(self):

        self.data.ctl_data_out[self.name]['PWM'] = cont_func.rate_limiter(
            self.data.ctl_data_out[self.name]['PWM'],
            self.data.cont_par[self.name]['PWM_min'],
            self.data.cont_par[self.name]['PWM_dt_sec_max'],
            self.data.cont_par[self.name]['PWM_dt_sec_min'],
            time_step)

    # ------------------------ On_enter_Events ----------------------------------------------------------------------- #

    def on_enter_on_full(self):
        # Weise aus der Parameter-Struktur den entsprechen Wert zu
        self.data.ctl_data_out[self.name]['PWM'] = self.data.cont_par[self.name]['PWM_max']

    def on_enter_off(self):
        # Weise den Wert "0.0" zum Ausschalten zu
        self.data.ctl_data_out[self.name]['PWM'] = 0.0

    def on_enter_on_idle(self):
        # Weise aus der Parameter-Struktur den entsprechen Wert zu
        self.data.ctl_data_out[self.name]['PWM'] = self.data.cont_par[self.name]['PWM_min']

    def on_enter_on_half(self):
        # Weise aus der Parameter-Struktur den entsprechen Wert zu
        self.data.ctl_data_out['fan_chill']['PWM'] = self.data.cont_par['fan_chill']['PWM_max'] / 2.0

    # ------------------------ On_exit_Events ------------------------------------------------------------------------ #

    def on_exit_on_standby_running_out(self):
        self.data.ctl_data_out[self.name]['PWM'] = 0.0

    # ------------------------ Conditions ---------------------------------------------------------------------------- #

    def timer_fan_chill_check_timeout(self):

        if self.timer_fan_chill_check_value >= 3.0:
            self.timer_fan_chill_check_value = 0.0
            return True
        else:
            return False

    def timer_fan_chill_check_count(self):
        # Inkrementiere den Timer um einen Zeit-Schritt
        self.timer_fan_chill_check_value = self.timer_fan_chill_check_value + time_step

    def if_fan_chill_turns(self):
        # Wenn der Lueftr mit einen Wert, der groesser als "_min" ist angesteuert wird, gibt "True" zurueck
        if self.data.ctl_data_out[self.name]['PWM'] <= self.data.cont_par[self.name]['PWM_min']:
            return False
        else:
            return True

    def if_voltage_supply_ok(self):
        return True


class StateMachineFanCond:
    parent: object

    def __init__(self, data):

        # Timer-Variabel vom State "Check"
        """
        Args:
            data:
        """
        self.timer_fan_cond_check_value = 0.0

        self.data = data

        # Im folgenden String wird der Name des Parallel-States definiert
        self.name = 'fan_cond'

        # Erstelle einen Praefix der aus den State-Machine-Namen und den "sub-State"-Symbol besteht
        self.pref = self.name + '↦'

        # Erstelle einen Praefix der aus den State-Machine-Namen und einen "underline"-Symbol besteht
        # fuer die Event-Signale
        self.e_pref = 'event_' + self.name + '_'

        # Im folgenden String wird der Init-Zustand definiert:
        self.init_state = 'init'

        # In der folgenden Liste sind alle States eingetragen
        self.states = [
            'init',
            'off',
            {'name': 'on', 'children': [
                'check',
                'error',
                {'name': 'standby', 'children': [
                    'standstill',
                    'running_out']},
                'automatic',
                'idle',
                'full',
                'manual']
             }
        ]

        # ---------------- Erstelle die "Transitions" von der Zustands-Machine --------------------------------------- #
        # trigger, source, dest, conditions=None, unless=None, before=None, after=None, prepare=None
        self.transitions = [

            # trigger (str): Event-Signal of the transitions
            # source (str): Source state of the transition.
            # dest (str): Destination state of the transition.
            # prepare (list): Callbacks executed before conditions checks.
            # unless (list): Callbacks evaluated to determine if the transition should be not executed.
            # conditions (list): Callbacks evaluated to determine if the transition should be executed.
            # before (list): Callbacks executed before the transition is executed
            #     but only if condition checks have been successful.
            # after (list): Callbacks executed after the transition is executed
            #     but only if condition checks have been successful.

            # -------------------- External Events ------------------------------------------------------------------- #
            #
            # Diese Signale werden verwendet um die Zustands-Aenderungen zu bewirken. Sie stellen eine Art Interaktion-
            # Schnittstelle dar. Folgende Signal stehen zu Verfuegung:
            #   TODO: Die Paefixe in einer Schleife im nachhienein hinzufuegen --> exl. loop
            #   -) wake_up      ↦ In den Betriebs-bereiten Zustand versetzen
            #   -) fall_asleep  ↦ In den Energie-spar-Modus versetzen
            #   -) manual       ↦ In den Betriebs-bereiten Zustand versetzen und Manuelle vorgaben weiterreichen
            #   -) stop         ↦ Von einen aktiven Betriebs-Zustand in den Betriebs-bereiten Zustand wechseln
            #   -) idle         ↦ In den aktiven Betriebs-Zustand "geringste Leistung" versetzen
            #   -) full         ↦ In den aktiven Betriebs-Zustand "maximale Leistung" versetzen
            #
            # (1)
            # Wenn das Event "wake_up" geschickt wurde, wird in den Zustand "on" gewechselt
            {'trigger': self.e_pref + 'wake_up', 'source': self.pref + 'off', 'dest': self.pref + 'on',
             'after': 'pic_update'},
            #
            # (2)
            # Wenn das Event "fall_asleep" geschickt wurde, wird in den Zustand "off" gewechselt
            {'trigger': self.e_pref + 'fall_asleep', 'source': self.pref + 'on', 'dest': self.pref + 'off',
             'after': 'pic_update'},
            #
            # (3)
            # Wenn das Event "manual" geschickt wurde, wird in den Zustand "manual" gewechselt
            {'trigger': self.e_pref + 'manual', 'source': self.pref + 'on↦standby', 'dest': self.pref + 'on↦manual',
             'after': 'pic_update'},
            #
            # (4)
            #  Wenn das Event "stop" geschickt wurde, wird in den Zustand "standby" gewechselt
            {'trigger': self.e_pref + 'stop', 'source': [self.pref + 'on↦idle', self.pref + 'on↦full',
                                                         self.pref + 'on↦manual', self.pref + 'on↦automatic'],
             'dest': self.pref + 'on↦standby', 'after': 'pic_update'},
            #
            # (5)
            # Wenn Event "idle" geschickt wurde, wird in den Zustand "idle" gewechselt
            {'trigger': self.e_pref + 'idle', 'source': self.pref + 'on↦standby', 'dest': self.pref + 'on↦idle',
             'after': 'pic_update'},
            #
            # (6)
            # Wenn das Event "full" geschickt wurde, wird in den Zustand "full" gewechselt
            {'trigger': self.e_pref + 'full', 'source': self.pref + 'on↦standby', 'dest': self.pref + 'on↦full',
             'after': 'pic_update'},
            #
            # (7)
            # Wenn das Event "full" geschickt wurde, wird in den Zustand "full" gewechselt
            {'trigger': self.e_pref + 'automatic', 'source': self.pref + 'on↦standby',
             'dest': self.pref + 'on↦automatic', 'after': 'pic_update'},
            #
            # -------------------- Transfer transitions -------------------------------------------------------------- #
            #
            # Diese Transitions bewirken interne Wechsel zwischen den States. Die Bedingungen in Ihnen werden in jeden
            # zyklischen Aufruf ueberprrueft und ein Wechsel bei erfuellung durchgefuehrt
            #
            # (101)
            # Ueberpruefe ob die Versorgung-Spannung in Ordnung ist
            {'trigger': 'loop', 'source': self.pref + 'on↦check', 'dest': self.pref + 'on↦standby',
             'conditions': ['if_voltage_supply_ok'], 'after': 'pic_update'},
            #
            # (102)
            # Wenn laenger als 3 Sekunden keine positive Freigabe kommt dann wird der Error-Zustand aktiv
            {'trigger': 'loop', 'source': self.pref + 'on↦check', 'dest': self.pref + 'on↦error',
             'prepare': 'timer_fan_cond_check_count', 'conditions': ['timer_fan_cond_check_timeout'],
             'after': 'pic_update'},
            #
            # (103)
            # Wenn ein schwere Fehler entdeckt wurde, wird in den Zustand Error gewechselt
            {'trigger': 'loop', 'source': [self.pref + 'on↦standby', self.pref + 'on↦automatic', self.pref + 'on↦full',
                                           self.pref + 'on↦idle', self.pref + 'on↦standby'],
             'dest': self.pref + 'on↦error', 'conditions': ['if_error_detected'], 'after': 'pic_update'},
            #
            # (104)
            # Wenn die Komponente sich ausgelaufen hat (langsam heruntergefahren) und somit still steht soll in den
            # State "standstill" gewechselt werden
            {'trigger': 'loop', 'source': self.pref + 'on↦standby↦running_out',
             'dest': self.pref + 'on↦standby↦standstill', 'unless': ['if_fan_cond_turns'], 'after': 'pic_update'},
            #
            # -------------------- Internal transitions -------------------------------------------------------------- #
            #
            # Dieser Kategorie werden Transitions zugewiesen welche fuer Initalisierungen oder zyklische Aufrufe
            # verwendet werden
            #
            # (201)
            # Initialisierung-Transition damit ein "on_enter_off_xxx" moeglich ist
            {'trigger': 'loop', 'source': self.pref + 'init', 'dest': self.pref + 'off', 'after': 'pic_update'},
            #
            # (202)
            # Das Event "init_on" wird von "enter_on" gesendet und ermoeglicht den sprung in den sub-state "on↦check"
            {'trigger': self.e_pref + 'init_on', 'source': self.pref + 'on', 'dest': self.pref + 'on↦check',
             'after': 'pic_update'},
            #
            # (203)
            # Wenn die Komponente beim betreten des "standby"-States bereits heruntergefahren ist,
            # soll direkt der State "standstill" aktiv werden
            {'trigger': self.e_pref + 'init_standby', 'source': self.pref + 'on↦standby',
             'dest': self.pref + 'on↦standby↦standstill', 'unless': ['if_fan_cond_turns'], 'after': 'pic_update'},

            # (204)
            # Wenn die Komponete eine groessere RPM als minRPM aufweist dann gehe davon aus das sie noch eingeschalten
            # ist, Ramp-down notwendig
            {'trigger': self.e_pref + 'init_standby', 'source': self.pref + 'on↦standby',
             'dest': self.pref + 'on↦standby↦running_out', 'conditions': ['if_fan_cond_turns'], 'after': 'pic_update'},

            # (205) TODO: kann entfernt werden????
            # Zyklischer call fuer den "standstill"-State
            {'trigger': 'loop', 'source': self.pref + 'on↦standby↦standstill', 'dest': None},

            # (206)
            # Zyklischer call fuer den "Running_Out"-State
            {'trigger': 'loop', 'source': self.pref + 'on↦standby↦running_out', 'dest': None,
             'conditions': ['if_fan_cond_turns'], 'after': 'fan_cond_ramp_down'},

            # (207)
            # Zyklischer call fuer den "automatic"-State
            {'trigger': 'loop', 'source': self.pref + 'on↦automatic', 'dest': None, 'after': 'pic_update'},

            # (208)
            # Zyklischer call fuer den "idle"-State
            {'trigger': 'loop', 'source': self.pref + 'on↦idle', 'dest': None},

            # (209)
            # Zyklischer call fuer den "full"-State
            {'trigger': 'loop', 'source': self.pref + 'on↦full', 'dest': None},

            # (210)
            # Zyklischer call fuer den "manual"-State
            {'trigger': 'loop', 'source': self.pref + 'on↦manual', 'dest': None,
             'after': 'during_' + self.name + '_manual'}

        ]

    def set_parent(self, parent):
        """
        Args:
            parent:
        """
        self.parent = parent

    def fan_cond_ramp_down(self):

        states: list = self.parent.state

        if 'comp↦off' in '\t'.join(states) or 'comp↦on↦standby↦standstill' in '\t'.join(states):
            self.data.ctl_data_out[self.name]['PWM'] = cont_func.rate_limiter(
                self.data.ctl_data_out[self.name]['PWM'],
                self.data.cont_par[self.name]['PWM_min'],
                self.data.cont_par[self.name]['PWM_dt_sec_max'],
                self.data.cont_par[self.name]['PWM_dt_sec_min'],
                time_step)

    def pi_control_update(self):
        pass

    def during_fan_cond_manual(self):

        self.data.ctl_data_out[self.name]['PWM'] = self.data.ctl_data_in['Manual_Input']['man_' + self.name + '_rpm']

    # ------------------------ On_enter_Events ----------------------------------------------------------------------- #

    def on_enter_on_full(self):
        self.data.ctl_data_out[self.name]['PWM'] = self.data.cont_par[self.name]['PWM_max']

    def on_enter_off(self):
        self.data.ctl_data_out[self.name]['PWM'] = 0.0

    def on_enter_on(self):
        self.parent.event_fan_cond_init_on()

    def on_enter_on_standby(self):
        # Send den Trigger 'init_standby' um in 'standstill' oder 'running_out' zu initalisieren
        self.parent.event_fan_cond_init_standby()

    def on_enter_on_automatic(self):
        pass

    def on_enter_on_idle(self):
        self.data.ctl_data_out[self.name]['PWM'] = self.data.cont_par[self.name]['PWM_min']

    # def on_enter_standstill(self):
    #     self.data.ctl_data_out[self.name]['PWM'] = 0.0

    # ------------------------ On_exit_Events ------------------------------------------------------------------------ #

    def on_exit_on_standby_running_out(self):
        self.data.ctl_data_out[self.name]['PWM'] = 0.0

    # ------------------------ Conditions ---------------------------------------------------------------------------- #

    def timer_fan_cond_check_timeout(self):

        if self.timer_fan_cond_check_value >= 3.0:
            self.timer_fan_cond_check_value = 0.0
            return True
        else:
            return False

    def timer_fan_cond_check_count(self):
        self.timer_fan_cond_check_value = self.timer_fan_cond_check_value + time_step

    def is_ok_fb_fan(self):
        return True

    def is_ok_com_to_fan(self):
        return True

    def if_fan_cond_turns(self):

        if self.data.ctl_data_out[self.name]['PWM'] <= self.data.cont_par[self.name]['PWM_min']:
            return False
        else:
            return True


class HCBloksStateMachine:
    __machine: HierarchicalGraphMachine

    def __init__(self, logging, dict_transfer_in, dict_transfer_par, configfile_write=None):
        """
        Args:
            logging:
            dict_transfer_in: configfile_write: Hierbei wird ein Funktionsaufruf
                mitgegeben um "dic_transfer_par" ins "config"-File zu schreiben
            dict_transfer_par:
            configfile_write:
        """
        self.logging = logging
        self.data = HCBloksData4SM()
        self.pic_file_path = '/tmp/ramdisk/'
        self.file_format = '.svg'
        self.dict_transfer = dict_transfer_in
        self.dict_transfer_par = dict_transfer_par
        self.configfile_write = configfile_write

        self.sm_modus = StateMachineModus(self.data, dict_transfer_par=self.dict_transfer_par)
        self.sm_comp = StateMachineComp(self.data)
        self.sm_pump = StateMachinePumpGlycol(self.data)
        self.sm_fan_cond = StateMachineFanCond(data=self.data)
        self.sm_fan_chill = StateMachineFanChill(data=self.data)

        self.list_sm_component = [self.sm_modus,
                                  self.sm_comp,
                                  self.sm_pump,
                                  self.sm_fan_cond,
                                  self.sm_fan_chill]

        logging.basicConfig(format='%(asctime)s: %(threadName)s: %(message)s', level=logging.INFO)


        # Setzt das Logging-Level von der State-Machine auf ERROR
        # logging.getLogger('transitions').setLevel(logging.ERROR)
        logging.getLogger('transitions').setLevel(logging.INFO)

        self.sm_hc_bloks = self.init_state_machine_core()

        # Damit die Events aus den Substates gerufen werden koennen benoetigen sie die Referenz von der MasterMachine
        self.sm_modus.set_parent(self)
        self.sm_comp.set_parent(self)
        self.sm_pump.set_parent(self)
        self.sm_fan_cond.set_parent(self)
        self.sm_fan_chill.set_parent(self)

        self.to_StateMachine()

        # Derivation_init
        self.temp_air_chil_in_rate = cont_func.Derivation(length=300, init_last_val=0.0)
        self.temp_air_chil_out_rate = cont_func.Derivation(length=300, init_last_val=0.0)

        self.temp_gly_chil_in_rate = cont_func.Derivation(length=300, init_last_val=0.0)
        self.temp_gly_chil_out_rate = cont_func.Derivation(length=300, init_last_val=0.0)

        self.temp_gly_phex_in_rate = cont_func.Derivation(length=300, init_last_val=0.0)
        self.temp_gly_phex_out_rate = cont_func.Derivation(length=300, init_last_val=0.0)

        self.temp_ref_phex_in_rate = cont_func.Derivation(length=300, init_last_val=0.0)
        self.temp_ref_phex_out_rate = cont_func.Derivation(length=300, init_last_val=0.0)

        self.temp_ref_cond_in_rate = cont_func.Derivation(length=300, init_last_val=0.0)
        self.temp_ref_cond_out_rate = cont_func.Derivation(length=300, init_last_val=0.0)

        self.temp_air_ambient_rate = cont_func.Derivation(length=300, init_last_val=0.0)

        self.pic_process = Process(target=self.process_update_graph,
                                   name=str(str('Pic_') + str(self.__class__.__name__)))

    def init_state_machine_core(self, basename:str = 'StateMachine'):
        """Lade die "States" und "Transitions" in list_states und
        list_transitions

        Args:
            basename (str):
        """
        self.name = basename

        self.pic_process = Process

        # Basis-Initalisierung um auf der obersten Ebene die parallel-States zu implementieren
        list_states = ['initialization', {'name': self.name, 'parallel': []}]

        # Erstelle eine Liste in welche die Transitions verkettet werden
        list_transitions = []

        # Durch-wandere alle Sub-State-Machine von "self.list_sm_component"
        for sub_state_machine in self.list_sm_component:
            # Lade die notwendigen Informationen (Namen, Kinder, Init-State) der
            # Sub-state-Machine in die Liste "list_states" als Dictionary
            list_states[1]['parallel'].append(
                {'name': sub_state_machine.name,
                 'children': sub_state_machine.states,
                 'initial': sub_state_machine.init_state})

            # Lade die Transitions der Sub-state-Machine als Liste und
            # verkette sie mit der Liste "list_transitions"
            list_transitions += sub_state_machine.transitions

        # Sorge dafuer das in den Transitions aller Sub-state-Machine der Name des Eltern-States angekettet wird
        self.concatenate_name_on_parallel_transitions(list_transitions, self.name + '↦')

        # ---------------- Erstelle die Zustands-Machine ------------------------------------------------------------- #

        self.__machine = Machine(self,
                                 states=list_states,
                                 transitions=list_transitions,
                                 initial='initialization',
                                 queued=True,
                                 title="PBX State-Machine",
                                 ignore_invalid_triggers=True,
                                 show_conditions=True,
                                 show_state_attributes=True
                                 # use_pygraphviz=False
                                 )

        # Durch-wandere alle Sub-State-Machine von "self.list_sm_component"
        for sub_state_machine in self.list_sm_component:

            method_list = []

            # Diese Funktion ladet die "on_enter"/"on_exit"-Methoden von der Sub-State-Machine "sub_state_machine"
            # in die aktuelle Klasse und erstellt auch die Callbacks fuer die State-Machine "self.__machine"
            self.generate_on_xxx_event_func(sub_state_machine, self.__machine, 'on_enter')
            self.generate_on_xxx_event_func(sub_state_machine, self.__machine, 'on_exit')

            # Hier werden alle anderen Methoden geladen welche nicht "on_enter"/"on_exit"-Methoden sind
            method_list += [func for func in dir(sub_state_machine)
                            if callable(getattr(sub_state_machine, func))
                            and not func.startswith("__") and not func.startswith("on_e")]

            for method in method_list:
                setattr(self, method, getattr(sub_state_machine, method))

        # print(self.get_list_of_registerd_methods())

        return self.__machine

    def get_list_of_registerd_methods(self):
        return [func for func in dir(self) if callable(getattr(self, func))]

    def pic_update(self):

        # try:
        # Wenn der Process nicht aktiv ist, dann ...
        if not self.pic_process.is_alive():
            # Actualise das Bild vom comp-Chart
            self.pic_process = Process(target=self.process_update_graph,
                                       name=str(str('Pic_') + str(self.__class__.__name__)))
            self.pic_process.start()

            # else:
            #     self.pic_process.terminate()
            #
            #     self.pic_process = Process(target=self.process_update_graph,
            #                                name=str(str('Pic_') + str(self.__class__.__name__)))
            #     self.pic_process.start()

        # except TypeError:
        #
        #     # Actualise das Bild vom comp-Chart
        #     # self.pic_process = Process(target=self.process_update_graph,
        #     #                            name=str(str('Pic_') + str(self.__class__.__name__)))
        #
        #     pass
        #     # self.pic_process.start()

    def process_update_graph(self):

        # Fange alle Warnings welche in der Einrueckung auftretten
        with warnings.catch_warnings():
            # Die aufgefangenen Warnings sollen ignoriert werden
            warnings.simplefilter("ignore")
            # print(time.time(), 'get_graph ++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++')
            # Erneuere die Bild-Datei
            self.get_graph().draw(self.pic_file_path + str(self.__class__.__name__) + self.file_format, prog='dot')

            # Beende den Thread
            # print(time.time(), 'fertig')
            return

    @staticmethod
    def concatenate_name_on_parallel_transitions(list_transitions: list, append_value: str):

        # Durchwandere jedes Element in der Liste
        """
        Args:
            list_transitions (list):
            append_value (str):
        """
        for element in list_transitions:

            # Da jedes Element ein Dictionary ist, wird das Dictionary an den "key's" durchlaufen
            for key, value in element.items():

                # Wenn der "key" den String "source" oder "dest" aufweist, dann ...
                if isinstance(value, str) and (key is 'source' or key is 'dest'):

                    # Haenge den String aus "append_value" am "value" vorne an
                    element[key] = append_value + value

                # Wenn der "value" wiederum kein String, sondern eine Liste ist, dann ...
                elif isinstance(value, list) and (key is 'source' or key is 'dest'):

                    # Erstelle eine temporaere Liste
                    list_tmp = []

                    # Druchwandere die Liste von "value"
                    for list_element in value:
                        # Holle aus der Original-Liste ein Element und haenge den String "append_value" vorne an
                        # und schiebe das Ergebnis in die temporaere Liste
                        list_tmp.append(append_value + list_element)

                    # Weise die temporaere Liste dem "value" von "key" zu
                    element[key] = list_tmp

    def generate_on_xxx_event_func(self, sub_state_machine, machine, direction):
        """Diese Funktion hat die Aufgabe die "on_enter" und "on_exit"-Methoden
        aus den Sub-State-Machine (parameter:"sub_state_machine") der
        State-Machine verfuegbar zu machen.

        Args:
            sub_state_machine:
            machine:
            direction:
        """
        # Lade alle Methoden-Namen als Liste aus dem Object "sub_state_machine" welche den String "direction" beinhalten
        list_on_xxx_methods = [func_name for func_name in dir(sub_state_machine) if (direction in func_name) and
                               callable(getattr(sub_state_machine, func_name))]

        dev_list_on_xxx_methods = list_on_xxx_methods.copy()

        # Erstelle eine Liste mit den State-Names (Bsp-Element: "StateMachine↦modus↦standby↦running_out")
        list_state_names = machine.get_nested_state_names()

        # Erstelle ein leeres Dictionary um den Original-"state-name" am key und
        # den bereinigten "state_name" am value zu sichern
        dic_state_names = {}

        # Durch-wandere die Liste der "state-name"'s um das Dictionary mit bereinigten/gueltigen Eintraegen zu befuellen
        for state_name in list_state_names:

            # Das "try" wird verwendet falls der "state_name" nicht mehr als zwei Elemente beinhaltet
            try:

                # Wenn sich im zweiten Element der Name der "sub_state_machine" befindet, dann ...
                if state_name.split('↦')[1] == sub_state_machine.name:

                    # Ermittle den Index an dem der String "sub_state_machine.name" im String "state_name" endet
                    stop_index = state_name.find(sub_state_machine.name) + len(sub_state_machine.name)

                    # Wenn der ermittelte Index groesser ist als die Names-laenge von "sub_state_machine.name", dann ...
                    if stop_index > len(sub_state_machine.name):

                        # Weise "state_name_clean" den String von "state_name" ohne
                        # Anfang bis Ende von "sub_state_machine.name" zu
                        state_name_clean = state_name[stop_index::]

                        # Wenn am Anfang von "state_name_clean" ein "Sub"-Operator sich befindet, dann ...
                        if state_name_clean.startswith('↦'):
                            # Entferne den "Sub"-Operator
                            state_name_clean = state_name_clean.replace('↦', '', 1)

                        # Wenn nach den vorherigen Filter immer noch etwas vom String vorhanden ist, dann ...
                        # Das ".strip()" ist zur Sicherheit, falls boese Leerzeichen auftauchen
                        if state_name_clean.strip():
                            # Zerteile den String zu einer Liste an den "Sub"-Operator und weise ihn dem value
                            # im Dictionary "dic_state_names" am key: "state_name" zu
                            dic_state_names[state_name] = state_name_clean.split(sep='↦')

            except IndexError:
                pass

        # Durch-wandere die Listen mit den von Benutzer definierten "on_enter"/"on_exit"-Methoden
        for method_name in list_on_xxx_methods:

            # Ueberspringe Methoden-namen welche nicht mit den String von "direction" anfangen ("on_enter"/"on_exit")
            if method_name.startswith(direction):

                # Entferne aus den Methoden-namen den Teil-String "on_enter" oder
                # "on_exit" welcher in "direction" hinterlegt ist
                method_name_clean = method_name.replace(direction + '_', '')

                # Durch-wandere das Dictionary mit den State-names
                for key, value in dic_state_names.items():

                    # Bilde einen String aus den Teil-String's des aktuellen value-Durchlauf's
                    test_string = '_'.join(value)

                    # Wenn dieser "test_string" gleich dem "method-name" der aeusseren Schleife ist, dann ...
                    if test_string == method_name_clean:

                        # Generiere den String unter welchen die Methode in der Klasse hinzugefuegt wird
                        new_method_name = direction + '_' + sub_state_machine.name + '_' + '_'.join(value)

                        # Fuege die Methode "method_name" von den Sub-State-Machines der State-Machine-Klasse
                        # unter den Namen "new_method_name" hinzu
                        self.__setattr__(new_method_name, getattr(sub_state_machine, method_name))

                        dev_list_on_xxx_methods.remove(method_name)

                        # Wenn es sich um ein "on_enter"-Event handelt, dann ...
                        if direction is 'on_enter':

                            # Erstelle den "on_enter_"-Callback der State-Machine
                            machine.on_enter(key, new_method_name)

                        # Wenn es sich um ein "on_exit"-Event handelt, dann ...
                        elif direction is 'on_exit':

                            # Erstelle den "on_exit_"-Callback der State-Machine
                            machine.on_exit(key, new_method_name)

                        # Weitere Durchlaeufe der inneren Schleife sind nicht mehr notwendig
                        continue

        if dev_list_on_xxx_methods:
            print('Nicht zugewiesene Events von %s :' % sub_state_machine.name, dev_list_on_xxx_methods)

    def step(self):
        """Diese Funktion wird aufgerufen um mit der State-Machine einen
        Zeitschritt zu vollziehen
        """
        # Aktualisiere die State-Machine
        self.loop()

    def gen_dict_select_1w_sesnors(self, dict_source, keyword):
        """Diese Funktion kopiert ein Dictionary in ein neues. Aber nur mit den
        Elementen welche das Schluesselwort "keyword" beinhalten. Fuer den
        "value" wird im Original-Dictionary von einen sub-key namens "value"
        ausgegangen.

        Args:
            dict_source (dict): Dictionary welches durchsucht wird
            keyword (str): Schluesselwort welches die key's enthalten muessen um
                uebernohmen zu werden

        Returns:
            dict: Ein Dictionary welches nur die key/value-paare beinhaltet die
            dass Schluesselwort enthalten
        """
        return {element: dict_source[element]['value'] for element in dict_source if keyword in element}

    def handle_1w_sensors(self, dic_data_in: dict):
        """Diese Funktion sorgt dafuer dass in dem Dictionary "self.data.cont_par" die Eintraege:
            - list_sensor_id
            - 1w_glycol
            - 1w_ref_air

        dem aktuellsten Stand entsprechen

        Args:
            dic_data_in (dict): Das zu bearbeitende Dictionary
        """

        # Ziehe die Sensor-Id's aus den Eingangsdaten und sichere diese als Liste
        self.data.cont_par['list_sensor_id'] = list(dic_data_in['data_in_1w'].keys())

        try:
            # Update das Dictionary mit den 1w-Sensor-Id's als key und Temp-Werten als von den Glycol-Sensoren als value
            self.data.cont_par['1w_glycol'].update(
                self.gen_dict_select_1w_sesnors(dict_source=dic_data_in['data_in_1w'], keyword='10-'))

            # Update das Dictionary mit den 1w-Sensor-Id's als key und den Temp-Werten von den Luft/Kaeltemittel-
            # Sensoren als Value
            self.data.cont_par['1w_ref_air'].update(
                self.gen_dict_select_1w_sesnors(dict_source=dic_data_in['data_in_1w'], keyword='28-'))

        except KeyError:
            self.data.cont_par.update(
                {'1w_glycol': self.gen_dict_select_1w_sesnors(dict_source=dic_data_in['data_in_1w'], keyword='10-'),
                 '1w_ref_air': self.gen_dict_select_1w_sesnors(dict_source=dic_data_in['data_in_1w'], keyword='28-')})

    def process_input_data(self, dic_data_in: dict, dic_data_par: dict):

        """
        Args:
            dic_data_in (dict):
            dic_data_par (dict):
        """
        self.handle_1w_sensors(dic_data_in=dic_data_in)

        # Uebergebe den Kommunikationstatus von der Can_0 Schnittstelle
        self.data.ctl_data_in['communication_status']['can_0'] = True

        # Uebergebe den Kommunikationstatus von der Can_1 Schnittstelle
        self.data.ctl_data_in['communication_status']['can_1'] = True

        self.data.ctl_data_in['communication_status']['SPI'] = True
        self.data.ctl_data_in['communication_status']['1-W'] = True

        # Uebergebe den Kommunikationstatus von der Sanden-Kompressor-Interaktion
        self.data.ctl_data_in['communication_status']['Sanden_com_ok'] = True

        self.data.ctl_data_in['Settings']['Mode'] = dic_data_par['data_par_gui']['Mode']
        self.data.ctl_data_in['Settings']['Temp_Set'] = dic_data_par['data_par_gui']['Temp_set']
        self.data.ctl_data_in['Settings']['Control_IO_cool'] = dic_data_par['data_par_gui']['Control_IO_cool']
        self.data.ctl_data_in['Settings']['Control_IO_heat'] = dic_data_par['data_par_gui']['Control_IO_heat']

        # Erstelle ein Dictionary mit den 1w-ID's am "key" und den Prozess-Namen am "value" aus dem Parameter-Dictionary
        dic_1w_translator = {list(process_data_name.keys())[0]: w1_id
                             for w1_id, process_data_name in dic_data_par['data_in_1w'].items()}

        # Erstelle eine Liste mit den
        list_sensor_positions = list(dic_1w_translator.keys())

        # Setzt den Zaehler der erkannten Sensoren zurueck
        self.data.cont_par['sum_detected_sensors'] = 0.0

        # Durch-wandere die Liste mit den Sensor-Positionen
        for sensor_position in list_sensor_positions:

            # Versuche ...
            try:

                # den Temperaturwert in das Statemachine-Data-Obj vom
                # Eingangsdaten-Dictionary zu laden
                self.data.ctl_data_in['Sensor_Data'][sensor_position] = \
                    dic_data_in['data_in_process_values'][sensor_position]

                # Inkremmentiere den ZÃƒÂ¤hler der erkannten Sensoren
                self.data.cont_par['sum_detected_sensors'] += 1.0

            except KeyError:
                # print('gibts nicht', sensor_position)
                # Im Eingangs-Daten-Dictionary gibt es keinen Eintrag fuer
                # die Sensor-Position "sensor_position"
                pass

        # Kaeltemittel-Verdampfungstemperatur
        self.data.ctl_data_in['Sensor_Data']['Temp_Ref_Evap'] = 0

        # Kaeltemittel-Verfluessingungstemperatur
        self.data.ctl_data_in['Sensor_Data']['Temp_Ref_Cond'] = 40.0

        self.data.ctl_data_in['Sensor_Data']['press_ref_low'] = dic_data_in['data_in_process_values']['press_ref_low']
        self.data.ctl_data_in['Sensor_Data']['press_ref_high'] = dic_data_in['data_in_process_values']['press_ref_high']

        # Feedback Verdichter Drehzahl
        self.data.ctl_data_in['Sensor_Data']['FB_Comp_RPM'] = dic_data_in['data_in_process_values']['comp_speed']

        # Feedback Verdichter Spannung
        self.data.ctl_data_in['Sensor_Data']['FB_Comp_Volt'] = dic_data_in['data_in_process_values']['comp_volt']

        # Feedback Verdichter Strom
        self.data.ctl_data_in['Sensor_Data']['FB_Comp_Current'] = dic_data_in['data_in_process_values']['comp_current']

        self.data.ctl_data_in['Sensor_Data']['fan_chill_tacho'] = 0.0
        self.data.ctl_data_in['Sensor_Data']['fan_cond_tacho'] = 0.0

        # Luft-Temperatur am Chiller-Ausgang-Rate-Berechnung
        self.data.ctl_data_in['Sensor_Data']['temp_air_chil_out_rate'] = \
            self.temp_air_chil_out_rate.update_val(self.data.ctl_data_in['Sensor_Data']['Temp_Air_Chil_out'])

        # Luft-Temperatur am Chiller-Eingang-Rate-Berechnung
        self.data.ctl_data_in['Sensor_Data']['temp_air_chil_in_rate'] = \
            self.temp_air_chil_in_rate.update_val(self.data.ctl_data_in['Sensor_Data']['Temp_Air_Chil_in'])

        # Glycol-Temperatur am Chiller-Eingang-Rate-Berechnung
        self.data.ctl_data_in['Sensor_Data']['temp_gly_chil_in_rate'] = \
            self.temp_gly_chil_in_rate.update_val(self.data.ctl_data_in['Sensor_Data']['Temp_Gly_Chil_in'])

        # Glycol-Temperatur am Chiller-Ausgang-Rate-Berechnung
        self.data.ctl_data_in['Sensor_Data']['temp_gly_chil_out_rate'] = \
            self.temp_gly_chil_out_rate.update_val(self.data.ctl_data_in['Sensor_Data']['Temp_Gly_Chil_out'])

        # Glycol-Temperatur am Plattenwaermetauscher-Eingang-Rate-Berechnung
        self.data.ctl_data_in['Sensor_Data']['temp_gly_phex_in_rate'] = \
            self.temp_gly_phex_in_rate.update_val(self.data.ctl_data_in['Sensor_Data']['Temp_Gly_Phex_in'])

        # Glycol-Temperatur am Plattenwaermetauscher-Ausgang-Rate-Berechnung
        self.data.ctl_data_in['Sensor_Data']['temp_gly_phex_out_rate'] = \
            self.temp_gly_phex_out_rate.update_val(self.data.ctl_data_in['Sensor_Data']['Temp_Gly_Phex_out'])

        # Kaeltemittel-Temperatur am Plattenwaermetauscher-Eingang-Rate-Berechnung
        self.data.ctl_data_in['Sensor_Data']['temp_ref_phex_in_rate'] = \
            self.temp_ref_phex_in_rate.update_val(self.data.ctl_data_in['Sensor_Data']['Temp_Ref_Phex_in'])

        # Kaeltemittel-Temperatur am Plattenwaermetauscher-Ausgang-Rate-Berechnung
        self.data.ctl_data_in['Sensor_Data']['temp_ref_phex_out_rate'] = \
            self.temp_ref_phex_out_rate.update_val(self.data.ctl_data_in['Sensor_Data']['Temp_Ref_Phex_out'])

        # Kaeltemittel-Temperatur am Verfluessiger-Eingang-Rate-Berechnung
        self.data.ctl_data_in['Sensor_Data']['temp_ref_cond_in_rate'] = \
            self.temp_ref_cond_in_rate.update_val(self.data.ctl_data_in['Sensor_Data']['Temp_Ref_Cond_in'])

        # Kaeltemittel-Temperatur am Verfluessiger-Ausgang-Rate-Berechnung
        self.data.ctl_data_in['Sensor_Data']['temp_ref_cond_out_rate'] = \
            self.temp_ref_cond_out_rate.update_val(self.data.ctl_data_in['Sensor_Data']['Temp_Ref_Cond_out'])

        # Luft-Eintritts-Temperatur am Verfluessiger-Rate-Berechnung
        self.data.ctl_data_in['Sensor_Data']['Temp_Air_Cond_in_rate'] = \
            self.temp_air_ambient_rate.update_val(self.data.ctl_data_in['Sensor_Data']['Temp_Air_Cond_in'])

        try:
            # Versuche aus dem Parameter-Dictionary die Werte entgegenzunehmen
            self.data.ctl_data_in['Manual_Input']['man_main_relay'] = dic_data_par['data_par_gui']['man_main_relay']
            self.data.ctl_data_in['Manual_Input']['man_heater_relay'] = dic_data_par['data_par_gui']['man_heater_relay']
            self.data.ctl_data_in['Manual_Input']['man_fan_cond_rpm'] = dic_data_par['data_par_gui']['man_fan_cond_rpm']
            self.data.ctl_data_in['Manual_Input']['man_fan_chill_rpm'] = dic_data_par['data_par_gui'][
                'man_fan_chill_rpm']
            self.data.ctl_data_in['Manual_Input']['man_pump_pwm_set'] = dic_data_par['data_par_gui']['man_pump_pwm_set']
            self.data.ctl_data_in['Manual_Input']['man_comp_relay'] = dic_data_par['data_par_gui']['man_comp_relay']
            self.data.ctl_data_in['Manual_Input']['man_comp_start_request'] = dic_data_par['data_par_gui'][
                'man_comp_start_request']
            self.data.ctl_data_in['Manual_Input']['man_comp_rpm_set'] = dic_data_par['data_par_gui']['man_comp_rpm_set']
            self.data.ctl_data_in['Manual_Input']['man_comp_pwr_set'] = dic_data_par['data_par_gui']['man_comp_pwr_set']

        except KeyError:
            # Sollte der Versuch gescheitert sein dann generiere die entsprechenden Eintraege
            dic_data_par['data_par_gui']['man_main_relay'] = self.data.ctl_data_in['Manual_Input']['man_main_relay']
            dic_data_par['data_par_gui']['man_heater_relay'] = self.data.ctl_data_in['Manual_Input']['man_heater_relay']
            dic_data_par['data_par_gui']['man_fan_cond_rpm'] = self.data.ctl_data_in['Manual_Input']['man_fan_cond_rpm']
            dic_data_par['data_par_gui']['man_fan_chill_rpm'] = self.data.ctl_data_in['Manual_Input'][
                'man_fan_chill_rpm']
            dic_data_par['data_par_gui']['man_pump_pwm_set'] = self.data.ctl_data_in['Manual_Input']['man_pump_pwm_set']
            dic_data_par['data_par_gui']['man_comp_relay'] = self.data.ctl_data_in['Manual_Input']['man_comp_relay']
            dic_data_par['data_par_gui']['man_comp_start_request'] = self.data.ctl_data_in['Manual_Input'][
                'man_comp_start_request']
            dic_data_par['data_par_gui']['man_comp_rpm_set'] = self.data.ctl_data_in['Manual_Input']['man_comp_rpm_set']
            dic_data_par['data_par_gui']['man_comp_pwr_set'] = self.data.ctl_data_in['Manual_Input']['man_comp_pwr_set']

            return dic_data_par

    def process_output_data(self):
        dic_actor_out = {}

        dic_actor_out['fan_cond_rpm'] = self.data.ctl_data_out['fan_cond']['PWM']
        dic_actor_out['fan_chill_rpm'] = self.data.ctl_data_out['fan_chill']['PWM']
        dic_actor_out['pump_pwm_set'] = self.data.ctl_data_out['pump']['PWM']
        dic_actor_out['pwm_1_freq'] = 100
        dic_actor_out['comp_relay'] = self.data.ctl_data_out['comp']['Relay']
        dic_actor_out['main_relay'] = self.data.ctl_data_out['Main_Relay']['Status']
        dic_actor_out['heater_relay'] = self.data.ctl_data_out['Heat']['Status']
        dic_actor_out['comp_start_request'] = self.data.ctl_data_out['comp']['Status']
        dic_actor_out['comp_rpm_set'] = self.data.ctl_data_out['comp']['RPM']
        dic_actor_out['comp_pwr_set'] = self.data.ctl_data_out['comp']['PWR']

        dic_actor_out['temp_air_chil_out_rate'] = self.data.ctl_data_in['Sensor_Data']['temp_air_chil_out_rate']
        dic_actor_out['temp_air_chil_in_rate'] = self.data.ctl_data_in['Sensor_Data']['temp_air_chil_in_rate']
        dic_actor_out['temp_gly_chil_in_rate'] = self.data.ctl_data_in['Sensor_Data']['temp_gly_chil_in_rate']
        dic_actor_out['temp_gly_chil_out_rate'] = self.data.ctl_data_in['Sensor_Data']['temp_gly_chil_out_rate']
        dic_actor_out['temp_gly_phex_in_rate'] = self.data.ctl_data_in['Sensor_Data']['temp_gly_phex_in_rate']
        dic_actor_out['temp_gly_phex_out_rate'] = self.data.ctl_data_in['Sensor_Data']['temp_gly_phex_out_rate']
        dic_actor_out['temp_ref_phex_in_rate'] = self.data.ctl_data_in['Sensor_Data']['temp_ref_phex_in_rate']
        dic_actor_out['temp_ref_phex_out_rate'] = self.data.ctl_data_in['Sensor_Data']['temp_ref_phex_out_rate']
        dic_actor_out['temp_ref_cond_in_rate'] = self.data.ctl_data_in['Sensor_Data']['temp_ref_cond_in_rate']
        dic_actor_out['temp_ref_cond_out_rate'] = self.data.ctl_data_in['Sensor_Data']['temp_ref_cond_out_rate']
        dic_actor_out['Temp_Air_Cond_in_rate'] = self.data.ctl_data_in['Sensor_Data']['Temp_Air_Cond_in_rate']

        # dic_actor_out['p_anteil'] = self.sm_comp.p_anteil
        # dic_actor_out['i_anteil'] = self.sm_comp.i_anteil
        # dic_actor_out['d_anteil'] = self.sm_comp.d_anteil
        # dic_actor_out['ctrl_error'] = self.sm_comp.ctrl_error
        # dic_actor_out['ctrl_error_rate'] = self.sm_comp.ctrl_error_rate
        #
        dic_actor_out['state_modes'] = self.state
        # dic_actor_out['state_comp'] = self.sm_comp.state

        return dic_actor_out
