#!/usr/bin/python3.7
# -*- coding: utf-8 -*-


class CompressorHandler:

    def __init__(self, source_in: dict, source_out: dict, channel: str = 'can0', id_list=None):

        """
        Args:
            source_in (dict):
            source_out (dict):
            channel (str):
            id_list:
        """
        if id_list is None:
            id_list = [0x14FF0144, 0x14FF0119]
        self.id_list = id_list

        self.source_in = source_in
        self.source_out = source_out
        self.channel = channel

    def update_process_data(self):

        local_transfer_dict = self.source_in['data_in_can0'].copy()
        # i = 0
        for name, sub_dict in local_transfer_dict.items():
            datagram = sub_dict['value']
            if name in str(self.id_list) and type(datagram) is bytearray:

                tmp = self.covert_msg(name, sub_dict)

                if tmp is not None:

                    self.source_in.update({'data_in_process_values': tmp})

    def covert_msg(self, name: str, sub_dict: dict) -> dict:

        """
        Args:
            name (str):
            sub_dict (dict):
        """
        datagram = sub_dict['value']

        if name in str(self.id_list[0]) and type(datagram) is bytearray:

            local_dic = {
                # Byte 1 --> EPAC_Cmpr_Stat
                'comp_on': bool(datagram[1] & 0x10),
                'comp_off': not bool(datagram[1] & 0x10),
                'comp_power_limit': bool(datagram[1] & 0x20),

                # Byte 2 --> EPAC_CmprSpd_Cval
                'comp_speed': datagram[2] * 50,

                # Byte 3 --> EPAC_CmprInVolt_Cval
                'comp_volt': datagram[3] / 2,

                # Byte 4 --> EPAC_CmprInCurr_Cval
                'comp_current': datagram[4] / 2,

                # Byte 5 --> EPAC_InvtrTemp_Cval
                'comp_inverter_temperature': datagram[5] - 50.0,

                # Byte 6 --> EPAC_CmprPhaseCurr_Cval
                'comp_phase_current': datagram[6] / 2,

                # Byte 7 --> EPAC_CmprSupVolt_24V_Stat
                'comp_24_over_volt': bool(datagram[7] & 0x1),
                'comp_24_under_volt': bool(datagram[7] & 0x2),

                # Byte 7 --> EPAC_OverHt_Stat
                'comp_overheat': bool(datagram[7] & 0x4),

                # Byte 7 --> EPAC_OverLd_Stat
                'comp_overload': bool(datagram[7] & 0x10),
                'comp_stall': bool(datagram[7] & 0x20),

                # Byte 7 --> EPAC_SupVolt_12V_Stat
                'comp_12_over_volt': bool(datagram[7] & 0x40),
                'comp_12_under_volt': bool(datagram[7] & 0x80)}

            # print(local_dic)
            return local_dic
