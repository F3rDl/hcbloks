﻿import smtplib
import ssl
import time
from typing import List
from email.mime.multipart import MIMEMultipart
from email.mime.text import MIMEText
from email.mime.base import MIMEBase
from email import encoders
import os.path
import pyzipper


class MailSend:

    def __init__(self, device_name: str = 'pbx_sn_01', hide_bcc: bool = True):

        # Laden den eindeutigen Anlagen-Namen
        self.device_name = device_name

        self.hide_bcc = hide_bcc

        # Definiere den Port 465
        self.port = 465

        # Definiere dem Gmail-Smtp-Server als string
        self.smtp_server = 'smtp.gmail.com'

        # Definiere die Konto-Einstellung vom Sender
        self.__sender_email__ = 'pbxgigasend@gmail.com'
        self.__password__ = 'hcbloks20'

        # Erstelle die Empfaenger-Adresse als string
        self.receiver_email = ['pbxtelemetrie+' + self.device_name + '@productbloks.com']
        self.bcc_receiver_email = ['pbxgigasend@gmail.com']

        # Definiere einen leeren String fuer den Email-Inhalt
        self.mail_content: str = ''

        # Initialisiere das Email-Typ-Format
        self.message = MIMEMultipart()

    def create_new_message(self):

        # Erstelle ein multipart/mixed message
        message = MIMEMultipart()

        # Erstelle den Email-Header
        message['From'] = self.__sender_email__
        message['To'] = ', '.join(self.receiver_email)

        # Wenn die "Bcc" nicht versteckt werden soll, dann ...
        if not self.hide_bcc:
            message['Bcc'] = ', '.join(self.bcc_receiver_email)

        message['Subject'] = 'Sendomoriono'

        return message

    @staticmethod
    def __load_attached_files_in_mail(list_files_to_send: [str, str], message: MIMEMultipart()):

        # Durch-wander die Liste mit den zu sendenden File's
        for attach_file_name in list_files_to_send:

            # Wenn das File auch wirklich existiert, dann ...
            if os.path.exists(attach_file_name):

                # Oeffne das File im binaeren-Lesemodus
                with open(attach_file_name, 'rb') as attach_file:
                    # Verwende das generischen 'application/octet-stream' Format um das File einzubetten
                    payload = MIMEBase('application', 'octet-stream')

                    # Setzte den File-Inhalt
                    payload.set_payload(attach_file.read())

                    # Wandle den Anhang (File) in's Base64-Format
                    encoders.encode_base64(payload)

                    # Erstelle den Header des Anhang's
                    payload.add_header('Content-Disposition', 'attachment', filename=attach_file_name)

                    # Fuege den Anhang (im ASCI-Format) am Email an
                    message.attach(payload)

    @staticmethod
    def encrypt_files(list_of_files: List[str], file_name: str):
        """
        Diese Methode erstellt eine komprimierte Datei in welche die File's aus der Liste "list_of_files"
        gepackt werden. Die komprimierte Datei erhaltet den Namen "filename".

        :param file_name: File-Name der zip-Datei
        :type file_name: str
        :param list_of_files: Eine Liste mit den File's welche in die zip-Datei hinzugefuegt werden sollen
        :type list_of_files: List[str]
        """
        with pyzipper.AESZipFile(file=file_name,
                                 mode='w',
                                 compression=pyzipper.ZIP_LZMA,
                                 encryption=pyzipper.WZ_AES) as zf:

            # Defniere das Password fuer die verschluesselung
            zf.setpassword(b'xsFnw4xUrWpSFLUV')

            # Durch-Wandere die Liste mit den File-namen
            for file in list_of_files:

                # Verschluessle und komprimieren das File
                zf.write(file)

    def send_mail(self, list_files_to_send: [str, str]):

        # Erstelle eine neue Email
        self.message = self.create_new_message()

        # Erstelle den Email-Text-Inhalt
        self.mail_content = 'Send on ' + time.strftime("%Y-%m-%d %H:%M:%S") + '\nDevice: ' + self.device_name

        # Weise den Email-Text-Inhalt der Email als 'plain'-Text zu
        self.message.attach(MIMEText(self.mail_content, 'plain'))

        # Fuege die Files
        self.encrypt_files(list_of_files=list_files_to_send, file_name=self.device_name + '.zip')

        # Lade die Anhaenge in die Email
        self.__load_attached_files_in_mail(list_files_to_send=[self.device_name + '.zip'], message=self.message)

        context = ssl.create_default_context()

        with smtplib.SMTP_SSL(self.smtp_server, self.port, context=context) as server:

            ret_val = server.login(self.__sender_email__, self.__password__)
            server.sendmail(self.__sender_email__, self.receiver_email + self.bcc_receiver_email, self.message.as_string())

            # 235 == 'Authentication successful'
            # 503 == 'Error: already authenticated'
            # print(ret_val)
            return ret_val


# send2ferdl = MailSend()
# send2ferdl.send_mail(list_files_to_send=['test.txt', 'test_2.txt'])
#
# send2ferdl2 = MailSend(device_name='pbx_sn_02')
# send2ferdl2.send_mail(list_files_to_send=['test.txt', 'test_2.txt'])
