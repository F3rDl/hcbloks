import time


class timer_sec(object):
    """Eine einfach Zeit-mess-Funktion.

    with timer("optional_name"):
        some code ... some more code
    """

    def __init__(self, name=None):

        # Sichere den Namen fuer print-Ausgabe
        """
        Args:
            name:
        """
        self.name = name

    # Diese Methode wird beim betretten der "with"-Anweisung ausgefuehrt
    def __enter__(self):

        # Sichere die Zeit zum Start-Zeitpunkt
        self.start = time.time()
        return self

    # Diese Methode wird beim verlassen der "with"-Anweisung ausgefuehrt
    def __exit__(self, *args):

        # Sichere die Zeit zum End-Zeitpunkt
        """
        Args:
            *args:
        """
        self.end = time.time()

        # Bestimme das Time-Delta
        self.interval = self.end - self.start

        # Wenn beim aufruf eine Name mitgegeben wurde, dann ...
        if self.name:

            # Printe den Namen mit der verstrichenen Zeit
            print("{} -  Elapsed time: {:.4f}s".format(self.name, self.interval))

        else:
            # Printe die verstrichene Zeit
            print("Elapsed time: {:.4f}s".format(self.interval))


class timer_ns(object):
    """Eine einfach Zeit-mess-Funktion.

    with timer("optional_name"):
        some code ... some more code
    """

    def __init__(self, name=None):

        # Sichere den Namen fuer print-Ausgabe
        """
        Args:
            name:
        """
        self.name = name

    # Diese Methode wird beim betretten der "with"-Anweisung ausgefuehrt
    def __enter__(self):

        # Sichere die Zeit zum Start-Zeitpunkt
        self.start = time.time_ns()
        return self

    # Diese Methode wird beim verlassen der "with"-Anweisung ausgefuehrt
    def __exit__(self, *args):

        # Sichere die Zeit zum End-Zeitpunkt
        """
        Args:
            *args:
        """
        self.end = time.time_ns()

        # Bestimme das Time-Delta
        self.interval = self.end - self.start

        # Wenn beim aufruf eine Name mitgegeben wurde, dann ...
        if self.name:

            # Printe den Namen mit der verstrichenen Zeit
            print("{} -  Elapsed time: {}ns".format(self.name, self.interval))

        else:
            # Printe die verstrichene Zeit
            print("Elapsed time: {}ns".format(self.interval))


class timer_ms(object):
    """Eine einfach Zeit-mess-Funktion.

    with timer("optional_name"):
        some code ... some more code
    """

    def __init__(self, name=None):

        # Sichere den Namen fuer print-Ausgabe
        """
        Args:
            name:
        """
        self.name = name

    # Diese Methode wird beim betretten der "with"-Anweisung ausgefuehrt
    def __enter__(self):

        # Sichere die Zeit zum Start-Zeitpunkt
        self.start = time.time_ns()
        return self

    # Diese Methode wird beim verlassen der "with"-Anweisung ausgefuehrt
    def __exit__(self, *args):

        # Sichere die Zeit zum End-Zeitpunkt
        """
        Args:
            *args:
        """
        self.end = time.time_ns()

        # Bestimme das Time-Delta
        self.interval = (self.end - self.start) / 1000000

        # Wenn beim aufruf eine Name mitgegeben wurde, dann ...
        if self.name:

            # Printe den Namen mit der verstrichenen Zeit
            print("{} -  Elapsed time: {}ms".format(self.name, self.interval))

        else:
            # Printe die verstrichene Zeit
            print("Elapsed time: {}ms".format(self.interval))


def deep_update(dict_transfer, overrides):
    """Aktualisiert ein verschachteltes Dictionary

    Args:
        dict_transfer:
        overrides:
    """
    # Durch-wandere alle key/values des overrides-Dictionary
    for key, value in overrides.items():

        # Wenn der "value" ein gueltiges Dictionry ist, dann ...
        if isinstance(value, dict) and value:

            # Rufe die Funktion mit den Dictionary an "value" rekursiv auf
            returned = deep_update(dict_transfer.get(key, {}), value)

            # Uebergebe das aktuelisierte Dictionary an den "value" des
            # aktuellen "key" des Schleifendurchlauf
            dict_transfer[key] = returned

        # Es wurde das gesuchte key/value-Paar gefunden
        else:
            # Ueberschreibe den "value" an "key" mit den "value" aus "overrides"
            dict_transfer[key] = overrides[key]

    # Gib das ueberschriebene Dictionary zurueck
    return dict_transfer


def delete_keys_from_dict(dict_del, key_clear):
    """Diese Funktion entfern alle key's aus einen Verschalteten Dictionary
    anhand des uebergeben Such-parameters

    Args:
        dict_del:
        key_clear:
    """
    # Erstelle eine lokale Kopie
    dict_tmp = dict_del.copy()

    # Durch-wandere die Keys im Dictionary
    for key in dict_tmp.keys():

        # Wenn der "key" des aktuellen Schleifendurchlauf der gesuchte ist, dann ...
        if key in key_clear:

            # Entferne diesen aus dem Dictionary
            del dict_del[key]

        # Wenn der "key" an seinen "value" ein Dictionary hat, dann ...
        elif isinstance(dict_tmp[key], dict):

            # Rufe die Funktion rekursiv auf
            delete_keys_from_dict(dict_del[key], key_clear)

    # Gib das bereinigte Dictionary zurueck
    return dict_del


def delete_path_element_from_dict(dict_del: dict, path_clear: list):
    """Diese Funktion entfern alle key's aus einen Verschalteten Dictionary
    anhand des uebergeben Such-parameters

    Args:
        dict_del (dict):
        path_clear (list):
    """
    # Erstelle eine lokale Kopie
    dict_tmp = dict_del.copy()

    # Durch-wandere die Keys im Dictionary
    for key in dict_tmp.keys():

        # Wenn der "key" des aktuellen Schleifendurchlauf der gesuchte ist, dann ...
        if key in path_clear[-1] and len(path_clear) > 1:

            # Entferne das letzte Element aus der Liste
            path_clear.pop()

            # Rufe rekursiv auf
            delete_path_element_from_dict(dict_del[key], path_clear)

        elif key in path_clear[-1] and len(path_clear) == 1:

            # Entferne diesen aus dem Dictionary
            del dict_del[key]
            break

    # Gib das bereinigte Dictionary zurueck
    return dict_del


def delete_path_element_from_dict_2(dict_del: dict, path_clear: list):
    """Diese Funktion entfern den "key" aus einen Verschalteten Dictionary
    anhand des uebergeben Such-Pfad

    Args:
        dict_del (dict):
        path_clear (list):
    """

    try:
        print('new try')
        print(path_clear)
        dict_del[path_clear[-1]] = dict_del[path_clear[-1]]
        # print(dict_del[path_clear[-1]])

        if len(path_clear) == 1:
            print('entferne', dict_del[path_clear[-1]])
            del dict_del[path_clear[-1]]
        else:
            print('rekursion\n')

            delete_path_element_from_dict_2(dict_del[path_clear.pop()], path_clear)

    except KeyError:
        print('except')

    return dict_del
