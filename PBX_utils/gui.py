#!/usr/bin/python3.7
# -*- coding: utf-8 -*-

import platform
import time
import tkinter as tk
import psutil
import pprint
import numpy as np
import matplotlib.animation as animation
from typing import List, Any, Dict
from tkinter import Menu, Frame, Button
from tkinter import messagebox, ttk, Label
from tkinter.ttk import Treeview
from matplotlib import pyplot as plt
from matplotlib import style, lines, axes
from matplotlib.backends.backend_tkagg import FigureCanvasTkAgg
from PIL import Image, ImageTk
from PBX_utils.pbx_misc import deep_update, delete_path_element_from_dict

if "Linux" == platform.system():
    import multiprocessing as mp
else:
    import multiprocess as mp


class TabBarLeft:

    def __init__(self, master=None, tab_width=25):
        """
        Args:
            master:
            tab_width:
        """
        self.root = master
        self.tab_width = tab_width

        self.dict_tab_objects = {}

        # Definiere das Style fuer die Tab-Bar
        self.style = tk.ttk.Style(self.root)

        # Lege eine vertikale Tab-Bar mit West/Nord-Ausrichtung fest
        self.style.configure('lefttab.TNotebook', tabposition='wn')

        # Definiere die Schrift als auch die Feld-Breite der Tab-bar
        self.style.configure('TNotebook.Tab', font=('URW Gothic L', '15', 'bold'), width=self.tab_width)

        # Erstelle das Tab-Abzeige-Objekt
        self.Tabs = tk.ttk.Notebook(self.root, style='lefttab.TNotebook', )

    def add_tab(self, name):
        # self.Tabs.grid_forget()

        """
        Args:
            name:
        """
        # Erstelle den Frame, welcher angezeigt werden soll wenn der entsprechende Tab ausgewaehlt ist
        self.dict_tab_objects[name] = tk.Frame(self.Tabs)

        # Haenge den Frame an die Tab-bar, mit einer Beschriftung fuer die Tab-Bar
        self.Tabs.add(self.dict_tab_objects[name], text=name)

        # Positioniere die Tab-Bar
        self.Tabs.grid(row=0, column=0, sticky="nw")

    def get_frame(self, name):
        """
        Args:
            name:
        """
        return self.dict_tab_objects[name]


class GuiOverview:
    pic_process_scheme: Label

    def __init__(self, master=None, logging=None,
                 dict_transfer_in: dict = None,
                 dict_transfer_out: dict = None,
                 dict_transfer_par: dict = None,
                 class_settings=None):

        """
        Args:
            master: In dieser Variable ist der Tkinter-Master der GUI hinterlegt
            logging: In dieser Variable ist einen Referenz auf das
                Logging-Objekt hinterlegt
            dict_transfer_in (dict): In diesen Dictionary sind alle
                Eingangs/Sensor-Daten hinterlegt
            dict_transfer_out (dict): In diesen Dictionary sind alle
                Ausgangs/Aktor-Daten hinterlegt
            dict_transfer_par (dict): In diesen Dictionary sind alle
                Parameter/Konfiguration-Daten hinterlegt
            class_settings: In dieser Variable ist die Referenz der
                Settings-Klasse hinterlegt
        """
        self.root = master

        self.logging = logging

        self.dict_transfer_in = dict_transfer_in
        self.dict_transfer_out = dict_transfer_out
        self.dict_transfer_par = dict_transfer_par
        self.class_settings = class_settings

        # In diesen Dictionary werden die Referenzen alle Grafiken der Oberflaeche hinterlegt
        self.dict_graphics = {}

        self.gui_properties = {'name': '',
                               'value': 0.0,
                               'pos_x': 0,
                               'pos_y': 0,
                               'width': 200,
                               'height': 80,
                               'unit': '',
                               'active': False}

        # Starte mit Zeit-Versatz den zyklischen call welchen die Anzeigen aktualisiert
        self.root.after(500, self.cyclic_call)

    def cyclic_call(self):

        # Wenn das Update-Flag gesetzt das, dann ...
        if self.class_settings.update_flag:

            # Bereinige die Anzeige-Flaeche
            self.clean_overview_field()

            # Erstelle die Anzeige-Flaeche
            self.create_overview_field()

            # Setzt das Update-Flag zurueck
            self.class_settings.update_flag = False

        # Aktualisiere die grafischen Anzeigen
        self.update_displays()

        # Rufe dich selber nach 200ms
        self.root.after(200, self.cyclic_call)

    def update_displays(self):

        # Durch-wandere alle Grafiken
        for graphic in self.dict_graphics:

            # Wenn die Grafik folgenden String enthaelt, dann ...
            if '_TextVarMeasured' in graphic:

                # Extrahiere den Parameter-namen des grafischen Elementes im aktuellen Schleifendurchlauf
                var_name = graphic.replace('_TextVarMeasured', '')

                try:
                    # Versuche aus den Prozess-Daten-Dictionary den Wert auf die Textvariable des Widget zuzuweisen
                    self.dict_graphics[graphic].set(self.dict_transfer_in['data_in_process_values'][var_name])

                # Wenn es in "self.dict_transfer_in" einen Eintrag fuer "var_name" nicht geben sollte, dann ...
                except KeyError:

                    # Versuche  es mit "self.dict_transfer_out"
                    try:
                        # Wenn der Wert im Dictionary an "var_name" bool-isch ist, dann ...
                        if isinstance(self.dict_transfer_out['data_out_process_values'][var_name], bool):

                            # Wenn die bool-sche Variable "True" ist, dann ...
                            if self.dict_transfer_out['data_out_process_values'][var_name]:

                                # Schreibe den String "On" in die Anzeige
                                self.dict_graphics[graphic].set('On')
                            else:
                                # Schreibe den String "Off" in die Anzeige
                                self.dict_graphics[graphic].set('Off')

                        # Sonst gehe davon aus dass es sich um eine Zahl handelt und ...
                        else:
                            # Versuche aus den Prozess-Daten-Dictionary den Wert auf die
                            # Text-Variable des Widget zuzuweisen
                            self.dict_graphics[graphic].set(self.dict_transfer_out['data_out_process_values'][var_name])

                    # Sollte im Input als auch im Output-Dictionary nichts gefunden werden dann mach nichts
                    except KeyError:
                        try:
                            # Versuche  es mit "self.dict_transfer_par"

                            # Wenn der Wert im Dictionary an "var_name" bool-isch ist, dann ...
                            if isinstance(self.dict_transfer_par['data_par_gui'][var_name], bool):

                                # Wenn die bool-sche Variable "True" ist, dann ...
                                if self.dict_transfer_par['data_par_gui'][var_name]:

                                    # Schreibe den String "On" in die Anzeige
                                    self.dict_graphics[graphic].set('On')
                                else:
                                    # Schreibe den String "Off" in die Anzeige
                                    self.dict_graphics[graphic].set('Off')

                            # Sonst gehe davon aus dass es sich um eine Zahl handelt und ...
                            else:
                                # Versuche aus den Prozess-Daten-Dictionary den Wert auf die
                                # Text-Variable des Widget zuzuweisen
                                self.dict_graphics[graphic].set(self.dict_transfer_par['data_par_gui'][var_name])

                        except KeyError:
                            pass

    def create_overview_field(self):
        """Diese Funktion erstellt den Inhalt von dem Tab "Overview"""

        # Lade das Prozess-Daten-Schema-Bild als ein Label
        self.pic_process_scheme = self.load_pic_to_lable(
            master=self.root,
            pic_path=self.dict_transfer_par['paths']['path_pic_process_diagram'],
            pic_zom=self.dict_transfer_par['gui']['general']['pic_zoom'])

        # Pack das Label mit den Bild
        self.pic_process_scheme.pack()

        self.create_display_windows(key_sub='data_in_process_values',
                                    dict_transfer=self.dict_transfer_in,
                                    dict_transfer_par=self.dict_transfer_par)

        self.create_display_windows(key_sub='data_out_process_values',
                                    dict_transfer=self.dict_transfer_out,
                                    dict_transfer_par=self.dict_transfer_par)

        self.create_display_windows(key_sub='data_par_gui',
                                    dict_transfer=self.dict_transfer_par,
                                    dict_transfer_par=self.dict_transfer_par)

        # self.create_display_windows(key_sub='data_in_1w',
        #                             dict_transfer_in=self.dict_transfer_in,
        #                             dict_transfer_par=self.dict_transfer_par)

        # self.create_display_windows(dict_transfer=self.dict_transfer_in, key='data_in_', sub='can0')

    def clean_overview_field(self):
        """Diese Funktion entfernt den Inhalt von dem Tab "Overview"""
        # Entferne das aktuelle Prozess-Schema
        self.pic_process_scheme.pack_forget()
        self.pic_process_scheme.destroy()

        # Iteriere ueber das Dictionary mit den Grafiken
        for key, graphic in self.dict_graphics.items():

            # Wenn einer der folgenden Strings im "key" vorhanden sind, dann  ...
            if '_LabelFrame' in key or \
                    '_FrameMeasured' in key or \
                    '_LabelTextVar' in key or \
                    '_LabelUnit' in key or \
                    '_EntryField' in key or \
                    '_EntryFieldSet' in key or \
                    '_ButtonOff' in key or \
                    '_ButtonOn' in key or \
                    '_EntryField_Button' in key:
                # Vergiss die Position an welcher sich das Widget befindet
                self.dict_graphics[key].pack_forget()

                # Entferne die Grafik von der Oberflaeche
                self.dict_graphics[key].destroy()

    def load_pic_to_lable(self, master=None, pic_path: str = None, pic_zom: float = 1.0):
        """Diese Funktion ladet eine Bild-Datei aus dem angegeben Pfad
        "pic_path" mit dem Zoom-Faktor "pic_zoom" in den uebergeben "master" als
        tk.Label

        Args:
            master (tkinter.Frame): der uebergeordnete Master "= tk.Tk()"
            pic_path (str): Pfad an dem sich das Bild befindet
            pic_zom (float): Der zoom-Wert: = 1.0 --> original, < 1.0 -->
                verkleinern, > 1.0 --> vergroessern

        Returns:
            tk.Label: das Label mit dem Bild
        """
        try:
            # Lade das Bild aus dem angegebenen Pfad
            image = Image.open(pic_path)
            # Definiere den zoom-Faktor
            zoom = float(pic_zom)

            # Berechne die die Pixel-Anzahl mit Zoom-Faktor
            pixels_x, pixels_y = tuple([int(zoom * x) for x in image.size])

            # Erstelle ein neues Bild mit angewendeten Zoom-Faktor
            img = ImageTk.PhotoImage(image.resize((pixels_x, pixels_y), Image.ANTIALIAS))

            # Erstelle Label in dem das Bild des Prozess-Schemas dargestellt werden soll
            pic_obj = tk.Label(master=master, image=img)

            # Laden in das Label die Bild-Daten "img"
            pic_obj.image = img

            # Return das erstellte Label
            return pic_obj

        except OSError:
            self.logging.info('could not load picture from path = %s', pic_path)
            return None

    def remove_display_windows(self):

        # Durchwandere das uebergebene Dictionary
        for key, graphic in self.dict_graphics.items():

            if '_LabelFrame' in key or \
                    '_FrameMeasured' in key or \
                    '_LabelTextVar' in key or \
                    '_LabelUnit' in key or \
                    '_EntryField' in key or \
                    '_EntryFieldSet' in key or \
                    '_ButtonOff' in key or \
                    '_ButtonOn' in key or \
                    '_EntryField_Button' in key:

                # print('inkludiert', key)

                self.dict_graphics[key].pack_forget()
                self.dict_graphics[key].destroy()
            else:
                print('exkludiert', key)

            # Entferne die Grafik vom Pack-Manager

            # print(graphic.pack_info())

            # try:
            #     graphic.place_forget()
            # except tk._tkinter.TclError:
            #     graphic.pack_forget()
            #     pass

            # # Entferne die Grafik vollstaendig
            # try:
            #     graphic.destroy()
            # except AttributeError:
            #     pass

    def create_display_windows(self, key_sub: str = '',
                               dict_transfer: dict = None,
                               dict_transfer_par: dict = None):

        """Diese Funktion durchsucht das Transfer-Dictionary und erstellt fuer
        allen Daten-Eintraegen falls nicht vorhanden ein Sub-Dictionary
        "gui_properties".

        Wenn im Sub-Dictionary von dem Eintrag der Key

        Args:
            key_sub (str):
            dict_transfer (dict):
            dict_transfer_par (dict):
        """

        # Durch-wandere die Eintraege aus dem "dict_transfer" unter dem key "key_sub"
        for data_set in dict_transfer[key_sub].keys():

            # Wenn der Eintrag "data_set" im "dict_transfer" noch kein sub-
            # Dictionary names "signal_properties" hat, dann ...
            if (key_sub + data_set) not in dict_transfer_par['gui']['signal_properties'].keys():

                # Erstelle ein sub-Dictionary in "dict_transfer_par" mit den "signal_properties"
                deep_update(dict_transfer_par, {
                    'gui': {
                        'signal_properties': {
                            key_sub + data_set: self.gui_properties.copy()}}})

                deep_update(dict_transfer_par, {
                    'gui': {
                        'signal_properties': {
                            key_sub + data_set: {'name': data_set}}}})

            # Wenn der Wert zur Darstellung aktiviert ist, dann ...
            if dict_transfer_par['gui']['signal_properties'][key_sub + data_set]['active']:

                # Wenn sich der String im aktuellen Schleifendurchlauf "man_" enthaelt, dann ...
                if 'man_' in data_set:

                    # Wenn der Modus auf "Manual" gestellt ist, dann ... (erlaube einen Eintrag)
                    if dict_transfer_par['data_par_gui']['Mode'] == 2.0:

                        # Erstelle ein leeres Display-Fenster mit den Eigenschaften aus "dict_transfer_par"
                        self.create_display_window_frame(dict_transfer_par, key_sub, data_set)

                        # Erstelle die erste Zeile im leeren Display-Frame
                        self.create_display_window_first_row(dict_transfer_par, key_sub, data_set)

                        # Erstelle eine zweite Zeile im Display-Frame
                        self.create_display_window_second_row(dict_transfer_par, key_sub, data_set)

                    else:
                        continue

                else:

                    # Erstelle ein leeres Display-Fenster mit den Eigenschaften aus "dict_transfer_par"
                    self.create_display_window_frame(dict_transfer_par, key_sub, data_set)

                    # Erstelle die erste Zeile im leeren Display-Frame
                    self.create_display_window_first_row(dict_transfer_par, key_sub, data_set)

                    # Wenn es sich um einen Ausgabe-Wert handelt, dann ...
                    # if 'out' in key_sub or 'par' in key_sub:
                    if 'Mode' in data_set or 'Temp_set' in data_set:

                        # Erstelle eine zweite Zeile im Display-Frame
                        self.create_display_window_second_row(dict_transfer_par, key_sub, data_set)

#################
                # # Wenn es sich um einen Ausgabe-Wert handelt, dann ...
                # # if 'out' in key_sub or 'par' in key_sub:
                # if 'par' in key_sub:
                #
                #     # Wenn der "data_set"-Name den String "man_" enhaelt --> Manueller Schalter, dann ...
                #     if 'man_' in data_set:
                #
                #         # Und wenn der Modus auch "Manual" ist, dann ...
                #         if dict_transfer_par['data_par_gui']['Mode'] == 2.0:
                #
                #             # Erstelle eine zweite Zeile im Display-Frame
                #             self.create_display_window_second_row(dict_transfer_par, key_sub, data_set)
                #
                #             # Ansonsten erstelle keine zweite Zeile, --> keine Manuellen-Schalter
                #
                #     else:
                #         # Erstelle eine zweite Zeile im Display-Frame (Modus, Temp_set, ...)
                #         self.create_display_window_second_row(dict_transfer_par, key_sub, data_set)
##################

    def create_display_window_frame(self, dict_transfer_par, key_sub, data_set):

        # Haenge einen neuen LabelFrame in das Dictionary "self.dict_graphics" mit Referenz auf den Tab
        """
        Args:
            dict_transfer_par:
            key_sub:
            data_set:
        """
        self.dict_graphics[data_set + '_LabelFrame'] = \
            tk.LabelFrame(self.root)

        # Lege die Fenster-Dimensionen und Koordinaten des LabelFrames fest und platziere diesen
        self.dict_graphics[data_set + '_LabelFrame']. \
            place(x=dict_transfer_par['gui']['signal_properties'][key_sub + data_set]['pos_x'],
                  y=dict_transfer_par['gui']['signal_properties'][key_sub + data_set]['pos_y'],
                  width=dict_transfer_par['gui']['signal_properties'][key_sub + data_set]['width'],
                  height=dict_transfer_par['gui']['signal_properties'][key_sub + data_set]['height'])

        # Konfiguriere vom LabelFrame die Ueberschrift und deren Groesse
        self.dict_graphics[data_set + '_LabelFrame']. \
            configure(text=dict_transfer_par['gui']['signal_properties'][key_sub + data_set]['name'],
                      font=("Courier", dict_transfer_par['gui']['general']['font_size_value_text']))

    def create_display_window_first_row(self, dict_transfer_par, key_sub, data_set):

        """
        Args:
            dict_transfer_par:
            key_sub:
            data_set:
        """
        # Erstelle im LabelFrame einen Frame um den Messwert und die Einheit zusammenzufassen
        self.dict_graphics[data_set + '_FrameMeasured'] = tk.Frame(self.dict_graphics[data_set + '_LabelFrame'])
        self.dict_graphics[data_set + '_FrameMeasured'].pack(fill='x')

        # Erstelle ein Label im zuvor erstellten LabelFrame in dem der Mess-Wert dargestellt wird
        self.dict_graphics[data_set + '_LabelTextVar'] = tk.Label(self.dict_graphics[data_set + '_FrameMeasured'])
        self.dict_graphics[data_set + '_LabelTextVar'].pack(side=tk.LEFT, ipadx=10)

        # Erstelle ein "tk.StringVar" um auf den dargestellten Messwert zugreifen zu koennen
        self.dict_graphics[data_set + '_TextVarMeasured'] = tk.StringVar()

        # Weise einen default-Wert der Text-Variable zu
        self.dict_graphics[data_set + '_TextVarMeasured'].set('No Data')

        # Konfiguriere im zuvor erstellen Label eine Text-Variable mit der zuvor erstellten "tk.StringVar"
        self.dict_graphics[data_set + '_LabelTextVar']. \
            configure(font=("Courier", dict_transfer_par['gui']['general']['font_size_value_text']),
                      textvariable=self.dict_graphics[data_set + '_TextVarMeasured'])

        # Erstelle ein Label welches die Einheit anzeigen soll
        self.dict_graphics[data_set + '_LabelUnit'] = tk.Label(self.dict_graphics[data_set + '_FrameMeasured'])

        # Konfiguriere fuer die Einheit des Messwertes den Text aus den "dict_transfer" und die Schriftgroesse
        self.dict_graphics[data_set + '_LabelUnit']. \
            configure(text=dict_transfer_par['gui']['signal_properties'][key_sub + data_set]['unit'],
                      font=("Courier", dict_transfer_par['gui']['general']['font_size_value_text']))

        # Platziere die Einheit neben den Messwert
        self.dict_graphics[data_set + '_LabelUnit'].pack(side=tk.LEFT)

    def create_display_window_second_row(self, dict_transfer_par, key_sub, data_set):

        """
        Args:
            dict_transfer_par:
            key_sub:
            data_set:
        """
        # Erstelle im LabelFrame einen Frame um die Eingabe und die Einheit zusammenzufassen
        self.dict_graphics[data_set + '_EntryField'] = tk.Frame(self.dict_graphics[data_set + '_LabelFrame'])

        self.dict_graphics[data_set + '_EntryField'].pack(fill='x')

        # Handelt es sich bei dem Wert um einen float-Wert, dann ...
        if isinstance(dict_transfer_par['gui']['signal_properties'][key_sub + data_set]['value'], float):

            # Erstelle ein "tk_StringVar" und sichere diese in "self.dict_graphics"
            self.dict_graphics[data_set + '_TextVarSet_float'] = tk.StringVar()

            # Erstelle ein Entry-Field mit einer verknuepften Textvariable
            self.dict_graphics[data_set + '_EntryFieldSet'] = \
                tk.Entry(self.dict_graphics[data_set + '_EntryField'],
                         textvariable=self.dict_graphics[data_set + '_TextVarSet_float'], width=8)

            # Packe das Eingabe-Feld
            self.dict_graphics[data_set + '_EntryFieldSet'].pack(side=tk.LEFT)

        # Sonst muss davon ausgegangen werden das es sich um eine boolesch Variable handelt
        else:
            # Erstelle ein "tk_BooleanVar" und sichere diese in "self.dict_graphics"
            self.dict_graphics[data_set + '_BoolVarSet'] = tk.BooleanVar()

            # Erstelle den Aus-Button mit den Value=False und verknuepfe ihn mit der Text-Variable
            self.dict_graphics[data_set + '_ButtonOff'] = \
                tk.Radiobutton(self.dict_graphics[data_set + '_EntryField'], text='OFF', padx=0, value=False,
                               variable=self.dict_graphics[data_set + '_BoolVarSet'])

            self.dict_graphics[data_set + '_ButtonOff'].pack(side=tk.LEFT)

            # Erstelle den Ein-Button mit den Value=True und verknuepfe ihn mit der Text-Variable
            self.dict_graphics[data_set + '_ButtonOn'] = \
                tk.Radiobutton(self.dict_graphics[data_set + '_EntryField'], text='ON', padx=5,
                               variable=self.dict_graphics[data_set + '_BoolVarSet'], value=True)

            self.dict_graphics[data_set + '_ButtonOn'].pack(side=tk.LEFT)

        # Erstelle den Apply-Button welcher dafuer sorgt das die vorgewaehlten Einstellungen in das
        # Dictionary geschrieben werden
        self.dict_graphics[data_set + '_EntryField_Button'] = \
            tk.Button(self.dict_graphics[data_set + '_EntryField'], text='Apply')

        self.dict_graphics[data_set + '_EntryField_Button'].pack(side=tk.LEFT)
        self.dict_graphics[data_set + '_EntryField_Button'].var_name = data_set
        self.dict_graphics[data_set + '_EntryField_Button'].var_key = key_sub

        # Binde die Funktion "apply_output" mit dem Button "Apply"
        self.dict_graphics[data_set + '_EntryField_Button'].bind("<ButtonRelease-1>", self.apply_output)

    def apply_output(self, event):

        """Diese Funktion ist an den Apply-Button gebunden

        Args:
            event:
        """

        # Lade die Eigenschaften vom widget welche diese Funktion aufruft in die lokale Variable "w"
        w = event.widget
        # print(w.var_name)
        # print(w.var_key)

        # Wenn der Wert im dictornary der float-variablen zu finden ist
        if w.var_name + '_TextVarSet_float' in self.dict_graphics:

            try:
                # Dann versuche den Eintrag aus dem Entryfield in eine float-variable umzuwandeln
                numb = float(self.dict_graphics[w.var_name + '_TextVarSet_float'].get())

                if w.var_key in self.dict_transfer_out:

                    deep_update(self.dict_transfer_out, {w.var_key: {w.var_name: numb}})

                else:
                    # print('ist in par')
                    deep_update(self.dict_transfer_par, {w.var_key: {w.var_name: numb}})

            # Wenn ein unerlaubtes Zeichen statt Zahlen und einen Punkt [.] dann werfe eine Fehlermeldung
            except ValueError:

                # Es wurde ein nicht erlaubtes Zeichen in das Eingabefeld fuer Dezimalzahlen eingegeben
                tk.messagebox.showerror("Fehlerhafte Eingabe identifiziert",
                                        "Erlaubte Zeichen [0-9] und [.] als Trennung der Dezimalstellen")

            # Leere das Eingabefeld
            self.dict_graphics[w.var_name + '_TextVarSet_float'].set("")

        # Wenn der Wert im dictornary der bool-variablen zu finden ist
        elif w.var_name + '_BoolVarSet' in self.dict_graphics:

            value = self.dict_graphics[w.var_name + '_BoolVarSet'].get()

            # deep_update(self.dict_transfer_out, {w.var_key: {w.var_name: value}})
            # deep_update(self.dict_transfer_par, {w.var_key: {w.var_name: value}})

            # Kann "var_key" im "self.dict_transfer_out" gefunden werden, dann ...
            if w.var_key in self.dict_transfer_out:

                # Aktualisiere den Wert in das Dictionary
                deep_update(self.dict_transfer_out, {w.var_key: {w.var_name: value}})

            else:
                # Aktualisiere den Wert in das Dictionary
                deep_update(self.dict_transfer_par, {w.var_key: {w.var_name: value}})

        # Wenn der "Apply"-Button gedrueckt wurde, dann ...
        if 'Mode' in w.var_name:

            # Setzte das Update-Flag
            self.class_settings.update_flag = True


class EntryFieldLimit:
    def __init__(self, root, label_1="text", label_2="text", default_max=50, default_min=-50):
        """
        Args:
            root:
            label_1:
            label_2:
            default_max:
            default_min:
        """
        self.root = root

        self.frame_field = tk.Frame(self.root)

        self.label_name_max = tk.Label(self.frame_field, text=label_1)
        self.entry_max_val = tk.Entry(self.frame_field)
        self.entry_max_val.insert(0, default_max)

        self.label_name_min = tk.Label(self.frame_field, text=label_2)
        self.entry_min_val = tk.Entry(self.frame_field)
        self.entry_min_val.insert(0, default_min)

        self.frame_field.pack()
        self.label_name_max.grid(row=0, column=0)
        self.entry_max_val.grid(row=0, column=1)
        self.label_name_min.grid(row=1, column=0)
        self.entry_min_val.grid(row=1, column=1)


class GuiLivePlot:
    dict_line: Dict[lines.Line2D, lines.Line2D]
    line: List[lines.Line2D]
    style.use('ggplot')

    def __init__(self, master: tk.NONE, dict_transfer_in: dict, dict_transfer_out: dict, logging):

        """
        Args:
            master (tk.NONE):
            dict_transfer_in (dict):
            dict_transfer_out (dict):
            logging:
        """
        self.dict_transfer_in = dict_transfer_in
        self.dict_transfer_out = dict_transfer_out
        self.logging = logging

        self.list_active_signals = []
        self.numb_of_val_save = 7200

        self.dic_temp_data = {}
        self.root = master

        self.x_axis = []

        self.fig = plt.figure(figsize=(13, 6), dpi=100)
        self.ax1 = self.fig.add_subplot(1, 1, 1)

        plotcanvas = FigureCanvasTkAgg(self.fig, self.root)
        plotcanvas.get_tk_widget().pack(expand=True)

        # matplotlib.lines.Line2D

        self.dict_line = {}

        self.axis_y_max = 1000
        self.axis_y_min = 0

        self.ax1.set_ylim(self.axis_y_min, self.axis_y_max)

        self.time_span = 300
        self.cursor_time = 0

        self.frame_master = tk.Frame(self.root)

        self.frame_1 = tk.Frame(self.frame_master)

        self.Limit_y = EntryFieldLimit(self.frame_1, label_1="Y-Axis max", label_2="Y-Axis min",
                                       default_max=50, default_min=-50)

        self.Limit_x = EntryFieldLimit(self.frame_1, label_1="Timespan", label_2="Cursor-Time",
                                       default_max=300, default_min=0)

        self.ani = animation.FuncAnimation(self.fig, self.animate, interval=1000)

        self.checkbutton_activ_plot = tk.BooleanVar()
        self.checkbutton_activ_plot.set(False)

        self.button_apply = tk.Button(self.frame_master, text="Apply", command=self.call_apply)

        self.listbox_available_signals = tk.Listbox(self.root)

        self.checkbox_visible_signal = tk.Checkbutton(self.root, text="Display", padx=20,
                                                      variable=self.checkbutton_activ_plot,
                                                      command=self.on_select_checkbutton,
                                                      onvalue=True, offvalue=False)

        # Packs
        self.frame_master.pack(side='left', pady=(5, 5), padx=(5, 5))
        self.frame_1.pack(side='left', pady=(5, 5), padx=(5, 5))
        self.button_apply.pack(side='left', pady=(5, 5))
        self.listbox_available_signals.pack(side='left', pady=(5, 5))
        self.checkbox_visible_signal.pack(side='left', pady=(5, 5))
        self.checkbox_visible_signal.config(state='disabled')

        # Binds
        self.listbox_available_signals.bind('<<ListboxSelect>>', self.on_select_listbox_available_signals)

    def on_select_checkbutton(self):

        # Lade den Signalnamen welcher ausgwaehlt wurde
        selected_signal_name = self.listbox_available_signals.selection_get()

        # Wenn das ausgewaehlte Signal als Graph schon vorhanden, dann ...
        if selected_signal_name in self.dict_line:

            # Entferne das Signal vom Dictionary der Graphen
            self.dict_line[selected_signal_name].remove()

            # Entferne den Dictionary-Eintrag
            del self.dict_line[selected_signal_name]

        else:
            # Erstelle ein neues Plot-Obj
            linet, = self.ax1.plot([], [])

            # Weise das Plot-Obj dem Dictionary zu
            self.dict_line[selected_signal_name] = linet

            # Trage den Signal-Namen in das Plot-Obj ein
            self.dict_line[selected_signal_name].set_label(selected_signal_name)

        # Aktualisiere die Legende
        self.legend_update()

    def legend_update(self):

        # Wenn Graphen im Dictionary "self.dict_line" vorhanden sind, dann ...
        if self.dict_line:

            # Aktualisiere die Legende
            self.ax1.legend(fancybox=True, framealpha=0.5)

        # Sonst gehe davon aus das keine Graphen vorhanden sind, und ...
        else:

            # Entferne die Legende
            self.ax1.get_legend().remove()

    def on_select_listbox_available_signals(self, evt):

        # Vererbe die Eigenschaften vom gebunden widget auf die lokale Variable
        """
        Args:
            evt:
        """
        w = evt.widget

        # Setzt die Ansicht der Checkbox auf sichtbar
        self.checkbox_visible_signal.config(state='normal')

        # Ist im widget etwas ausgewaehlt/markiert
        if self.listbox_available_signals.curselection():

            # lade den ausgewaehlten Index in die lokale Variable "select_index"
            select_index = int(w.curselection()[0])

            # Speichere den Name vom ausgewaehlten Element
            value = w.get(select_index)

            # Ist der Name in der Liste der ausgewaehlten signale vorhanden
            if value in self.dict_line:

                # Dann setzt das Hakerl in der Checkbox
                self.checkbutton_activ_plot.set(True)

            else:

                # Dann setzt das Hackerl in der Checkbox zurueck
                self.checkbutton_activ_plot.set(False)

    def call_apply(self):
        # self.numb_of_val_show = self.Limit_y.entry_max_val.get()
        self.ax1.set_ylim(float(self.Limit_y.entry_min_val.get()), float(self.Limit_y.entry_max_val.get()))
        self.time_span = int(self.Limit_x.entry_max_val.get())
        self.cursor_time = int(self.Limit_x.entry_min_val.get())

    def add_data_to_memory(self, data_set):

        # Gehe in den uebergebenen Dictionary jedes Element durch
        """
        Args:
            data_set:
        """
        for key, value in data_set.items():

            # Wenn es sich bei "value" um eine Textvariable handelt und
            # diese nicht aus den Worten 'No Value' besteht dann, ...
            if isinstance(value, tk.StringVar) and value.get() not in 'No Value':

                # Sichere den Text
                text_value = value.get()

                # Ueberpruefe ob die Textvariable das Wort 'OFF' enthaelt
                if text_value in 'OFF':

                    # Wenn Ja dann weise den Wert 0 zu
                    num_val = 0.0

                # Ueberpruefe ob die Textvariable das Wort 'ON' enthaelt
                elif text_value in 'ON':

                    # Wenn Ja dann weise den Wert 1 zu
                    num_val = 1.0

                # Sonst gehe davon aus des es sich um einen Zahlenwert in Form eines string handelt und ...
                else:
                    # Wandle den Wert von einem String in einen float
                    num_val = float(text_value)

            # Wenn der "value" ein Integer oder float ist dann, ...
            elif isinstance(value, int) or isinstance(value, float):

                # Versuche den wert des elements des Dictionary in ein float umzuwandeln
                num_val = float(value)

            # Sonst ...
            else:
                # Unterbreche den aktuellen Schleifendurchlauf der uebergeordneten for-Schleife
                # da es sich um einen ungueltigen Wert handelt
                continue
            # TODO: ES SOLL deque verwendet werden (from collections import deque)
            # Wenn es im temporaeren Dictionary "dic_temp_data" noch keinen Eintrag gibt, dann ...
            if key not in self.dic_temp_data:

                # Erstelle einen Eintrag mit einer leeren Liste
                self.dic_temp_data[key] = []
                # self.dic_temp_data[key] = np.empty((0, 1), float)

                # Haenge in die leere Liste den aktuellen Wert an
                self.dic_temp_data[key].append(num_val)
                # self.dic_temp_data[key] = np.append(self.dic_temp_data[key], num_val)

                # Fuege den Namen des Signals "key" in die Listbox ein
                self.listbox_available_signals.insert(-1, key)

            # Es gibt schon einen Eintrag, deshalb ...
            else:
                # Wenn der Historienspeicher-Anzahl erreicht ist, dann ...
                if len(self.dic_temp_data[key]) >= self.numb_of_val_save:

                    # Entferne den Eintrag an der vordersten Stelle
                    self.dic_temp_data[key] = self.rotate(self.dic_temp_data[key], 1)
                    # self.dic_temp_data[key] = np.roll(self.dic_temp_data[key], -1)

                    # Weise der letzten Stelle den aktuellen Wert zu
                    self.dic_temp_data[key][-1] = num_val

                else:
                    # Haenge den Wert an der Liste hinten an
                    self.dic_temp_data[key].append(num_val)
                    # self.dic_temp_data[key] = np.append(self.dic_temp_data[key], num_val)

    @staticmethod
    def rotate(list_var: list, number: int):
        """Diese Funktion rotiert einen gegebene Liste mit der Anzahl der in
        Number festgelegt.

        Bsp.:
            ans = rotate([0, 1, 2, 3, 4, 5], -1) print(ans) >> [1, 2, 3, 4, 5,
            0]

        Args:
            list_var (list): Die Liste welche rotiert werden soll
            number (int): Rotier-Richtung left-shift --> Minus; right-shift -->
                Plus

        Returns:
            list: Die rotierte Liste
        """
        return list_var[number:] + list_var[:number]

    def animate(self, i: int):

        """
        Args:
            i (int):
        """
        data_set = self.dict_transfer_in['data_in_process_values'].copy()
        data_set.update(self.dict_transfer_out['data_out_process_values'].copy())

        # Daten werden in Vergangenheit-Speicher eingetragen
        self.add_data_to_memory(data_set)

        # Wenn die vom Benutzer festgelegte Cursor-Time 0 ist --> aktuellster Wert
        if self.cursor_time == 0:

            # Wenn die aktuelle Aufzeichnung laenger ist als gefordert, dann ...
            if i > self.time_span:

                # Die aktuelle Aufzeichnung ist laenger als die vom
                # Benutzer geforderte Zeitspanne (--> Normaler Betrieb)
                # Also starte die Darstellung der x-Achse bei Null
                self.ax1.set_xlim((i - self.time_span, i))
            else:

                # Die aktuelle Aufzeichnung ist kuerzer als die vom
                # Benutzer geforderte Zeitspanne (--> Programmstart)
                # Also starte die Darstellung der x-Achse bei Null
                if i != 0:
                    self.ax1.set_xlim(left=0, right=i)
        else:
            # Setzt x-min und x-max anhand der Eingabe
            self.ax1.set_xlim(self.cursor_time - self.time_span, self.cursor_time)

        # Durchwandere das Dictionary mit den Graphen
        for signal_name in self.dict_line:

            # Hole die Daten-Reihe des Graphen
            data = self.dic_temp_data[signal_name]

            # Wenn die Daten-laenge kleiner als der Darstellungs-Raum ist, dann ...
            if i < len(data):

                self.dict_line[signal_name].set_data(list(range(0, len(data))), data)

            else:
                self.dict_line[signal_name].set_data(list(range(i - len(data), i)), data)


class GuiSettings:
    button_close: Button
    button_save: Button
    frame_action_area: Frame
    settings_tree: Treeview

    def __init__(self, master: tk, dict_transfer: dict, configfile_write=None):

        """
        Args:
            master (tk):
            dict_transfer (dict):
            configfile_write:
        """
        self.sub_root = master
        self.window = None

        self.dict_transfer = dict_transfer

        self.dict_graphics = {}

        self.list_path = []

        self.configfile_write = configfile_write

        self.update_flag = True

    def close_window(self):
        self.clear_action_area_elements()
        self.dict_graphics = {}

        # Schliesse Fenster
        self.window.destroy()
        self.window = None

    def open_window(self):
        self.window = tk.Toplevel(self.sub_root)

        # Weise dem Fenster-Titel einen Namen zu
        self.window.wm_title('Settings')

        # Definiere Beendigungs-Routine
        self.window.protocol("WM_DELETE_WINDOW", self.close_window)

        # self.window.geometry("720x480")

        # Erstelle auf der linken Seite des Fensters einen "Treeview"
        self.settings_tree = self.create_tree_view(master=self.window, header_tv='Settings',
                                                   func=self.event_treeview_selected)

        # Befuelle den TreeView mit den Inhalt von "self.dict_transfer"
        self.fill_treeview(tree_view_obj=self.settings_tree, dictionary=self.dict_transfer)

        # Erstelle Buttons auf der Unterseite des Fensters
        self.create_button_save_close(master=self.window)

        # Erstelle eine Anzeige-Flaeche, auf welcher die Parameter-
        # Einstellungen dargestellt werden
        self.create_frame_action_area(master=self.window)

    @staticmethod
    def create_tree_view(master, row: int = 0, column: int = 0, header_tv: str = '', func=None):
        """Diese Funktion erstellt eine Tree-View-Box und haengt diese auf den
        uebergebenen "master" ein.

        Args:
            master (tkinter.widget): tkinter-widget auf dem der "TreeView"
                angezeigt werden soll
            row (int): Zeile in welcher der "TreeView" eingehaengt werden soll
            column (int): Spalte in welcher der "TreeView" eingehaengt werden
                soll
            header_tv (str): String der als Ueberschrift im TreeView angezeigt
                wird
            func (function): Call-function weclche gerufen wird wenn etwas
                angeklickt wird

        Returns:
            ttk.Treeview: Das "Treeview"-Object
        """
        # Erstelle den Treeview fuer die Settings
        tree_view_obj = ttk.Treeview(master, height=15)

        tree_view_obj.column('#0', width=400)
        tree_view_obj.grid(sticky='N', row=row, column=column, padx=5, pady=(5, 5))

        # --------------------------------------------------------------------------------------------------------------
        # tree_view_obj = ttk.Treeview(master, selectmode='browse')
        # tree_view_obj.pack(side='left')
        #
        # vsb = ttk.Scrollbar(master, orient="vertical", command=tree_view_obj.yview)
        # vsb.pack(side='right', fill='y')
        #
        # tree_view_obj.configure(yscrollcommand=vsb.set)
        #
        # --------------------------------------------------------------------------------------------------------------
        #
        # tree_view_obj = ttk.Treeview(master)
        # tree_view_obj.grid()
        #
        # vsb = ttk.Scrollbar(master, orient="horizontal", command=tree_view_obj.xview_scroll)
        # vsb.grid(sticky='N', columnspan=2)
        #
        # tree_view_obj.configure(xscrollcommand=vsb.set)
        # --------------------------------------------------------------------------------------------------------------

        # Gebe den Treeview einen Header
        tree_view_obj.heading('#0', text=header_tv, anchor='w')  # center

        # Binde einen Event-Listener auf den TreeView, wenn in diesen etwas ausgewaehlt wird,
        # wird die Funktion "func" aufgerufen
        tree_view_obj.bind(sequence="<<TreeviewSelect>>", func=func)

        return tree_view_obj

    def create_button_save_close(self, master=None):
        """Diese Funktion erstellt Buttons und haengt diese in den uebergebenen
        "master" ein

        Args:
            master (tkinter.widget): tkinter-widget auf dem die "Button"'s
                angezeigt werden soll
        """
        # Button zum speicher der Einstellungen
        self.button_save = tk.Button(master, text="Save", command=self.configfile_write)
        self.button_save.grid(sticky='S', row=10, column=4, padx=5, pady=(0, 5))

        # Button zum schliessen des Einstellungs-Fensters
        self.button_close = tk.Button(master, text="Close", command=self.close_window)
        self.button_close.grid(sticky='S', row=10, column=5, padx=5, pady=(0, 5))

    def create_frame_action_area(self, master=None):
        """Es wird ein Frame auf den uebergebenen "master" eingefuegt.

        Args:
            master (tkinter.widget): tkinter-widget auf dem der "Frame"
                angezeigt werden soll
        """
        # Erstelle Frame Action-Area
        self.frame_action_area = tk.Frame(master=master)
        self.frame_action_area.grid(sticky='N', row=0, column=1, rowspan=10, columnspan=10, padx=5, pady=(20, 20))

    def fill_treeview(self, tree_view_obj, dictionary, mother: str = ''):
        """Diese Funktion befuellt einen treeview mit den keys eines
        verschachtelten Dictionary

        Args:
            tree_view_obj:
            dictionary:
            mother (str):
        """

        # Durch-wandere das Dictionary
        for element, sub in dictionary.items():

            # Wenn "sub" wiederum ein Dictionary ist, dann ...
            if isinstance(sub, dict):

                element = element. \
                    replace('data_in_process_values', 'Process input '). \
                    replace('data_out_process_values', 'Process output '). \
                    replace('data_in', 'Input'). \
                    replace('data_out', 'Output').\
                    replace('_', ' ')

                # Haenge das neues "element" in den Treeview
                tree_view_obj.insert(parent=mother, index='end', iid=element, text=element)

                # Rufe die Funktion rekursiv auf
                self.fill_treeview(tree_view_obj, sub, element)

    def event_treeview_selected(self, event):
        """Diese Funktion wird als Event aufgerufen wenn im Tree-View etwas
        ausgewaehlt worden ist. Da sich er Treeview ueber mehrere Ebenen
        erstreckt und in der "action_area" mehere Ebenen dargestellt werden,
        wird eine Liste mit den Kompletten Zweig des ausgewaehlten Parameters
        erstellt und der Funktion "fill_action_area" uebergeben

        Args:
            event:
        """
        w: tk.ttk.Treeview = event.widget

        # Wenn im Treeview etwas ausgewaehlt worden ist, dann ...
        if w.selection():

            # Lade den ausgewaehlten Eintrag in die lokale Variable "select_index"
            select_index = w.selection()[0]

            # select_index = select_index.replace('Input', 'data_in').replace('Output', 'data_out').replace(' ', '_')

            # Erstelle eine lokale Liste und fuege "select_index" ein
            list_path = [select_index]

            # Wenn an der letzten Stelle der Liste sich kein leerer String befindet, dann ...
            while list_path[-1] != '':
                # Nimm das letzte Element der Liste und haenge dessen Eltern-namen an die Liste an
                list_path.append(w.parent(list_path[-1]))

            # Entferne das letzt Element --> sollte '' sein
            list_path.pop()

            # Kehre die Reihenfolge der Liste um
            list_path.reverse()

            # Sichere die Liste fuer andere Zwecke
            self.list_path = list_path

            self.fill_action_area(list_path=list_path)

    def fill_action_area(self, list_path: list = None):

        # Erstelle eine lokale Kopie von den Dictionary
        """
        Args:
            list_path (list):
        """
        dict_tmp = self.dict_transfer.copy()

        new_set = [x.replace('Process input ', 'data_in_process_values')
                    .replace('Process output ', 'data_out_process_values')
                    .replace('Input', 'data_in')
                    .replace('Output', 'data_out')
                    .replace(' ', '_') for x in list_path]

        list_path = new_set

        # Durch-wandere die Liste mit den key's zu den letzten Element des
        # Verschalteten Dictionary
        for list_element in list_path:
            # Kopiere die Sub-Ebene des Dictionary in das lokale Dictionary
            dict_tmp = dict_tmp[list_element].copy()

        # Entferne alle Grafiken aus der "action_area"
        self.clear_action_area_elements()

        # Erstelle alle Grafiken in der "action_area"
        self.fill_action_area_elements(dict_action_area=dict_tmp)

    def clear_action_area_elements(self):

        # Durchwandere das uebergebene Dictionary
        for key, graphic in self.dict_graphics.items():
            # Entferne die Grafik vom Pack-Manager
            graphic.grid_forget()

            # Entferne die Grafik vollstaendig
            graphic.destroy()

        self.dict_graphics = {}

    def clear_treeview(self):
        # Entferne alle Inhalte
        tree_view_obj = self.settings_tree
        tree_view_obj.delete(*tree_view_obj.get_children())

    def fill_action_area_elements(self, dict_action_area: dict):

        """
        Args:
            dict_action_area (dict):
        """
        elements_per_column = 8
        iter_column = 0
        iter_row = 0

        if not dict_action_area:
            self.create_listbox_node(row=iter_row)
            self.create_button_add_node(row=9)
            self.create_button_clear_node(row=10)

        # Durchwandere das uebergebene Dictionary
        for par_name, par_value in dict_action_area.items():

            # Wenn "par_value" kein Dictionary ist, dann ...
            if not isinstance(par_value, dict):

                # Wenn schon die Anzahl der Zeilen in der aktuellen Spalte erreicht ist, dann ...
                if iter_row >= elements_per_column:

                    # Setze den Zeilen-Iterator auf 1
                    iter_row = 1

                    # Inkrementiere den Spalten-Iterator
                    iter_column += 1

                # Sonst ...
                else:
                    # Inkrementiere den Zeilen-Iterator
                    iter_row += 1

                # Erstelle den Label mit dem Parameter-Namen
                self.dict_graphics[par_name + '_EntryField_Label'] = tk.Label(self.frame_action_area, text=par_name)
                self.dict_graphics[par_name + '_EntryField_Label'].grid(row=iter_row, column=iter_column, sticky="nw")

                # Inkrementiere den Zeilen-Iterator damit Label und Eingabefeld nicht in der selben Zeile sind
                iter_row += 1

                # Erstelle das Eingabefeld fuer den Parameter-Wert
                self.dict_graphics[par_name + '_EntryField'] = tk.Entry(self.frame_action_area)
                self.dict_graphics[par_name + '_EntryField'].grid(row=iter_row, column=iter_column, sticky="nw")

                # Wenn "par_value" ein leer String ist, dann ...
                if str(par_value) is '':
                    # Fuege den Text "undefined" in das Eingabefeld ein
                    self.dict_graphics[par_name + '_EntryField'].insert(0, 'undefined')

                # Wenn es sich um einen String handelt der in bool intpretiert werden soll, dann ...
                elif isinstance(par_value, str) and par_value.lower() in ['true', 'false', 'on', 'off']:

                    # Wenn ein positiver Ausdruck interpretiert werden kann, dann ...
                    if par_value.lower() in ['true', 'on']:

                        # Schreibe "On" in das Eingabe-Feld
                        self.dict_graphics[par_name + '_EntryField'].insert(0, 'On')
                    else:
                        # Schreibe "Off" in das Eingabe-Feld
                        self.dict_graphics[par_name + '_EntryField'].insert(0, 'Off')

                elif isinstance(par_value, bool):

                    if par_value:
                        # Schreibe "On" in das Eingabe-Feld
                        self.dict_graphics[par_name + '_EntryField'].insert(0, 'On')
                    else:
                        # Schreibe "Off" in das Eingabe-Feld
                        self.dict_graphics[par_name + '_EntryField'].insert(0, 'Off')


                else:
                    # Fuege den Parameter-Wert in das Eingabefeld ein
                    self.dict_graphics[par_name + '_EntryField'].insert(0, str(par_value))

                if 'Button_Apply' not in self.dict_graphics:
                    self.create_button_apply(row=10)

            # Bei "par_name" handelt es sich um ein Dictionary, also ...
            else:

                # Wenn noch keine 'Listbox_Node' im Grafik-Dictionary vorhanden ist, dann ...
                if 'Listbox_Node' not in self.dict_graphics:
                    self.create_listbox_node(row=iter_row)
                    iter_row += 1

                # Damit "Listbox_Leaf" seine Informationen bekommen kann,
                # update das key/value-Paar in das Sub-Dictionary
                self.dict_graphics['Listbox_Node'].sub_dict.update({par_name: par_value})

                # Leere die List-Box
                self.dict_graphics['Listbox_Node'].delete(0, tk.END)

                # Befuelle die List-Box mit dem Inhalt aus "dict_action_area"
                for item in dict_action_area:
                    self.dict_graphics['Listbox_Node'].insert(tk.END, item)

                # Binde einen Event-Listener auf die List-Box, wenn in der List-Box etwas ausgewaehlt wird,
                # wird die Funktion "self.event_listbox_select_node" aufgerufen
                self.dict_graphics['Listbox_Node'].bind(sequence='<<ListboxSelect>>',
                                                        func=self.event_listbox_select_node)

                # Wenn noch kein 'Button_Add_Node' im Grafik-Dictionary vorhanden ist, dann ...
                if 'Button_Add_Node' not in self.dict_graphics:
                    self.create_button_add_node(row=9)

                # Wenn noch kein 'Button_Clear_Node' im Grafik-Dictionary vorhanden ist, dann ...
                if 'Button_Clear_Node' not in self.dict_graphics:
                    self.create_button_clear_node(row=10)

    def create_button_add_node(self, row: int = 0):

        # Erstelle einen Button mit angegeben Text und function-call
        """
        Args:
            row (int):
        """
        self.dict_graphics['Button_Add_Node'] = \
            tk.Button(self.frame_action_area, text="Add Node",
                      command=lambda: self.event_button_add_node('Listbox_Node'))

        # Fuege den Button am Raster ein
        self.dict_graphics['Button_Add_Node'].grid(sticky='N', row=row, column=0, padx=10, pady=(10, 10))

    def create_button_add_leaf(self):
        self.dict_graphics['Button_Add_Leaf'] = \
            tk.Button(self.frame_action_area, text="Add Leaf",
                      command=lambda: self.event_button_add_leaf('Listbox_Leaf'))

        # Fuege den Button am Raster ein
        self.dict_graphics['Button_Add_Leaf'].grid(sticky='N', row=9, column=1, padx=10, pady=(10, 10))

    def create_button_clear_leaf(self):

        self.dict_graphics['Button_Clear_Leaf'] = \
            tk.Button(self.frame_action_area, text="Clear Leaf",
                      command=lambda: self.event_button_clear_leaf('Listbox_Leaf'))

        # Fuege den Button am Raster ein
        self.dict_graphics['Button_Clear_Leaf'].grid(sticky='N', row=10, column=1, padx=10, pady=(10, 10))

    def create_button_clear_node(self, row: int = 0):

        """
        Args:
            row (int):
        """
        self.dict_graphics['Button_Clear_Node'] = \
            tk.Button(self.frame_action_area, text="Clear Node",
                      command=lambda: self.event_button_clear_node('Listbox_Node'))

        # Fuege den Button am Raster ein
        self.dict_graphics['Button_Clear_Node'].grid(sticky='N', row=row, column=0, padx=10, pady=(10, 10))

    def create_button_apply(self, row: int = 0):

        """
        Args:
            row (int):
        """
        self.dict_graphics['Button_Apply'] = \
            tk.Button(self.frame_action_area, text="Apply",
                      command=self.event_button_apply)

        # Fuege den Button am Raster ein
        self.dict_graphics['Button_Apply'].grid(sticky='N', row=row, column=0, padx=10, pady=(10, 10))

    def create_listbox_node(self, master=None, row: int = 0, column: int = 0):

        # Erstelle eine List-Box
        """
        Args:
            master:
            row (int):
            column (int):
        """
        self.dict_graphics['Listbox_Node'] = tk.Listbox(self.frame_action_area, exportselection=0, width=50)
        self.dict_graphics['Listbox_Node'].grid(sticky='N', row=row, column=column, padx=10, pady=(10, 10))

        # Haenge ein leeres Dictionary an das Object
        self.dict_graphics['Listbox_Node'].sub_dict = {}

    def create_listbox_leaf(self):

        # Erstelle eine List-Box
        self.dict_graphics['Listbox_Leaf'] = tk.Listbox(self.frame_action_area, exportselection=0)
        self.dict_graphics['Listbox_Leaf'].grid(sticky='N', row=0, column=1, padx=10, pady=(10, 10))

    def event_listbox_select_node(self, evt):
        """Wenn aus der List-Box mit den "Node"'s etwas ausgewaehlt wurde wird
        diese Funktion aufgerufen.

        Es wird eine zweite List-Box erstellt. In dieser werden die Sub-
        Elemente der vorherigen List-Box angezeigt. Desweiteren werden Buttons
        zum hinzufuegen und entfernen von Listen-Eintraegen eingefuegt.

        Args:
            evt (tkinter.Event + widget-type): wird intern von Event-Handler
                angehaengt
        """
        widget = evt.widget
        select_index = widget.curselection()[0]
        selected_name = widget.get(select_index)
        sub_dict = self.dict_graphics['Listbox_Node'].sub_dict

        # Wenn noch keine 'Listbox_Leaf' im Grafik-Dictionary vorhanden ist, dann ...
        if 'Listbox_Leaf' not in self.dict_graphics:
            # Erstelle eine List-Box
            self.create_listbox_leaf()

        # Leere die List-Box
        self.dict_graphics['Listbox_Leaf'].delete(0, tk.END)

        # Befuelle die List-Box mit dem Inhalt aus "sub_dict"
        for item in sub_dict[selected_name]:
            self.dict_graphics['Listbox_Leaf'].insert(tk.END, item)

        # Wenn noch kein "Button_Add_Leaf" im Grafik-Dictionary vorhanden ist, dann ...
        if 'Button_Add_Leaf' not in self.dict_graphics:
            self.create_button_add_leaf()

        # Wenn noch kein "Button_Clear_Leaf" im Grafik-Dictionary vorhanden ist, dann ...
        if 'Button_Clear_Leaf' not in self.dict_graphics:
            self.create_button_clear_leaf()

    def event_button_add_node(self, par):

        # Erstelle eine Dialogbox
        """
        Args:
            par:
        """
        input_dialog = GuiDialogBox(self.window)

        # Halte den mainloop an, bis die Dialogbox geschlossen ist
        self.window.wait_window(input_dialog.top)
        parameter = input_dialog.parameter

        if parameter is '':
            tk.messagebox.showerror('Fehler',
                                    'Es wurde nichts eingegeben')
            return
        elif len(parameter) > 512:
            tk.messagebox.showerror('Fehler',
                                    'Parameter-Name ist zu lang')
            return

        # Lade den Dictionary-Pfad (von "self.dict_trasfer_par") an welchen der
        # neue "Node" eingefuegt werden soll
        tmp_path = self.list_path.copy()

        # Haenge den Eltern-"Node" in der Liste an
        tmp_path.append(parameter)

        # Die Reihenfolge der Liste wird umgekehrt
        tmp_path.reverse()

        # Fuege in das lokale Dictionary ein Dictionary an, mit den
        # Namen des eingegebenen "Leaf" als "key" und einem leeren
        # Dictionary an "value" und entferne diesen aus der Liste "tmp_path"
        dict_tmp = {tmp_path.pop(0): {}}

        for key in tmp_path:
            dict_tmp = {key: dict_tmp}

        deep_update(self.dict_transfer, dict_tmp)

        # Entferne den Inhalt des TreeView
        self.clear_treeview()

        # Befuelle den TreeView mit den Inhalt von "self.dict_transfer"
        self.fill_treeview(tree_view_obj=self.settings_tree, dictionary=self.dict_transfer)

        # Bereinige die Anzeige-Flaeche
        self.clear_frame_action_area()

        # Befuelle die Anzeige-Flaeche
        self.create_frame_action_area(master=self.window)

    def clear_frame_action_area(self):

        self.clear_action_area_elements()

        self.frame_action_area.grid_forget()

        self.frame_action_area.destroy()

    def event_button_add_leaf(self, par):

        # Erstelle eine Dialogbox
        """
        Args:
            par:
        """
        input_dialog = GuiDialogBox(self.window)

        # Halte den mainloop an, bis die Dialogbox geschlossen ist
        self.window.wait_window(input_dialog.top)

        # Hole die Eingabe ab und lege sie auf die lokale Variable "new_leaf"
        new_leaf = input_dialog.parameter

        if new_leaf is '':
            tk.messagebox.showerror('Fehler',
                                    'Es wurde nichts eingegeben')
            return
        elif len(new_leaf) > 512:
            tk.messagebox.showerror('Fehler',
                                    'Parameter-Name ist zu lang')
            return

        # Hole den ausgewaehlten Namen des "Node" aus der Listbox mit den "Node"s ab
        selected_index = self.dict_graphics['Listbox_Node'].curselection()[0]
        selected_name = self.dict_graphics['Listbox_Node'].get(selected_index)

        # Lade den Dictionary-Pfad (von "self.dict_transfer_par") an welchen das
        # neue "Leaf" eingefuegt werden soll
        tmp_path = self.list_path.copy()

        # Haenge den Eltern-"Node" in der Liste an
        tmp_path.append(selected_name)

        # Haenge das neue "Leaf" mit den Namen "new_leaf" an
        tmp_path.append(new_leaf)

        # Die Reihenfolge der Liste wird umgekehrt
        tmp_path.reverse()

        # Fuege in das lokale Dictionary ein Dictionary an, mit den
        # Namen des eingegebenen "Leaf" als "key" und einem leeren
        # String an "value" und entferne diesen aus der Liste "tmp_path"
        dict_tmp = {tmp_path.pop(0): ''}

        # Durch-wandere die Liste
        for key in tmp_path:
            # Nimm das aktuelle lokale Dictionary und verschachtle es
            # mit den "key" aus der for-Schleife
            dict_tmp = {key: dict_tmp}

        # Update das Dictionary "dict_tmp" in "self.dict_transfer_par"
        deep_update(self.dict_transfer, dict_tmp)

        # Entferne den Inhalt des TreeView
        self.clear_treeview()

        # Befuelle den TreeView mit den Inhalt von "self.dict_transfer"
        self.fill_treeview(tree_view_obj=self.settings_tree, dictionary=self.dict_transfer)

        # Bereinige die Anzeige-Flaeche
        self.clear_frame_action_area()

        # Befuelle die Anzeige-Flaeche
        self.create_frame_action_area(master=self.window)

    def event_button_clear_leaf(self, par):
        """
        Args:
            par:
        """
        try:
            # Hole den ausgewaehlten Namen des "Leaf" aus der Listbox mit den "Leaf"s ab
            selected_index = self.dict_graphics['Listbox_Leaf'].curselection()[0]
            selected_leaf = self.dict_graphics['Listbox_Leaf'].get(selected_index)

            # Hole den ausgewaehlten Namen des "Node" aus der Listbox mit den "Node"s ab
            selected_index = self.dict_graphics['Listbox_Node'].curselection()[0]
            selected_node = self.dict_graphics['Listbox_Node'].get(selected_index)

        # In der Listbox Leaf wurde nichts ausgewaehlt
        except IndexError:
            tk.messagebox.showerror('Fehler',
                                    'Keine Parameter zum entfernen ausgewaehlt')
            return

        # Lade den Dictionary-Pfad (von "self.dict_trasfer_par")
        # an welchen das "Leaf" entfernt werden soll
        tmp_path = self.list_path.copy()

        # Haenge den Eltern-"Node" in der Liste an
        tmp_path.append(selected_node)

        # Haenge das zu entfernende "Leaf" an
        tmp_path.append(selected_leaf)

        # Die Reihenfolge der Liste wird umgekehrt
        tmp_path.reverse()

        # Rufe die Funktion welche das Leaf an den angegebenen Pfad im Dictionary entfernt
        tmp_dict = (delete_path_element_from_dict(dict_del=self.dict_transfer.copy(), path_clear=tmp_path))

        # Weise obersten Key im aktualisierten Dictionary dem obersten Key von "self.dict_transfer" zu
        self.dict_transfer[self.list_path[0]] = tmp_dict[self.list_path[0]]

        # # Rufe die Funktion welche das Leaf an den angegebenen Pfad im Dictionary entfernt --> wurde ersetzt ^^
        # self.dict_transfer = delete_path_element_from_dict(dict_del=self.dict_transfer.copy(), path_clear=tmp_path)

        # Entferne den Inhalt des TreeView
        self.clear_treeview()

        # Befuelle den TreeView mit den Inhalt von "self.dict_transfer"
        self.fill_treeview(tree_view_obj=self.settings_tree, dictionary=self.dict_transfer)

        # Bereinige die Anzeige-Flaeche
        self.clear_frame_action_area()

        # Befuelle die Anzeige-Flaeche
        self.create_frame_action_area(master=self.window)

    def event_button_clear_node(self, par):
        """
        Args:
            par:
        """
        try:
            # Hole den ausgewaehlten Namen des "Node" aus der Listbox mit den "Node"s ab
            selected_index = self.dict_graphics['Listbox_Node'].curselection()[0]
            selected_node = self.dict_graphics['Listbox_Node'].get(selected_index)

        # In der Listbox Node wurde nichts ausgewaehlt
        except IndexError:
            tk.messagebox.showerror('Fehler',
                                    'Keine Knoten zum entfernen auswaehlt')
            return

        # Lade den Dictionary-Pfad (von "self.dict_trasfer_par")
        # an welchen das "Leaf" entfernt werden soll
        tmp_path = self.list_path.copy()

        # Haenge den Eltern-"Node" in der Liste an
        tmp_path.append(selected_node)

        # Die Reihenfolge der Liste wird umgekehrt
        tmp_path.reverse()

        # Rufe die Funktion welche das Leaf an den angegebenen Pfad im Dictionary entfernt
        tmp_dict = (delete_path_element_from_dict(dict_del=self.dict_transfer.copy(), path_clear=tmp_path))

        # Weise obersten Key im aktualisierten Dictionary dem obersten Key von "self.dict_transfer" zu
        self.dict_transfer[self.list_path[0]] = tmp_dict[self.list_path[0]]

        # Entferne den Inhalt des TreeView
        self.clear_treeview()

        # Befuelle den TreeView mit den Inhalt von "self.dict_transfer"
        self.fill_treeview(tree_view_obj=self.settings_tree, dictionary=self.dict_transfer)

        # Bereinige die Anzeige-Flaeche
        self.clear_frame_action_area()

        # Befuelle die Anzeige-Flaeche
        self.create_frame_action_area(master=self.window)

    def event_button_apply(self):

        # Durchwandere an den key's das Grafik-Dictionary
        for graphic in self.dict_graphics:

            # Wenn der key den String "EntryField" aber ohne "Label" beinhaltet, dann ...
            if 'EntryField' in graphic and 'Label' not in graphic:

                # Weise auf den lokalen String "key_name" den Namen von "graphic",
                # ohne den Teil-String "_EntryField", zu
                key_name = graphic.replace('_EntryField', '')

                # Weise der lokalen Variable den Wert vom Eingabe-Feld zu
                value = self.dict_graphics[graphic].get()

                # if value.lstrip('-').replace('.', '').isdigit():
                if value.replace('.', '', 1).isdigit():

                    if '.' in value:
                        value = float(value)
                    else:
                        value = int(value)

                elif isinstance(value, str) and value.lower() in ['true', 'false', 'on', 'off']:
                    if value.lower() in ['true', 'on']:
                        value = True
                    else:
                        value = False

                # Lade den Dictionary-Pfad (von "self.dict_trasfer_par")
                tmp_path = self.list_path.copy()

                # Haenge den Namen vom Eingabe-Feld in der Liste an
                tmp_path.append(key_name)

                # Die Reihenfolge der Liste wird umgekehrt
                tmp_path.reverse()

                new_set = [x.replace('Process input ', 'data_in_process_values')
                            .replace('Process output ', 'data_out_process_values')
                            .replace('Input', 'data_in')
                            .replace('Output', 'data_out')
                            .replace(' ', '_') for x in tmp_path]

                tmp_path = new_set

                # Erzeuge ein lokales Dictionary mit dem Namen und dem Wert des Eingabe-Feldes
                dict_tmp = {tmp_path.pop(0): value}

                # Bau den Teil-strang des Verschatelten Dictionary
                # mit dem Eintrag des Eingabe-Feldes nach
                for tmp_key in tmp_path:
                    dict_tmp = {tmp_key: dict_tmp}

                # Schreibe das lokal-erzeugte 
                deep_update(self.dict_transfer, dict_tmp)

        self.update_flag = True


class GuiDialogBox:

    def __init__(self, parent):
        # Erstelle ein Fenster
        """
        Args:
            parent:
        """
        top = self.top = tk.Toplevel(parent)

        # Sorge dafuer dass nur dieses Fenster aktiv ist
        self.top.grab_set()

        # Erstelle einen Label mit der Eingabe
        self.label_text = tk.Label(top, text='Enter parameter')
        self.label_text.pack()

        # Erstelle ein Eingabefeld
        self.entry_parameter = tk.Entry(top)
        self.entry_parameter.pack()

        # Setze den Cursor auf das Eingabefeld
        self.entry_parameter.focus_set()

        # Binde das aufrufen der Funktion "self.event_return" an das druecken der Enter-Taste
        self.entry_parameter.bind(sequence='<Return>', func=self.event_return)

        # Erstelle einen Bestaetigungs-Button
        self.button_submit = tk.Button(top, text='Submit', command=self.event_submit)
        self.button_submit.pack()

        # Definiere einen Parameter in welchen die Eingabe entgegengenommen wird
        self.parameter: str = ''

    def event_submit(self):
        # Weise den Text vom Eingabefeld dem "self.parameter" zu
        self.parameter = str(self.entry_parameter.get())

        # Entferne das Dialog-Fenster
        self.top.destroy()

    def event_return(self, evt):
        """
        Args:
            evt:
        """
        self.event_submit()


class Gui(mp.Process):
    live_plot: GuiLivePlot
    settings_window: GuiSettings
    sys_overview: GuiOverview
    frame_process_scheme: Label
    tab_bar: TabBarLeft
    file_menu: Menu
    menubar: Menu
    root: tk.Tk

    def __init__(self, logging=None,
                 dict_transfer_in: dict = None,
                 dict_transfer_out: dict = None,
                 dict_transfer_par: dict = None,
                 configfile_write=None):

        """
        Args:
            logging:
            dict_transfer_in (dict):
            dict_transfer_out (dict):
            dict_transfer_par (dict): In diesen Dictionary befinden sich die
                Einstellungen aus den Configfile
            configfile_write: Hierbei wird ein Funktionsaufruf mitgegeben um
                "dic_transfer_par" ins "config"-File zu schreiben
        """
        mp.Process.__init__(self, name="Gui")

        self.logging = logging

        self.dict_transfer_in = dict_transfer_in
        self.dict_transfer_out = dict_transfer_out
        self.dict_transfer_par = dict_transfer_par
        self.configfile_write = configfile_write

    def setup_root_window(self):
        """Diese Funktion gibt der Titel-Leiste den Namen der Applikation und
        setzt die Fenstergroesse auf "Maximal"
        """
        # Setze den Titel des Fensters
        self.root.title('Overture 2.0.4')

        # Definiere Beendigungs-Routine
        self.root.protocol("WM_DELETE_WINDOW", self.close_main_window)

        # Setze das Fesnter auf maximal-Groesse
        self.root.geometry("%dx%d+0+0" % (self.root.winfo_screenwidth(), self.root.winfo_screenheight()))
        # self.root.state('zoomed')

        icon = ImageTk.PhotoImage(file='./icon.ico')
        self.root.tk.call('wm', 'iconphoto', self.root._w, icon)

    def close_main_window(self):

        # Schliesse das Hauptfenster
        self.root.quit()

    def open_settings_window(self):
        """Diese Funktion oeffnet ein neues Fenster wenn es nicht bereits offen
        ist. Sollte es bereits offen sein wird es in den Vorder- Grund gebracht.
        """
        # Wenn das Settings-Window nicht bereits offen ist, dann ...
        if None is self.settings_window.window:

            # Oeffne das Fenster mit den Einstellungen
            self.settings_window.open_window()
        else:
            # Hole das Fenster in den Vordergrund
            self.settings_window.window.lift()

    def create_menuebar(self):
        """Diese Funktion erstellt die Menue-Leiste welche oben unter der
        Titel-Leiste angezeigt wird
        """
        # Erstelle eine Menue-Leiste
        self.menubar = tk.Menu(self.root)

        self.file_menu = tk.Menu(self.menubar, tearoff=0)
        self.file_menu.add_command(label="Config", command=self.open_settings_window)
        self.file_menu.add_command(label="Save", command=self.root.quit)
        self.file_menu.add_separator()
        self.file_menu.add_command(label="Exit", command=self.root.quit)
        self.menubar.add_cascade(label="File", menu=self.file_menu)

        # display the menu
        self.root.config(menu=self.menubar)

    def cyclic_function(self):
        """Diese Funktion ruft sich selber zyklisch auf um die Daten auf der
        grafischen Benutzer-Oberflaeche zu aktualisieren.
        """
        start = time.time()

        ende = time.time()
        self.root.after(int(500 - ((ende - start) * 1000)), self.cyclic_function)

    def run(self):
        """Mit den aufruf von "run" wurde der GUI-Prozess gestartet. Es wird ein
        Fenster erstellt
        """
        # Schreibe ins logging die Process-ID
        self.logging.info('Gui start with PID = ' + str(self.pid.numerator))

        # Erstelle Tkinter-Object
        self.root = tk.Tk()

        # Wende Einstellungen auf das Haupt-Fenster an
        self.setup_root_window()

        # Initialisiere die Settings-Klasse
        self.settings_window = GuiSettings(master=self.root,
                                           dict_transfer=self.dict_transfer_par,
                                           configfile_write=self.configfile_write)

        # Erstelle eine Menue-Leiste
        self.create_menuebar()

        # Erstelle das Tab-Bar-Object
        self.tab_bar = TabBarLeft(master=self.root)

        # Erstelle die Tab's
        self.tab_bar.add_tab('Overview')
        self.tab_bar.add_tab('Signal Monitor')

        # Initialisiere die Overview-Klasse
        self.sys_overview = GuiOverview(master=self.tab_bar.dict_tab_objects['Overview'],
                                        logging=self.logging,
                                        dict_transfer_in=self.dict_transfer_in,
                                        dict_transfer_par=self.dict_transfer_par,
                                        dict_transfer_out=self.dict_transfer_out,
                                        class_settings=self.settings_window)

        # Erstelle den Inhalt von Tab "Overview"
        self.sys_overview.create_overview_field()

        # Erstelle den Inhalt von Tab "LivePlot"
        self.live_plot = GuiLivePlot(self.tab_bar.dict_tab_objects['Signal Monitor'],
                                     self.dict_transfer_in,
                                     self.dict_transfer_out,
                                     self.logging)

        # Starte die Haupt-Schleife
        self.root.mainloop()
