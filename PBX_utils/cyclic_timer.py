import time
import datetime


class simple_timer_sec(object):
    """Eine einfache Zyklische-Timer-Funktion. Diese Implementierung unterliegt
    einen leichten Zeit-drift. Des Weiteren wird bei einer Laufzeitverschiebung
    das Intervall-Fenster um einen zyklus verletzt.

    with timer_sec("interval"):
        some code ... some more code
    """

    def __init__(self, interval=None):

        """
        Args:
            interval:
        """
        self.interval = interval

    # Diese Methode wird beim betretten der "with"-Anweisung ausgefuehrt
    def __enter__(self):

        # Sichere die Zeit zum Start-Zeitpunkt
        self.start = time.time_ns()
        return self

    # Diese Methode wird beim verlassen der "with"-Anweisung ausgefuehrt
    def __exit__(self, *args):

        # Bestimme das Time-Delta
        """
        Args:
            *args:
        """
        elapsed_time_in_sec = (time.time_ns() - self.start) / 1000000000

        # Wenn "elapsed_time_in_sec" groesser als "self.interval" Sekunden ist, dann ...
        if elapsed_time_in_sec >= self.interval:

            # print('Laufzeit-Verletzung, es wurden %s Sekunden benoetigt' % elapsed_time_in_sec)

            # Gib die CPU fuer 100ms frei
            time.sleep(self.interval)

        else:
            # Gib die CPU fuer 100ms minus "elapsed_time_in_sec" frei
            # print('%.2f' % ((self.interval - elapsed_time_in_sec) * 1000) + '% Leerlauf')
            time.sleep(self.interval - elapsed_time_in_sec)


class timer_sec(object):
    """Eine einfache Zyklische-Timer-Funktion. Diese Implementierung unterliegt
    keinen Zeit-drift. Des Weiteren wird bei einer Laufzeitverschiebung das
    Intervall- Fenster nicht verletzt.

    with timer_sec("interval"):
        some code ... some more code

        # Test-Funktion --> Nachkommastellen duerfen nicht driften
        print(time.time_ns()/100000000)
    """

    def __init__(self, interval=None):

        """
        Args:
            interval:
        """
        self.interval = interval
        self.next_call = datetime.datetime.now()

    # Diese Methode wird beim betretten der "with"-Anweisung ausgefuehrt
    def __enter__(self):

        # Lade die aktuelle datetime in "now"
        now = datetime.datetime.now()
        # print('now              = ' + str(now))

        # Dividiere durch 100.000 um die 10tel Sekunden auf der Einer-Stelle zu haben
        time_drift = now.microsecond/100000
        # print('Einerstelle      = ' + str(time_drift))

        # Ziehe die Einer-Stelle (10tel Sekunden) ab und dividiere durch 10 um auf volle Mikro-Sekunden zu kommen
        time_drift = (time_drift - int(time_drift))/10
        # print('Einerstelle -    = ' + str(time_drift))

        # Ziehe den time_drift von den zyklischen interval ab und konvertiere das Ergbniss ins datetime-format
        time_drift_delta = datetime.timedelta(seconds=self.interval - time_drift)
        # print('time_drift_delta = ' + str(time_drift_delta))
        # print('old nextcall     = ' + str(self.next_call))

        # Bestimme den Zeitpunkt wann der naechste Aufruf sein soll
        self.next_call = self.next_call + time_drift_delta
        # print('entered          = ' + str(now) + '\nnextcall         = ' + str(self.next_call))

        return self

    # Diese Methode wird beim verlassen der "with"-Anweisung ausgefuehrt
    def __exit__(self, *args):

        # print('ended            = ' + str(datetime.datetime.now()))

        # Bestimme die Zeit zwischen dem naechsten Aufruf und dem aktuellen Zeitpunkt
        # und weise die differenz in Sekunden "sleep_time" zu
        """
        Args:
            *args:
        """
        sleep_time = (self.next_call - datetime.datetime.now()).total_seconds()
        # print('sleep_time       = ' + str(sleep_time) + '\n')

        # print('%.2f' % ((self.interval - sleep_time) * 1000) + '% Last')

        if sleep_time <= 0:
            time.sleep(0.00001)
            # print('Runtime-Error -------------------------------------------')
        else:
            time.sleep(sleep_time)
