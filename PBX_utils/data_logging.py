#!/usr/local/bin/python3.7

import atexit
import csv
import os
import shutil
from datetime import datetime, timezone
from typing import List, Dict, Union, Tuple
import pyzipper
from PBX_utils import data_transfer_sftp
from multiprocessing import Process


class Logging:

    prefix_name: str
    file_type: str
    file_name: str
    header_timestamp: str
    fieldnames: List[str]

    csv.register_dialect('PBXDialect', delimiter=';')

    def __init__(self, name: str = 'HCBloks_0001',
                 max_timespan_in_sec: float = 900.0,
                 max_file_size_in_mb: float = 10.0,
                 ramdisk_path: str = '/tmp/ramdisk/',
                 ramdisk_folder_name: str = '.Logfiles_ram',
                 logfolder_path: str = '/home/pi/',
                 logfolder_name: str = 'Logfiles',
                 max_files_size_b4_compress: float = 100000000.0,  # Byte --> 100MegaByte
                 max_files_size_b4_delete: float = 15000000000.0):  # Byte --> 15000MegaByte --> 15GigaByte
        """
        Args:
            name (str):
            max_timespan_in_sec (float):
            max_file_size_in_mb (float):
            ramdisk_path (str):
            ramdisk_folder_name (str):
            logfolder_path (str):
            logfolder_name (str):
            max_files_size_b4_compress (float):
            max_files_size_b4_delete (float):
        """

        self.max_file_size_in_mb = max_file_size_in_mb
        self.max_timespan_in_sec = max_timespan_in_sec
        self.max_files_size_b4_compress = max_files_size_b4_compress
        self.max_files_size_b4_delete = max_files_size_b4_delete

        # Dieses Praefix wird verwendet damit es moeglich ist im 'self.name_id' nur Zahlen zu verwenden weil
        # manche Dateisystem ein Problem damit haben wenn ein der Name eines Files mit Zahlen beginnt
        self.prefix_name = 'Log_'

        # In dieser String-Variable wird der Uebergabe-Paramter 'name' gesichert
        self.name_id = name

        # In dieser String-Variable wird der Filetyp fuer die Filenamen hinterlegt
        self.file_type = '.csv'

        # Der string in dieser Variabel wird den Dateinamen beim Oeffnen angehaengt und beim
        # ordentlichen abschliessen des Log-Files wieder entfernt
        self.postfix_name = '.tmp'

        # In dieser String-Variable wird der Name der aktuellen Datei gehalten.
        self.file_name = ''
        self.create_new_file_name()

        # In dieser String-Variable wird die Spalten-Beschriftung des Zeitstempels hinterlegt
        self.header_timestamp = 'Timestamp'

        # In dieser String-Variable wird die Spalten-Beschriftung des Zeitstempels-Excel hinterlegt
        self.header_timestamp_excel = 'Timestamp_Excel'

        # In dieser Liste werden die Spalten-Beschriftungen (Header) des Log-Files hinterlegt
        self.fieldnames = []

        # In dieser Variable wird der naechst Logfile-Create-Zeitpunkt (Unix-format) gehalten
        self.next_logfile_change = self.get_utc_timestamp() + max_timespan_in_sec

        self.ramdisk_path = ramdisk_path
        self.logfolder_path = logfolder_path  # '/home/pi/'

        self.ramdisk_folder_name = ramdisk_folder_name
        self.ramdisk_folder_name = self.ramdisk_folder_name + '/'

        self.logfolder_name = logfolder_name
        self.logfolder_name = self.logfolder_name + '/'

        # Sorge dafuer das am Pfad des Ram-Disk fuer die Log-Files ein Ordner vorhanden ist
        os.makedirs(self.ramdisk_path + self.ramdisk_folder_name, exist_ok=True)

        # In diesen Dictionary wird der Header fuer die Spalten-Beschriftung zwischengespeichert
        self.dic_header_names = {}

        self.log_mgmt = LogfileManagement(logfolder_path=self.logfolder_path, logfolder_name=self.logfolder_name,
                                          max_files_size_b4_delete=self.max_files_size_b4_delete,
                                          max_files_size_b4_compress=self.max_files_size_b4_compress,
                                          logfile_prefix=self.prefix_name)

        self.transfer_hcbloks_to_finki = data_transfer_sftp.FileTransferSFTP(
            host='194.149.135.40', username='pbx', port=7384, log_path='/home/pi/Logfiles',
            path_private_key='/usr/local/bin/pbx/Overture_2_0_3/SFTP_File_Transfer/id_rsa.pem',
            tmp_file='/home/pi/Logfiles/last_transferred_file.hcbloks')

        self.transfer_filo_to_finki = data_transfer_sftp.FileTransferSFTP(
            host='194.149.135.40', username='pbx', port=7384, log_path='/home/pi/FiLo_Logs',
            path_private_key='/usr/local/bin/pbx/Overture_2_0_3/SFTP_File_Transfer/id_rsa.pem',
            tmp_file='/home/pi/FiLo_Logs/last_transferred_file.filo')

        self.process_sftp_finki = Process(target=self.transfer_hcbloks_to_finki.send_files, args=('/upload',),
                                          name=str(str('Log_Transfer_SFTP_') + str(self.__class__.__name__)))

        # Es wird ein exit-handler eingerichtet
        atexit.register(self.exit_handler)

    def exit_handler(self):
        """Diese Methode wird beim Beenden vom Programm aufgerufen.

        Es wird in dieser Routine dafuer gesorgt das die Log-Datei
        Ordnungs-gemaess abgeschlossen wird.
        """

        # Schliesse alte/aktuelle Datei ab
        self.__finish_old_logfile()

    def swap_logfile(self):
        """Dieser Methode sorgt dafuer das in eine neue Logdatei aufgezeichnet
        wird.

        Hiefuer wird zu erst das alte Log-File abgeschlossen und der neue
        Log-File-Name gebildet. Abschliessend wird der "Timer" fuer den
        naechsten Log-File-wechsel gesetzt.
        """

        # Schliesse alte/aktuelle Datei ab
        self.__finish_old_logfile()

        # if self.process_sftp_finki.is_alive():
        #     print('process_sftp_finki is alive')
        # else:
        #     print('process_sftp_finki is NOT alive')

        # Erstelle einen Process um die Files auf den Finki-Server zu landen
        self.process_sftp_finki = Process(target=self.transfer_hcbloks_to_finki.send_files, args=('/upload',),
                                          name=str(str('Log_Transfer_SFTP_') + str(self.__class__.__name__)))

        self.process_sftp_finki.daemon = True
        self.process_sftp_finki.start()

        # if self.process_sftp_finki.is_alive():
        #     print('process_sftp_finki is alive')
        # else:
        #     print('process_sftp_finki is NOT alive')

        # Rufe Routine zur Speicherverwaltung der Logfiles auf
        self.log_mgmt.update_log_rotate()

        # Generiere den Namen fuer das naechste Log-File
        self.create_new_file_name()

        # Erstelle ein neues Log-File und schreibe die Header in dieses
        self.write_header()

        # Lege die Zeit in Unix-Format fuer den naechsten Log-File-wechsel fest
        self.next_logfile_change = self.get_utc_timestamp() + self.max_timespan_in_sec

    def __finish_old_logfile(self):
        """Diese Methode schliesst das aktuelle Logfile ab und schiebt es ins
        Archiv

        Es wird ueberprueft ob der Log-File-Ablage-Ordner existiert, wenn
        nicht, wird er erstellt. Es wird aus dem Dateinamen des Logfiles der
        String "self.postfix_name" entfernt und der Pfad an dem das File liegt
        umbenannt.
        """

        # Wenn es den Log-File-Ablage-Ordner nicht gibt, dann ...
        if not os.path.exists(self.logfolder_path + self.logfolder_name):

            # Erstelle den Ordner
            os.makedirs(self.logfolder_path + self.logfolder_name, exist_ok=True)

        # Benenne das Logfile von alt (Parameter 1) nach neu (Parameter 2) um.
        # Hierbei wechselt auch der Pfad an dem das File im Verzeichnis liegt
        # os.rename(self.ramdisk_path + self.ramdisk_folder_name + self.file_name,
        #           self.logfolder_path + self.logfolder_name + self.file_name.replace(self.postfix_name, ''))

        shutil.move(self.ramdisk_path + self.ramdisk_folder_name + self.file_name,
                    self.logfolder_path + self.logfolder_name + self.file_name.replace(self.postfix_name, ''))

    def create_new_file_name(self):
        """Erstelle einen Datei-Namen fuer das aktuelle Logfile.

        Diese Methode aktualisiert die String-Variable "self.file_name".
        Hierfuer wird die "datetime.now()"-Funktion verwendet. In diese wird
        mittels Formatierung "self.prefix_name" und "self.name_id" eingefuegt.
        Abgeschlossen wird der String mit "self.file_type" und dem
        "self.postfix" (Anzeichen ob korrekt abgeschlossen).
        """

        # Aktualisiere die String-Variable "self.file_name" mit den aktuellen Zeitwert
        self.file_name = datetime.now(tz=timezone.utc).strftime(
            self.prefix_name + self.name_id + "_%Y_%m_%d_%H_%M_%S" + self.file_type + self.postfix_name)

    def write_header(self, dict_data: dict = None):
        """Diese Methode schreibt in die erste Zeile des Log-Files
        "self.file_name" alle Spalten-Beschriftungen (Header)

        Args:
            dict_data (dict): Mit diesem Uebergabe-Parameter wird das Dictionary
                mitgegeben welches aufgezeichnet werden soll. Die Methode selbst
                zieht sich die ".keys" um diese als Header im Log-File zu
                verwenden.
        """

        if dict_data is not None:
            self.dic_header_names = dict_data

        # Da der Zeit-Stempel nicht im Dictionary vorhanden ist, muss dessen Header "self.header_timestamp"
        # und "self.header_timestamp_excel" manuell hinzugefuegt werden.
        # An diesen werden die ".keys" des Dictionary angehaengt.
        self.fieldnames = [self.header_timestamp] + [self.header_timestamp_excel] + [*self.dic_header_names]

        # Wenn es den Log-File-Ablage-Ordner nicht gibt, dann ...
        if not os.path.exists(self.ramdisk_path + self.ramdisk_folder_name):
            # Erstelle den Ordner
            os.makedirs(self.ramdisk_path + self.ramdisk_folder_name, exist_ok=True)

        # Oeffne das Log-File "self.file_name" im Schreib-modus mit deaktivierten newline-Zeichen
        with open(self.ramdisk_path + self.ramdisk_folder_name + self.file_name, 'w', newline='') as csvFile:

            # Erstelle ein CSV-Dictionary-Writer-Objekt und ubergebe den Parameter "fieldnames" die
            # Klassen-Variable "self.fieldnames" bei verwendung des PBXDialect (--> delimiter=';')
            writer = csv.DictWriter(csvFile, fieldnames=self.fieldnames, dialect='PBXDialect')

            # Rufe aus dem CSV-Dictionary-Writer-Objekt die Methode zum schreiben des Headers auf
            writer.writeheader()

    @staticmethod
    def __modify_data_for_logfile(dict_data: dict, fieldnames: list):
        """Diese Methode filtert Daten-Eintraege heraus welche nicht in
        "fieldnames" vorhanden sind und bessert folgende Daten-Werte aus:

            - Wenn ein bool'scher Wert vorliegt wird "T" oder "F" verwendet
            - Wenn ein float vorliegt wird auf zwei Nachkoma-Stellen limitiert

        Args:
            dict_data (dict): Die zu bearbeitenden Daten im Dictionary-Format
            fieldnames (list): Eine Liste mit Header-Namen welche den filter
                passieren sollen

        Returns:
            dict: Das bereinigte und modifizierte Dictionary
        """
        # kopiere nur die key/value-paare welche in fieldnames stehen
        dic_new = dict(filter(lambda elem: elem[0] in fieldnames, dict_data.items()))

        # Sortiere alle Werte aus welche nicht "Bool" oder "Float" sind
        # dic_new = dict(filter(lambda elem: isinstance(elem[1], bool) or isinstance(elem[1], float), dic_new.items()))

        # Durchwandere das neue Dictionary
        for key, value in dic_new.items():

            # Wenn der Value von DatenTyp bool ist, dann ...
            if isinstance(value, bool):

                # Wenn der value dem Zustand "True" entspricht, dann ...
                if value is True:
                    # Dann weise dem "value" den String 'T' zu
                    dic_new[key] = 'true'

                # Sonst duerfte es sich um "False" handeln, also ...
                else:
                    # Weise dem "value" den String 'F' zu
                    dic_new[key] = 'false'

            # Sonst-Wenn der Value von DatenTyp float ist, dann ...
            elif isinstance(value, float) or isinstance(value, int):

                # Limitiere den Wert auf zwei Nachkommastellen
                dic_new[key] = "%.2f" % value

        return dic_new

    def update_swap_logfile(self):
        """Check ob ein Logfile-Wechsel statt finden soll und wenn ja wird er
        auch erledigt

        Um dies zu bewerkstelligen werden folgende Kriterien herangezogen:
            - Wird in das File schon seit "self.max_timespan_in_sec" Sekunden
              aufgezeichnet
            - Ist die File-Groesse mehr als in "self.max_file_size_in_mb"
              angegeben ist

        Sollte einer der beiden Bedingungen wahr sein wird die Funktion
        "self.swap_logfile()" aufgerufen und das Logfile wird ins archiv
        geschoben. Ein neues File wird gestartet.
        """
        # Wenn die aktuelle Zeit gleich derer in "self.next_logfile_change" ist, dann ...
        if self.get_utc_timestamp() > self.next_logfile_change:

            # Fuehre ein wechsel der Lofile-Datei durch
            self.swap_logfile()

        # Wenn das aktuelle Logfile groesser ist als der Wert in "self.max_file_size_in_mb" ist, dann ...
        elif os.path.getsize(self.ramdisk_path + self.ramdisk_folder_name + self.file_name) > \
                self.max_file_size_in_mb * 1024000:

            # Fuehre ein wechsel der Lofile-Datei durch
            self.swap_logfile()

    def __add_timestamp_dict(self, dict_data: dict):
        """Dieser Funktion schreibt den aktuellen Zeit-Stempel in das
        uebergebene Dictionary

        Args:
            dict_data (dict): Das Dictionary in welches der Zeit-Stempel
                eingetragen werden soll
        """
        # Erstelle ein lokales Dictionary. In diesem wird als key der String von "self.header_timestamp", als
        # Value der Unix-Zeit-Stempel (--> String). Der Zeitstempel wird auf drei Nachkommastellen limitiert
        # dic_timestamp_str: Dict[str, str] = {self.header_timestamp: "%.3f" % self.get_utc_timestamp()}
        dic_timestamp_str: Dict[str, str] = {self.header_timestamp: "%d" % int(self.get_utc_timestamp()*1000)}

        # Erstelle ein lokales Dictionary. In diesem wird als key der String von "self.header_timestamp_excel", als
        # Value der lokale Zeit-Stempel (UTC+1 inkl SZ/WZ) hinterlegt
        dic_timestamp_excel_str: Dict[str, str] = {self.header_timestamp_excel:
                                                   datetime.now().strftime("%Y-%m-%dT%H:%M:%S")}

        # Integriere das lokale Zeitstempel-Dictionary in das empfangene Daten-Dictionary
        dict_data.update(dic_timestamp_str)

        # Integriere das lokale Zeitstempel-Dictionary in das empfangene Daten-Dictionary
        dict_data.update(dic_timestamp_excel_str)

    def write_data(self, dict_data: dict):
        """Dieser Aufruf schreibt die Daten des uebergebenen Dictionary in das
        Logfile

        Args:
            dict_data (dict): Prozess-Daten im Dictionary-Format
        """
        # Check ob ein Logfile-Wechsel statt finden soll und wenn ja wird er auch erledigt
        self.update_swap_logfile()

        # Fuege den Daten den aktuellen Zeit-Stempel hinzu
        self.__add_timestamp_dict(dict_data=dict_data)

        # Rufe die Methode auf um das Dictionary nach Spezifikation zu modifizieren
        dict_data = self.__modify_data_for_logfile(dict_data, self.fieldnames)

        # Oeffne das Log-File "self.file_name" im Anhaeng-modus mit deaktivierten newline-Zeichen
        with open(self.ramdisk_path + self.ramdisk_folder_name + self.file_name, 'a', newline='') as csvFile:

            # Erstelle ein CSV-Dictionay-Writer-Objekt und ubergebe den Parameter "fieldnames" die
            # Klassen-Variable "self.fieldnames" bei verwendung des PBXDialect (--> delimiter=';')
            writer = csv.DictWriter(csvFile, fieldnames=self.fieldnames, dialect='PBXDialect')

            # Schreibe das "dict_data" in das CSV-File
            writer.writerow(dict_data)

    @staticmethod
    def get_utc_timestamp():
        return datetime.now(tz=timezone.utc).timestamp()


class LogfileManagement:
    """Dieser Klasse stellt Funktionen zur Verfuegung um einen Ordner mit
    Logfiles zu verwalten.

    Es werden folgende Funktionen bereitgestellt:
        - wenn der Speicherbedarf der Logfiles die Grenze von
          "max_files_size_b4_compress" uebersteigt werden alle Files welche
          nicht vom aktuellen Tag sind, komprimiert
        - wenn der Speicherbedarf der komprimierten Files die Grenze
          "max_files_size_b4_delete" uebersteigt wird das aelteste komprimierte
          File geloescht

    Damit die korrekte Funktion sichergestellt ist, muessen folgende Anweisungen eingehalten werden:
        - Die File-Namen der Logfiles sollten folgenden Aufbau aufweisen:
          logfile_prefix + whatever +'_%Y_%m_%d_%H_%M_%S' + '.zip'
        - Da der Zip-File-Loesch-part das File entfernt welches in der
          alphabetischen Reihenfolge ganz oben steht, sollten keine anderen
          Methoden/Funktionen/User in diesen Ordner mit zip-Files arbeiten.

    Verwendung:

    Es muss eine instanz erstellt werden:
        log_mgmt = LogfileManagement(logfolder_path='foo/beef/bar/',
            logfolder_name='foo_folder', max_files_size_b4_delete=1337.0,
            max_files_size_b4_compress=4242, logfile_prefix='log_foo')

    Und periodisch folgende Funktion aufgerufen werden:
        log_mgmt.update_log_rotate()

    Besonders hervorzuheben gilt die Methode "update_log_rotate". Wenn diese
    Aufgerufen wird, wird ueberprueft ob die stats der Files "logfolder_path +
    logfolder_name" die limits der Parameter "max_files_size_b4_delete",
    "max_files_size_b4_compress" verletzt sind.

    Sollte einer der Faelle eintreten ruft "update_log_rotate" folgende Funktionen auf:
        - die Komprimierung-Funktion "zip_compress_files()"
        - die File-remove-Funktion "remove_files()"

    Unter Zuhilfenahme folgende Hilfsfunktion:
        - determine_files_to_be_compressed()
        - determine_oldest_zip_file()
        - get_date_from_filename()
        - is_date_valid()
    """

    def __init__(self,
                 logfolder_path: str,
                 logfolder_name: str,
                 max_files_size_b4_delete: float = 100000000.0,   # Byte --> 100 MegaByte
                 max_files_size_b4_compress: float = 10000000.0,  # Byte --> 10 MegaByte
                 logfile_prefix: str = 'log'):
        """In der Init werden alle notwendigen Parameter als Klassen-Variablen
        angelegt.

        Args:
            logfolder_path (str): Der Pfad an dem sich der Logfile-Folder
                befindet
            logfolder_name (str): Name des Logfile-Folders
            max_files_size_b4_delete (float): Die Grenze in byte ab der
                zip-files geloescht werden wenn neue zip-file erstellt werden
            max_files_size_b4_compress (float): Die Grenze in byte ab der
                Logfiles komprimiert werden
            logfile_prefix (str): Alle Files muessen mit im File-namen dieses
                Prefix enthalten damit die Datums-Ausschluss- Funktion von
                Tages-aktuellen Logfiles korrekt funktioniert
        """
        self.logfolder_path = logfolder_path
        self.logfolder_name = logfolder_name
        self.max_files_size_b4_delete = max_files_size_b4_delete
        self.max_files_size_b4_compress = max_files_size_b4_compress
        self.logfile_prefix = logfile_prefix
        self.logfile_types = ['csv', 'txt', 'log']
        self.file_compress_type = 'zip'

    def update_log_rotate(self):
        """Die Methode ueberprueft zu ob die Summe aller komprimierten Files
        (Dateityp: self.file_compress_type) das Limit "max_files_size_b4_delete"
        verletzt. Sollte dies der Fall sein wird das aelterste Zip-File
        entfernt.

        Danach wird uerbprueft ob die Summe aller Logfile (Dateityp:
        self.logfile_types) das Limit "self.max_files_size_b4_compress"
        verletzt. Sollte dies der Fall sein werden alle ausser den
        tagesaktuellen Files komprimiert. Vorausgesetzt hierfuer folgender
        File-Namen-Aufbau:

            logfile_prefix + whatever +'_%Y_%m_%d_%H_%M_%S' + ...

        Sollte dieser Aufbau nicht eingehalten werden werden auch
        Tages-aktuelle Files komprimiert. Nach dem komprimieren werden die
        Original-Files entfernt.
        """
        # Der Pfad und der Ordnername werden verkettet
        log_path = os.path.join(self.logfolder_path, self.logfolder_name)

        # Wenn am Pfad "log_path" der Speicher-Bedarf der Files (mit File-typ "file_compress_type") groesser ist als
        # in Parameter "self.max_files_size_b4_delete" vorgegeben, dann ...
        if self.rec_get_size_of_files(start_path=log_path,
                                      file_type=[self.file_compress_type]) >= self.max_files_size_b4_delete:

            # Bestimme welches File aus dem Speicher entfernt werden soll
            file_2_remove = self.determine_oldest_zip_file(folder_path=log_path)

            # Entferne das bestimmte File
            self.remove_files(file_2_remove)

        # Wenn sich im Log-Ordner mehr Files als in "self.max_files_size_b4_compress" definiert sind, dann ...
        if self.rec_get_size_of_files(start_path=log_path,
                                      file_type=self.logfile_types) >= self.max_files_size_b4_compress:

            # Erstelle eine Liste mit den zu komprimierenden File's
            list_files_2_compress = self.determine_files_to_be_compressed(folder_path=log_path,
                                                                          filename_startswith=self.logfile_prefix)

            # Wenn in der Liste "list_files_2_compress" etwas eingetragen ist,  dann ...
            if len(list_files_2_compress) > 0:

                # Erstelle einen Filenamen fuer das komprimierte File
                file_name_compress = datetime.now(tz=timezone.utc).strftime(
                    log_path + 'Compressed_Logs' + '_%Y_%m_%d_%H_%M_%S')

                # Komprimiere die File's
                self.zip_compress_files(file_name=file_name_compress, path=log_path, list_files=list_files_2_compress)

                # Entferne die Original-File's
                self.remove_files(path=log_path, list_files=list_files_2_compress)

    @staticmethod
    def rec_get_size_of_files(start_path: str = '.', file_type: Union[str, List[str]] = '') -> int:
        """Diese Funktion gibt die Groesse der File's in Byte zurueck.

        Diese Funktion arbeite recursive und durchsucht am angegeben Pfad und
        dessen Unterordner. Des weiteren kann ein File-Type angegeben werden.
        Wenn mehrere File-Typen erforderlich sind, koennen diese als Liste
        uebergeben werden.

        Args:
            start_path (str): Pfad an dem (rekursiv) die groesse der File's
                bestimmt wird
            file_type (str, List[str]): Bezeichnung des File-typs als string

        Returns:
            int: groesse der File's in Byte
        """

        # Wenn der "file_type" eine Liste ist, dann ...
        if isinstance(file_type, list):
            # Wandle die Liste in einen Tupel um
            file_type = tuple(file_type)

        # Initialisiere eine lokale Variable um die Gesamt-Groesse zu erfassen
        total_size: int = 0

        # Durch-Wandere alle Ordner und Sub-Ordner
        for dir_path, dir_names, filenames in os.walk(start_path):

            # Durch-Wandere in der aktuellen Ordner/Verzeichnis-Ebene alle Datei/Files
            for file_element in filenames:

                # Kombiniere Dateinamen mit Pfad zu einen string
                str_path_file = os.path.join(dir_path, file_element)

                # Wenn das aktuelle Element keine Verknuepfung ist, dann ...
                if not os.path.islink(str_path_file):

                    # Wenn das aktuelle Element mit dem gesuchten Datei-Typ "file_type" endet
                    if str_path_file.endswith(file_type):

                        # addiere die File-Groesse vom aktuellen File auf die Gesamt-Summe auf
                        total_size += os.path.getsize(str_path_file)

        # Gibt in byte die summe aller Files zurueck
        return int(total_size)

    def get_date_from_filename(self, filename: str = '') -> tuple:
        """Diese Methode parst alle Zahlen aus den gegebenen "filename" und
        versucht den eingetragene Zeit-Stempel heraus- zu-selektieren und als
        tuple(year, month, day) zurueckzugeben.

        Damit diese Funktion korrekt arbeitet, muessen folgende Voraussetzungen erfuellt sein:
            - Der Zeit-Stempel muss wie folgt aufgebaut sein: [YEAR, MONTH, DAY,
              HOUR, MINUTE, SECOND]. Alternativ geht folgender Aufbau: [DAY,
              MONTH, YEAR, HOUR, MINUTE, SECOND]
            - Der Zeit-Stempel muss am Ende des "filename" stehen

        Args:
            filename (str): Der File-Name welcher

        Returns:
            tuple: Datum des File-Name's (YEAR, MONTH, DAY)
        """
        # Erstelle eine Liste mit allen Zahlen von "filename"
        # Ersetzte im string "filename" alle "." durch "_" der reultierende String wird
        # an den "_" geteilt/geSPLITtet. Wenn der teil-String ein Zahl darstellt wird dieser
        # nach int umgewandelt und in die Liste "list_numbers" eingetragen
        list_numbers = [int(i) for i in filename.replace('.', '_').split('_') if i.isdigit()]

        # Wenn Sechs oder mehr Zahlen im File-Namen erkannt worden sind, dann...
        if len(list_numbers) >= 6:

            # Es wird davon ausgegangen dass das Datum am Ende des File-Namen sich befindet.
            # Deswegen entferne so lange die vorderste/oberste Zahl in der Liste, bis nur noch
            # sechs Elemente uebrig sind. [YEAR, MONTH, DAY, HOUR, MINUTE, SECOND]
            [list_numbers.pop(0) for element in range(len(list_numbers)) if len(list_numbers) > 6]

            # Wenn das Datum folgendes Format [2020, 12, 31] aufweist, dann ...
            if self.is_date_valid(list_numbers[0], list_numbers[1], list_numbers[2]):
                return list_numbers[0], list_numbers[1], list_numbers[2]

            # Sonst/Wenn das Datum folgendes Format [31, 12, 2020] aufweist, dann ...
            elif self.is_date_valid(list_numbers[2], list_numbers[1], list_numbers[0]):
                return list_numbers[2], list_numbers[1], list_numbers[0]

        # Lade die aktuelle Zeit-Information
        now = datetime.now(tz=timezone.utc)

        # Da aus den File-Namen keine Zeit-Information extrahiert werden konnte, gib den aktuellen Zeit-Stempel zurueck
        return now.year, now.month, now.day

    @staticmethod
    def is_date_valid(year: int, month: int, day: int) -> bool:
        """Diese Funktion ueberprueft ein gegebenes Datum auf gueltigkeit.

        Es wird "true" zurueckgegeben wenn mit den Datum ein
        "datetime"-Object erzeugt werden, ohne eine Exception zu werfen.

        Args:
            year (int): Year [1970 ... 2042]
            month (int): Monat [1 ... 12]
            day (int): Tag [0 ... 31]

        Returns:
            bool: True, wenn das uebergebene Datum gueltig ist
        """
        try:
            # Versuche mit dem Datum ein "datetime"-Object zu erzeugen und wandle es in einen string, sollte
            # hierbei keinen Exception geworfen werden gibt die ueberpruefung auf ein Asci-Zeichen ein true zurueck
            result = str(datetime(year, month, day)).isascii()
        except ValueError:

            # Sollte eine Exception geworfen werden haandelt es sich um kein gueltiges Datum und
            # es wird "false" zurueck-gegeben.
            result = False
        return result

    @staticmethod
    def zip_compress_files(file_name: str = '', path: str = '', list_files: List[str] = '') -> None:
        """Diese Funktion komprimiert File's welche in "list_files" hinterlegt
        sind am Pfad "path". Das Komprimierte File wir mit Namen "file_name"
        erzeugt.

        Args:
            file_name (str): File-Namen der zip-Files
            path (str): Ordner an welches sich die Files aus der Liste befinden
            list_files (List[str]): Liste mit den File-Namen

        Returns:
            None: Diese Methode gibt nichts zurueck
        """
        # Wenn keine File-Namen in der Liste "list_files" vorhanden sind, dann ...
        if list_files == '':

            # Breche den Aufruf ohne Aktion ab
            return

        # Wenn es ein File mit den Namen "file_name" bereits gibt dann, ...
        if os.path.isfile(file_name):

            # Haenge ein underline-Zeichen an "file_name" an
            file_name += '_'

        # Oeffne einen Stream um das Zip-File zu schreiben
        with pyzipper.AESZipFile(file=file_name + '.zip',
                                 mode='w',
                                 compression=pyzipper.ZIP_LZMA) as zf:

            # Durch-Wandere die Liste mit den File-Namen
            for file in list_files:

                # Schreibe das File in das zip-File
                zf.write(path + file, arcname=file)

    @staticmethod
    def remove_files(path: str = '', list_files: List[str] = '') -> None:
        """Diese Methode entfernt Files aus dem File-system welche sich am Pfad
        des Parameters "path" befinden und in der List "list_files"

        Args:
            path (str):
            list_files (List[str]):
        """
        # Wenn die Liste leer ist, dann ...
        if not list_files:

            # Breche den Aufruf ohne Aktion ab
            return

        # Durch-Wandere die Liste "list_files"
        for file in list_files:

            # Entferne das File "file" aus dem File-System
            os.remove(path + file)

    def determine_files_to_be_compressed(self, folder_path: str, filename_startswith: str) -> list:
        """Diese Funktion durchsucht an einen angegebenen Pfad einen Ordner und
        gibt eine Liste mit Filenamen zurueck welche komprimiert werden sollen,

        Hierbei wird nach folgenden Schema vorgegangen:
            - Zu begin werden alle Files des Ordner in die Liste aufgenommen
            - Wenn es ein Zip-File ist dann wird es aus der Liste entfernt
            - Wenn der Filename mit "filename_startswith" anfaengt und den
              Zeitstempel des aktuellen Tage beinhaltet wird es aus der Liste
              entfernt.
            - Alle verbliebenen File-Namen in der Liste werden von der Methode
              zurueck gegeben

        Args:
            folder_path (str):
            filename_startswith (str):

        Returns:
            list: Ein Liste aus strings mit Dateinamen welche komprimiert werden
            sollen
        """
        # Initialisiere eine lokale Liste fuer den ausgenommenen File's
        list_exclude_files = []

        # Lade die aktuelle Zeit-Information
        date_tuple = datetime.now(tz=timezone.utc).utctimetuple()

        # Extrahiere das Jahr
        year = date_tuple.tm_year

        # Extrahiere das Monat
        month = date_tuple.tm_mon

        # Extrahiere den Tag
        day = date_tuple.tm_mday

        # Erstelle eine Liste mit allen vorhanden Files im Log-Ordner
        list_of_filenames = os.listdir(folder_path)

        # Durch-wandere die einzelnen Files des Log-Ordners
        for filename in list_of_filenames:

            # Wenn der File-name mit den String "self.prefix_name + self.name_id" startet, dann ...
            if filename.startswith(filename_startswith):

                # Extrahiere das Datum aus dem File-namen
                file_year, file_month, file_day = self.get_date_from_filename(filename=filename)

                # Wenn der "filename" ein Datum des aktuellen Tages aufweist, dann ...
                if file_year == year and file_month == month and file_day == day:
                    # Trage den "filename" in die Liste der nicht zu komprimierten File's ein
                    list_exclude_files.append(filename)

            # Wenn es sich um ein komprimiertes File handelt, dann ...
            elif filename.endswith('.zip'):

                # Trage den "filename" in die Liste der nicht zu komprimierten File's ein
                list_exclude_files.append(filename)

            # Wenn es sich um das "last_sendet_files" handelt, dann ...
            elif filename.endswith('.hcbloks'):

                # Trage den "filename" in die Liste der nicht zu komprimierten File's ein
                list_exclude_files.append(filename)

        return list(set(list_of_filenames) - set(list_exclude_files))

    @staticmethod
    def determine_oldest_zip_file(folder_path: str) -> str:
        """Diese Funktion gibt das aelteste zip-file eines gegebenen Pfad's
        zurueck.

        Um diese Aufgabe vollstaendig zu loesen muss in den Filenamen das
        Datum nach folgenden Schema vorhanden sein:

            Year _ Month _ Day _ Hour_ ...

        Diese Methode arbeitet nicht recursive, es werden keine Unter-Ordner
        durchsucht.

        Args:
            folder_path (str): Pfad, welcher durchsucht werden soll, nicht
                recusiv

        Returns:
            str: Filename des aeltersten zip-file
        """
        # Erstelle eine Liste mit allen vorhanden Files im Log-Ordner
        list_of_filenames = os.listdir(folder_path)

        # Bereinge die Liste damit nur Zip-Files vorhanden sind
        list_of_filenames = [filename for filename in list_of_filenames if filename.endswith('.zip')]

        # Sortiere nach Datum aufsteigend
        list_of_filenames.sort(reverse=True)

        # Gib den Filenamen des ersten (aelteste) zurueck
        return list_of_filenames.pop()


class HeaderConversion:
    """In dieser Klasse befindet sich ein dictionary welches die Zuordnung
    zwischen Hex-ID und Klartext-Namen des Logfile's definiert.
    """
    @staticmethod
    def get_dic_conversion():

        dict_conversion = {
            'Temp_Gly_Chil_out': '00000017',
            'Temp_Gly_Chil_in': '00000018',
            'Temp_Air_Chil_in': '00000019',
            'Temp_Air_Chil_out': '0000001A',
            'Temp_Gly_Phex_out': '0000001B',
            'Temp_Gly_Phex_in': '0000001C',
            'Temp_Ref_Phex_out': '0000001D',
            'Temp_Air_Cond_in': '0000001E',
            'Temp_Ref_Cond_out': '0000001F',
            'Temp_Ref_Cond_in': '00000020',
            'Temp_Ref_Phex_in': '00000021',
            'comp_on': '00000022',
            'comp_off': '00000023',
            'comp_power_limit': '00000024',
            'comp_speed': '00000025',
            'comp_volt': '00000026',
            'comp_current': '00000027',
            'comp_inverter_temperature': '00000028',
            'comp_phase_current': '00000029',
            'comp_24_over_volt': '0000002A',
            'comp_24_under_volt': '0000002B',
            'comp_overheat': '0000002C',
            'comp_overload': '0000002D',
            'comp_stall': '0000002E',
            'comp_12_over_volt': '0000002F',
            'comp_12_under_volt': '00000030',
            'fan_chill_tacho': '00000031',
            'fan_cond_tacho': '00000032',
            'press_ref_high': '00000033',
            'press_ref_low': '00000034',
            'comp_start_request': '00000035',
            'comp_rpm_set': '00000036',
            'comp_pwr_set': '00000037',
            'comp_relay': '00000038',
            'main_relay': '00000039',
            'heater_relay': '0000003A',
            'pwm_1_freq': '0000003B',
            'pump_pwm_set': '0000003C',
            'fan_chill_rpm': '0000003D',
            'fan_cond_rpm': '0000003E',
            'Temp_set': '00000050',
            'Mode': '00000053',
            # 'state_modes': '00000054'
        }

        return dict_conversion
