﻿#!/usr/bin/python3.7
# -*- coding: utf-8 -*-
import pysftp
import os
from datetime import datetime, timezone, timedelta


class FileTransferSFTP:

    def __init__(self, host: str, username: str, port: int, path_private_key: str, log_path: str,
                 tmp_file: str = './tmp'):
        self.host = host
        self.username = username
        self.port = port
        self.path_private_key = path_private_key

        self.data_picker = TransferManager(path=log_path, file_type='csv', path_temp_file=tmp_file)

    def send_files(self, path_target_folder: str, list_files_to_transfer=None):

        if list_files_to_transfer is None:
            list_files_to_transfer = self.data_picker.define_files_to_transfer()

        if list_files_to_transfer:
            cnopts = pysftp.CnOpts()
            # cnopts.hostkeys.load('/home/pi/.ssh/known_hosts')
            cnopts.hostkeys = None

            with pysftp.Connection(host=self.host, username=self.username, port=self.port,
                                   log="/tmp/ramdisk/pysftp.log",
                                   private_key=os.path.abspath(self.path_private_key), cnopts=cnopts) as sftp:

                # Wechsle im Remote-Verzeichnis
                with sftp.cd(path_target_folder):
                    for file in list_files_to_transfer:
                        sftp.put(file)

            last_sendet_file = list_files_to_transfer[-1].split('/')[-1]

            self.data_picker.write_last_sendet_filename(last_sendet_file)


class TransferManager:

    def __init__(self, path: str, file_type, path_temp_file: str = './last_transferred_file'):
        self.path = path
        self.file_type = file_type
        self.path_temp_file = path_temp_file

        self.max_files_per_call = 12

    def get_list_logfiles(self):

        filtered = filter(lambda file_name: True if file_name.endswith('csv') else False, os.listdir(self.path))

        return list(filtered)

    def determine_last_sendet_logfile(self):

        # Existiert das temporaere File (in dem das letzt gesendete File drinnen steht)
        if os.path.isfile(self.path_temp_file):

            with open(self.path_temp_file, 'r') as tmp_file:

                # Lies die erste Zeile des temporaeren File
                str_timestamp = tmp_file.readline().strip()

            time_obj = datetime.strptime(str_timestamp, "%Y-%m-%d %H:%M:%S.%f%z")

            # Schein nicht notwendig zu sein da im zeitstempel des files die zeitzone bereits inkludiert ist
            # time_obj = time_obj.replace(tzinfo=timezone.utc)

            return time_obj

        else:
            timestamp = datetime.now(tz=timezone.utc) - timedelta(minutes=30)

            str_timestamp = timestamp.strftime('%Y-%m-%d %H:%M:%S.%f%z')

            # Da es kein File gibt, erstelle ein neues
            with open(self.path_temp_file, 'w') as tmp_file:

                # Nimm den aktuellen Zeitstempel und schreibe diesen in das File
                tmp_file.write(str(str_timestamp))

            return timestamp

    def define_files_to_transfer(self):

        last_sendet_logfile = self.determine_last_sendet_logfile()

        list_files = self.get_list_logfiles()

        list_files.sort()

        list_files_to_transfer = []

        for filename in list_files:

            date_time_obj = self.extract_timestamp_from_filename(filename)

            date_time_obj = date_time_obj.replace(tzinfo=timezone.utc)

            if date_time_obj > last_sendet_logfile:
                list_files_to_transfer.append(self.path + '/' + filename)

        return list_files_to_transfer[0:self.max_files_per_call]

    @staticmethod
    def extract_timestamp_from_filename(filename):

        if filename.startswith('Logger__'):
            extract_data = [int(i) for i in filename.replace('-', '_').split('_') if i.isdigit()]

        else:
            # Erstelle eine Liste mit allen Zahlen von "filename"
            # Ersetzte im string "filename" alle "." durch "_" der reultierende String wird
            # an den "_" geteilt/geSPLITtet. Wenn der teil-String ein Zahl darstellt wird dieser
            # nach int umgewandelt und in die Liste "list_numbers" eingetragen
            extract_data = [int(i) for i in filename.replace('.', '_').split('_') if i.isdigit()]

            # Es wird davon ausgegangen dass das Datum am Ende des File-Namen sich befindet.
            # Deswegen entferne so lange die vorderste/oberste Zahl in der Liste, bis nur noch
            # sechs Elemente uebrig sind. [YEAR, MONTH, DAY, HOUR, MINUTE, SECOND]
        [extract_data.pop(0) for element in range(len(extract_data)) if len(extract_data) > 6]

        # Wandle die "datetime"-Liste in ein datetime-object
        return datetime(*extract_data[0:6])

    def write_last_sendet_filename(self, filename):

        date_time_obj = self.extract_timestamp_from_filename(filename=filename)

        # Fuege die Zeit-Zonen-Information und die Millisekunden an das "datetime"-Object an
        date_time_obj = date_time_obj.replace(tzinfo=timezone.utc).isoformat(sep=' ', timespec='milliseconds')

        with open(self.path_temp_file, 'w') as tmp_file:
            # Wandle das "datetime"-Object in eine String um und schreibe diesen in das File
            tmp_file.write(str(date_time_obj))


if __name__ == "__main__":
    data_picker = TransferManager(path='./Archive/Logfiles', file_type='csv')
    list_files = data_picker.define_files_to_transfer()

    send_obj = FileTransferSFTP(host='194.149.135.40', username='pbx', port=7384,
                                path_private_key='./SFTP_File_Transfer/id_rsa.pem',
                                log_path='./Archive/Logfiles', tmp_file='./last_transferred_file')
    send_obj.send_files(path_target_folder='/upload', list_files_to_transfer=list_files)
