#!/usr/bin/python3.7
# -*- coding: utf-8 -*-

import atexit
import multiprocessing
import os
import queue
import time
from typing import List, Any, Union, Dict

import can
from can import CyclicSendTaskABC, Bus


# /etc/can.conf
# [default]
# interface = socketcan
# channel = can0
# bitrate = 500000
#
# self.bus = can.util.load_config()


class CanBufferedReader(can.BufferedReader):

    def __init__(self, channel=None):
        """Diese Klasse ist ein Wrapper fuer "can.BufferedReader". Dies ist
        notwendig da es effizienter ist die Nachricht in Form eine Tupel in den
        Buffer (Queue) zu schieben/auszulesen.

        Args:
            channel:
        """
        can.BufferedReader.__init__(self)

        self.can_queue = multiprocessing.Queue()
        self.channel = channel

    def on_message_received(self, msg):
        """Diese Methode ueberschreibt in
        "can.BufferedReader.on_message_received"

        Args:
            msg (can.message.Message): Die Nachricht welche beim Empfangen das
                Event ausgeloest hat
        """

        # print(hex(msg.arbitration_id))
        if self.channel is msg.channel or self.channel is None:

            # Schiebe die Empfangene Nachricht als Tupel in die Queue
            self.can_queue.put((msg.channel, msg.arbitration_id, msg.dlc, msg.data, msg.timestamp))

    def get_tupel_msg(self):
        """Diese Methode gibt der aufrufenden Funktion einen Nachrichten-Tupel
        aus der Queue
        """
        try:
            # Gib eine Nachrichten-Tupel zurueck
            return self.can_queue.get()

        except queue.Empty:
            # Gib None zurueck
            return None


class CanBusBoard(multiprocessing.Process):

    dict_send_tasks: Dict[Any, CyclicSendTaskABC]
    list_bus: List[Union[Bus, Any]]

    def __init__(self, logging=None,
                 dict_transfer_in: dict = None,
                 dict_transfer_out: dict = None,
                 bus_speed=500000,
                 list_channels=None):

        """
        Args:
            logging:
            dict_transfer_in (dict):
            dict_transfer_out (dict):
            bus_speed:
            list_channels:
        """
        multiprocessing.Process.__init__(self, name="Can_Bus")

        # Sichere die Logging-Referenz
        self.logging = logging

        # Sichere die Data-Transfer-Referenz
        self.dict_transfer_in = dict_transfer_in
        self.dict_transfer_out = dict_transfer_out

        # Uebernehme die Bus-Geschwindigkeit
        self.bus_speed = bus_speed

        # Uebernehme die Listen mit den geforderten Can-Schnittstellen
        self.list_channels = list_channels

        # In dieser Liste werden alle Adressen hinterlegt welche nach Empfang weiter-verarbeitet werden sollen
        self.list_filter_id = []

        # In dieser Listen werden alle Referenzen auf die Can-Schnittstellen gehalten
        self.list_bus = []

        # In dieser Liste werden alle Referenzen auf die Listener gehalten
        self.list_listener = []

        # In dieser Liste werden alle Referenzen auf die Notifier gehalten
        self.list_notifier = []

        # In diesem Dictionary werden die Periodischen Sende-Tasks gehalten
        self.dict_send_tasks = {}

        # Bereinigung-Routine
        # atexit.register(self.cleanup_can_interfaces)

    def instantiate_can_interface(self, channel: str = 'can0', bus_speed=500000):
        """Diese Funktion sorgt dafuer dass die Can-Schnittstelle geladen ist.
        Dies geschieht mittels eines aufrufs an das Linux-OS. Hierbei wird die
        Bus-Geschwindigkeit, die Nachrichten-Buffer-Groesse und das
        Fehler-Verhalten definiert.

        Args:
            channel (str): Kanal-name zB.: 'can0', 'can1', ...
            bus_speed: Daten-Rate in Bit/sec
        """

        # Rufe den Terminal-Befehl zum initalisieren der Can-Schnittstelle
        # Bash-CMD: 'sudo ifconfig can0 txqueuelen 65500'
        #           'sudo ip link set can0 up type can bitrate 500000 restart-ms 3000'
        ret_1 = os.popen('sudo ifconfig ' + channel + ' txqueuelen 100')
        ret_2 = os.popen('sudo ip link set ' + channel + ' up type can bitrate ' + str(bus_speed) + ' restart-ms 3000')

        # Lese den Terminal-Output in die lokale Variable "output"
        output = ret_1.read()
        output += ret_2.read()

        # Wenn im "output" etwas steht, dann ...
        if output:

            # Setze eine Warnung ab dass die Schnittstelle nicht geladen werden konnte
            self.logging.warning('Error while loading ' + channel + ': %s', output)

        else:
            # Setze ein Info-Medlung das alles erfolgreich war.
            # Haenge aber totzdem "output" an --> Normalfall = empty
            self.logging.info(channel + ' successfully loaded%s', output)

    def remove_can_interface(self, channel: str = 'can0'):
        """Diese Funktion sorgt dafuer dass die Can-Schnittstelle entfernt wird

        Args:
            channel (str): Kanal-name zB.: 'can0', 'can1', ...
        """

        # Rufe den Terminal-Befehl zum entfernen der Can-Schnittstelle
        stream = os.popen('sudo ip link set ' + channel + ' down')

        # Lese den Terminal-Output in die lokale Variable "output"
        output = stream.read()

        # Wenn im "output" etwas steht, dann ...
        if output:

            # Setze eine Warnung ab dass die Schnittstelle nicht entladen werden konnte
            self.logging.warning('Error while closing ' + channel + ': %s', output)

        else:

            # Setze ein Info-Medlung das alles erfolgreich war.
            # Haenge aber totzdem "output" an --> Normalfall = empty
            self.logging.info(channel + ' successfully removed%s', output)

    def cleanup_can_interfaces(self):

        # Stoppt alle aktiven periodischen Aufgaben
        # self.list_bus[0].stop_all_periodic_tasks()

        # Stoppt alle aktiven periodischen Aufgaben und schliesse den Socket.
        # self.list_bus[0].shutdown()

        # self.list_notifier[0].remove_listener(self.list_listener[0])

        # self.remove_can_interface(channel=self.list_channels[0])
        # self.remove_can_interface(channel=self.list_channels[1])
        self.terminate()

    def transfer_data_receive(self, listener: int = 0, source: dict = None, max_loop: int = 500):
        """Diese Funktion liest so viele Eintraege aus der Queue wie der
        Parameter "max_loop" es erlaubt oder der Buffer gefuellt ist und ladet
        diese in das uebergebene Dictionary von "source"

        Args:
            listener (int): Hierbei muss der Listener der jeweiligen
                Schnittstelle angegeben werden (can0, can1, ...)
            source (dict): In diese Dictionary werden die Empfangenen
                CAN-Nachrichten rein-aktualisiert
            max_loop (int): Maximal diese Anzahl an empfangenen Nachrichten
                werden pro Zeit-schritt aus der Queue des CanBufferedReader
                verarbeitet.
        """

        # Bestimme die Anzahl an Can-Nachrichten im Empfangs-Buffer
        buffer_size = self.list_listener[listener].can_queue.qsize()

        # Wenn sic im Empfangs-Buffer mehr Nachrichten befinden als "max_loop" erlaubt, dann ...
        if buffer_size > max_loop:
            # Begrenze den Wert in "buffer_size" auf den Wert von "max_loop"
            buffer_size = max_loop

        # Mach so viele Schleifen-durchlaeufe, wie in "buffer_size" steht
        for loop in range(buffer_size):

            # Ziehe ein Nachricht aus der Queue des Empfangs-Buffer
            msg = self.list_listener[listener].get_tupel_msg()

            try:
                # Aktualisiere das "value/timestamp"-Paar in "source"
                source['data_in_' + str(msg[0])].update({str(msg[1]): {'value': msg[3], 'time': msg[4]}})

            # Diese Exception wird geworfen wenn es den geforderten key "can_id" nicht gibt
            except KeyError:
                pass

        return source

    def add_2_recv_filter(self, bus: int = 0, arbitration_id=0, mask=0x1FFFFFFF, extended=True):
        """Diese Funktion fuegt dem Empfangs-filter die Can-Adresse
        "arbitration_id" mit der Maske "mask" hinzu

        Args:
            bus (int):
            arbitration_id:
            mask:
            extended:
        """
        # Fuege der Liste "self.list_filter_id" die uebergebenen Werte hinzu
        self.list_filter_id.append({'can_id': arbitration_id, 'can_mask': mask, "extended": extended})

        # Setze in der Liste mit der Bus-Schnittstelle den Filter auf die Listen-Eintraegen von "self.list_filter_id"
        self.list_bus[bus].set_filters(self.list_filter_id)

    def reset_recv_filter(self, bus=0):
        """Diese Funktion setzt alle Empfangs-filter zurueck

        Args:
            bus:
        """
        self.list_filter_id.clear()
        self.list_bus[bus].set_filters(None)

    def filter_all_recv_msg(self, bus=0):
        """Diese Funktion filtert alle Empfangs-Nachrichten

        Args:
            bus:
        """
        self.list_bus[bus].set_filters([{'can_id': 0x0, 'can_mask': 0xFFFFFFFF}])

    def update_broadcast(self, bus: str = 'can0', source: dict = None):
        """Diese Funktion aktualisiert alle zyklischen Can-Sende-Nachrichten mit
        den Eintraege aus dem Dictionary "source" an dem key "data_out_x" zb:
        can0 --> "data_out_can0"

        Args:
            bus (str): Schnittstellen-Namen im OS
            source (dict): Dictionary mit den Sende-Daten
        """

        # Durch-wandere alle Adressen
        for msg_id in source['data_out_' + bus].keys():

            # Erstelle eine Can-Nachricht an die Ziel-Adresse "msg_id", mit den Werten aus "source"
            msg = can.Message(arbitration_id=int(msg_id, 0),
                              # is_extended_id=True,
                              dlc=8,
                              data=source['data_out_' + bus][msg_id]['value'])

            # Modifiziere die Broadcast-Message
            self.dict_send_tasks[msg_id].modify_data(msg)

    def init_broadcast(self, bus: str = 'can0'):
        """Diese Funktion initalisiert alle zyklischen Can-Sende-Nachrichten mit
        den Eintraege aus dem Manager-Dictionary "dict_transfer" an dem key
        "data_out_x" zb: can0 --> "data_out_can0"

        Args:
            bus (str): Schnittstellen-Namen im OS
        """
        # Durch-wandere alle Adressen
        for msg_id in self.dict_transfer_out['data_out_' + bus].keys():

            # self.add_2_recv_filter(bus=0, arbitration_id=int(msg_id))

            # Erstelle eine Can-Nachricht an die Ziel-Adresse "msg_id", mit den Werten aus "self.dict_transfer_out"
            msg = can.Message(arbitration_id=int(msg_id),
                              is_extended_id=True,
                              data=self.dict_transfer_out['data_out_' + bus][str(msg_id)]['value'])

            # Erstelle den periodischen Sende-Task mit der Nachricht "msg" und der Periode
            # aus dem entsprechen Eintrag im "dict_transfer" der Bus-Schnittstelle "bus"
            # an der Nachricht-Adresse "msg"
            self.dict_send_tasks[msg_id] = self.list_bus[0].send_periodic(
                msg=msg,
                period=self.dict_transfer_out['data_out_' + bus][msg_id]['time'])

    def init_can_filter(self, bus: str = 'can0'):
        """Diese Funktion fuegt dem Empfangs-filter die Can-Adresse
        "arbitration_id" mit der Maske "mask" hinzu. Hierfuer wird
        "dict_transfer" auf der can0- Schnittstelle ausgelesen. Alle vorhandenen
        Elemente werden dem Filter hinzugefuegt

        Args:
            bus (str):
        """

        # Sorge dafuer das die Liste "leer" ist
        self.list_filter_id.clear()

        # Wenn es in "self.dict_transfer_in" Eintraege vorhanden sind, dann ...
        if self.dict_transfer_in['data_in_' + bus].__len__() > 0:

            # Durch-wandere alle Adressen an der "bus"-Schnittstelle
            for msg_id in self.dict_transfer_in['data_in_' + bus].keys():
                # Fuege der Liste "self.list_filter_id" die uebergebenen Werte hinzu
                # self.list_filter_id.append({'can_id': int(msg_id), 'can_mask': 0x1FFFFFFF})
                self.add_2_recv_filter(bus=0, arbitration_id=int(msg_id.lower(), 0))

            # Setze in der Liste mit der Bus-Schnittstelle den Filter auf die Eintraegen von "self.list_filter_id"
            self.list_bus[0].set_filters(self.list_filter_id)
        #
        # else:
        #     self.filter_all_recv_msg(bus=0)

        # Gib den Process frei damit der Filter angewendet werden kann
        time.sleep(0.5)

        # Leer den Empfangs-Buffer
        self.list_bus[0].flush_tx_buffer()

        # Sorge dafuer das die Queue geleert wird
        while not self.list_listener[0].can_queue.empty():
            self.list_listener[0].can_queue.get()

    def run(self):

        # Es wird im Logging das starten des Prozesses protokolliert
        self.logging.info('Can start with PID = ' + str(self.pid.numerator))

        # Alle uebergebenen Channels werden eingerichtet
        for channel in self.list_channels:

            # Richte die Schnittstellen im Betriebsystem ein
            self.instantiate_can_interface(channel=channel, bus_speed=self.bus_speed)

            # Oeffne den "channel" und trage die Referenz in die Liste ein
            self.list_bus.append(can.interface.Bus(bustype='socketcan',
                                                   channel=channel,
                                                   bitrate=self.bus_speed))

            # Erstelle einen Listener fuer 'can0' und hange die Referenz in der Liste an
            self.list_listener.append(CanBufferedReader(channel=channel))

            # Sorge dafuer das die Queue geleert wird
            while not self.list_listener[0].can_queue.empty():
                self.list_listener[0].can_queue.get()

        # self.test_out_data()
        # self.test_in_data()

        # Starte den Notifier auf den Bus 'can0' mit den entsprechenden Listener
        self.list_notifier.append(can.Notifier(self.list_bus[0], [self.list_listener[0]]))

        # Starte den Notifier auf den Bus 'can1' mit den entsprechenden Listener
        # self.list_notifier.append(can.Notifier(self.list_bus[1], [self.list_listener[1]]))

        # Initalisiere die Empfangsfilter aus "dict-transfer" von "can0"
        self.init_can_filter(bus=self.list_channels[0])

        # Initialisiere alle zyklischen Send-Task aus "dict-transfer" von "can0"
        self.init_broadcast(bus=self.list_channels[0])

        while True:

            # Trage die Empfangen Nachrichten in "self.dict_transfer_in" ein
            self.dict_transfer_in.update(self.transfer_data_receive(listener=0, source=self.dict_transfer_in.copy()))

            # Aktualisiere den Sende-Broadcast
            self.update_broadcast(bus='can0', source=self.dict_transfer_out.copy())

            time.sleep(0.05)

    def test_out_data(self):

        """Diese Funktion erstellt zu test-zwecken Eintraege in
        "dict_trasfer['data_out_*']"

        Es handelt sich nicht um eine Funktion welche im regulaeren Betrieb
        zum Einsatz kommt
        """

        msg_can = []
        channel = 'can0'
        can_id = {'HVAC_C05': 0x10000000}
        test_loop = 100

        dict_transfer = self.dict_transfer_out.copy()

        for counter in range(test_loop):
            msg_can.append(can.Message(arbitration_id=can_id['HVAC_C05'] + counter,
                                       is_extended_id=True,
                                       data=[int(counter % 256),
                                             0x00, 0x00, 0x00,
                                             0x00, 0x00, 0x00, 0x00]))

            # Trage die Adresse und die Zykluszeit ein
            dict_transfer['data_out_' + str(channel)].update(
                {str(msg_can[-1].arbitration_id): {'value': msg_can[-1].data, 'time': 0.1}})

        self.dict_transfer_out.update(dict_transfer)

    def test_in_data(self):
        """Diese Funktion erstellt zu test-zwecken Eintraege in
        "dict_trasfer['data_in_*']"

        Es handelt sich nicht um eine Funktion welche im regulaeren Betrieb
        zum Einsatz kommt
        """
        msg_can = []
        channel = 'can0'
        can_id = {'HVAC_C05': 0x14FF0144}
        test_loop = 2

        dict_transfer = self.dict_transfer_in.copy()

        for counter in range(test_loop):
            msg_can.append(can.Message(arbitration_id=can_id['HVAC_C05'] + counter,
                                       is_extended_id=True,
                                       data=[int(counter % 256),
                                             0x00, 0x00, 0x00,
                                             0x00, 0x00, 0x00, 0x00]))

            # Trage die Adresse und die Zykluszeit ein
            dict_transfer['data_in_' + str(channel)].update(
                {str(msg_can[-1].arbitration_id): {
                    'value': msg_can[-1].data,
                    'time': 0.1}})

        self.dict_transfer_in.update(dict_transfer)


class ProcessDataHandler:

    def __init__(self, dict_transfer_par: dict, dict_transfer_in: dict, dict_transfer_out: dict):

        """
        Args:
            dict_transfer_par (dict):
            dict_transfer_in (dict):
            dict_transfer_out (dict):
        """
        self.dict_transfer_par = dict_transfer_par
        self.dict_transfer_in = dict_transfer_in
        self.dict_transfer_out = dict_transfer_out

        local_dict_in = self.dict_transfer_in.copy()
        local_dict_out = self.dict_transfer_out.copy()

        # Durch-wandere alle Key's der ersten Ebene von dem Parameter-Dictionary
        for interface in dict_transfer_par.keys():

            # Wenn der Parameter die Teil-Strings 'can' und 'in' beinhaltet, dann ...
            if 'can' in interface and 'in' in interface:

                # Durch-wandere alle Can-ID's im Sub-Parameter-Dictionary
                for msg_id in dict_transfer_par[interface].keys():

                    # Erzeuge einen Eintrag im "dict_transfer_in" damit die Adresse im
                    # Can-Filter beim initialisieren der Can-Schnittstelle eingetragen wird
                    local_dict_in[str(interface)].update({str(msg_id): {'time': 0.0, 'value': 0.0}})

                    # Durch-wandere alle Prozess-Daten-Namen
                    for data_name in dict_transfer_par[interface][msg_id].keys():

                        # # Lade den Prozess-Daten-Namen welcher aus 'msg_id' extrahiert
                        # # werden konnten in das Prozess-Eingangs-Daten-Dictionary
                        # local_dict_in['data_in_process_values'].update({data_name: 0.0})

                        conversion = dict_transfer_par[interface][msg_id][data_name]
                        datagram = [0] * 8

                        # Lade den Prozess-Daten-Namen welcher aus 'msg_id' extrahiert
                        # werden konnten in das Prozess-Eingangs-Daten-Dictionary
                        local_dict_in['data_in_process_values'].update({data_name: eval(conversion)})



            # Wenn-sonst der Parameter die Teil-Strings 'can' und 'out' beinhaltet, dann ...
            elif 'can' in interface and 'out' in interface:

                # Durch-wandere alle Can-ID's im Sub-Parameter-Dictionary
                for msg_id in dict_transfer_par[interface].keys():

                    # Erzeuge einen Eintrag im "dict_transfer_out" damit die Adresse im
                    # Can-Broadcast beim initialisieren der Can-Schnittstelle eingetragen wird
                    local_dict_out[str(interface)].update(
                        {str(msg_id): {'time': dict_transfer_par[interface][msg_id]['time'],
                                       'value': dict_transfer_par[interface][msg_id]['init_value']}})

                    # Durch-wandere alle Prozess-Daten-Namen
                    for data_name in dict_transfer_par[interface][msg_id].keys():

                        # Lade den Prozess-Daten-Namen welcher aus 'msg_id' extrahiert
                        # werden konnten in das Prozess-Ausgangs-Daten-Dictionary
                        local_dict_out['data_out_process_values'].update({data_name: 0})

        # Update den Inhalt des aktualisierten lokalen Dictionary dem Transfer-Dictionary
        dict_transfer_in.update(local_dict_in.copy())
        dict_transfer_out.update(local_dict_out.copy())

    @staticmethod
    def update_process_data_in(local_dict_in, local_dict_par):

        # Durch-wandere alle Key's der ersten Ebene von dem Parameter-Dictionary
        """
        Args:
            local_dict_in:
            local_dict_par:
        """
        for interface in local_dict_in.keys():

            # Wenn das Interface die Teil-Strings 'can' und 'in' beinhaltet, dann ...
            if 'can' in interface and 'in' in interface:

                # Durch-wandere alle Empfangenen Can-Nachrichten
                for can_msg, data in local_dict_in[interface].items():

                    # Lade die Daten von der Can-Message in die lokale Variable "datagram"
                    # --> "eval(conversion)" greift auf diese Information zu
                    datagram = data['value']

                    # Wenn es fuer die Can-Adresse in Parameter-Dictionary einen Eintrag gibt, dann ...
                    if can_msg in local_dict_par[interface]:

                        # Durch-wandere das Parameter-Dictionary und hole den Prozess-Daten-Namen
                        # und seine zugewiesene Umrechnung
                        for par_name, conversion in local_dict_par[interface][can_msg].items():

                            try:
                                # Versuche die Umrechnung zu machen und weise das Ergebnis
                                # den Eingags-Daten-Dictionary im Prozess-Daten-Teil zu
                                local_dict_in['data_in_process_values'][par_name] = eval(conversion)

                            except TypeError:
                                pass
        # Update den Inhalt des aktualisierten lokalen Dictionary dem Transfer-Dictionary
        return local_dict_in.copy()

    @staticmethod
    def update_process_data_out(local_dict_out: dict, local_dict_par: dict):

        # Durch-wandere alle Key's der ersten Ebene von dem Ausgangs-Daten-Dictionary
        """
        Args:
            local_dict_out (dict):
            local_dict_par (dict):
        """
        for interface in local_dict_out.keys():

            # Wenn das Interface die Teil-Strings 'can' und 'out' beinhaltet, dann ...
            if 'can' in interface and 'out' in interface:

                # Durch-wandere alle Sende-Can-Nachrichten
                for can_id in local_dict_out[interface].keys():

                    # Erstelle eine lokale Liste, jede Stelle entspricht den entsprechenden Daten-Byte
                    # in der Can-Nachricht
                    datagram = [0] * 8

                    for data_name, conversion in local_dict_par[interface][can_id].items():

                        # Wenn sich bei "data_name" nicht um "time" oder "init_value" handelt, dann ...
                        if 'time' not in data_name and 'init_value' not in data_name:

                            # Erstelle eine lokale Variable mit den Namen der Prozess-Ausgangs-Variable und
                            # dem aktuellen Wert aus diesem
                            exec(data_name + ' = ' + str(local_dict_out['data_out_process_values'][data_name]))

                            # Fuehre die Konvertierung durch und weise das Ergebnis "datagram" zu
                            exec(conversion)

                    # Weise "datagram" dem Ausgangs-Daten-Dictionary zu
                    local_dict_out[interface][can_id]['value'] = datagram

        # Update den Inhalt des aktualisierten lokalen Dictionary dem Transfer-Dictionary zu
        return local_dict_out.copy()
