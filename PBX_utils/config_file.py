#!/usr/bin/python3.7
# -*- coding: utf-8 -*-

import json


class ConfigFile:

    def __init__(self, logging=None, path: str = './', dict_transfer: dict = None, default_content: dict = None):

        # Uebernehme den Pfad wo das Configuration-File im Dateisystem liegen soll
        """
        Args:
            logging:
            path (str):
            dict_transfer (dict):
            default_content (dict):
        """
        self.path = path

        # Uebernehme das Dictionary in welchen die Configuration hinterlegt ist
        self.dict_transfer = dict_transfer

        # Definiere den Filenamen des Config-File
        self.file_name = 'hcbloks.conf'

        self.logging = logging

        self.default_content = default_content

    def write_file(self):
        """Diese Funktion schreibt den Inhalt aus "dict_transfer" in ein File
        nach dem .json-Format
        """
        # Oeffne das File im Schreibmodus
        with open(self.path + self.file_name, "w") as file_tmp:

            # Schreibe das Dictionary in das File im .json-Format
            file_tmp.write(json.dumps(self.dict_transfer.copy(), indent=5))

    def read_file(self):
        """Diese Funktion liesst den Inhalt des Configuration-Files in das
        Dictionary "dict_transfer" ein. Es werden dabei die Faelle
        "FileNotFound" und "FileIsCorrupted" beim auftretten dahingehend
        behandelt das ein "default-file" erstellt wird.
        """
        # Verwende try falls file open/read in die Hose geht
        try:
            # Oeffne das File im Lesemodus
            with open(self.path + self.file_name, "r") as file_tmp:

                # Update das Dictionary mit dem Inhalt des Config-File
                self.dict_transfer.update(json.load(file_tmp))

                # Gib eine Info-Meldung aus dass das Lesen des Config-Files positiv war
                self.logging.info('Config-file could be read successfully')

        except FileNotFoundError:

            # Gib ein Warning aus dass das Config-File nicht vorhanden ist
            self.logging.warning('Config-file could not be read --> creat new config-file')

            # Erstelle ein neues Config-File
            self.default_config_file()

        except json.decoder.JSONDecodeError:

            # Gib ein Warning aus dass das Config-File beschaedigt ist
            self.logging.warning('Config-file is corrupted --> creat new config-file')

            # Erstelle ein neues Config-File
            self.default_config_file()

    def default_config_file(self):
        """Diese Funktion initalisiert einen default-Inhalt in "dict_transfer"
        und ruft dann die "write_file"-Methode auf um ein neues Config-File zu
        erstellen.
        """

        try:
            # Weise die default-config dem Dictionary zu
            self.dict_transfer.update(self.default_content)

            # Schreibe die Configuration ins File
            self.write_file()

            # Gib eine Info-Meldung aus dass das Schreiben des Config-Files positiv war
            self.logging.info('New config-file successfully created')

        except TypeError:
            # Gib eine Info-Meldung aus dass das Lesen des Config-Files positiv war
            self.logging.info('No default-Configuration are available - '
                              'Config-File are not created')
