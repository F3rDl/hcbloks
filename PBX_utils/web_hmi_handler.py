#!/usr/bin/python3.7
# -*- coding: utf-8 -*-

import os
import json
from json.decoder import JSONDecodeError

from PBX_utils.pbx_misc import deep_update


class WebHmiHandler:

    def __init__(self, dict_tarnsfer_par):

        """
        Args:
            dict_tarnsfer_par:
        """
        self.dict_transfer_par = dict_tarnsfer_par

        self.file_change_time = 0.0

        self.path = '/tmp/ramdisk/'
        self.file_name_mosi = '.tansfer_hmi_mosi'
        self.file_name_miso = '.tansfer_hmi_miso'

        self.inputs = {'op_state_set': False,
                       'temp_set': 0.0}

        self.outputs = {'op_state_act': False,
                        'temp_act': 0.0}

    def update(self, dict_transfer_in, dict_transfer_par):

        # Versuche ...
        """
        Args:
            dict_transfer_in:
            dict_transfer_par:
        """
        try:
            # Die File-Change-Time auszulesen
            act_file_change_time = os.path.getctime(self.path + self.file_name_miso)

        except FileNotFoundError:

            # Sollte der Versuch misslungen sein, setzte den Zeit-stempel auf Null
            act_file_change_time = 0.0

        # Wenn das File neuer ist als der gesicherte Zeitstempel
        if self.file_change_time < act_file_change_time:

            # Sichere die File-Change-Time
            self.file_change_time = act_file_change_time

            # Lesen vom HMI-Input-File
            self.read_from_miso_file()

            # Wenn die vom File gelesene Operation-Sate den Zustand "OFF" haben, dann ...
            if self.inputs['op_state_set'] is False:

                # Schreibe ein 0.0 in das "self.dict_transfer_par"
                deep_update(self.dict_transfer_par, {'data_par_gui': {'Mode': 0.0}})

            else:
                # Schreibe ein 1.0 in das "self.dict_transfer_par"
                deep_update(self.dict_transfer_par, {'data_par_gui': {'Mode': 1.0}})

            # Schreibe die gelesene Set-Temperatur in das "self.dict_transfer_par"
            deep_update(self.dict_transfer_par, {'data_par_gui': {'Temp_set': self.inputs['temp_set']}})

        # Wenn im "self.dict_transfer_par" der Operation-Sate den Zustand "OFF" hat, dann ...
        if dict_transfer_par['data_par_gui']['Mode'] == 0.0:

            self.outputs['op_state_act'] = False
        else:
            self.outputs['op_state_act'] = True

        try:
            self.outputs['temp_act'] = dict_transfer_in['data_in_process_values']['Temp_Air_Chil_in']
        except KeyError:
            self.outputs['temp_act'] = 1337.0

        self.write_to_mosi_file()

    def write_to_mosi_file(self):

        # Oeffne das File im Schreib-modus
        with open(self.path + self.file_name_mosi, "w") as file_tmp:

            # Schreibe das Dictionary in das File im .json-Format
            file_tmp.write(json.dumps(self.outputs.copy(), indent=5))

    def read_from_miso_file(self):

        try:
            # Oeffne das File im Lese-modus
            with open(self.path + self.file_name_miso, "r") as file_tmp:

                # Update das Dictionary mit dem Inhalt des Config-File
                self.inputs.update(json.load(file_tmp))

        except FileNotFoundError:
            pass

        except JSONDecodeError:
            pass
