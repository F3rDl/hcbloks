#!/usr/bin/python3.7
# -*- coding: utf-8 -*-

import os
import time
import queue
import platform
import threading
import multiprocessing
from typing import Optional


class OneWireTempSensor(multiprocessing.Process):
    """Die Klasse erstellt threads fuer einen jenden Sensor. Die einzelnen
    threads lesen zyklisch die Werte der Sensoren aus und stellen diese
    """

    dict_transfer: Optional[dict]

    def __init__(self, logging=None):
        """Die 1w-Schnittstelle wird initalisiert

        Args:
            logging:
        """
        multiprocessing.Process.__init__(self, name="One_Wire")

        # Sichere die Logging-Referenz
        self.logging = logging

        self.dic_1w = {}
        self.threads = []
        self.out_queue = queue.Queue()
        self.sensor_thread_name_prefix = 'OneWire_read_'
        self.path_sensor_device = '/sys/bus/w1/devices/'
        self.path_sensor_listing = '/sys/devices/w1_bus_master1/w1_master_slaves'

        # Sorge dafuer das der Treiber fuer den DS2482 geladen ist
        self.instantiate_ds2482()

        self.dict_com = multiprocessing.Manager().dict()

    def instantiate_ds2482(self):
        """Diese Funktion sorgt dafuer das der Treiber fuer den DS2482 geladen
        ist
        """
        # Wenn es sich um ein Linux-OS handelt dann, ...
        if "Linux" == platform.system():

            # Rufe den Terminal-Befehl zum laden des Treibers
            stream = os.popen('sudo modprobe ds2482')

            # Lese den Terminal-Output in die lokale Variable "output"
            output = stream.read()

            # Wenn im "output" etwas steht, dann ...
            if output:

                # Setze eine Warnung ab dass der Treiber nicht geldaden werden konnte
                self.logging.warning('Error while loading driver ds2482: %s', output)

            else:

                # Setze ein Info-Medlung das alles erfolgreich war.
                # Haenge aber totzdem "output" an --> Normalfall = empty
                self.logging.info('Driver ds2482 successfully loaded%s', output)

    def get_sensor_ids_from_threads(self):
        """Diese Funktion list die Namen der 1w-threads welche schon einen
        Lese-thread besitzen aus und gibt die ID's als Liste der aufrufenden
        Funktion zurueck

        Returns:
            list: Eine Liste mit 1w-ID's welche bereits einen Lese-thraed
            besitzen
        """
        # Initalisiere eine Leere lokale Liste
        sensor_names = []

        # Durchwandere die Liste mit den thread-Objecten
        for thread_id in self.threads:
            # Holle den namen vom thread und haenge in an die Liste "sensor_names"
            sensor_names.append(thread_id.getName()[len(self.sensor_thread_name_prefix):])

        return sensor_names

    def check_for_new_sensors(self):
        """Diese Funktion ueberprueft ob neue Sensoren ab 1-w-Bus hinzugekommen
        sind.

        Dazu wird die Liste der bereits eingetragenen Sensoren und mit der
        Liste in welcher sich alle derzeit verfuegbaren Sensoren befinden
        verglichen.

        Sollte festgestellt werden dass ein Sensor verfuegbar ist welcher
        noch keinen Eintag/thread hat wird fuer diesen Sensor ein Eintrag
        erstellt und ein Lese-thread gestartet
        """
        # Lade von allen aktiven threads die ID's der Sensoren als Liste in "sensor_names"
        sensor_names = self.get_sensor_ids_from_threads()

        # Lade alle aktiven 1w-Sensoren welche am Bus fehlerfrei ansprechbar sind
        w1_slaves = self.get_sensor_ids_from_os(self.path_sensor_listing)

        # Durchwandere die Liste "w1_slaves"
        for w1_slave in w1_slaves:

            # Wenn es sich bei "w1_slave" um einen Temperatur-Sensor handelt, dann ...
            if w1_slave.startswith("28-") or w1_slave.startswith("10-"):

                # Wenn fuer diese ID noch kein thread gestartet wurde
                if w1_slave not in sensor_names:
                    # Erstelle eine thread mit der Aufgabe den Sensor "w1_slave" auszulesen
                    # und das Ergebniss in die queue "self.out_queue" zurueck-zuschreiben
                    self.threads.append(threading.Thread(name=self.sensor_thread_name_prefix + str(w1_slave),
                                                         target=self.read_temp,
                                                         args=(w1_slave, self.out_queue)))

                    # Starte den Thread --> letztes Element der Liste
                    self.threads[-1].start()

                    # Setze eine Infomeldung ab das ein neuer thread gestartet wurde
                    self.logging.info('Thread for Sensor %s was started', w1_slave)

    def check_for_lost_sensors(self):
        """Diese Funktion ueberprueft ob der jeweilige Sensor-thread noch aktiv
        ist, falls das nicht der Fall sein sollte, wird sein Eintrag aus den
        jeweiligen Listen ausgetragen und eine Warning-Meldung abgesetzt
        """
        # Durchwandere alle "thread" in "threads"-List
        for thread in self.threads:

            # Wenn der thread nicht lebt
            if not thread.is_alive():
                # Entfernen seinen Eintrag aus dem Dictionary mit den ID/Temperatur-Wertepaaren
                del self.dic_1w[thread.getName()[len(self.sensor_thread_name_prefix):]]

                # Entferne ihn aus der Thread-List
                self.threads.remove(thread)

                # Setze eine Info-Meldung ab das ein neuer thread gestartet wurde
                self.logging.warning('Thread for Sensor %s was removed', thread)

    def __get_values_in_dict_form__(self):
        """Diese Funktion liest die Queue aus welche von den einzelnen
        Sensor-Threads befuellt wird und gibt die Werte

        Returns:
            dict: Dictionary mit dem key "1w-ID" und als value wiederum ein
            Dictionary welches aus den Elemrnten 'value': Temperatur-Wert und
            'time': Aufzeichnung-Zeitpunkt besteht
        """

        # Erstelle ein lokales Dictionary in welches die Werte der Sensor-threads geschrieben werden
        dic_1w = {}

        # Aktualisiere aus den Queues auf das Dictionary "dic_1w" solange
        # bis in der Queue nichts mehr vorhanden ist
        while not self.out_queue.empty():
            # Lese den Tupel von der Queue in "sensor_data"
            sensor_data = self.out_queue.get_nowait()

            # Erzeuge aus den Tupel einen Dictionary-Eintrag und update diesen in ins lokale "dic_1w"
            dic_1w.update({sensor_data[0]: {'value': sensor_data[1],
                                            'time': sensor_data[2]}})

        # Gib das lokale Dictionary der aufrufen Funktion zurueck
        return dic_1w

    def run(self):
        """Hauptschleife des 1w-Prozesses :return: :rtype:"""

        # Melde das starten vom Prozess mit seiner PID im Event-File
        self.logging.info('1w start with PID = ' + str(self.pid.numerator))

        # Endlosschleife
        while True:

            # Ueberpruefe ob es neue Sensoren am 1w-Bus aufgetaucht
            # sind und starte einen thread fuer diese
            self.check_for_new_sensors()

            # Ueberpruefe ob Sensor-threads sich beendet haben weil der
            # der im zugewiesene Sensor keine neuen Werte liefert
            self.check_for_lost_sensors()

            # Schiebe die Werte von der Queue in "self.dic_1w"
            self.dic_1w.update(self.__get_values_in_dict_form__())

            # Wenn "self.dict_transfer_in" ein "DictProxy" ist, dann ...
            # if isinstance(self.dict_transfer_in, type(multiprocessing.Manager().dict())):

            self.dict_com.update(self.dic_1w)

            # Schicke den process fuer 1 sec schlafen
            time.sleep(1.0)

    def get_raw_data(self):
        """Mittels dieser Funktion kann auf die Roh-werte zugegriffen werden
        :return: Dictionary mit den Roh-werten :rtype: dict
        """
        return self.dict_com

    def get_converted_data(self, dic_translation: dict):

        # Erstelle ein lokales Dictionary
        """
        Args:
            dic_translation (dict):
        """
        local_dict_in = {}

        # Erstelle eine lokale Liste mit den ID's fuer die es Mess-Werte gibt
        list_existing_ids = self.dict_com.keys()

        # Durch-wandere das Dictionary "dic_translation"
        for process_1w_id, sub_dict in dic_translation.items():

            # Wenn es fuer die 1w-Id ("process_1w_id") einen Eintrag in der Liste ("list_existing_ids")
            # der verfuegbaren Sensoren gibt, dann ...
            if process_1w_id in list_existing_ids:

                # Lade den Prozess-Namen aus "dic_translation"
                process_name = list(sub_dict.keys())[0]

                # Lade den Prozess-Umrechnung aus "dic_translation"
                process_conversion = list(sub_dict.values())[0]

                # Lade den Raw-Wert (Temperatur-Wert) aus "self.dict_com" am key "process_1w_id"
                datagram = self.dict_com[process_1w_id]['value']

                # Wenn der Raw-Wert den Daten-Typ "float" aufweist
                if isinstance(datagram, float):
                    try:
                        # Versuche die Umrechnung zu machen und weise das Ergebnis den lokalen Dictionary zu
                        local_dict_in[process_name] = eval(process_conversion)

                    except (TypeError, SyntaxError):
                        pass

        # Gib der aufrufenden Funktion das erstellte Process-Daten-Dictionary zurueck
        return local_dict_in

    @staticmethod
    def transfer_data(source: dict, sink: dict, signal_properties: dict):

        # Gehe alle 1w-Sensoren (Adressen, Sensor-Werte) welche das 1w-Modul lesen konnte, durch
        """
        Args:
            source (dict):
            sink (dict):
            signal_properties (dict):
        """
        for adr, val in source.items():

            # Erstelle eine Liste mit allen ID's der Sensoren welche im "sink"-Dictonary bereits vorhanden sind
            # id_list = [key for key in sink['inputs']['1w']]

            # Wenn in der id_list die gesuchte ID "adr" vorhanden, dann ...
            # if adr in id_list:
            if adr in [key for key in sink]:

                # Aktualisiere den Value
                sink[adr].update({'value': val, 'time': time.time()})

            else:
                # Lege den 1w-Sensor mit der Adresse "adr" und dem Wert "val" im Sink-Dictionary an
                sink.update({adr: signal_properties})
                sink[adr].update({'source_address': str(adr),
                                  'source': '1w'})

        return sink

    @staticmethod
    def read_temp(w1_slave: str, q_out: queue):

        """
        Args:
            w1_slave (str):
            q_out (queue):
        """
        try:
            while True:

                # dic_1w = {}
                # spare_value = 'no_value'
                spare_value = 1337.0

                # Oeffne das 1-wire-Slave-File im Lesemodus
                with open('/sys/bus/w1/devices/' + w1_slave + '/w1_slave') as file:

                    # Lesen den kompletten Inhalt des Files in "content"
                    content = file.read()

                # Wenn in "content" am Ergebnis des CRC-Checks "YES" steht, dann ...
                # if content.split('\n')[0].split('=')[1].split(' ')[1] == 'YES':
                if 'YES' in content:

                    # Temperaturwert aut "content" selektieren --> 2.Zeile --> "space" --> "=" --> "temperature"
                    # temperature = content.split("\n")[1].split(' ')[9].split('=')[1]
                    temperature = content.rstrip().split('=')[2]

                    # Wandle in einen float mit Komma-Zeichen an der richtigen Position fuer Grad Celsius
                    # und schiebe den Wert in das lokale Dictionary mit dem key der 1w-Adresse
                    # dic_1w[w1_slave] = float(temperature) / 1000
                    q_out.put((w1_slave, float(temperature) / 1000, time.time()))

                else:

                    # Wenn der CRC-Check negativ war schiebe "no value" in das lokale Dictionary
                    # dic_1w[w1_slave] = "no_value"
                    q_out.put((w1_slave, spare_value, time.time()))

                # Schicke den process fuer 0.2 sec schlafen
                time.sleep(0.20)

        except (FileNotFoundError, OSError):

            # Beende den thread
            return

    @staticmethod
    def get_sensor_ids_from_os(path: str):

        """
        Args:
            path (str):
        """
        content = []
        try:
            # Lese Adressen der 1-wire-Teilnehmer aus filesystem
            with open(path) as file:

                # Entferne das "\n"-Zeichen
                for line in file:
                    content.append(line.strip())

            return content
        except FileNotFoundError:
            return []


class ProcessDataHandler:
    """ProcessDataHandler transferriert die Daten in das
    Base-Manager-Dictionary
    """

    def __init__(self, dict_transfer_par: dict, dict_transfer_in: dict):
        """Die Init-Funktion :param dict_transfer_par: :type dict_transfer_par:
        dict :param dict_transfer_in: :type dict_transfer_in: dict

        Args:
            dict_transfer_par (dict): Das Dictionary mit den Parameter
            dict_transfer_in (dict): Das Dictionary mit den Eingangsdaten
        """
        self.dict_transfer_par = dict_transfer_par
        self.dict_transfer_in = dict_transfer_in

        local_dict_in = self.dict_transfer_in.copy()

        # Durch-wandere alle Key's der ersten Ebene von dem Parameter-Dictionary
        for interface in dict_transfer_par.keys():

            # Wenn der Parameter die Teil-Strings '1w' und 'in' beinhaltet, dann ...
            if '1w' in interface and 'in' in interface:

                # Durch-wandere alle 1w-ID's im Sub-Parameter-Dictionary
                for msg_id in dict_transfer_par[interface].keys():

                    # Erzeuge einen Eintrag im "dict_transfer_in" damit die Adresse im
                    # 1w-Filter beim initialisieren der 1w-Schnittstelle eingetragen wird
                    local_dict_in[str(interface)].update({str(msg_id): {'time': 0.0, 'value': 0.0}})

                    # Durch-wandere alle Prozess-Daten-Namen
                    for data_name in dict_transfer_par[interface][msg_id].keys():
                        # Lade den Prozess-Daten-Namen welcher aus 'msg_id' extrahiert
                        # werden konnten in das Prozess-Eingangs-Daten-Dictionary
                        local_dict_in['data_in_process_values'].update({data_name: 0.0})

        # Update den Inhalt des aktualisierten lokalen Dictionary dem Transfer-Dictionary
        dict_transfer_in.update(local_dict_in.copy())

    @staticmethod
    def update_process_data_in(local_dict_in: dict, local_dict_par: dict):
        """
        Args:
            local_dict_in (dict):
            local_dict_par (dict):
        """
        # Durch-wandere alle Key's der ersten Ebene von dem Data-Input-Dictionary
        for interface in local_dict_in.keys():

            # Wenn das Interface die Teil-Strings '1w' und 'in' beinhaltet, dann ...
            if '1w' in interface and 'in' in interface:

                # Durch-wandere alle 1w-Typen
                for sub_sys, data in local_dict_in[interface].items():

                    # Lade die Daten vom Sub-System in die lokale Variable "datagram"
                    datagram = data['value']

                    # Wenn es fuer das Subsystem in Parameter-Dictionary einen Eintrag gibt, dann ...
                    if sub_sys in local_dict_par[interface]:

                        # Durch-wandere das Parameter-Dictionary und hole den Prozess-Daten-Namen
                        # und seine zugewiesene Umrechnung
                        for par_name, conversion in local_dict_par[interface][sub_sys].items():

                            try:
                                # Versuche die Umrechnung zu machen und weise das Ergebnis
                                # den Eingags-Daten-Dictionary im Prozess-Daten-Teil zu
                                local_dict_in['data_in_process_values'][par_name] = eval(conversion)
                                # print(local_dict_in['data_in_process_values'][par_name])
                            except (TypeError, SyntaxError):
                                pass

        # Update den Inhalt des aktualisierten lokalen Dictionary dem Transfer-Dictionary zu
        return local_dict_in.copy()
