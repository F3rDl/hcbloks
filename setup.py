from setuptools import setup

setup(
    name='HCBloks',
    version='2.0.3',
    packages=['PBX_utils'],
    url='https://github.com/F3rDl/HCBloks',
    license='',
    author='ferdl',
    author_email='ferdinand.nittnaus@productbloks.com',
    description='Controller-App HC-Bloks for Linux',
    install_requires=['pyzipper',
                      'plotly',
                      'dash',
                      'Flask',
                      'psutil',
                      'numpy',
                      'matplotlib',
                      'future',
                      'transitions',
                      'pygraphviz',
                      'setuptools',
                      'Pillow',
                      'python-can',
                      'crcmod',
                      'spidev',
                      'pysftp']
)
