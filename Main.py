#!/usr/bin/python3.7
# -*- coding: utf-8 -*-

import os
import sys
import time
import logging
import datetime
import platform
import pprint
import psutil
import gc

if "Linux" == platform.system():
    import multiprocessing
else:
    import multiprocess as multiprocessing

from PBX_utils import pbx_misc
from PBX_utils import config_file
from PBX_utils import one_wire
from PBX_utils import cyclic_timer
from PBX_utils import canbus
from PBX_utils import gui
from PBX_utils import monarco_board
from PBX_utils import data_logging
from PBX_utils.data_logging import HeaderConversion
from PBX_utils import statemachine
from PBX_utils import web_hmi_handler

# Es wird aus dem "logging"-Modul ein Ereignisprotokollierungssystem initalisiert
logging.basicConfig(format='%(asctime)s: %(filename)s %(message)s',
                    level=logging.INFO, filename='/tmp/ramdisk/hcbloks.log')
# logging.basicConfig(format='%(asctime)s: %(processName)s: %(threadName)s: %(message)s', level=logging.INFO)


class HCBloksMain:

    # In diesem Dictionary befinden sich die Initial-Entry's fuer dict_transfer_in
    dict_transfer_in_default = {'data_in_1w':   {},
                                'data_in_can0': {},
                                'data_in_can1': {},
                                'data_in_io':   {},
                                'data_in_process_values': {}}

    dict_transfer_out_default = {'data_out_can0': {},
                                 'data_out_can1': {},
                                 'data_out_io':   {},
                                 'data_out_process_values': {
                                     # 'state_modes': []
                                 }}

    dict_transfer_par_default = {'gui': {},
                                 'paths': {}}

    dict_par_default = {'gui': {'general': {'pic_zoom': 1.4,
                                            'font_size_value_text': 19},
                                'signal_properties': {}},
                        'paths': {'path_app': '',
                                  'path_ram_disk': '/tmp/ramdisk/',
                                  'path_external_memory': '',
                                  'path_pic_process_diagram': './pic.jpg'},
                        'data_in_1w': {},
                        'data_in_can0': {
                            '352256324': {
                                'comp_on': 'bool(datagram[1] & 0x10)',
                                'comp_off': 'not bool(datagram[1] & 0x10)',
                                'comp_power_limit': 'bool(datagram[1] & 0x20)',
                                'comp_speed': 'datagram[2] * 50',
                                'comp_volt': 'datagram[3] / 2',
                                'comp_current': 'datagram[4] / 2',
                                'comp_inverter_temperature': 'datagram[5] - 50.0',
                                'comp_phase_current': 'datagram[6] / 2',
                                'comp_24_over_volt': 'bool(datagram[7] & 0x1)',
                                'comp_24_under_volt': 'bool(datagram[7] & 0x2)',
                                'comp_overheat': 'bool(datagram[7] & 0x4)',
                                'comp_overload': 'bool(datagram[7] & 0x10)',
                                'comp_stall': 'bool(datagram[7] & 0x20)',
                                'comp_12_over_volt': 'bool(datagram[7] & 0x40)',
                                'comp_12_under_volt': 'bool(datagram[7] & 0x80)'}
                        },
                        'data_out_can0': {
                            '352256281': {
                                'time': 0.5,
                                'init_value': 0,
                                'comp_start_request':
                                    'datagram[1] = 0x10 if comp_start_request == True else 0x00',
                                'comp_rpm_set':
                                    'datagram[2] = int(0 if comp_rpm_set < 700 '
                                    'else 5000/50 if comp_rpm_set > 5000 '
                                    'else comp_rpm_set/50)',
                                'comp_pwr_set':
                                    'datagram[3] = int(0 if comp_pwr_set < 0.04 '
                                    'else 10.0/0.04 if comp_pwr_set > 10 '
                                    'else comp_pwr_set/0.04)'}
                        },

                        'data_in_io': {
                            'counter_1_val': {
                                'fan_chill_tacho': 'datagram'},
                            'counter_2_val': {
                                'fan_cond_tacho':  'datagram'},
                            'analog_in_1': {
                                'press_ref_high':  'datagram'},
                            'analog_in_2': {
                                'press_ref_low':   'datagram'}
                        },

                        'data_out_io': {
                            'digital_output_1': {
                                'heater_relay':  'datagram[0] = bool(heater_relay)'},
                            'digital_output_3': {
                                'comp_relay':    'datagram[0] = bool(comp_relay)'},
                            'digital_output_4': {
                                'pump_relay':    'datagram[0] = bool(pump_relay)'},
                            'pwm_1_freq': {
                                'pwm_1_freq':    'datagram[0] = pwm_1_freq'},
                            'pwm_1_b_val': {
                                'pump_pwm_set':  'datagram[0] = pump_pwm_set'},
                            'analog_output_1': {
                                'fan_chill_rpm':  'datagram[0] = fan_chill_rpm'},
                            'analog_output_2': {
                                'fan_cond_rpm': 'datagram[0] = fan_cond_rpm'}
                        },

                        'data_par_gui': {
                            'Control_IO_cool': 'in',
                            'Control_IO_heat': 'in',
                            'Mode': 0.0,
                            'Temp_set': 4.0,
                            'man_comp_pwr_set': 0.0,
                            'man_comp_relay': False,
                            'man_comp_rpm_set': 0.0,
                            'man_comp_start_request': False,
                            'man_fan_chill_rpm': 0.0,
                            'man_fan_cond_rpm': 0.0,
                            'man_heater_relay': False,
                            'man_main_relay': False,
                            'man_pump_pwm_set': 0.0
                        }}

    def __init__(self):
        """Dies ist die Initalisierungsfunktion der Gesamt-Applikation. In
        dieser befinden sich alle Aufrufe welche noetig sind bevor der
        zyklische/periodische Aufruf gestarte wird.
        """

        # Initalisierung eines Multiprocessing objects
        self.manager = multiprocessing.Manager()

        # Deklarieren der Base-Manager-Dictionary fuer Eingangs-Datenstrom
        self.dict_transfer_in = self.manager.dict()

        # Deklarieren der Base-Manager-Dictionary fuer Ausgangs-Datenstrom
        self.dict_transfer_out = self.manager.dict()

        # Deklarieren der Base-Manager-Dictionary fuer Parameter-Datenstrom
        self.dict_transfer_par = self.manager.dict()

        # Initalisiere die Dictionary
        self.dict_transfer_in.update(self.dict_transfer_in_default)
        self.dict_transfer_out.update(self.dict_transfer_out_default)
        self.dict_transfer_par.update(self.dict_transfer_par_default)

        # Lade den Pfad in dem das Script der Applikation aufgerufen wird als String
        self.path_app = os.path.dirname(sys.argv[0]) + '/'

        # Lade den Pfad in dem das Config-File liegt als String
        self.path_conf = self.path_app

        # Initialisiere die Config-File-Klasse
        self.conf_file = config_file.ConfigFile(logging=logging,
                                                path=self.path_conf,
                                                dict_transfer=self.dict_transfer_par,
                                                default_content=self.dict_par_default)
        # Lese das Configuration-File
        self.conf_file.read_file()

        # Initialisiere den Daten-Handler der One-Wire-Klasse
        self.w1_data_handler = one_wire.ProcessDataHandler(dict_transfer_par=self.dict_transfer_par,
                                                           dict_transfer_in=self.dict_transfer_in)

        # Initialisiere die 1w-Temperatur-Sensor-Klasse
        self.w1 = one_wire.OneWireTempSensor(logging=logging)
        self.w1.daemon = True
        self.w1.start()

        # Initialisiere den Daten-Handler der CanBusBoard-Klasse
        self.can_data_handler = canbus.ProcessDataHandler(dict_transfer_par=self.dict_transfer_par,
                                                          dict_transfer_in=self.dict_transfer_in,
                                                          dict_transfer_out=self.dict_transfer_out)

        # Initialisiere den Daten-Handler der MonarcoBoard-Klasse
        self.io_data_handler = monarco_board.ProcessDataHandler(dict_transfer_par=self.dict_transfer_par,
                                                                dict_transfer_in=self.dict_transfer_in,
                                                                dict_transfer_out=self.dict_transfer_out)

        # Initialisiere die CanBusBoard-Klassen
        self.can = canbus.CanBusBoard(logging=logging,
                                      dict_transfer_in=self.dict_transfer_in,
                                      dict_transfer_out=self.dict_transfer_out,
                                      list_channels=['can0'])
                                      # list_channels=['can0', 'can1'])
        self.can.daemon = True
        self.can.start()

        # Initialisiere die MonarcoBoard-Klasse
        self.io_board = monarco_board.MonarcoBoard(logging=logging,
                                                   dict_transfer_in=self.dict_transfer_in,
                                                   dict_transfer_out=self.dict_transfer_out)
        self.io_board.daemon = True
        self.io_board.start()

        # Sleep damit die One-Wire-Sensoren alle erscheinen koennen
        time.sleep(3)

        # Lade Eingangs-Daten aus den Hardware-Modulen
        self.get_input_data()

        # Initialisiere die Gui-Klasse
        self.gui_app = gui.Gui(logging=logging,
                               dict_transfer_in=self.dict_transfer_in,
                               dict_transfer_out=self.dict_transfer_out,
                               dict_transfer_par=self.dict_transfer_par,
                               configfile_write=self.conf_file.write_file)
        self.gui_app.daemon = True
        self.gui_app.start()

        self.web_hmi = web_hmi_handler.WebHmiHandler(self.dict_transfer_par)

        # Initialisiere die State-Machine-Klasse
        self.machine = statemachine.HCBloksStateMachine(logging=logging,
                                                        dict_transfer_in=self.dict_transfer_in,
                                                        dict_transfer_par=self.dict_transfer_par,
                                                        configfile_write=self.conf_file.write_file)

        # Initalisiere das Logging-Modul
        self.logging_csv = data_logging.Logging()
        self.dict_conversion = HeaderConversion.get_dic_conversion()

    def start_logfile_recording(self):

        # Erzeuge einen neuen File-Namen
        self.logging_csv.create_new_file_name()

        # Erstelle ein leeres lokales Dictionary
        tmp = {}

        # Update die Eingangs-Prozess-Daten
        tmp.update(self.dict_transfer_in['data_in_process_values'].copy())

        # Update die Ausgangs-Prozess-Daten
        tmp.update(self.dict_transfer_out['data_out_process_values'].copy())

        # Update die Parameter-Daten
        tmp.update(self.dict_transfer_par['data_par_gui'].copy())

        # Entferne Bullshit
        del tmp['init_value']
        del tmp['time']


        # Benenne alle key um. Von klarnamen auf hex-id, mit "self.dict_conversion"
        tmp = dict((self.dict_conversion[key], value) for (key, value) in tmp.items() if key in self.dict_conversion.keys())

        # Verwende das lokale Dictionary um die Header-Zeile im Logfile zu schreiben
        self.logging_csv.write_header(tmp)

    def stop_logfile_recording(self):

        # Schliesse das Logfile ab und verschiebe es in den Logfile-Ordner
        self.logging_csv.exit_handler()

    def get_input_data(self):

        # Ziehe eine lokale Kopie von den Eingangs-Daten-Dictionary
        local_dict_in = self.dict_transfer_in.copy()

        # Ziehe eine lokale Kopie von den Parameter-Dictionary
        local_dict_par = self.dict_transfer_par.copy()

        # Lade die 1w-Raw-Daten ins lokale Dictionary "local_dict_in"
        local_dict_in['data_in_1w'].update(self.w1.get_raw_data())

        # Lade die 1w-Daten aufbereitet mit den Infos aus "local_dict_par" ins lokale Dictionary "local_dict_in"
        local_dict_in['data_in_process_values'].update(self.w1.get_converted_data(local_dict_par['data_in_1w']))

        # Lass den Handler handeln von Prozess-Daten nach Eingangs-Daten
        local_dict_in.update(self.can_data_handler.update_process_data_in(local_dict_in=local_dict_in,
                                                                          local_dict_par=local_dict_par))

        # Lass den Handler handeln von Prozess-Daten nach Eingangs-Daten
        local_dict_in.update(self.io_data_handler.update_process_data_in(local_dict_in=local_dict_in,
                                                                         local_dict_par=local_dict_par))

        # Kopiere das aktualisierten lokale Dictionary's ins Eingangs-Daten-Dictionary
        self.dict_transfer_in.update(local_dict_in)

    def output(self):
        """In dieser Funktion wird das Prozzes-Output-Dictionary an die
        Prozess-Output-Handler uebergeben.
        """

        # Ziehe eine lokale Kopie von den Ausgangs-Daten-Dictionary
        local_dict_out = self.dict_transfer_out.copy()

        # Ziehe eine lokale Kopie von den Parameter-Dictionary
        local_dict_par = self.dict_transfer_par.copy()

        local_dict_out['data_out_process_values'].update(self.machine.process_output_data())

        # Lass den Handler handeln von Prozess-Daten nach Ausgangs-Daten
        local_dict_out.update(self.can_data_handler.update_process_data_out(local_dict_out=local_dict_out,
                                                                            local_dict_par=local_dict_par))

        # Lass den Handler handeln von Prozess-Daten nach Ausgangs-Daten
        local_dict_out.update(self.io_data_handler.update_process_data_out(local_dict_out=local_dict_out,
                                                                           local_dict_par=local_dict_par))

        # Kopiere das aktualisierten lokale Dictionary's ins Ausgangs-Daten-Dictionary
        self.dict_transfer_out.update(local_dict_out)

    def process(self):

        # pprint.pprint(self.dict_transfer_par.copy(), width=1)
        # pprint.pprint(self.dict_transfer_in.copy(), width=1)
        # pprint.pprint(self.dict_transfer_out.copy(), width=1)
        # print('-----------------------------------------------------------------------')
        self.web_hmi.update(self.dict_transfer_in.copy(), self.dict_transfer_par.copy())

        # Uebergebe der State-Machine die Input-Daten
        ret_val = self.machine.process_input_data(self.dict_transfer_in.copy(), self.dict_transfer_par.copy())

        # Wenn die State-Machine einen Wert zurueck gibt (return-value),
        # (falls Manuellen-Steuer-Variablen fehlern), dann ...
        if ret_val:
            tmp2 = self.dict_transfer_par.copy()
            tmp2.update(ret_val)

            pbx_misc.deep_update(self.dict_transfer_par, {'data_par_gui': tmp2['data_par_gui']})

        # State-Machine macht einen Zeitschritt
        self.machine.step()

        time.sleep(0.000000001)

        tmp = {}
        tmp.update(self.dict_transfer_in['data_in_process_values'].copy())
        tmp.update(self.dict_transfer_out['data_out_process_values'].copy())
        tmp.update(self.dict_transfer_par['data_par_gui'].copy())

        # Entferne Bullshit
        del tmp['init_value']
        del tmp['time']
        local_tmp = {}

        # Benenne die Header-Zeile von Klarnamen auf Hex-Namen um
        for (key, value) in tmp.items():
            try:
                local_tmp[self.dict_conversion[key]] = value
            except KeyError:
                # print(key)
                pass

        tmp = local_tmp

        # Uebergib das lokale temporary Dictionary an die Logfile-write-Methode
        self.logging_csv.write_data(tmp)

    def run(self):

        logging.info('Main start with PID = ' + str(psutil.Process().pid))
        logging.info('Base-Manager start with PID = ' + str(self.manager._process.ident))

        self.get_input_data()
        self.start_logfile_recording()
        # print(gc.isenabled())
        # gc.set_debug(gc.DEBUG_STATS)
        # sys.stderr = open('./err.txt', 'w')
        while True:

            with cyclic_timer.timer_sec(interval=0.1):
                # print(gc.get_count())
                self.get_input_data()
                self.process()
                self.output()

                # Wenn der GUI-Process nicht mehr aktiv ist, dann ...
                if not self.gui_app.is_alive():

                    # Beende die Endlos-Schleife
                    self.can.cleanup_can_interfaces()

                    # Beende die Logfileaufzeichnung
                    self.logging_csv.exit_handler()
                    break

        return 0


def app():

    app_1 = HCBloksMain()
    app_1.run()


if __name__ == "__main__":
    app()
