﻿import email
import imaplib
import os
import pyzipper
from io import BytesIO

# URL: pbxfileserver.dynv6.net


class ReceiveEmail:

    def __init__(self):

        self.receiver_adress = 'pbxgigasend@gmail.com'
        self.__password__ = 'hcbloks20'
        self.imap_server = 'imap.gmail.com'
        self.zip_pwd = b'xsFnw4xUrWpSFLUV'

        self.path_target_folder = '/home/pi/Logfiles/'

    @staticmethod
    def break_up_list_content(data: list):

        # Erstelle eine Hilf-Liste
        list_splinted = []

        # Durch-Wandere die Liste
        for segment in data:

            # Die Liste wird an den "Leerzeichen" geteilt
            # b'1 2 3'.split() => [b'1', b'2', b'3']
            list_splinted += segment.split()

        # Gib die zerteilte Liste zurueck
        return list_splinted

    # def write_zip_and_extract_files(self, zip_payload, target_path: str):
    #
    #     # Oeffne einen "Schreibe-Binaer"-File-Handler an den angegebenen Pfad
    #     with open(self.path_target_folder + zip_payload.get_filename(), 'wb') as file_writer:
    #
    #         # Schreibe das Zip-File an den angegebenen Pfad
    #         file_writer.write(zip_payload.get_payload(decode=True))
    #
    #     # Oeffne einen Zip-Handler mit den Pfad es Zip-Files
    #     with pyzipper.AESZipFile(file=self.path_target_folder + zip_payload.get_filename()) as zf:
    #
    #         # Definiere das Password fuer die Entschluesselung
    #         zf.setpassword(self.zip_pwd)
    #
    #         # Entpacke und schreibe die Files an den angegebenen Pfad
    #         zf.extractall(path=target_path)

    def extract_zip_and_write_files(self, zip_payload, target_path: str):

        with pyzipper.AESZipFile(BytesIO(zip_payload.get_payload(decode=True))) as zf:

            # Definiere das Password fuer die Entschluesselung
            zf.setpassword(self.zip_pwd)

            # Entpacke und schreibe die Files an den angegebenen Pfad
            zf.extractall(path=target_path)

    @staticmethod
    def extract_device_number(mail_adress: str):

        return str(mail_adress.split('+')[1].split('@')[0])

    def move_logs_into_archive(self, mail, device_nr: str):

        if not os.path.exists(self.path_target_folder + device_nr):
            os.makedirs(self.path_target_folder + device_nr)

        self.extract_zip_and_write_files(mail, target_path=self.path_target_folder + device_nr)

    def receive_mail(self):

        with imaplib.IMAP4_SSL(self.imap_server) as server:

            server.login(self.receiver_adress, self.__password__)

            server.select('inbox')

            # Diese Funktion sendet eine "Such-Anfrage" an den Mail-Server. Diese gibt einen Tupel zurueck, im ersten
            # Element "status" wird "OK" zurueckgegeben wenn der Anfrage-Befehl erfolgreich war. Und im zweiten
            # Element "data" ist eine durch Leerzeichen getrennte Liste übereinstimmender Nachrichten-Nummern (ID's)
            status, data = server.search(None, 'ALL')

            # Sorge dafuer dass jede Nachrichten-Nummer (ID der Email) als Element in einer Liste abgelegt wird
            list_mail_ids = self.break_up_list_content(data)

            # Durch-wandere die Liesten mit den Email-ID's
            for mail_id in list_mail_ids:

                # Lade die Email vom Server im 'RFC822'-Format.
                # Diese gibt einen Tupel zurueck, im ersten Element "status" wird "OK" zurueckgegeben
                # wenn die Fetch-Anfrage erfolgreich war. Im zweiten Element "data" befindet sich eine
                # Liste mit folgenden Inhalten:
                #   - Element_1 : Tupel aus zwei Elementen
                #       - Header: Enthaltet die Email-ID (in bezug auf die Such-anfrage), das Format ('RFC822')
                #                 und die Anzahl an Text-Zeichen der Email
                #       - Content: Der komplette Email-Nachrichten-Inhalt
                #   - Element_2: Abschluss-Signalisierung --> byte b ')'
                status, data = server.fetch(mail_id, '(RFC822)')

                # Durch-wandere die Liste "data"
                for response_part in data:

                    # Wenn es sich bei "response_part" um einen "tuple" handelt, dann ...
                    if isinstance(response_part, tuple):

                        # Es wird das zweite Element (der Nachrichten-Inhalt) heraus-selektiert
                        message = email.message_from_bytes(response_part[1])

                        mail_from = message['from']
                        mail_subject = message['subject']
                        mail_to = message['to']
                        mail_date = message['date']

                        device_nr = self.extract_device_number(mail_to)

                        # Wenn es sich um eine mehrteilige Email handelt, dann ...
                        if message.is_multipart():

                            mail_content = ''
                            # message.walk()

                            # auf mehrteilig haben wir die Text-Nachricht und andere Dinge wie Attachments, HTML-Version
                            # der Nachricht, etc. In diesem Fall durchlaufen wir die E-Mail-Nutzdaten
                            for mail_part in message.get_payload():

                                # Dateien ohne Erweiterung werden Application/Octet-Stream zugeordnet,
                                # Wenn dies zutrifft, dann gehe davon aus das es sich um ein Zip-File handelt und ...
                                if mail_part.get_content_type() == 'application/octet-stream':

                                    # Lade die Zip-Files aus der Email, extrahiere diese und schiebe diese
                                    # in den entsprechenden Device-Unterordner
                                    self.move_logs_into_archive(mail=mail_part, device_nr=device_nr)

                                # Wenn es sich um reinen Text in der Email handelt, dann ...
                                if mail_part.get_content_type() == 'text/plain':

                                    # Sammle diesen und schicke diesen ins Nirvana
                                    mail_content += mail_part.get_payload()

                        # Wenn es sich nicht um eine mehr-teilige Email handelt, dann ...
                        else:
                            # Da es sich um reinen Text in der Email handelt,
                            # Sammle diesen und schicke diesen ins Nirvana
                            mail_content = message.get_payload()


if __name__ == "__main__":
    mail_rec = ReceiveEmail()
    mail_rec.receive_mail()
