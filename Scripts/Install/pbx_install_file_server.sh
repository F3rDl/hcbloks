﻿#!/bin/bash


# - Image auf SD-Karte aufspielen
# - leeres File mit den Namen "ssh" auf der Partition "boot" anlegen
# - falls erforderlich das File "wpa_supplicant.conf" auf die Partition "boot" kopieren
# - Hardware sicher entfernen

# - SD-Karte herausnehmen und in einen USB-Karten-Leser stecken
# - Karten-Leser auf einen Raspberry mit anderen Image starten
# - Sicher gehen das gparted installiert ist "sudo apt-get install gparted"

# - Gparted starten -> "/dev/sdx" auswählen -> rechter mausklick "rootfs" -> "Resize/Move"
# - "New Size 8000 MiB" -> "Resize" ->
# . rechter mausklick auf "unallocated" -> auf "New" klicken -->
# - "New Size 20000 MiB" -> "Filesystem fat32" -> "Label data" -> "Add" -> "Edit" ->
# - "Apply all operations" -> Apply

# - Wenn das erstellen der Partition fertig, unmounten, Kartenleser entfernen und herunterfahren

# - SD-Karte von Kartenleser entfernen und in den Raspberry stecken und einschalten
# - Tera-Term/Putty starten und mit IP, Benutzer: pi, Passwort: raspberry anmelden
# - sudo rasp-config -> System Options -> Hostname -> pbxserver0000 ->
# - Localisation Options -> Locale -> de_AT.UTF-8 und en_US.UTF-8 aktivieren [leertaste]

# - Reboot
# - Update "sudo apt-get update"

# - Install git "sudo apt-get install git"
# - mkdir "Downloads"
# - cd /home/pi/Downloads/
# - git clone https://F3rDl:d7ecd0e6ee0835a0ff0122d276a3121c9539bd15@github.com/F3rDl/HCBloks.git
# - chmod +x ./HCBloks/Scripts/Install/pbx_install_file_server.sh


# - ./HCBloks/Scripts/Install/pbx_install_file_server.sh

# Neu-einlesen der Paket-listen
sudo apt-get -y update

# Alle Parkete auf letzt-Stand aktualisieren
sudo apt-get -y upgrade

sudo apt-get install putty-tools

# sudo hostnamectl set-hostname pbxfileserver

########################################################################################################################
###################################### Deklaration von Variablen #######################################################

FTPGroup="filetransfer"
FTPUser="finki"
FTPUserPassw="ftp123"

SSHUser="sshuser"
SSHUserPassw="pbxpi123ssh"

########################################################################################################################
###################################### Deklaration von Funktionen ######################################################

function create_ssh_auth {

  echo -e "Lege den Authorized-keys fuer Benutzer $1 an\n"

  # Erstelle folgende Ordner
  sudo mkdir /home/"$1"/.ssh

  # Gibt den User "$1" die Besitz-Rechte von den erstellten Ordnern
  sudo chown "$1":"$1" /home/"$1"/*

  # Gibt den User "$1" die Besitz-Rechte von den erstellten Ordnern
  sudo chown "$1":"$1" /home/"$1"/.ssh

  # Erstelle ein leeres File
  sudo su "$1" -c "touch /home/$1/.ssh/authorized_keys"

  # Generiere den
  sudo su "$1" -c "ssh-keygen -b 4096 -f /home/$1/.ssh/id_rsa -N '' "

  sudo su "$1" -c "cat /home/$1/.ssh/id_rsa.pub >> /home/$1/.ssh/authorized_keys"

  sudo su "$1" -c "puttygen /home/$1/.ssh/id_rsa -o /home/$1/.ssh/id_rsa.ppk"

  sudo chown root:root /home/"$1"/.ssh/id_rsa
  sudo chown root:root /home/"$1"/.ssh/id_rsa.pub
  sudo chown root:root /home/"$1"/.ssh/id_rsa.ppk
  sudo chown root:root /home/"$1"/.ssh/authorized_keys

  sudo chmod 600 /home/"$1"/.ssh/id_rsa.pub

}


########################################################################################################################
###################################### Erstelle den SSH-Wartung-User ###################################################

# Lege den Benutzer "$SSHUser" an
echo -e "Lege den Benutzer $SSHUser an\n"
sudo useradd -m $SSHUser

# Lege fuer den Benutzer "$SSHUser" ein Passwort "$SSHUserPassw" fest
echo -e "Lege fuer den Benutzer $SSHUser ein Passwort fest\n"
sudo echo -e "$SSHUser:$SSHUserPassw" | sudo chpasswd

# Trage den User "sshuser" in die Liste der "sshd_config", Berechtigung fuer ssh-Verbindungsaufbau
echo -e "Trage den User sshuser in die Liste der sshd_config\n"
sudo echo -e "\nAllowUsers $SSHUser $FTPUser\n" | sudo tee -a /etc/ssh/sshd_config > /dev/null

# Folgende Einstellung legt das SSH-Protokoll auf die Version 2 fest.
# Version 1 gilt als veraltet und unsicher und existiert nur noch aus Kompatibilitätsgruenden.
echo -e "Lege fuer SSH-Verbindungen das Protocol 2 fest\n"
sudo echo -e "Protocol 2\n" | sudo tee -a /etc/ssh/sshd_config > /dev/null

# Mit LoginGraceTime wird dem Anmeldeversuch, bzw. dem Benutzer 30 Sekunden lang Zeit gegeben sich anzumelden.
# Geschieht dies nicht, wird die Verbindung wieder getrennt.
echo -e "Lege fuer SSH-Verbindungen 30 Sekunden fest, sich anzumelden\n"
sudo echo -e "LoginGraceTime 30\n" | sudo tee -a /etc/ssh/sshd_config > /dev/null

# Jedes System hat Standard-Benutzer für die kein Passwort festgelegt ist.
# Für diese Benutzer sollte kein Zugriff per SSH möglich sein. Folgende Einstellung erlaubt
# keinen Zugriff für Benutzer, die kein Passwort aufweisen.
echo -e "Lege fuer SSH-Verbindungen fest das User ohne Passwort sich nicht anmelden duerfen\n"
sudo sed -i 's+#PermitEmptyPasswords no+PermitEmptyPasswords no+g' /etc/ssh/sshd_config

echo -e "Lege fuer SSH-Verbindungen den Port 1337 fest\n"
sudo sed -i 's+#Port 22\n+Port 1337\n+g' /etc/ssh/sshd_config

# Nach mehrheitlicher Meinung sollte der Root-Zugang per SSH unmöglich sein.
# Der Administrator sollte sich immer als normaler User einloggen und sich erst dann
# bedarfsweise die Rechte von "root" verschaffen.
echo -e "Lege fuer SSH-Verbindungen fest das root sich nicht anmelden darf\n"
sudo echo -e "PermitRootLogin no\n" | sudo tee -a /etc/ssh/sshd_config > /dev/null

# StrictModes ueberprueft die allgemeine Sicherheit für den SSH-Dienst, beispielsweise, dass gewisse Dateien
# die benoetigt werden, bestimmte Zugriffsrechte und Besitzer haben. So wird verhindert, dass beispielsweise
# mit falschen Zugriffsrechten ausgestattete authorized_keys (muss das Zugriffsrecht 0644 haben) den
# Sicherheitsmechanismus aushebeln koennten.
echo -e "Lege fuer SSH-Verbindungen den StrictMode fest\n"
sudo echo -e "StrictModes yes\n" | sudo tee -a /etc/ssh/sshd_config > /dev/null

# PasswordAuthentication auf no bedeutet, dass reine Passwort-Logins ohne Schluessel generell nicht erlaubt sind.
echo -e "Lege fuer SSH-Verbindungen fest, dass reine Passwort-Logins ohne Schluessel generell nicht erlaubt sind\n"
sudo echo -e "PasswordAuthentication no\n" | sudo tee -a /etc/ssh/sshd_config > /dev/null

sudo echo -e "RSAAuthentication yes\n" | sudo tee -a /etc/ssh/sshd_config > /dev/null
sudo echo -e "PubkeyAuthentication yes\n" | sudo tee -a /etc/ssh/sshd_config > /dev/null
sudo echo -e "#AuthorizedKeysFile     %h/.ssh/authorized_keys\n" | sudo tee -a /etc/ssh/sshd_config > /dev/null

create_ssh_auth "$SSHUser"

# systemctl status ssh.service
sudo service ssh restart


########################################################################################################################
###################################### Erstelle den SFTP-Server ########################################################

# Backup von der SSH-Konfigurationsdatei
sudo cp /etc/ssh/sshd_config /etc/ssh/sshd_config_backup

# Lege den Benutzer "$FTPUser" an  --> Kontrolle "compgen -u"
sudo useradd -m $FTPUser

# Lege fuer den Benutzer "$FTPUser" ein Passwort "$FTPUserPassw" fest
sudo echo -e "$FTPUser:$FTPUserPassw" | sudo chpasswd

# Weise an den "internal-sftp" zu verwenden.
# Beide "sftp-server" und "internal-sftp" sind Teil von OpenSSH. Das "sftp-server" ist eine eigenständige Binärdatei.
# Das "internal-sftp" ist nur ein Konfigurations-Schluesselwort, das angibt den "sshd" in den integrierten
# SFTP-Servercode zu verwenden, anstatt einen anderen Prozess auszuführen.
sudo sed -i 's+Subsystem\tsftp\t/usr/lib/openssh/sftp-server+Subsystem\tsftp\tinternal-sftp+g' /etc/ssh/sshd_config

# Definiere die ssh-Settings von der Gruppe "$FTPGroup"
sudo echo -e "\nMatch Group $FTPGroup
    ChrootDirectory %h
    X11Forwarding no
    AllowTcpForwarding no
    ForceCommand internal-sftp\n" | sudo tee -a /etc/ssh/sshd_config > /dev/null

# Starte den SSH-Server neu um die Einrichtung zu uebernehmen
sudo service ssh restart

# Erstelle die Gruppe "$FTPGroup" --> Kontrolle "getent group"
sudo addgroup --system "$FTPGroup"

# Fuege den "$FTPUser" der Gruppe "$FTPGroup" hinzu
sudo usermod -G "$FTPGroup" "$FTPUser"

# Veraendere die Besitzrechte vom Heim-Verzeichnis des Users "$FTPUser"
# Diese Benutzer kann jetzt keine Dateien in seinen Basisverzeichnissen erstellen,
# da diese Verzeichnisse nun dem Root-Benutzer gehoert.
sudo chown root:root /home/"$FTPUser"

# Veraendere die Zugriffsrechte vom Heim-Verzeichnis des Users "$FTPUser"
sudo chmod 755 /home/"$FTPUser"

# Erstelle folgende Ordner
sudo mkdir /home/"$FTPUser"/logs
sudo mkdir /home/"$FTPUser"/misc
sudo mkdir /home/"$FTPUser"/.ssh

# Gibt den User "$FTPUser" die Besitz-Rechte von den erstellten Ordnern
sudo chown "$FTPUser":"$FTPGroup" /home/"$FTPUser"/*

create_ssh_auth "$FTPUser"


########################################################################################################################




########################################################################################################################
###################################### Definiere Root-Sicherheit #######################################################

# Wenn eine IP-Adresse innerhalb einer bestimmten Zeit mehrere fehlgeschlagene Verbindungen initiiert hat,
# dann riecht das stark nach einem Angriff. Vielleicht sogar automatisiert durch einen Bot, der das Passwort
# durch automatisiertes Durchprobieren erraten will.
#
# Diese Einstellung besteht aus drei durch einen Doppelpunkt voneinander getrennten Zahlen (start:rate:full):
#
#   - "start" bedeutet, dass 3 SSH-Verbindungen gleichzeitig geoeffnet werden koennen, zum einloggen.
#   - Ab der dritten gibt es aber nur eine "rate" (30%ige) Wahrscheinlichkeit, dass eine weitere geöffnet werden kann.
#   - Das ganze steigt linear um jeweils 10% an, bis nach "full" 10 offenen Verbindungen definitiv keine neue mehr
#     geöffnet werden kann, da wir dann 100% erreicht haben.
#
# Bereits eingeloggte Benutzer sind hiervon ausgenommen.
#
# Das hindert automatisierte Scripte daran, mehrere SSH-Sitzungen gleichzeitig zu öffnen und man auf diese Weise
# natürlich auch mehrere Passworte auf einen Treffer hin austesten kann.
sudo sed -i 's+#MaxStartups 10:30:100+MaxStartups 3:30:100+g' /etc/ssh/sshd_config

# Sorge dafuer das fuer sudo-Befehle ueber ssh das root-Passwort notwendig ist
sudo echo -e "\nDefaults        rootpw\n" | sudo tee -a /etc/sudoers > /dev/null

# Lege fuer den Benutzer "root" ein Passwort fest
sudo echo "root:pbxpi123root" | chpasswd

# Lege fest das fuer einen jede sudo-Eingabe, eine Passwort-Abfrage stattfindet
sudo sed -i 's+pi ALL=(ALL) NOPASSWD: ALL+pi ALL = (ALL) PASSWD: ALL+g' /etc/sudoers.d/010_pi-nopasswd

# Manuell Installieren: Automatische updates von Sicherheits-patch's
# sudo apt-get install unattended-upgrades
# sudo dpkg-reconfigure -plow unattended-upgrades
