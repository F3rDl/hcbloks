#!/bin/bash

programm_name="Overture_2_0_4"
python_version="3.7"

# Lade die Seriennummer
serial_number_hex="$(cat /proc/cpuinfo | grep Serial | cut -d' ' -f2)"

# Wandle Seriennummer in decimal
serial_number_dec="$((16#$serial_number_hex))"

# Lade den Pfad an dem sich das Skript befindet in "DIR"
DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" >/dev/null 2>&1 && pwd )"

# printf "%s\n" "$DIR"

# Springe an den Pfad von dem Skript
cd "$DIR"  || exit

# Neu-einlesen der Paket-listen
sudo apt-get -y update

# Alle Pakete auf letzt-Stand aktualisieren
sudo apt-get -y upgrade

# Liste mit den Programmen welche installiert werden sollen
declare -a Programs=(
  "python-dev"
  "graphviz"
  "libgraphviz-dev"
  "pkg-config"
  "libjpeg-dev"
  "libfreetype6"
  "libfreetype6-dev"
  "zlib1g-dev"
  "can-utils"
  "device-tree-compiler"
  "gparted"
  "pv"
  "nginx"
  "hostapd"
  "dnsmasq"
  "dos2unix"
  "p7zip-full"
  "cpuinfo"
  "sshpass"
  "python3-flask"
  "python3-matplotlib"
  "python3-pygraphviz"
  "python3-pillow"
  "python3-numpy"
  "python3-psutil"
  "python3-can"
  "python3-spidev"
  "python3-smbus"
  "python3-crcmod"
)

#  "network-manager"
#  "network-manager-gnome"

# Liste mit den Python-Modulen welche installiert werden sollen
declare -a Python_moduls=(
  "transitions"
  "uwsgi"
  "pyzipper"
  "dash"
  "plotly"
  "shutil"
)

########################################################################################################################
                              # --> Programme laden und installieren <-- #
########################################################################################################################

# For-Schleife der Programm-Liste
for val in "${Programs[@]}"; do

  # Bildschirmausgabe mit den aktuellen Check des Programm-namens
  echo "Check if $val is installed"

  # Aufruf ob das Programm installiert ist
  dpkg -s "$val" &>/dev/null

  # Wenn der Return-value ungleich 0 ist, dann ...
  if [ $? -ne 0 ]; then

    # Bildschirmausgabe mit den aktuellen Status dass das Programm geladen/installiert wird
    echo "$val is NOT installed"
    echo "Install $val"

    # Befehl um Programm zu laden und installieren
    sudo apt-get install -y -qq "$val"

    # Zeilenvorschub zur besseren Lesbarkeit
    echo -e "\n"

  # Wenn das Programm bereits installiert ist, dann ...
  else

    # Bildschirmausgabe dass das Programm bereits installiert ist
    echo -e "$val is already installed\n"

  fi

done

########################################################################################################################
                                        # --> Python-Module laden <-- #
########################################################################################################################

# Upgrade pip
echo -e "Check if python-pip is up to date"
sudo python"$python_version" -m pip install --upgrade pip

# For-Schleife der Programm-Liste
for val in "${Python_moduls[@]}"; do

  # Bildschirmausgabe mit den aktuellen Check des Programm-namens
  echo "Check if $val is installed"

  # Aufruf ob das Programm installiert ist
  python -c "import $val"

  # Wenn der Return-value ungleich 0 ist, dann ...
  if [ $? -ne 0 ]; then

    # Bildschirmausgabe mit den aktuellen Status dass das Programm geladen/installiert wird
    echo "$val is NOT installed"
    echo "Install $val"

    # Befehl um Programm zu laden und installieren
    sudo pip"$python_version" install "$val"

    # Zeilenvorschub zur besseren Lesbarkeit
    echo -e "\n"

  # Wenn das Programm bereits installiert ist, dann ...
  else

    # Bildschirmausgabe dass das Programm bereits installiert ist
    echo -e "$val is already installed\n"

  fi

done

########################################################################################################################
                                        # --> manuell Python-Module laden <-- #
########################################################################################################################

  # Aktualisieren vom Modul "Pillow"
echo -e "Update Modul pillow"
sudo pip3 install pillow --upgrade

########################################################################################################################
                                        # --> entferne Ballast <-- #
########################################################################################################################

# Entferne die Python-IDE "Thonny"
sudo apt-get purge thonny -y

# Entferne VLC-Media-Player


# Entfernt die herunter-geladenen Pakete aus dem lokalen Paket-Cache /var/cache/apt/archives/ und gibt den Speicher frei
sudo apt-get clean -y

# Entferne nicht mehr benoetigte Pakete, die als Abhaengigkeit installiert wurden
sudo apt-get autoremove -y

########################################################################################################################
                                        # --> OneWire-Modul laden <-- #
########################################################################################################################

# Setup One-Wire-Master DS2482
echo -e "Load Modul ds2482\n\n"
sudo modprobe ds2482




########################################################################################################################
                                        # --> RAM-Disk <-- #
########################################################################################################################
####

sudo mkdir /var/ramdisk
sudo nano /etc/fstab
tmpfs /tmp/ramdisk   tmpfs   nodev,nosuid,size=50M 0 0
sudo mount -a
df -h

########################################################################################################################
                                        # --> Programm-Ordner anlegen <-- #
########################################################################################################################

echo "Check if Folder pbx exist in /usr/local/bin/"
DIR="/usr/local/bin/pbx"
if [ -d "$DIR" ]; then
  echo -e "Folder pbx exist in /usr/local/bin/\n"
else
  echo "Folder pbx does NOT exist in /usr/local/bin/"
  echo -e "Create it\n"
  # Erstelle den Ordner
  sudo mkdir -p "$DIR"
fi

echo "Check if Folder $programm_name exist in /usr/local/bin/pbx/"
DIR="/usr/local/bin/pbx/$programm_name"
if [ -d "$DIR" ]; then
  echo -e "Folder $programm_name exist in /usr/local/bin/pbx/\n"
else
  echo "Folder $programm_name does NOT exist in /usr/local/bin/pbx/"
  echo -e "Create it\n"
  # Erstelle den Ordner
  sudo mkdir -p "$DIR"
fi

########################################################################################################################
                                # --> Programm im Programm-Ordner ablegen <-- #
########################################################################################################################

echo -e "Copy files to target\n"
sudo cp -r ../../* /usr/local/bin/pbx/"$programm_name"

########################################################################################################################
                        # --> Symlink auf den Logfile-Folder in der RW-Partition DATA <-- #
########################################################################################################################
echo -e "Create Symlink Logfiles\n"
sudo mkdir /media/pi/DATA/Logfiles
sudo chown 777 /media/pi/DATA/Logfiles
sudo ln -s /media/pi/DATA/Logfiles /home/pi/Logfiles

########################################################################################################################
                      # --> SPI/CAN-Schnittstelle einrichten und Turbo einschalten <-- #
########################################################################################################################
echo -e "Create Entry in /boot/config.txt\n"
# Write Entry in "/boot/config.txt"
sudo echo -e "\n\n##################  PBX  ##################################
dtoverlay=mcp2515-can1,oscillator=16000000,interrupt=6
dtoverlay=mcp2515-can0,oscillator=16000000,interrupt=27
dtoverlay=spi-gpio-cs
force_turbo=1\n" | sudo tee -a /boot/config.txt > /dev/null

# Uebertrage das overlay fuer die SPI/CAN-Kommunikation an seinen Bestimmungsort
sudo cp spi-gpio-cs.dtbo /boot/overlays

########################################################################################################################
                                 # --> WEB-Server einrichten <-- #
########################################################################################################################

# Bildschirmausgabe dass der Web-Server eingerichtet wird
echo -e "Setup Web-server\n"

# Setze das Benutzer-Recht des Web-HMI-Ordners
sudo chown www-data /usr/local/bin/pbx/"$programm_name"/Web_HMI

# Um uWSGI Einstellungen bereitzustellen, wird eine Initialisierung-Datei erstellt
sudo echo -e "[uwsgi]\n
chdir = /usr/local/bin/pbx/$programm_name/Web_HMI
module = hmi:app

master = true
processes = 1
threads = 2

uid = www-data
gid = www-data

socket = /tmp/ramdisk/flask_hmi.sock
chmod-socket = 664
vacuum = true

die-on-term = true

# touch-reload = /usr/local/bin/pbx/$programm_name/Web_HMI/hmi.py" | sudo tee -a /usr/local/bin/pbx/"$programm_name"/Web_HMI/uwsgi.ini > /dev/null

# Entferne die Standard-Seite vom Web-Server (wenn es existiert)
sudo rm /etc/nginx/sites-enabled/default

# NGINX wird jetzt so konfiguriert, dass Datenverkehr an uWSGI weitergeleitet wird. Dies wird als "Reverse Proxy" bezeichnet.
sudo echo -e "server {
listen 80;
server_name localhost;

location / { try_files \$uri @app; }
location @app {
include uwsgi_params;
uwsgi_pass unix:/tmp/ramdisk/flask_hmi.sock;
}\n}" | sudo tee -a /etc/nginx/sites-available/web_hmi_proxy > /dev/null

# Erstelle einen Link aus diesem File zum Verzeichnis "sites-enabled"
sudo ln -s /etc/nginx/sites-available/web_hmi_proxy /etc/nginx/sites-enabled

# Starte den NGINX-Server neu
sudo systemctl restart nginx

# Bildschirmausgabe dass der Web-Server gestartet werden soll bei System-Start
echo -e "Setup Web-server-Auto-start\n"

# Mit den folgenden File wird der Auto-start des Web-Servers eingerichtet
sudo echo -e "[Unit]
Description=uWSGI Service
After=network.target

[Service]
User=www-data
Group=www-data
WorkingDirectory=/usr/local/bin/pbx/$programm_name/Web_HMI/
ExecStart=/usr/local/bin/uwsgi --ini /usr/local/bin/pbx/$programm_name/Web_HMI/uwsgi.ini

[Install]
WantedBy=multi-user.target" | sudo tee -a /etc/systemd/system/uwsgi.service > /dev/null

# Der Befehl veranlasst systemd dazu, alle Konfigurationsdateien neu zu laden und alle Units neu zu starten.
# Dies ist z.B. notwendig, wenn man im laufenden Betrieb eine aktive Unit-Datei editiert hat und die so geänderte
# Unit direkt nutzen will, jedoch ohne das System neu zu starten.
sudo systemctl daemon-reload

# Starte den uWSGI.service
sudo systemctl start uwsgi.service

# Sorge dafuer das bei Systemstart der Service automatische gestartet wird
# Dieser Befehl sollte folgende Ausgabe produzieren:
# "Created symlink /etc/systemd/system/multi-user.target.wants/uwsgi.service /etc/systemd/system/uwsgi.service"
sudo systemctl enable uwsgi.service

########################################################################################################################
                                 # --> Wifi-Access-Point einrichten <-- #
########################################################################################################################
echo -e "Config Wifi-Access-Point\n"

# Stoppe den Host-Access-Point-Daemon und DNS-Masq
sudo systemctl stop hostapd
sudo systemctl stop dnsmasq

echo -e "interface wlan0
    static ip_address=192.168.2.1/24
    nohook wpa_supplicant" | sudo tee -a /etc/dhcpcd.conf > /dev/null

# Restart DHCP-Client-Daemon-Config-File
sudo systemctl restart dhcpcd

# Write Entry in Host-Access-Point-Daemon-Config-File
sudo echo -e "interface=wlan0

ssid=hcbloks${serial_number_dec: -4}
channel=1
hw_mode=g
ieee80211n=1
ieee80211d=1
country_code=AT
wmm_enabled=1

auth_algs=1
wpa=2
wpa_key_mgmt=WPA-PSK
rsn_pairwise=CCMP
wpa_passphrase=pbxpi123" | sudo tee -a /etc/hostapd/hostapd.conf > /dev/null

sudo chmod 600 /etc/hostapd/hostapd.conf

# Replace Entry in Host-Access-Point-Daemon-Config-File
sudo sed -i 's+#DAEMON_CONF=""+RUN_DAEMON=yes\nDAEMON_CONF="/etc/hostapd/hostapd.conf"+g' /etc/default/hostapd

sudo systemctl unmask hostapd
sudo systemctl start hostapd
sudo systemctl enable hostapd

# Sichere den Original-State von dnsmasq.conf
sudo mv /etc/dnsmasq.conf /etc/dnsmasq.conf.orig

# Write Entry in DNS-Masq
sudo echo -e "# DHCP-Server aktiv fuer WLAN-Interface
interface=wlan0

# IPv4-Adressbereich und Lease-Time
dhcp-range=192.168.2.100,192.168.2.200,255.255.255.0,24h

# DNS
# dhcp-option=option:dns-server,192.168.2.1" | sudo tee -a /etc/dnsmasq.conf > /dev/null

# Write Entry hosts-name
sudo echo -e "192.168.0.1             www.pbx.net" | sudo tee -a /etc/hosts > /dev/null

# Das SSID-Refresher-Skript ausfuehrbar machen
sudo chmod 777 /usr/local/bin/pbx/"$programm_name"/Scripts/Pi/ssid_refresher.sh

# Den Power-Save-Modus deaktivieren
sudo iw wlan0 set power_save off

########################################################################################################################
                                          # --> FiLo einrichten <-- #
########################################################################################################################

# Port-Weiterleitung einschalten
sudo sed -i 's+net.ipv4.ip_forward=0+net.ipv4.ip_forward=1+g' /etc/default/hostapd
sudo sysctl -w net.ipv4.ip_forward=1

sudo iptables -t nat -A POSTROUTING -o eth1 -j MASQUERADE
sudo iptables -A FORWARD -i eth1 -o eth0 -m state --state RELATED,ESTABLISHED -j ACCEPT
sudo iptables -A FORWARD -i eth0 -o eth1 -j ACCEPT
sudo iptables -A FORWARD -i wlan0 -o eth1 -j REJECT
sudo sh -c "iptables-save > /etc/iptables.ipv4.nat"

# Sorge dafuer dass das Skript im Unix-Format ist
sudo dos2unix /usr/local/bin/pbx/Overture_2_0_3/Scripts/Pi/filo_transfer.sh

# Sorge dafuer dass das Skript ausfuehrbar ist
sudo chmod 777 /usr/local/bin/pbx/Overture_2_0_3/Scripts/Pi/filo_transfer.sh

# Erstelle den Logfile-Ordner fuer den FiLo auf der DATA-Partition
sudo mkdir /media/pi/DATA/FiLo_Logs

# Sorge dafuer dass jeder und alles zugreifen kann
sudo chown 777 /media/pi/DATA/FiLo_Logs

# Erstelle einen Sysm-Link
sudo ln -s /media/pi/DATA/FiLo_Logs /home/pi/FiLo_Logs

# Definiere die statische IP-Adresse
sudo echo -e "interface eth0
    static ip_address=192.168.0.1/24" | sudo tee -a /etc/dhcpcd.conf > /dev/null

# Definiere den erlaubten Adressbereich
sudo echo -e "# DHCP-Server aktiv fuer FiLo
interface=eth0
# IPv4-Adressbereich und Lease-Time
dhcp-range=192.168.0.130,192.168.0.200,255.255.255.0,24h" | sudo tee -a /etc/dnsmasq.conf > /dev/null

# Sorge das beim start die "eth0"-Schnittstelle hochgefahren wird
sudo echo -e "auto eth0
allow-hotplug eth0" | sudo tee -a /etc/dnsmasq.conf > /dev/null


########################################################################################################################
                                 # --> App-Auto-Start einrichten <-- #
########################################################################################################################

# Auto-start der Applikation einrichten
echo -e "Setup App-Autostart\n"

# Stelle sicher dass der Ordner "autostart" vorhanden ist
echo "Check if Folder autostart exist in /home/pi/.config/"
DIR="/home/pi/.config/autostart/"
if [ -d "$DIR" ]; then
  echo "Folder autostart exist in /home/pi/.config/"
else
  echo "Folder autostart does NOT exist in /home/pi/.config/"
  echo "Create it"

  # Erstelle den Ordner
  sudo mkdir -p "$DIR"
fi

# Erstelle die Verknuepfung am Desktop
echo -e "Create Shortcut\n"
sudo echo -e "#!/bin/bash

# Stelle die System-Uhr auf den Wert der RTC-Uhr vom Monarco-Board
sudo hwclock --hctosys

# Strom-spar-Modus Wifi deaktivieren
sudo iw wlan0 set power_save off

# Stelle die Netzwerk-Regeln wieder her
/sbin/iptables-restore < /etc/iptables.ipv4.nat

# Kontrolliere ob die SSID der Wifi-Schnittstelle richtig der aktuellen HW-Seriennummer entspricht
/usr/local/bin/pbx/$programm_name/Scripts/Pi/ssid_refresher.sh

# Wechsle in das Verzeichnis der Applikation
cd /usr/local/bin/pbx/$programm_name

# Starte die Applikation
sudo python$python_version /usr/local/bin/pbx/$programm_name/Main.py" | sudo tee -a /home/pi/Desktop/"$programm_name".sh > /dev/null

# Sorge dafuer dass die Verknuepfung ausfuehrbar ist
sudo chmod 777 /home/pi/Desktop/"$programm_name".sh

# Erstelle ein .desktop-File welches nach dem laden des Desktops aufgerufen wird
echo "Create Autostart-Entry"
sudo echo -e "[Desktop Entry]
Name=Overture
Type=Application
Comment=cooler than cool
Exec=/home/pi/Desktop/$programm_name.sh" | sudo tee -a /home/pi/.config/autostart/pbx.desktop > /dev/null

# HDMI-Port ausschalten
sudo /usr/bin/tvservice -o

#  # Lade das shrink.sh-Skript herunter
#  echo Get shrink-script
#  cd /home/pi/Downloads || exit
#  wget https://raw.github.com/qrti/shrink/master/script/shrink.sh


#parlocale=de_AT.UTF-8
#sudo raspi-config nonint do_change_locale $parlocale

#parlayout=AT
#sudo raspi-config nonint do_configure_keyboard $parlayout

# Timezone
# timedatectl set-timezone Europe/Berlin

# Aktive Modul fuer RTC (RealTimeClock)
sudo modprobe rtc-ds1307

sudo raspi-config nonint do_resolution 2 82

# Schalte den VNC-Server ein
sudo raspi-config nonint do_vnc 0

# Aktive das Overlay-Filesystem
sudo raspi-config nonint enable_overlayfs

# Aktiviere den Read-Only-Zugriff für "/boot"
sudo raspi-config nonint enable_bootro

## Erstelle die Verknuepfung am Desktop
#echo "Create Shortcut Read_only_OFF"
#sudo echo -e "#!/bin/bash
#
## Schalte das FS-Overlay AUS
#sudo raspi-config nonint disable_overlayfs
#
## Schalte den Schreibschutz von boot AUS
#sudo raspi-config nonint disable_bootro
#" | sudo tee -a /home/pi/Desktop/Read_only_OFF.sh > /dev/null

# Sorge dafuer dass die Verknuepfung ausfuehrbar ist
sudo chmod 777 /home/pi/Desktop/Read_only_OFF.sh

echo Setup Finish
sudo reboot