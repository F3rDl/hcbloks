#!/bin/bash

serial_number="$(cat /proc/cpuinfo | grep Serial | cut -d' ' -f2)"

# Print Serien-Nummer
# echo "$serial_number"

# Print letzte 4 Stellen der Serien-Nummer
# echo "${serial_number: -4}"

# Wandle Seriennummer in decimal
# echo $((16#$serial_number))

return "$serial_number"