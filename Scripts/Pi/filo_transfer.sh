﻿#!/bin/bash

# Dieses Skript uebertraegt die Logfiles vom Field-Logger mit der Bezeichnung "PBX.FILO SN02/2020"
# auf den Controller in den Pfad "TargetFolder"

# crontab -e
# */15 * * * * /home/pi/Desktop/filo_transfer.sh

FiLoIP="192.168.0.196"
FiLoUserName="root"
FiLoPasswd="130119"
FiLoLogPath="/gins/fs/online/sd0/"
FiLoLogTpye="csv"
TargetFolder="/home/pi/FiLo_Logs/"

echo -e "Check if Folder $TargetFolder exist"
if [ -d "$TargetFolder" ]; then
  echo -e "Folder $TargetFolder exist"
else
  echo -e "Folder $TargetFolder does NOT exist"
  echo -e "Create it"

  # Erstelle den Ordner
  mkdir -p "$TargetFolder"
fi
echo "Check if FiLO is reachable"
if ping -c1 -w3 "$FiLoIP" # > /dev/null 2>&1
then
    echo "FiLO is reachable"
    # sshpass -p "$FiLoPasswd" sftp -a "$FiLoUserName"@"$FiLoIP":"$FiLoLogPath"*."$FiLoLogTpye" "$TargetFolder"
    sshpass -p "$FiLoPasswd" sftp -o StrictHostKeyChecking=no -a "$FiLoUserName"@"$FiLoIP":"$FiLoLogPath"*."$FiLoLogTpye" "$TargetFolder"
else
    echo "FiLo is NOT reachable"
fi
