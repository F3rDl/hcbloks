#!/bin/bash

# Lade die komplette Configuration
vcgencmd get_config int

# Counter fuer wiederkehrenden Header
Counter=14

# Header-Zeile
DisplayHeader="Time      Temp    CPU          Health         Vcore"

# Endlos-Schleife
while true ; do

  # Inkrementiere den Counter
  ((++Counter))

  # Wenn der Counter 15 ist, dann ...
  if [ ${Counter} -eq 15 ]; then

    # Printe die Header-Zeile
	  echo -e "${DisplayHeader}"

	  # Setzte den Counter zurueck
	  Counter=0
  fi

  # Lade die Clock-Flags
  Health=$(perl -e "printf \"%19b\n\", $(vcgencmd get_throttled | cut -f2 -d=)")

  # Lade die CPU-Chip-Temperatur
  Temp=$(vcgencmd measure_temp | cut -f2 -d=)

  # Lade die CPU-Frequenz
  clock_speed=$(vcgencmd measure_clock arm | awk -F"=" '{printf ("%0.0f",$2/1000000); }' )

  # Lade die CPU-Versorgungsspannung
  CoreVolt=$(vcgencmd measure_volts | cut -f2 -d= | sed 's/000//')

  # Formatiere und prite die geladenen Daten
  echo -e "$(date '+%H:%M:%S') ${Temp} $(printf '%4s' "${clock_speed}")MHz $(printf '%020u' ${Health}) ${CoreVolt}"

  # Schlafe fuer 5 Sec
  sleep 5
done