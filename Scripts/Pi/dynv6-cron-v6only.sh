﻿#!/bin/sh -e

# Cronjob shell scripts um die IPv6 adresses auf dynv6.com zu aktualisieren
#
# Damit jede Minute das Script aufgerufen wird:
#
# crontab -e
#
# Sende IPv6-Adresse zu dynv6:
# */1 * * * * /srv/dynv6-cron-v6only.sh whatever.dynv6.net token-for-auth > /dev/null 2>&1

hostname=$1
token=$2
device=$3
file=$HOME/.dynv6.addr6

# Wenn das "$file" existiert dann laden den Inhalt dessen in "old"
[ -e "$file" ] && old=$(cat "$file")

# Wenn die Parameter "$hostname" oder "$token" leer sind, dann ...
if [ -z "$hostname" ] || [ -z "$token" ]; then
  # Gib eine Fehlermeldung aus
  echo "Usage: whatever.dynv6.net <token-for-auth> [device]"
  # Beende das Programm mit einer Fehlermeldung
  exit 1
fi

# Wenn der Parameter "$netmask" nicht angegeben ist, dann ...
if [ -z "$netmask" ]; then
  # Lege den Parameter "$netmask" auf 128 fest
  netmask=128
fi

# Wenn "$device" nicht leer ist
if [ -n "$device" ]; then
  device="dev $device"
fi

# Lade mittels des "ip"-Befehls die IPv6-Adresse
address=$(ip -6 addr list scope global "$device" | grep -v " fd" | sed -n 's/.*inet6 \([0-9a-f:]\+\).*/\1/p' | head -n 1)

# Wenn es das Programm "curl" existiert, dann ...
if [ -e /usr/bin/curl ]; then
  # Weise den "curl"-Aufruf der "bin"-Variable zu
  # -f = keine Ausgabe bei Serverfehlern
  # -s = Keine Fortschrittsanzeige
  # -S = Fehlermeldungen anzeigen
  bin="curl -fsS"
# Sonst-Wenn es das Programm "wget" existiert, dann ...
elif [ -e /usr/bin/wget ]; then
  # Weise den "wget"-Aufruf der "bin"-Variable zu
  # -O- = "wget" schreibt den Inhalt der herunter-geladenen Datei direkt auf die Standardausgabe (stdout) des Terminals
  bin="wget -O-"
else
  # Fehlermeldung das weder "curl" noch "wget" vorhanden ist
  echo "Neither curl or wget found"
  # Beende das Programm mit einer Fehlermeldung
  exit 1
fi

# Wenn keine IP-Adresse in "$address" vorhanden ist
if [ -z "$address" ]; then
  # Fehlermeldung das keine IP-Adresse ermittelt werden konnte
  echo "No IPv6 address found"
  # Beende das Programm mit einer Fehlermeldung
  exit 1
fi

# Kombiniere die IP-Adresse mit der Netz-maske
current=$address/$netmask

# Wenn der Inhalt aus "$old" und "$current" gleich ist, dann ...
if [ "$old" = "$current" ]; then
  # Gib eine Meldung aus das keine aktualisierung notwendig ist aus
  echo "IPv6 address unchanged"
  # Beende das Programm
  exit
fi

# Sende die IP-Adresse zu dynv6
$bin "http://dynv6.com/api/update?hostname=$hostname&ipv6=$current&token=$token"

# Sichere die ermittelte IP-Adresse in das File "$file"
echo "$current" > "$file"
