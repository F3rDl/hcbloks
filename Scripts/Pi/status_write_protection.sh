﻿#!/bin/bash

search_term="boot=overlay"
file_of_interest=/proc/cmdline


if grep -q "$search_term" "$file_of_interest"
then
    echo Schreibschutz aktiv
else
    echo Schreibschutz inaktiv
fi