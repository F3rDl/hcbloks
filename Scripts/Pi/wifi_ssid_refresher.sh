﻿#!/bin/bash

search_term="boot=overlay"
file_of_interest=/proc/cmdline

# Lade die Seriennummer
serial_number_hex="$(cat /proc/cpuinfo | grep Serial | cut -d' ' -f2)"

# Wandle Seriennummer in decimal
serial_number_dec="$((16#$serial_number_hex))"

# Bilde einen string aus "hcbloks" und den letzten 4 Stellen der Seriennummer
set_ssid=hcbloks${serial_number_dec: -4}

# Lies die aktuelle SSID aus
actual_ssid="$(sudo iw dev wlan0 info | grep ssid | cut -d' ' -f2)"
# config_ssid=`sudo cat /etc/hostapd/hostapd.conf | grep ssid | cut -d'=' -f2`

# Wenn die aktuelle SSID jener gleich ist welche sein sollte, dann ...
if [ "$actual_ssid" = $set_ssid ]
then
    echo "SSID gleich"
    exit

else
    echo SSID ungleich
    echo "$actual_ssid"
    echo "$set_ssid"

    # Wenn der Eintrag "boot=overlay" im File "/proc/cmdline" gefunden werden kann (Schreibschutz aktiv), dann ...
    if grep -q "$search_term" "$file_of_interest"
    then
        echo Schreibschutz aktiv

        # Schalte den Schreibschutz aus
        sudo raspi-config nonint disable_overlayfs

        # Starte Neu
        sudo reboot
    else
        echo Schreibschutz inaktiv

        # Ersetze die alte SSID mit der neuen
        sudo sed -i 's+ssid=.*+ssid='$set_ssid'+g' /etc/hostapd/hostapd.conf

        # Aktiviere den Schreibschutz
        sudo raspi-config nonint enable_overlayfs

        # Starte Neu
        sudo reboot

    fi
fi