import tkinter as tk


class GuiDialogBox:

    def __init__(self, parent):
        """
        Args:
            parent:
        """
        self.top = self.top = tk.Toplevel(parent)
        self.label_text = tk.Label(self.top, text='Enter parameter')
        self.label_text.pack()
        self.entry_parameter = tk.Entry(self.top)
        self.entry_parameter.pack()
        self.button_submit = tk.Button(self.top, text='Submit', command=self.send)
        self.button_submit.pack()
        self.parameter = ''

    def send(self):

        self.parameter = self.entry_parameter.get()
        self.top.destroy()


def on_click():

    input_dialog = GuiDialogBox(root)
    root.wait_window(input_dialog.top)
    print('parameter: ', input_dialog.parameter)


root = tk.Tk()
label_main = tk.Label(root, text='Example for pop up input box')
label_main.pack()

button_main = tk.Button(root, text='Click me', command=on_click)
button_main.pack()

root.mainloop()
