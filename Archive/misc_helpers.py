#!/usr/local/bin/python3.7

import ast


def list_right_rotation(r_list=None, n=0):

    """Rotiert die gegebene List um n Schritte nach rechts

    Args:
        r_list:
        n:
    """

    return r_list[-n:] + r_list[:-n]


def list_left_rotation(r_list=None, n=0):

    """Rotiert die gegebene List um n Schritte nach links

    Args:
        r_list:
        n:
    """

    return r_list[n:] + r_list[:n]


def str2bool(v):

    """Gibt den boolischen Zustand einer Information aus einen String zurueck

    Args:
        v:
    """

    return v.lower() in ("yes", "true", "t", "1", "on")


def read_file_to_dic(path: str, keyword_dic: str) -> dict:

    """Diese Funktion liest anhand einer Dateipfad-Eingabe und eines
    Schluesselwortes ein Dictionay aus einer Datei.

    Wenn es das Schlueselwort in der Datei nicht gibt dann wird eine neue
    Zeile mit leeren Inhalt erstellt

    Wenn es das File nicht gibt dann wird ein File erstellt mit dem gesuchten
    Schluesselwort und leerem Dictionary-Eintrag

    Args:
        path (str):
        keyword_dic (str):

    Returns:
        dict: Gibt das Dictionary welches unter dem Schuesselwort gefunden wurde zurueck,
            wenn keins vorhanden ist wird ein leeres Dictionary zurueckgegeben
    """

    # Versuche ein File zu oeffnen, sofern ...
    try:
        # Oeffne den Zugang zu einem File im Lese-modus
        with open(path, "r") as temp_file:

            # Lese das File und speichere den Inhalt in content_1
            content_1 = temp_file.readlines()

        # Gehe den Content Zeile fuer Zeile durch
        for line_cont in content_1:

            # Kann in der Zeile das Wort keyword_dic_sig_name_i gefunden werden
            if keyword_dic in line_cont:
                # Zerlege den eingelesene Eintrag an " = "
                content_2 = line_cont.strip().split(" = ")

                # Uebergebe das zweite Element an das dictionary der Signalnamen
                return ast.literal_eval(content_2[1])

            # Wenn es sich um die letzte Stelle handelt dann, ... # TODO: statt den if ein elif
            if line_cont == content_1[-1]:

                # Oeffne den Zugang zu einem File im Schreibe-anhaeng-modus
                with open(path, "a") as temp_file:

                    # Fuege das Schluesselwort mit leeren Dichtionary-Eintrag ein
                    temp_file.writelines("\n" + keyword_dic + " = {}")

                    # Gebe ein leeres Dictionary zurueck
                    return ast.literal_eval('{}')

    # Das file konnte nicht gefunden werden
    except FileNotFoundError:

        # Oeffne das File im Schreibe-anhaeng-modus
        with open(path, "a") as temp_file:

            # Fuege das Schluesselwort mit leeren Dichtionary-Eintrag ein
            temp_file.writelines("\n" + keyword_dic + " = {}")

            # Gebe ein leeres Dictionary zurueck
            return ast.literal_eval('{}')


def dict_keys_swap_values(dic_org) -> dict:

    """
    Args:
        dic_org:
    """
    xir = list(dic_org.items())
    return dict(map(reversed, xir))


def method3(list, search_age):
    """
    Args:
        list:
        search_age:
    """
    return list.keys()[list.values().index(search_age)]


def get_all_keys(dic_org, search_term) -> list:
    """Diese Funktion gibt alle keys aus einem Dictionary, als Liste zurueck,
    welche den selben Wert wie der Parameter "search_term" haben.

    Args:
        dic_org:
        search_term:

    Returns:
        list: Eine Liste mit den key welche eine uebereinstimmung mit
        "search_term" aufweisen
    """
    # return [key for key in dic_org.keys() if (dic_org[key] == search_term)]

    return [filter(lambda x: dic_org[x] == search_term, dic_org), [None]][0]


def diff_lists(first, second):
    """
    Args:
        first:
        second:
    """
    second = set(second)
    return [item for item in first if item not in second]

#########################################################
# compare_2_dic
#set(dict_one.keys()).intersection(set(dict_two.keys()))
