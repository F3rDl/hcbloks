﻿from transitions.extensions import HierarchicalGraphMachine
from transitions.extensions.nesting import NestedState
import time

NestedState.separator = '↦'  # '↦'
time_step = 0.1


class Model:

    def show_graph(self, **kwargs):
        self.get_graph().draw('state_custom_pic.png', prog='dot', format='png')

    @staticmethod
    def rate_limiter(current_value, target_value, dt_pos, dt_neg, time_step):

        # Verletzt der geforderte Sprung die die maximal erlaubte negative Sprungweite, dann ...
        """
        Args:
            current_value:
            target_value:
            dt_pos:
            dt_neg:
            time_step:
        """
        if target_value < current_value + (dt_neg * time_step):

            # Mit maximal erlaubter negativen Sprungweite in Richtung "target_value"
            return current_value + (dt_neg * time_step)

        # Verletzt der geforderte Sprung die die maximal erlaubte positive Sprungweite, dann ...
        elif target_value > current_value + (dt_pos * time_step):

            # Mit maximal erlaubter positiven Sprungweite in Richtung "target_value"
            return current_value + (dt_pos * time_step)

        # Geforderter Sprung ist kleiner als maximal positiv/negativer Sprungweite deshalb, ...
        else:
            # fuehre direkete zuweisung durch
            return target_value


class ComponentBehavior:
    def __init__(self):
        # Output
        self.rpm_out = 0.0

        # Parameter
        self.rpm_off = 0.0
        self.rpm_min = 0.0
        self.rpm_idle = 0.0
        self.rpm_max = 0.0
        self.rpm_dt_min = 0.0
        self.rpm_dt_max = 0.0
        self.supply_min = 0.0
        self.supply_max = 0.0

        # Input
        self.man_val = 0.0
        self.supply_val = 0.0


class StateMachineTimer:

    def __init__(self, time_step: float = 0.1, duration_time: float = 1.0):
        self.time_val = duration_time
        self.time_step = time_step
        self.duration_time = duration_time

    def reset(self):
        self.time_val = self.duration_time

    def step(self):
        self.time_val -= self.time_step

    def is_time_over(self):
        if self.time_val <= 0.0:
            return True
        else:
            return False

    def set_duration_time(self, value: float = 0.0):
        self.duration_time = float(value)


class StateMachinePWMTimer:

    def __init__(self, time_step: float = 0.1, duration_time: float = 1.0, duty_cycle: float = 0.75):

        self.time_val = 0.0
        self.time_step = time_step
        self.duration_time = duration_time
        self.duty_cycle = duty_cycle

    def reset(self):
        self.time_val = 0.0

    def step(self):
        self.time_val += self.time_step

        if self.time_step >= self.duration_time:
            self.reset()

    def is_value_true(self):
        if self.time_val > self.duration_time * self.duty_cycle:
            return True
        else:
            return False

    def set_duration_time(self, value: float = 0.0):
        self.duration_time = float(value)

    def set_duty_cycle(self, value: float = 0.75):
        self.duty_cycle = float(value)



class TurningComponent(Model):

    def __init__(self, name: str = ''):

        self.name = name

        self.component = ComponentBehavior()
        self.component.supply_min = 22.0
        self.component_timer_check = StateMachineTimer(duration_time=3.0)
        self.component_timer_toggle = StateMachinePWMTimer(duration_time=5.0)

        self.states = [
            'init',
            'off',
            {'name': 'on', 'initial': 'check', 'children': [
                'check',
                'error',
                'automatic',
                'idle',
                'half',
                'full',
                'manual',
                {'name': 'toggle', 'initial': 'on', 'children': [
                    'on',
                    'off']},
                {'name': 'standby', 'initial': 'running_out', 'children': [
                    'standstill',
                    'running_out']}]
             }
        ]
        self.transitions = [

            # trigger (str): Event-Signal of the transitions
            # source (str): Source state of the transition.
            # dest (str): Destination state of the transition.
            # prepare (list): Callbacks executed before conditions checks.
            # unless (list): Callbacks evaluated to determine if the transition should be not executed.
            # conditions (list): Callbacks evaluated to determine if the transition should be executed.
            # before (list): Callbacks executed before the transition is executed
            #     but only if condition checks have been successful.
            # after (list): Callbacks executed after the transition is executed
            #     but only if condition checks have been successful.

            # -------------------- External Events ------------------------------------------------------------------- #
            #
            # Diese Signale werden verwendet um die Zustands-Aenderungen zu bewirken. Sie stellen eine Art Interaktion-
            # Schnittstelle dar. Folgende Signal stehen zu Verfuegung:
            #
            #   -) wake_up      ↦ In den Betriebs-bereiten Zustand versetzen
            #   -) fall_asleep  ↦ In den Energie-spar-Modus versetzen
            #   -) manual       ↦ In den Betriebs-bereiten Zustand versetzen und Manuelle vorgaben weiterreichen
            #   -) stop         ↦ Von einen aktiven Betriebs-Zustand in den Betriebs-bereiten Zustand wechseln
            #   -) idle         ↦ In den aktiven Betriebs-Zustand "geringste Leistung" versetzen
            #   -) half         ↦ In den aktiven Betriebs-Zustand "halber Leistung" versetzen
            #   -) full         ↦ In den aktiven Betriebs-Zustand "maximale Leistung" versetzen

            # (1)
            # Wenn das Event "wake_up" geschickt wurde, wird in den Zustand "on" gewechselt
            {'trigger': 'wake_up', 'source': 'off', 'dest': 'on'},
            #
            # (2)
            # Wenn das Event "fall_asleep" geschickt wurde, wird in den Zustand "off" gewechselt
            {'trigger': 'fall_asleep', 'source': 'on', 'dest': 'off'},
            #
            # (3)
            # Wenn das Event "manual" geschickt wurde, wird in den Zustand "...↦on↦manual" gewechselt
            {'trigger': 'manual', 'source': 'on↦standby', 'dest': 'on↦manual'},
            #
            # (4)
            #  Wenn das Event "stop" geschickt wurde, wird in den Zustand "...↦on↦standby" gewechselt
            {'trigger': 'stop', 'source': ['on↦idle', 'on↦full', 'on↦manual', 'on↦automatic',
                                           'on↦toggle', 'on↦half'], 'dest': 'on↦standby'},
            #
            # (5)
            # Wenn Event "idle" geschickt wurde, wird in den Zustand "...↦on↦idle" gewechselt
            {'trigger': 'idle', 'source': ['on↦standby', 'on↦full', 'on↦toggle', 'on↦half',
                                           'on↦automatic', 'on↦manual'], 'dest': 'on↦idle'},
            #
            # (6)
            # Wenn das Event "full" geschickt wurde, wird in den Zustand "...↦on↦full" gewechselt
            {'trigger': 'full', 'source': ['on↦standby', 'on↦idle', 'on↦toggle', 'on↦half',
                                           'on↦automatic', 'on↦manual'], 'dest': 'on↦full'},
            #
            # (7)
            # Wenn das Event "automatic" geschickt wurde, wird in den Zustand "...↦on↦full" gewechselt
            {'trigger': 'automatic', 'source': ['on↦standby', 'on↦toggle', 'on↦idle', 'on↦half',
                                                'on↦full', 'on↦manual'], 'dest': 'on↦automatic'},
            #
            # (8)
            # Wenn das Event "toggle" geschickt wurde, wird in den Zustand "...↦on↦toggle" gewechselt
            {'trigger': 'toggle', 'source': ['on↦standby', 'on↦full', 'on↦idle', 'on↦half',
                                             'on↦automatic', 'on↦manual'], 'dest': 'on↦toggle'},
            #
            # (9)
            # Wenn das Event "half" geschickt wurde, wird in den Zustand "...↦on↦half" gewechselt
            {'trigger': 'half', 'source': ['on↦standby', 'on↦full', 'on↦idle', 'on↦toggle',
                                           'on↦automatic', 'on↦manual'], 'dest': 'on↦half'},
            #
            # -------------------- Transfer transitions -------------------------------------------------------------- #
            #
            # Diese Transitions bewirken interne Wechsel zwischen den States. Die Bedingungen in Ihnen werden in jeden
            # zyklischen Aufruf ueberprrueft und ein Wechsel bei erfuellung durchgefuehrt
            #
            # (101)
            # Ueberpruefe ob die Versorgung-Spannung in Ordnung ist
            {'trigger': 'loop', 'source': 'on↦check', 'dest': 'on↦standby', 'conditions': 'if_supply_ok'},
            #
            # (102)
            # Wenn laenger als 3 Sekunden keine positive Freigabe kommt dann wird der Error-Zustand aktiv
            {'trigger': 'loop', 'source': 'on↦check', 'dest': 'on↦error', 'after': self.component_timer_check.reset,
             'prepare': self.component_timer_check.step, 'conditions': [self.component_timer_check.is_time_over],
             'label':'[after(3,sec)]'},
            #
            # (103)
            # Wenn ein schwere Fehler entdeckt wurde, wird in den Zustand Error gewechselt
            {'trigger': 'loop', 'source': ['on↦standby', 'on↦full', 'on↦half', 'on↦idle', 'on↦toggle',
                                           'on↦automatic', 'on↦manual'],
             'dest': 'on↦error', 'conditions': ['if_error_detected']},
            #
            # (104)
            # Wenn die Komponente sich ausgelaufen hat (langsam heruntergefahren) und somit still steht soll in den
            # State "standstill" gewechselt werden
            {'trigger': 'loop', 'source': 'on↦standby↦running_out', 'dest': 'on↦standby↦standstill',
             'conditions': 'if_component_is_still'},
            #
            # (105)
            # Wenn der Zustand "...↦toggle↦on" seit x Sekunden aktiv ist dann wechsle in "...↦toggle↦off"
            {'trigger': 'loop', 'source': 'on↦toggle↦on', 'dest': 'on↦toggle↦off', 'label': '[toggle on]',
             'prepare': self.component_timer_toggle.step, 'conditions': self.component_timer_toggle.is_value_true},
            #
            # (106)
            # Wenn der Zustand "...↦toggle↦off" seit x Sekunden aktiv ist dann wechsle in "...↦toggle↦on"
            {'trigger': 'loop', 'source': 'on↦toggle↦off', 'dest': 'on↦toggle↦on', 'label': '[toggle off]',
             'prepare': self.component_timer_toggle.step, 'unless': self.component_timer_toggle.is_value_true},
            #
            #
            # # -------------------- Internal transitions -------------------------------------------------------------- #
            # #
            # # Dieser Kategorie werden Transitions zugewiesen welche fuer Initalisierungen oder zyklische Aufrufe
            # # verwendet werden
            #
            # (201)
            # Initialisierung-Transition damit ein "on_enter_off_xxx" moeglich ist
            {'trigger': 'loop', 'source': 'init', 'dest': 'off'},
            # #
            # # (203)
            # # Wenn die Komponente beim betreten des "standby"-States bereits heruntergefahren ist,
            # # soll direkt der State "standstill" aktiv werden
            # {'trigger': 'init_standby', 'source': 'on↦standby',
            #  'dest': 'on↦standby↦standstill', 'unless': ['if_pump_turns']},
            #
            # # (204)
            # # Wenn die Komponete eine groessere RPM als minRPM aufweist dann gehe davon aus das sie noch eingeschalten
            # # ist, Ramp-down notwendig
            # {'trigger': 'init_standby', 'source': 'on↦standby',
            #  'dest': 'on↦standby↦running_out', 'conditions': ['if_pump_turns']},
            #
            # # (205)
            # # Zyklischer call fuer den "standstill"-State
            # {'trigger': 'loop', 'source': 'on↦standby↦standstill', 'dest': None},
            #
            # # (206)
            # # Zyklischer call fuer den "Running_Out"-State
            # {'trigger': 'loop', 'source': 'on↦standby↦running_out', 'dest': None,
            #  'conditions': ['is_not_zero_pump_rpm'], 'after': 'pump_ramp_down'},
            #
            # # (207)-Visio
            # # Zyklischer call fuer den "automatic"-State
            # {'trigger': 'loop', 'source': 'on↦automatic', 'dest': None},
            #
            # # (208)
            # # Zyklischer call fuer den "idle"-State
            # # {'trigger': 'loop', 'source': 'on↦idle', 'dest': None},
            #
            # # (209)
            # # Zyklischer call fuer den "full"-State
            # # {'trigger': 'loop', 'source': 'on↦full', 'dest': None},
            #
            # # (210)
            # # Zyklischer call fuer den "manual"-State
            # {'trigger': 'loop', 'source': 'on↦manual', 'dest': None,
            #  'after': 'during_' + '_manual'},
            # #
            # # (211)
            # # Das Event "init_toggle" wird von "enter_on" gesendet und ermoeglicht den sprung in den
            # # sub-state "toggle↦on"
            # {'trigger': 'init_toggle', 'source': 'on↦toggle',
            #  'dest': 'on↦toggle↦on'}
    ]

    def if_supply_ok(self):
        if self.component.supply_min < self.component.supply_val < self.component.supply_max:
            return True

    def if_component_is_still(self):
        if self.component.rpm_min <= self.component.rpm_out:
            return True

    def during_manual(self):

        self.component.out = self.component.man_val

    # ------------------------ Cyclic-functions ---------------------------------------------------------------------- #

    def pump_ramp_down(self):

        self.component.rpm_out = self.rate_limiter(current_value=self.component.rpm_out,
                                                   target_value=self.component.rpm_min,
                                                   dt_pos=self.component.rpm_dt_max, dt_neg=self.component.rpm_dt_min,
                                                   time_step=time_step)

    # ------------------------ On_enter_Events ----------------------------------------------------------------------- #

    def on_enter_on_full(self):
        self.component.rpm_out = self.component.rpm_max

    def on_enter_off(self):
        self.component.rpm_out = self.component.rpm_off

    def on_enter_on_idle(self):
        self.component.rpm_out = self.component.rpm_idle

    def on_enter_on_standby_standstill(self):
        self.component.rpm_out = self.component.rpm_off

    def on_enter_on_toggle_on(self):
        self.component.rpm_out = self.component.rpm_max

    def on_enter_on_toggle_off(self):
        self.component.rpm_out = self.component.rpm_off

    def on_enter_on_half(self):
        self.component.rpm_out = self.component.rpm_max / 2.0

    # ------------------------ On_exit_Events ------------------------------------------------------------------------ #

    def on_exit_on_standby_running_out(self):
        self.component.rpm_out = self.component.rpm_off

    # ------------------------ Conditions ---------------------------------------------------------------------------- #

    def timer_pump_toggle_on_timeout(self):

        active_time = self.data.cont_par['pump']['toggle_periode'] * self.data.cont_par['pump']['toggle_duty_cycle']

        if self.timer_pump_toggle_on_value > active_time:
            self.timer_pump_toggle_on_value = 0.0
            return True

    def timer_pump_toggle_on_count(self):
        self.timer_pump_toggle_on_value = self.timer_pump_toggle_on_value + time_step

    def timer_pump_toggle_off_timeout(self):

        inactive_time = self.data.cont_par['pump']['toggle_periode'] * (
                1 - self.data.cont_par['pump']['toggle_duty_cycle'])

        if self.timer_pump_toggle_off_value > inactive_time:
            self.timer_pump_toggle_off_value = 0.0
            return True

    def timer_pump_toggle_off_count(self):
        self.timer_pump_toggle_off_value = self.timer_pump_toggle_off_value + time_step

    def if_pump_turns(self):
        if self.component.rpm_out > self.component.rpm_off:
            return False
        else:
            return True


class HcBloksStateMachine():

    def __init__(self):

        extra_args = dict(title='SM_Component',
                          show_conditions=True, show_state_attributes=True)

        self.fan_cond = TurningComponent(name='fan_cond')
        self.fan_chil = TurningComponent(name='fan_chil')

        sub_state_machine = self.fan_chil

        states = [
            {'name': 'components', 'parallel': [
                {'name': 'fan_cond', 'initial': 'init', 'children': self.fan_cond.states},
                {'name': 'fan_chil', 'initial': 'init', 'children': self.fan_chil.states}]}]

        rtransitions = \
            self.get_transitions_with_prefix(model=self.fan_cond.transitions, prefix='components↦fan_cond↦') + \
            self.get_transitions_with_prefix(model=self.fan_chil.transitions, prefix='components↦fan_chil↦')

        method_list = []

        # Hier werden alle anderen Methoden geladen welche nicht "on_enter"/"on_exit"-Methoden sind
        method_list += [func for func in dir(sub_state_machine)
                        if callable(getattr(sub_state_machine, func))
                        and not func.startswith("__") and not func.startswith("on_e")]

        for method in method_list:
            setattr(self, method, getattr(sub_state_machine, method))

        self.machine = HierarchicalGraphMachine(model=[self, self.fan_cond, self.fan_chil],
                                                states=states,
                                                transitions=rtransitions,
                                                queued=True,
                                                initial='components',
                                                show_auto_transitions=False,
                                                ignore_invalid_triggers=True,
                                                **extra_args)



    @staticmethod
    def get_transitions_with_prefix(model, prefix: str = ''):

        for element in model:

            if element['trigger'] != 'loop':
                element['trigger'] = prefix + element['trigger']

            if isinstance(element['source'], list):
                element['source'] = [prefix + sub_element for sub_element in element['source']]

            else:
                element['source'] = prefix + element['source']

            if not isinstance(element['dest'], list):
                element['dest'] = prefix + element['dest']

        return model

    def print_graph(self):
        self.machine.get_graph().draw(str(self.machine.__class__.__name__), prog='dot')


if __name__ == "__main__":
    State_machine = HcBloksStateMachine()
    # State_machine.fan_cond.component.supply_val = 24.0
    time.sleep(1.0)
    State_machine.loop()
    print(State_machine.state)

    method_to_call = getattr(State_machine, 'components↦fan_cond↦wake_up')
    method_to_call()
    State_machine.loop()
    # State_machine.loop()
    print(State_machine.state)
    # State_machine.machine.trigger_event(State_machine.fan_cond_machine, 'components↦fan_cond↦wake_up')
    # print(State_machine.machine.model[0].name)
    # print(State_machine.machine.model[0].loop)

    # Wenn die "model" auskommentiert sind 09:06
    # State_machine.machine.loop()
    # State_machine.machine.wake_up()


    # print(State_machine.machine.get_nested_state_names())
    # print(State_machine.machine.models)

    State_machine.print_graph()
