import multiprocessing
import time


class BaseMangerTest:

    def __init__(self):

        # Initalisierung eines Multiprocessing objects
        self.manager = multiprocessing.Manager()

        # Initalisierung des Base-Manager-Dictionary
        self.dict_transfer = self.manager.dict()

        self.dict_transfer['save'] = 1337

    def test_1(self):
        start = time.time_ns()
        self.dict_transfer = fill_with_data()
        print(time.time_ns()-start)

    def test_2(self):
        start = time.time_ns()
        self.dict_transfer.update(fill_with_data())
        print(time.time_ns() - start)

    def test_3(self):
        start = time.time_ns()
        self.dict_transfer['data'] = fill_with_data()
        print(time.time_ns() - start)




def fill_with_data() -> dict:

    local_dic = {}
    local_dic.update({
        'Area_1': {
            'sub_area_1': {
                'chapter_1': {
                    'Page_1': time.time()}}, },
        'Area_2': {
            'sub_area_1': {
                'chapter_1': {
                    'Page_1': time.time()}}, },
        'Area_3': {
            'sub_area_1': {
                'chapter_1': {
                    'Page_1': time.time()}}, },
        'Area_4': {
            'sub_area_1': {
                'chapter_1': {
                    'Page_1': time.time()}}, },
        'Area_5': {
            'sub_area_1': {
                'chapter_1': {
                    'Page_1': time.time()}}, },
        'Area_6': {
            'sub_area_1': {
                'chapter_1': {
                    'Page_1': time.time()}}, },
        'Area_7': {
            'sub_area_1': {
                'chapter_1': {
                    'Page_1': time.time()}}, },
        'Area_8': {
            'sub_area_1': {
                'chapter_1': {
                    'Page_1': time.time()}}, },
        'Area_9': {
            'sub_area_1': {
                'chapter_1': {
                    'Page_1': time.time()}}, },
        'Area_10': {
            'sub_area_1': {
                'chapter_1': {
                    'Page_1': time.time()}}, },
        'Area_11': {
            'sub_area_1': {
                'chapter_1': {
                    'Page_1': time.time()}}, },
        'Area_12': {
            'sub_area_1': {
                'chapter_1': {
                    'Page_1': time.time()}}, },
        'Area_13': {
            'sub_area_1': {
                'chapter_1': {
                    'Page_1': time.time()}}, },
        'Area_14': {
            'sub_area_1': {
                'chapter_1': {
                    'Page_1': time.time()}}, },
        'Area_15': {
            'sub_area_1': {
                'chapter_1': {
                    'Page_1': time.time()}}, },
        'Area_16': {
            'sub_area_1': {
                'chapter_1': {
                    'Page_1': time.time()}}, },
        'Area_17': {
            'sub_area_1': {
                'chapter_1': {
                    'Page_1': time.time()}}, },
        'Area_18': {
            'sub_area_1': {
                'chapter_1': {
                    'Page_1': time.time()}}, },
        'Area_19': {
            'sub_area_1': {
                'chapter_1': {
                    'Page_1': time.time()}}, },
        'Area_20': {
            'sub_area_1': {
                'chapter_1': {
                    'Page_1': time.time()}}, },
    })

    return local_dic


if __name__ == "__main__":

    foo = BaseMangerTest()
    while True:
        
        # foo.test_1()    # Platz 3
        time.sleep(0.1)
        foo.test_2()  # Platz 2 (koennte aber auch beser als Platz 1 sein)
        time.sleep(0.1)
        foo.test_3()    # Platz 1

        # print(foo.dict_transfer.copy())
        print('\n')
        time.sleep(0.1)

        if 'save' in foo.dict_transfer:
            print('gefunden')
