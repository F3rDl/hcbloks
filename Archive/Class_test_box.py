﻿###############################################################################################################


# class Beef:
#
#     def print_foo(self):
#         print('foo')
#         return 'bar'
#
#
# class Cow(Beef):
#     def get_int(self):
#         return 1
#
#
# test = Cow()
#
# print(test.get_int())
# print(test.print_foo())
###############################################################################################################


# class Beef:
#
#     def __init__(self):
#         print('2')
#
#     def print_foo(self):
#         print('foo')
#         return 'bar'
#
#
# class Cow(Beef):
#
#     # def __init__(self):
#     #     print('1')
#
#     def get_int(self):
#         return 1
#
#
# test = Cow()
#
# print(test.get_int())
# print(test.print_foo())
###############################################################################################################


# class Beef:
#
#     def __init__(self, string_i: str):
#         print('2')
#         self.string_i = string_i
#         print(self.test_var)
#
#     def print_foo(self):
#         print('foo')
#         return self.string_i
#
#
# class Bone:
#
#     def __init__(self, string_i: str):
#         print('2222')
#         self.string_i = string_i
#
#     def print_foo(self):
#         print('foo0000')
#         return self.string_i
#
#
# class Cow(Beef, Bone):
#
#     def __init__(self):
#         self.test_var = 100000000
#         print('1')
#         Beef.__init__(self, 'bar')
#         Bone.__init__(self, 'baaaar')
#         print('3')
#
#     def get_int(self):
#         return 42
#
#
# test = Cow()
# print('4')
#
# print(test.get_int())
# print(test.print_foo())


class Beef:

    def print_foo(self):
        print('foo')
        return 'bar'


class Cow(Beef):
    def get_int(self):
        self.print_foo()
        return 1


test = Cow()

print(test.get_int())
print(test.print_foo())
