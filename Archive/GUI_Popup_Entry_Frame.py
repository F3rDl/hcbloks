from tkinter import *
import sys


class PopupWindow(object):

    def __init__(self, master):

        """
        Args:
            master:
        """
        top = self.top = Toplevel(master)
        self.label_1 = Label(top, text="Hello World")
        self.label_1.pack()
        self.entry_1 = Entry(top)
        self.entry_1.pack()
        self.button_3 = Button(top, text='Ok', command=self.cleanup)
        self.button_3.pack()

    def cleanup(self):

        self.value = self.entry_1.get()
        self.top.destroy()


class MainWindow(object):

    def __init__(self, master):

        """
        Args:
            master:
        """
        self.master = master
        self.button_1 = Button(master, text="click me!", command=self.popup)
        self.button_1.pack()
        self.b2 = Button(master, text="print value", command=lambda: sys.stdout.write(self.entry_value()+'\n'))
        self.b2.pack()

    def popup(self):
        self.w = PopupWindow(self.master)
        self.button_1["state"] = "disabled"
        self.master.wait_window(self.w.top)
        self.button_1["state"] = "normal"

    def entry_value(self):
        return self.w.value


if __name__ == "__main__":
    root = Tk()
    m = MainWindow(root)
    root.mainloop()
