﻿import plotly.graph_objects as go
import pandas as pd
from pandas import read_excel

file_name = './GPS_Data/FiLo.xlsx'
col_lat = 'Unnamed: 17'
col_lon = 'Unnamed: 18'

dfs = read_excel(file_name, sheet_name=None)

fig = go.Figure(data=go.Scattergeo(
    lat=dfs['Tabelle1'][col_lat][1::].div(100),
    lon=dfs['Tabelle1'][col_lon][1::].div(100),
    mode='lines',
    line=dict(width=2, color='blue'),
))

fig.update_layout(
    title_text='Stefans Tour',
    showlegend=False,
    geo=dict(
        resolution=50,
        showland=True,
        showlakes=True,
        landcolor='rgb(204, 204, 204)',
        countrycolor='rgb(204, 204, 204)',
        lakecolor='rgb(255, 255, 255)',
        projection_type="equirectangular",
        coastlinewidth=2,
        lataxis=dict(
            range=[20, 60],
            showgrid=True,
            dtick=10
        ),
        lonaxis=dict(
            range=[-100, 20],
            showgrid=True,
            dtick=20
        ),
    )
)

fig.show()
