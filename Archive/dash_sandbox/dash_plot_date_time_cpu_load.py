﻿import dash
from dash.dependencies import Output, Input
import dash_core_components as dcc
import dash_html_components as html
import plotly
import plotly.graph_objs as go
from collections import deque
import datetime
import psutil


class WebPlot:
    def __init__(self, window_len: int = 30):
        self.window_len = window_len
        self.deque_x_axis = deque(maxlen=self.window_len)
        self.deque_x_axis.append(datetime.datetime.now())
        self.deque_y_axis = deque(maxlen=self.window_len)
        self.deque_y_axis.append(0)

        self.app = dash.Dash(__name__)

        self.app.layout = html.Div(
            [dcc.Graph(id='live-graph', animate=False, config=dict(displayModeBar=False)),
             dcc.Interval(id='graph-update', interval=20000, n_intervals=0)])

        self.app.callback(Output('live-graph', 'figure'),
                          [Input('graph-update', 'n_intervals')])(self.update_graph_scatter)

    def update_graph_scatter(self, n):

        self.deque_x_axis.append(datetime.datetime.now())
        self.deque_y_axis.append(psutil.cpu_percent())

        data = go.Scatter(x=list(self.deque_x_axis), y=list(self.deque_y_axis), name='CPU load', mode='lines+markers', showlegend=True)

        return {'data': [data],
                'layout': go.Layout(xaxis=dict(range=[min(self.deque_x_axis), max(self.deque_x_axis)]),
                                    yaxis=dict(range=[min(self.deque_y_axis) - 1, max(self.deque_y_axis) + 1]))}


if __name__ == '__main__':
    gui = WebPlot()
    gui.app.run_server(host='192.168.0.153', port=8080, debug=False, use_reloader=False)
