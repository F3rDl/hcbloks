﻿import dash
import flask
from dash.dependencies import Output, Input
import dash_core_components as dcc
import dash_html_components as html
import plotly
import plotly.graph_objs as go
import psutil, datetime
from collections import deque


class WebPlot:
    def __init__(self, server=None,  window_len: int = 30, memory_len: int = 3600*24):

        self.window_len = window_len
        self.memory_len = memory_len

        self.deque_x_axis = deque(maxlen=self.memory_len)
        self.deque_x_axis.append(datetime.datetime.now())
        self.deque_y_axis = deque(maxlen=self.memory_len)
        self.deque_y_axis.append(0)

        if server is None:
            self.server = flask.Flask(__name__)
        else:
            self.server = server

        # self.app = dash.Dash(__name__)
        self.app = dash.Dash(__name__, server=self.server)

        self.app.layout = html.Div(
            [dcc.Graph(id='live-graph', animate=False, config=dict(displayModeBar=False)),
             dcc.Interval(id='graph-update', interval=1000, n_intervals=0)])

        self.app.callback(Output('live-graph', 'figure'),
                          [Input('graph-update', 'n_intervals')])(self.update_graph_scatter)

    def update_graph_scatter(self, n):
        if n is None:
            return {'data': [go.Scatter(x=[None], y=[None])]}

        self.deque_x_axis.append(datetime.datetime.now())
        self.deque_y_axis.append(psutil.cpu_percent())

        data = go.Scatter(x=list(self.deque_x_axis), y=list(self.deque_y_axis), name='Scatter', mode='lines+markers')

        x_axis_max = self.deque_x_axis[-1]
        try:
            xaxis_min = self.deque_x_axis[0 - self.window_len]
            yaxis_min = min(list(self.deque_y_axis)[0 - self.window_len:-1])
            yaxis_max = max(list(self.deque_y_axis)[0 - self.window_len:-1])
        except IndexError:
            xaxis_min = self.deque_x_axis[0]
            yaxis_min = min(self.deque_y_axis)
            yaxis_max = max(self.deque_y_axis)

        return {'data': [data],
                'layout': go.Layout(xaxis=dict(range=[xaxis_min, x_axis_max]),
                                    yaxis=dict(range=[yaxis_min, yaxis_max]))}


if __name__ == '__main__':
    gui = WebPlot()
    # gui.app.run_server(host='0.0.0.0', port=8080, debug=False)
    gui.app.run_server(port=8080, debug=False)
