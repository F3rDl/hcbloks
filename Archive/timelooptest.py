import time
import datetime
from timeloop import Timeloop
from datetime import timedelta

tl = Timeloop()


@tl.job(interval=timedelta(milliseconds=100))
def sample_job_1():
    # print("job_1 current time : {}".format(time.ctime()))
    # print(datetime.datetime.now())
    print(time.time_ns() / 100000000)


if __name__ == "__main__":
    tl.start(block=True)
