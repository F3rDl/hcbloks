from multiprocessing import Pool, Manager


def func(logtree):
    """
    Args:
        logtree:
    """
    logtree['x'] = 'x'


def main():
    manager = Manager()
    logtrees = manager.dict()
    logtrees['a'] = manager.dict()
    logtrees['b'] = manager.dict()
    logtrees['c'] = manager.dict()

    pool = Pool(2)
    for key in logtrees.keys():
        pool.apply_async(func, args=(logtrees[key],))

    pool.close()
    pool.join()
    print(logtrees.__class__)
    logtrees = {key: dict(logtrees[key]) for key in logtrees.keys()}
    print(logtrees)
    print(logtrees.__class__)


if __name__ == '__main__':
    main()