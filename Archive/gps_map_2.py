﻿import plotly.graph_objects as go
import pandas as pd

# Definition des Files
file_name = './GPS_Data/FiLo.xlsx'

# Spalten-Beschriftung "Latitude"
col_latitude = 'Unnamed: 17'

# Spalten-Beschriftung "Longitude "
col_longitude = 'Unnamed: 18'


# Funktion um das NMEA-Format ins "Dezimal-Grad"-Format umzuwandeln
def nmea2dg(value):
    try:
        decimal_grad = int(float(value) / 100)
        decimal_seconds = float(value) - decimal_grad * 100
        return decimal_grad + decimal_seconds / 60
    except ValueError:
        return None


# Einlesen des Excel-Files
data_frame = pd.read_excel(file_name, sheet_name=None)

# Konvertiere "Latitude" von NMEA-Format auf "Dezimal-Grad"-Format
latitude_dec_deg_format = data_frame['Tabelle1'][col_latitude].apply(nmea2dg)

# Konvertiere "Longitude" von NMEA-Format auf "Dezimal-Grad"-Format
longitude_dec_deg_format = data_frame['Tabelle1'][col_longitude].apply(nmea2dg)

# Erstelle den Plot
fig = go.Figure(data=go.Scattermapbox(
    lat=latitude_dec_deg_format,
    lon=longitude_dec_deg_format,
    mode='lines',
    line=dict(width=2, color='blue')
))

# Passe den Plot an, damit die Darstellung besser aussieht
fig.update_layout(margin={'l': 0, 't': 0, 'b': 0, 'r': 0}, mapbox={'style': "stamen-terrain", 'zoom': 1})

# Zeige den Plot --> Erstelle einen kleinen Web-Socket, Oeffne den Browser und zeige ihn
fig.show()
