

class Kind:

    def __init__(self):
        self.name = 'Hans'

    def druck(self):
        print(self.name)

    def tete(self):
        self.druck()

class Master:

    def __init__(self):
        self.name = 'Franz'

    def druck(self):
        print('foobar')


setattr(Master, "druck", Kind.druck)
a_master = Master()
a_master.druck()

########################################################################################################################


class Kind:
    name = 'Hans'

    def __init__(self):
        self.name = 'Hans'

    def druck(self):
        print(self.name)


class Master:

    def __init__(self):
        # self.name = 'Franz'
        pass


setattr(Master, "druck", Kind.druck)
setattr(Master, "name", Kind.name)
a_master = Master()
a_master.druck()

########################################################################################################################


class Kind:

    def __init__(self):
        self.name = 'Hans'

    def druck(self):
        print(self.name)

    def tata(self):
        self.tete()


class Master:

    def __init__(self):
        self.name = 'Franz'
        setattr(Master, "druck", Kind.druck)
        setattr(Master, "tata", Kind.tata)


    def druck(self):
        print('foobar')

    def tete(self):
        self.druck()




a_master = Master()
a_master.druck()
a_master.tata()

