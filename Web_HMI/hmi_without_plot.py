﻿from flask import Flask, request
from flask import render_template
from flask import jsonify
import json


class WebHmi:

    def __init__(self):

        self.step_size = 0.5
        self.temp_set_limit_upper = 30.0
        self.temp_set_limit_lower = -20.0

        self.inputs = {'op_state_set': False,
                       'temp_set': 0.0}

        self.outputs = {'op_state_act': False,
                        'temp_act': 0.0}

        self.path = '/tmp/'
        self.file_name_mosi = '.tansfer_hmi_mosi'
        self.file_name_miso = '.tansfer_hmi_miso'

        self.write_miso_file()

    def write_miso_file(self):

        # Oeffne das File im Schreib-modus
        with open(self.path + self.file_name_miso, "w") as file_tmp:

            # Schreibe das Dictionary in das File im .json-Format
            file_tmp.write(json.dumps(self.inputs.copy(), indent=5))

    def read_from_mosi_file(self):
        """
        In dieser Funktion wird ueberprueft ob das File mit den Werten fuer die Anzeigen existiert.
        """
        try:
            # Oeffne das File im Lese-modus
            with open(self.path + self.file_name_mosi, "r") as file_tmp:

                # Update das Dictionary mit dem Inhalt des Config-File
                self.outputs.update(json.load(file_tmp))

        except FileNotFoundError:

            # Oeffne das File im Schreib-modus
            with open(self.path + self.file_name_mosi, "w") as file_tmp:

                # Schreibe das Dictionary in das File im .json-Format
                file_tmp.write(json.dumps(self.outputs.copy(), indent=5))

    def check_if_file_miso_exist(self):
        """
        In dieser Funktion wird ueberprueft ob das File mit den Werten der Schalter und Buttons existiert.
        """
        pass

    def inc_temp_set(self):

        if self.inputs['temp_set'] < self.temp_set_limit_upper:
            self.inputs['temp_set'] += self.step_size
        else:
            self.inputs['temp_set'] = self.temp_set_limit_upper

    def dec_temp_set(self):

        if self.inputs['temp_set'] > self.temp_set_limit_lower:
            self.inputs['temp_set'] -= self.step_size
        else:
            self.inputs['temp_set'] = self.temp_set_limit_lower

    def toggle_op_state(self):
        self.inputs['op_state_set'] = not self.inputs['op_state_set']
        print('power_button is pressed: ' + str(self.inputs['op_state_set']))

    def get_cpu_temp(self):
        with open("/sys/class/thermal/thermal_zone0/temp", "r") as cpu_temp:
            return float(cpu_temp.readline()) / 1000.0


app = Flask(__name__)
app_class = WebHmi()


@app.route('/call_button_minus')
def button_callback_minus():
    app_class.dec_temp_set()
    app_class.write_miso_file()
    return "nothing"


@app.route('/call_button_plus')
def button_callback_plus():
    app_class.inc_temp_set()
    app_class.write_miso_file()
    return "nothing"


@app.route('/call_button_pwr')
def button_callback_pwr():
    app_class.toggle_op_state()
    app_class.write_miso_file()
    return "nothing"


@app.route('/')
def index():
    return render_template('index.html')


@app.route("/update", methods=['GET'])
def update():
    app_class.read_from_mosi_file()
    return jsonify({"act_temperature": app_class.outputs['temp_act'], # app_class.get_cpu_temp(),
                    "set_temperature": app_class.inputs['temp_set'],
                    "op_state_act": app_class.outputs['op_state_act']})


if __name__ == '__main__':
    app.run(host='0.0.0.0', port=8080, debug=False)
