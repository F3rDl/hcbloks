function updateTemperature() {
    fetch("/update")
        .then(response => {
            if (!response.ok)
                //throw new Error("fetch failed PBX");
                return false
            else
                return response.json();

        })
        .then(json => {
            act_temperature = json.act_temperature
            set_temperature = json.set_temperature
            op_state_act = json.op_state_act

            updatePowerButton()
            document.querySelector("#act_temperature").textContent = act_temperature;
            document.querySelector("#set_temperature").textContent = set_temperature
        })
        .catch(error => alert(error))

}

function updatePowerButton()
{
    console.log(op_state_act)
    if  (op_state_act === true)

        document.getElementById("btn_pwr").className = "material-icons green-text waves-effect";
    else
        document.getElementById("btn_pwr").className = "material-icons grey-text waves-effect";

    return false;
}

updateTemperature();
setInterval(updateTemperature, 500);

var act_temperature
var set_temperature
var op_state_act
op_state_act = true

// Diese Funktion wird vom Button Minus aufgerufen
$(function () {
    $('#btn_minus').bind('click', function () {
        $.getJSON('/call_button_minus');
        return false;
    });
});

$(function () {
    $('#btn_plus').bind('click', function () {
        $.getJSON('/call_button_plus');
        return false;
    });
});

$(function () {
    $('#btn_pwr').bind('click', function () {
        $.getJSON('/call_button_pwr');
        // console.log(document.getElementById("btn_pwr").className)
        return false;
    });
});