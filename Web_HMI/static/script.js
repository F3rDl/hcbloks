/* Initialize carousel with responsive height */

var boxHeight = document.getElementById("mainBox").clientHeight;

$(".carousel.carousel-slider")
  .carousel({
    fullWidth: true,
    fullHeight: true,
    indicators: true,
  })
  .height(boxHeight);
