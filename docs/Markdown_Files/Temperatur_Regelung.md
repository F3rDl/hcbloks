﻿# Funktionsweise
Im Kern der Temperatur-Regelung befindet sich ein PI-Regler, welcher die Aufgabe hat seine Ausgangs-Größe so zu stellen, 
dass die Regelabweichung am Eingang des PI-Reglers auf null geht. 

![Alt-Text](./Pictures/Regelkreis.png)

# Regelabweichung
In der Abbildung (unten) wird auf den Eingang 1 der Soll-Wert (Führungsgröße) übergeben und auf Eingang 2 der aktuelle 
Ist-Wert (Rückführung). Die Differenz (Regelabweichung) wird am Ausgang 3 ausgegeben.

![Alt-Text](./Pictures/Subtract.png)

# PI-Regler
Der PI-Regler setzt sich aus folgenden zwei Komponenten zusammen: 

- Proportional-Verstärkung
- Integral-Verstärkung

Für den P-Anteil wird die Regelabweichung auf Eingang 1 übernommen und mit den Verstärkungs-Faktor [kp] multipliziert. 
Das Ergebnis der Multiplikation wird dem Summations-Punkt zugeführt.
Damit der I-Anteil bestimmt werden kann, wird zuerst die Regelabweichung mit einen zeitlichen Quantisierung am 
Integrator aussummiert. Der resultierende Integrator wird mit dem Verstärkungs-Faktor [ki] multipliziert und das 
Resultat hieraus dem Summations-Punkt übergeben.

Der Summations-Punkt summiert alle seine Eingänge auf und stellt das Ergebnis am Ausgang 4 zur Verfügung.
Damit bei Start oder Führungsgrößen-Änderung der Integrator korrigiert werden kann gibt es folgenden Funktionen:
 - Eingang 2: Initial-Wert auf welchen der Integrator bei einen Reset initialisiert wird
 - Eingang 3: Trigger um einen Reset des Integrators auszulösen

![Alt-Text](./Pictures/PI_Regler.png)

# PI-Regler mit Stellgrößen-Beschränkung
Oft hat das Stellglied eine Größen-Beschränkung. Beispielweise hat ein Verbrennungs-Motor eine maximal Drehzahl und 
eine Leerlaufdrehzahl welche nicht über/unter-schritten werden darf. Um dies zu erreichen wird eine Sättigungs-Stufe 
eingesetzt. Diese bekommt über die Eingänge 4 und 5 einen oberen und einen unteren Grenzwert mitgeteilt. Sollte die 
Sättigungs-Stufe Werte außerhalb der Grenzen bekommen wird am Ausgang der Sättigungs-Stufe der limitiert Wert 
ausgegeben. Somit kann der PI-Regler am Beispiel des Verbrennungsmotors 9000 RPM verlangen am Ausgang wird aber auf 
den Wert von Eingang 4 limitiert, zum Beispiel 4700 RPM. Dasselbe gilt für die untere Grenze wenn am Eingang 5 ein 
Wert von 700 RPM vorgegeben wird, kann der Regler sogar - 2000 RPM ausgeben aber Wert wird auf die 700 RPM limitiert.

![Alt-Text](./Pictures/PI_Regler_StellGrBesch.png)

Diese Struktur sollte in der Form nicht zum Einsatz kommen da es zu einen unerwünschten aufrollen des Integrators 
kommen kann, mehr dazu im folgenden Abschnitt.

# PI-Regler mit Stellgrößen-Beschränkung und Anit-Wind-Up-Methode
Da der Integrator die Regelabweichung über die Zeit auf-integriert kann es zu unerwünschten Effekten kommen. Diese 
können auftreten, wenn die Regelabweichung über einen sehr langen Zeitraum auftritt oder der Regler sich an einer der 
beiden Sättigungsgrenzen befindet. In diesen Fall könnte der Wert des Integrators weit über oder unter die 
Sättigungs-Grenzen sich “aufrollen” → “wind-up”. Um dies zu verhindern wird abgefragt, ob die Differenz zwischen 
Regler-Output und Sättigungsblock-Output einen gewissen Wert überschreitet. Wenn dies eintritt, wird dem Integrator 
eine 0 zum Integrieren gegeben. Somit bleibt der Wert des Integrators fixiert bis die Sättigungs-Grenze des 
Regler-Outputs nicht mehr verletzt ist. 

![Alt-Text](./Pictures/PI_Regler_AntiWind_StellGrBesch.png)

# Bestimmung der Änderung
Als Basis dient zum Bespiel ein Geschwindigkeit-Signal, welches von einem Sensor ausgelesen wird. Es gibt verschiedene 
Varianten von Geschwindigkeit-Sensoren. Im Automobilbereich werden für diese Aufgabe oft Induktionsgeber eingesetzt. 
Hierbei ist eine Zahnscheibe an der sich drehen Welle befestigt. 

![Alt-Text](./Pictures/Drehzahlsensor_Bsp.png)

Der Sensor wird in einen geringen Abstand zu den Zähnen an einen nicht beweglichen Bauteil montiert. 
Das resultierende Signal ist ein Rechtecksignal. 

![Alt-Text](./Pictures/Rechtecksignal.png)

Da jede Flanke eine fixe Wegstrecke darstellt muss nur die Anzahl der Flanken über einen Zeitabschnitt gezählt werden. 
Umso mehr Flanken in einen Zeitabschnitt gezählt werden desto größer ist die die zurückgelegte Strecke pro Zeiteinheit 
und somit die Geschwindigkeit. Bei diesen Vorgang wird somit der Weg nach der Zeit abgeleitet und heraus kommt die 
Geschwindigkeit  [v(t)]. 

![Alt-Text](./Pictures/Differenzieren.png)

 - x(t) = Weg
 - v(t) = Geschwindigkeit
 - a(t) = Beschleunigung

Um die Beschleunigung zu ermitteln, muss ein weiteres Mal nach der Zeit abgeleitet werden. In der Praxis gibt es 
mehrer Möglichkeiten dies zu erreichen. Wir wollen aber nur eine Methode näher betrachten. 

Hierfür zählen wir die Zähne, die pro Zeitabschnitt am Sensor erfasst werden und merken uns die Zahlen. 

|                    |t0 |t1 |t2 |t3 |t4 |t5 |t6 |t7 |t8 |t9 |t10|
|--------------------|---|---|---|---|---|---|---|---|---|---|---|
|Anzahl der Zähne    |0  |1  |2  |3  |4  |4  |4  |4  |3  |2  |1  |
|Delta: x(t) - x(t-1)|   |1  |1  |1  |1  |0  |0  |0  |-1 |-1 |-1 |

Nun bilden wir in einen jeden Zeitabschnitt die Differenz aus der Zahnanzahl des aktuellen Zeitabschnitts zu der Anzahl 
der Zähne aus dem vorherigen Zeitabschnitt. Wenn das Ergebnis der Differenzen der jeweiligen Zeitabschnitte über die 
Zeit in ein Diagramm eintragen wird erhalten wir die Beschleunigung [a(t)].

In der Abbildung unten wird die Änderungs-Berechnung über 6 Zeitabschnitte in die Vergangenheit dargestellt. Hierfür 
wird am Eingang 1 der aktuelle Wert der Schaltung zugeführt. Nun wird die Differenz zwischen den aktuellen und den 
letzten Messwert gebildet. Der Block mit [1/z] ist ein Verzögerungsglied. Der Wert an seinen Eingang liegt einen 
Zeitschritt später an seinen Ausgang an. Alle Differenzen in der Verzögerungs-Schlange werden aufsummiert und am Ausgang
2 ausgegeben. 

![Alt-Text](./Pictures/Rate_calc.png)

# PI-Regler mit Stellgrößen-Beschränkung und Anit-Wind-Up-Methode regelt auf Änderung
Dieser Aufbau (Abbildung unten) ist ein Zusammen-Schluss der Elemente der letzten Kapitel. Es wird aus Eingang 1 und 2 
die eine Differenz gebildet. Diese wird für die weitere Berechnung als die “SOLL-Änderung“ betrachtet. Des Weiteren 
werden die Werte aus Eingang 2 in die Rate-Berechnung geleitet. Und am Ausgang des Summations-Punkt, 
der Rate-Bestimmung, erhalten wir die “IST-Änderung”. Diese wird mit den Parameter “Rate_factor” multipliziert. 
Das Ergebnis wird dem Subtraktions-Punkt (Abbildung unten, mitte) zugeführt. Dieser Subtraktions-Punkt vergleicht 
“Soll-Änderung” mit “IST-Änderung” und führt das Ergebnis der PI-Regel-Struktur zu.  

![Alt-Text](./Pictures/Pi_Rate_Controller.png)

Somit wird erreicht dass der PI-Regler nicht direkt auf die Temperatur-Differenz regelt. Sondern er stellt die 
Ausgangs-Größe so lange nach bis die aktuelle Änderung der vorgegeben Änderung entspricht. Und die Soll-Änderung wird 
aus der aktuellen Temperatur-Differenz (Temp_set minus Temp_sensor) bestimmt. 