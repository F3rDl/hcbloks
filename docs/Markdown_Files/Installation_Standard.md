﻿# Installation Standard (ohne Log-Partition)

- [Installation](#installation)
	- [Raspberry Pi OS auf die SD-Karte aufspielen unter Windows](#anker-raspberry-pi-os-auf-die-sd-karte-unter-windows)
		- [SD-Karte formatieren und Betriebssystem aufspielen](#anker-sd-karte-formatieren)
		- [Einrichtung für Headless-Betrieb](#anker-einrichtung-für-headless-betrieb)
	- [Erst-Einrichtung durchführen](#anker-erst-einrichtung-durchfuehren)
		- [Verbindung aufbauen unter Windows](#anker-verbindung-aufbauen-unter-windows)
			- [Verbindungsaufbau mittels Tera-Term](#anker-verbindungsaufbau-mittels-tera-term)
		- [VNC-Schnittstelle aktivieren](#anker-vnc-schnittstelle-aktivieren)
		- [Erst-Einrichtung über VNC-Viewer abschliessen](#anker-erst-einrichtung-ueber-vnc-viewer-abschliessen)
	- [PBX-HCBloks-Folder übertragen](#anker-pbx-hcbloks-folder-uebertragen)
		- [Manueller-Download-und-WinSCP](#anker-manueller-download-und-winscp)
		- [Herunterladen direkt am Controller](#anker-herunterladen-direkt-am-controller)
	- [PBX-Installations-Skript ausführen](#anker-pbx-installations-skript-ausführen)

# Installation
Der Installations-Prozess besteht aus folgenden Schritten:
* Raspberry Pi OS auf die SD-Karte aufspielen
* Erst-Einrichtung durchführen
* PBX-HCBloks-Folder übertragen
* PBX-Installations-Skript ausführen

<a name="anker-raspberry-pi-os-auf-die-sd-karte-unter-windows"></a>
## Raspberry Pi OS auf die SD-Karte aufspielen unter Windows

Folgende Programm sollten auf den Windows-Rechner verfügbar sein:
* [Raspberry Pi Imager](https://www.raspberrypi.org/downloads/)
* [TeraTerm](https://ttssh2.osdn.jp/index.html.en) oder [Putty](https://www.putty.org/)
* [WinSCP](https://winscp.net/eng/download.php)
* [VNCViewer](https://www.realvnc.com/de/connect/download/viewer/)

Zusätzlich wird ein SD-Kartenleser am Rechner benötigt

<a name="anker-sd-karte-formatieren"></a>
### SD-Karte formatieren und Betriebssystem aufspielen

Zu beginn muss die SD-Karte mittels des Kartenleser am Rechner verbunden werden.

<img src=https://upload.wikimedia.org/wikipedia/commons/4/40/Speicherkartenleser.jpg width="400">

Als nächstes wird das Programm "Raspberry Pi Imager" gestartet. Die Benutzer-Konten-
Abfrage muss mit "JA" bestätigt werden und auf der Anzeige sollte folgende Oberfläche 
erscheinen.

<img src="https://www.raspberrypi.org/app/uploads/2020/03/RPI_intro-e1583228263677.png" width="400">

Wir klicken auf den Punkt "CHOOSE OS" und wählen den ersten Menü-Punkt "Raspberry Pi OS (32-bit) aus

<img src="https://www.raspberrypi.org/app/uploads/2020/03/IMAGING-UTILITY-OS-300x196.png" width="400">

Als nächstes muss die SD-Karte ausgewählt werden, hierfür klicken wir auf "CHOOSE SD CARD" und wählen
das Laufwerk aus unter welchen Windows die SD-Karte erkannt hat.

<img src="https://www.raspberrypi.org/app/uploads/2020/03/IMAGING-UTILITY-SD-300x196.png" width="400">

Wenn das erledigt ist dann auf "WRITE" klicken und fertig.

Nun beginnt der "Raspberry Pi Imager" die SD-Karte zu formatieren und anschlißen das Betriebssystem "Raspberry PI OS" aufzuspielen

Wenn die Meldung erscheint das der Vorgang abgeschlossen ist, auf ok klicken und es MUSS die SD-Karte vom Rechner
entfernt werden.

<a name="anker-einrichtung-für-headless-betrieb"></a>
### Einrichtung für Headless-Betrieb

Damit die Erst-Einrichtung [Headless](https://de.wikipedia.org/wiki/Headless) durchgeführt werden kann müssen noch zwei 
Files auf die SD-Karte übertragen werden.

Hierfür stecken wir die SD-Karte wirder in den Rechner und öffen im Explorer die "boot"-Partitation. 

<img src=https://canox.net/wp-content/uploads/2017/05/rpi-ssh1-800x422.png width="400">

Nun muss eine leer Text-Datei mit den Datei-Namen "ssh" erstellt werden. Am schnellsten wird dies
über einen rechter Mausklick (Kontextmenü) --> NEU --> Textdokument erreicht. 
Als Dateinamen geben wir "ssh" ohne ".txt" oder ähnliches ein. 

Damit der Raspberry Pi sich automatisch mit einen Wlan-AP verbindet kann eine Konfiguration in Form
einer .conf-Datei hinterlegt werden. Hierfür legen wir wieder eine neues Text-Dokument an jedoch mit den
Datei-Namen: "wpa_supplicant.conf"

In diese tragen wir folgende Zeilen ein:
```
ctrl_interface=DIR=/var/run/wpa_supplicant GROUP=netdev
update_config=1
country=AT

network={
	ssid="Wlan-Router-Name"
	psk="Wlan-Router-Passwort"
	key_mgmt=WPA-PSK
}
```
Die Anführungszeichen [""] müssen in der Datei für "ssid" und "psk" vorhanden sein. 

Zum abschluss muss die SD-Karte sicher "Ausgeworfen/unmounted/ausgehängt" werden. 
[Beschreibung](https://support.microsoft.com/de-de/windows/in-windows-10-hardware-sicher-entfernen-1ee6677d-4e6c-4359-efca-fd44b9cec369)

Nun wird die SD-Karte in den Raspberry eingelegt und die Spannungsversorgung des PI's eingeschalten

<img src="https://www.raspberrypi.org/app/uploads/2020/03/SD-READER-CU.jpg" width="400">

#### WARUM FUNKTIONIERT DAS?
Beim Start überprüft "Raspberry PI OS" den Inhalt von "/boot" auf der SD-Karte auf bestimmte Dateien.

Wenn eine Datei mit dem Namen "wpa_supplicant.conf" erkannt wird , kopiert "Raspberry Pi OS" die Datei in "/etc/wpa_supplicant" und ersetzt alle vorhandenen Dateien "wpa_supplicant.conf". Die Datei auf "/boot" wird dann entfernt. 
Wenn eine leere Datei mit dem Namen ssh erkannt wird, passt "Raspberry Pi OS" die Einstellungen an, um SSH-Verbindungen zu akzeptieren. Die SSH- Datei wird dann gelöscht.

<a name="anker-erst-einrichtung-durchfuehren"></a>
## Erst-Einrichtung durchführen

Wenn der Raspberry zum ersten mal von einen neuen Image startet, kann dies etwas länger dauern, da dieverse 
Einrichtungschritte durchgeführt werden und das Dateisystem auf den ganzen Speicher des SD-Karte ausgedehnt wird.

Nun wird ein Terminal-Programm (Tera-Term, Putty, Linux-Terminal, ...) verwendet.

<a name="anker-verbindung-aufbauen-unter-windows"></a>
### Verbindung aufbauen unter Windows

Zunächst muss die IP-Adresse des Raspberry Pi in erfahrung gebracht werden. 

Wenn der Raspberry über eine Ethernet-Leitung an einen Router angehängt ist muss auf der Service-Seite des Routers nachgeschaut werden.
Das selbe gilt wenn dem Raspberry in "wpa_supplicant.conf" eine Wlan-Konfiguration mitgeteilt wurde. Auch hier kann die IP-Adresse des 
Raspberry auf der Service-Seite des Routers in erfahrung gebracht werden. 

Wenn kein Router zur verfügung steht kann der Raspberry auch mittels einen Ethernet-Kabel auch direkt mit einen Windows-PC verbunden werden.
Einfach "Windows"-Taste + "R" drücken und "cmd" in das Feld eingeben und mit "OK" bestätigen

<img src=https://praxistipps.s3.amazonaws.com/das-ausfuehren-fenster_529c8f72.jpg width="400">

Nun öffnet sich ein Terminal-Fenster, in dieses wird folgender Befehl eingegeben:

```ping -4 raspberrypi.local```

nun sollte auf den Terminal in etwa folgende Ausgabe erscheinen:

```
Ping wird ausgeführt für raspberrypi.local [169.254.175.9] mit 32 Bytes Daten:
Antwort von 169.254.175.9: Bytes=32 Zeit<1ms TTL=64
Antwort von 169.254.175.9: Bytes=32 Zeit<1ms TTL=64
Antwort von 169.254.175.9: Bytes=32 Zeit<1ms TTL=64
Antwort von 169.254.175.9: Bytes=32 Zeit<1ms TTL=64

Ping-Statistik für 169.254.175.9:
    Pakete: Gesendet = 4, Empfangen = 4, Verloren = 0
    (0% Verlust),
Ca. Zeitangaben in Millisek.:
    Minimum = 0ms, Maximum = 0ms, Mittelwert = 0ms
```

Die IP-Adresse wäre in diesen Fall "169.254.175.9" Diese merken wir uns und schließen das Fenster der Eingabeaufforderung.

<a name="anker-verbindungsaufbau-mittels-tera-term"></a>
#### Verbindungsaufbau mittels Tera-Term

Wenn das Programm "Tera-Term" gestartet wurde erscheint eine Anmeldemaske

<img src=https://heise.cloudimg.io/width/998/q75.png-lossy-75.webp-lossy-75.foil1/_www-heise-de_/download/media/tera-term-51776/tera-term-1_1-1-9.png width="400">

Im Feld "Host" wird die IP-Adresse des Raspberry Pi eingetragen. Als TCP-Port verwenden wir "22" und als Dienst wählen wir "SSH" mit der Protokoll-Version "SSH2" aus.
Das ganze bestätigen wir mit "OK".

Es kann (muss aber nicht) vorkommen das ein Fenster mit "SICHERHEITSWARNUNG" erscheint. Dieses Fenster will uns darauf hinweisen das der Kommunikations-Schlüssel
neu vergeben wird. Die Checkbox "Den vorhandenen Schlüssel durch den neuen ersetzen" auswählen und "Fortsetzen" drücken.

Nun kann der Benutzername: "pi" mit dem Passwort: "raspberry" eingeben werden und die Eingabe mit "OK" bestätigt werden.

Im Optimal-Fall sollte nun folgende Ausgabe am Bildschirm erscheinen. 

```
Linux raspberrypi 509.4.51-v7+ #1333 SMP Mon Aug 10 16:45:19 BST 2045 armv7l

The programs included with the Debian GNU/Linux system are free software;
the exact distribution terms for each program are described in the
individual files in /usr/share/doc/*/copyright.

Debian GNU/Linux comes with ABSOLUTELY NO WARRANTY, to the extent
permitted by applicable law.
pi@raspberrypi:~ $
```
Wenn nicht dann wenden Sie sich bitte an Ihren Administrator

<a name="anker-vnc-schnittstelle-aktivieren"></a>
### VNC-Schnittstelle aktivieren

Um die VNC-Schnittstelle zu aktiveren muss in den Terminal folgender Befehl eingegeben werden:

```
pi@raspberrypi:~ $ sudo raspi-config
```

Es sollte eine Ausgabe erscheinen wie folgt:

```
Raspberry Pi 3 Model B Plus Rev 1.3


┌─────────┤ Raspberry Pi Software Configuration Tool (raspi-config) ├──────────┐
│                                                                              │
│  1 Change User Password Change password for the 'pi' user                    │
│  2 Network Options      Configure network settings                           │
│  3 Boot Options         Configure options for start-up                       │
│  4 Localisation Options Set up language and regional settings to match your  │
│  5 Interfacing Options  Configure connections to peripherals                 │
│  6 Overclock            Configure overclocking for your Pi                   │
│  7 Advanced Options     Configure advanced settings                          │
│  8 Update               Update this tool to the latest version               │
│  9 About raspi-config   Information about this configuration tool            │
│                                                                              │
│                                                                              │
│                                                                              │
│                                                                              │
│                     <Select>                     <Finish>                    │
│                                                                              │
└──────────────────────────────────────────────────────────────────────────────┘
```

Mit der Pfeil-Taste "nach unten" zum Punkt "5 Interfacing Options" navigieren und "Enter" drücken.

```
┌─────────┤ Raspberry Pi Software Configuration Tool (raspi-config) ├──────────┐
│                                                                              │
│  P1 Camera      Enable/Disable connection to the Raspberry Pi Camera         │
│  P2 SSH         Enable/Disable remote command line access to your Pi using   │
│  P3 VNC         Enable/Disable graphical remote access to your Pi using Rea  │
│  P4 SPI         Enable/Disable automatic loading of SPI kernel module        │
│  P5 I2C         Enable/Disable automatic loading of I2C kernel module        │
│  P6 Serial      Enable/Disable shell and kernel messages on the serial conn  │
│  P7 1-Wire      Enable/Disable one-wire interface                            │
│  P8 Remote GPIO Enable/Disable remote access to GPIO pins                    │
│                                                                              │
│                                                                              │
│                                                                              │
│                                                                              │
│                                                                              │
│                     <Select>                     <Back>                      │
│                                                                              │
└──────────────────────────────────────────────────────────────────────────────┘
```
Nun mit der Pfeil-Taste "nach unten" zum Punkt "P3 VNC" navigieren und "Enter" drücken.
```
          ┌──────────────────────────────────────────────────────────┐
          │                                                          │ 
          │ Would you like the VNC Server to be enabled?             │ 
          │                                                          │ 
          │                                                          │ 
          │                                                          │ 
          │                                                          │ 
          │                                                          │ 
          │                                                          │ 
          │                                                          │ 
          │                                                          │ 
          │                                                          │ 
          │                                                          │ 
          │                                                          │ 
          │                                                          │ 
          │                                                          │ 
          │                                                          │ 
          │               <Yes>                  <No>                │ 
          │                                                          │ 
          └──────────────────────────────────────────────────────────┘ 
```
Wir bestätigen die Auswahl ("YES") mit der Taste "Enter"
```
          ┌──────────────────────────────────────────────────────────┐
          │                                                          │ 
          │ The VNC Server is enabled                                │ 
          │                                                          │ 
          │                                                          │ 
          │                                                          │ 
          │                                                          │ 
          │                                                          │ 
          │                                                          │ 
          │                                                          │ 
          │                                                          │ 
          │                                                          │ 
          │                                                          │ 
          │                                                          │ 
          │                                                          │ 
          │                                                          │ 
          │                                                          │ 
          │                          <Ok>                            │ 
          │                                                          │ 
          └──────────────────────────────────────────────────────────┘ 
```
Wir bestätigen die Meldung mit der Taste "Enter"
Nun sollte wieder das erste Menü-Fenster angezeigt werden. Wir drücken auf der Tastatur zwei mal auf rechts damit "<Finish>" makiert ist und bestätigen mit "Enter".
Das Tera-Term-Fenster kann nun geschlossen werden.
	
<a name="anker-erst-einrichtung-ueber-vnc-viewer-abschliessen"></a>	
### Erst-Einrichtung über VNC-Viewer abschliessen

Zu beginn starten wir das Programm "VNC Viewer". In der Adress-Leiste geben wir die IP-Adresse des Raspberry Pi's ein 

<img src=https://www.realvnc.com/en/connect/_images/raspberry-pi-direct-connect.png width="400">

In der darauf folgenden Maske muss der Benutzername: "pi" mit dem Passwort: "raspberry" eingeben werden und mit "OK" bestätigen.
Die Checkbox "Kennwort speichern" muss nicht aktiviert werden, da wir das Passwort im weiteren Verlauf noch abändern.
Nun sollte folgender Bildschirm erscheinen:

<img src=https://www.raspberrypi.org/app/uploads/2018/06/piwiz.gif width="400">

Druck klicken auf "next" kommen wir auf die nächste Seite "Set Country". Weil diese Einstellungen auch auswirkungen auf den verwendeten Frequenzbereich des Wlan
haben muss hier min. ein Land aus Europa ausgewählt sein. 

Standard-Config:
* Country: Austria
* Language: Austrian German
* Timezone: Vienna

Anschließend auf "Next" drücken um auf die nächste Seite ("Passwort ändern") zu gelangen. Bis zu diesen Zeitpunkt ist das Passwort "raspberry". Da dies unsicher ist
muss ein neues Passwort mit min. 8 Stellen definiert werden. Nach der zweifachen Passwort-Eingabe drücken wir wieder den "Next"-Button um auf die Seite "Set up screen" zu gelangen. Diesen Konfigurations-Schritt überspringen wir mit "Next". Auf der Seite "Select Wifi Network" kann neben den bereits in "wpa_supplicant.conf"-File definierten Netzwerken noch zusätzliche Netzwerke hinzugefügt werden. Sollte dies nicht notwendig sein kann der Schritt mit "Skip" übersprungen werden. Auf der drauf folgenden Seite kann der Update-Vorgang durch "Next" bestätigt werden oder mit "Skip" übersprungen werden. Empfehlung: Update durchführen.
Wenn der Update-Vorgang zu Ende ist, drücken wir auf den "Restart"-Button und Beenden damit die "Erst-Einrichtung". Der Raspberry führt darauf hin einen Neustart durch.

<a name="anker-pbx-hcbloks-folder-uebertragen"></a>	
## PBX-HCBloks-Folder übertragen

In den HCBloks-Ordner ist alles vorhanden, was für eine Installation sowie für den Betrieb der Applikation notwendig ist.

<a name="anker-manueller-download-und-winscp"></a>
### Manueller Download und WinSCP
Auf der Git-hub-Seite ganz oben den grünen "Code"-Button drücken und "Download ZIP auswählen".

Nun muss WinSCP geöffnet werden. Im Anmeldungsfenster muss sichergestellt sein das "Neues Verbindungsziel" ausgewählt ist. Für die restlichen Felder gelten folgende Einstellungen:
* Serveradresse: "IP-Adresse des Raspberry Pi's" zBsp.: 169.254.175.9
* Portnummer: 22
* Benutzernamen: pi
* Passwort: "Jenes Passwort welches in der Erste-Einrichtung festgelegt wurde"

	
Wenn das alles eingegeben ist dann starten wir den Verbindungsaufbau durch drücken des "Anmelden"-Button's.

Nun steht ein zwei-geteilter Explorer zur verfügung. Auf der linken Seite ist das Dateisystem des Windows-Rechners zu sehene und auf der rechten Seite jene des Raspberry Pi's. Auf der linken Seite navigieren wir zum Ordner in welchen der von Github heruntergeladene HCBloks-Ordner sich befindet. Auf der rechten Seite wechseln wir in den Ordner "Dowloads" (Der absolute Pfad: /home/pi/Dowloads). Nun ziehen wir den Ordner vom Windows-rechner mit gedrücker linker Maustaste von der linken auf die rechte Seite.

<a name="anker-herunterladen-direkt-am-controller"></a>
### Herunterladen direkt am Controller
Mit folgenden Befehlen kann im Linux-Terminal des Raspberry Pi's der PBX-HCBloks-Ordner direkt am Controller heruntergeladen werden:
```
cd /home/pi/Downloads/
git clone https://F3rDl:d7ecd0e6ee0835a0ff0122d276a3121c9539bd15@github.com/F3rDl/HCBloks.git
```

<a name="anker-pbx-installations-skript-ausführen"></a>
## PBX-Installations-Skript ausführen

Im HCBLOKS-Ordner befindet sich unter "Scripts" ein Unterordner "Install". In diesem befindet sich ein Bash-Script names "pbx_install_hc_bloks.sh" dieses
muss bei einer bestehenden Internet-Verbindung aufgerufen werden. 

Falls das Script nicht ausführbar ist, bitte folgenden Befehl eingeben:
```
cd /home/pi/Downloads/HCBloks/Scripts/Install/
sudo chmod 777 pbx_install_hc_bloks.sh
./pbx_install_hc_bloks.sh
```
Wenn die Installation positiv verläuft, startet der Controller neu.
Falls der Controller bis zu diesen Punkt über Wlan in Betrieb genommen wurde, wird sich nach dem Neustart der Controller nicht automatisch am VNC-Viewer melden. Der Grund hierfür ist dass die Wlan-Schnittstelle von Client-Modus in den Server-Modus geändert wurde. Das bedeutet das am Laptop (so wie nach einen Router) nach einen Wlan-Netz mit den Namen "hcbloks_xxx" gesucht werden muss. Wenn mit diesen eine Verbindung hergestellt wurde, muss die eigene (deines PC'/Laptop's) IP-Adresse in Erfahrung gebracht werden. Bei dieser ist die letzte Stelle durch eine 1 zu ersetzten und mit dieser kann wie gewohnt über VNC/TeraTerm/WinSCP eine Verbindung aufgebaut werden.