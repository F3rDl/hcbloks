﻿# HCBloks
Controller-App HC-Bloks for Linux

## Funktions-Übersicht

Die Software für HC-Bloks-V1.0 soll folgende Funktionen bereitstellen:

- [Installation](#installation)
- State-Machine bereitstellen
  - Betriebs-Modi
  - Komponenten-Verwaltung
  - [Temperatur-Regelung](docs/Markdown_Files/Temperatur_Regelung.md)
- Aufzeichnungen durchführen
  - Prozess-Daten
  - Log-Files 
- Interaktion mit Bediener
  - WEB-Server über Wifi-Access-Point
  - Developer/Labor-Oberfläche
- Konfigurationen-Management 
  - lesen 
  - verwalten
  - anwenden
- Diagnose
  - Identifizieren von Fehler-Zuständen
  - Reagieren auf Fehler (Sicherer Zustand)
- Senden der Logfiles auf File-Server
- Interaktion mit externem Daten-Erfassungssystem
  - Synchronisieren des Zeitstempels
  - abholen von Daten
  - Terminal-Session
- Lesen des Fahrzeug-Buses

<a name="installation"></a>
# Installation

Die Software am HC-Bloks-Controller lässt sich in folgenden Komponenten unterteilen:
 - Image: 
    - Betriebssystem
    - zusätzliche Programme
    - zusätzliche Konfigurationen
  
 - Applikation

## Standard-Installation

Diese Variante lässt sich am schnellsten umsetzten und wird auf folgender Seite beschrieben: 

[Standard-Installation](docs/Markdown_Files/Installation_Standard.md)

## Installation mit zusätzlicher Partition

Diese Variante ist der "Standard-Installation" sehr ähnlich, jedoch wird auf der Speicherkarte 
neben den bereits vorhanden Partitionen ein zusätzliche angelegt. Somit sind folgende Partitionen vorhanden:
 - /boot
 - /rootfs
 - /Data

Der Vorgang wird auf folgender Seite beschrieben:

[Installation with Data-Partition](docs/Markdown_Files/Installation_with_add_Partition.md)

und eine schnellere Variante:

[Installation with Data-Partition short](docs/Markdown_Files/Installation_with_add_Partition_short.md)